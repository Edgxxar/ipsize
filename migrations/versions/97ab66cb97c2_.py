"""remove dropped column, add connection column to probe_packets_public

Revision ID: 97ab66cb97c2
Revises: 98fd64e2c454
Create Date: 2023-09-29 10:29:23.796718

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '97ab66cb97c2'
down_revision = '98fd64e2c454'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('probe_packets_public', sa.Column('connection', sa.String(), nullable=True))
    op.drop_column('probe_packets_public', 'dropped')


def downgrade() -> None:
    op.add_column(
        'probe_packets_public',
        sa.Column('dropped', sa.BOOLEAN(), server_default=sa.text('true'), autoincrement=False, nullable=False)
    )
    op.drop_column('probe_packets_public', 'connection')
