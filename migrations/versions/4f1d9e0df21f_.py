"""add location and autonomous_system columns to target_hosts

Revision ID: 4f1d9e0df21f
Revises: dda3db328186
Create Date: 2023-06-09 12:14:26.427423

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '4f1d9e0df21f'
down_revision = 'dda3db328186'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('target_hosts', sa.Column('location', postgresql.JSONB(astext_type=sa.Text()), nullable=True))
    op.add_column('target_hosts', sa.Column('autonomous_system', postgresql.JSONB(astext_type=sa.Text()), nullable=True))


def downgrade() -> None:
    op.drop_column('target_hosts', 'autonomous_system')
    op.drop_column('target_hosts', 'location')
