"""added probe packet bulk table without primary key

Revision ID: 0b74d3b494c7
Revises: 2ca48531c456
Create Date: 2023-08-10 20:11:15.976730

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '0b74d3b494c7'
down_revision = '2ca48531c456'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'probe_packets_bulk',
        sa.Column('probe_series_id', sa.SmallInteger(), nullable=False),
        sa.Column('sequence_number', sa.Integer(), nullable=False),
        sa.Column('server_id', sa.SmallInteger(), nullable=False),
        sa.Column('target_service_id', sa.Integer(), nullable=False),
        sa.Column('send_time', sa.DateTime(timezone=True), nullable=True),
        sa.Column('recv_time', sa.DateTime(timezone=True), nullable=True),
        sa.Column('delta', sa.Integer(), nullable=True),
        sa.Column('size', sa.SmallInteger(), nullable=False),
        sa.Column('dropped', sa.Boolean(), nullable=False, server_default=sql.true()),
        sa.Column('req_packet_id', sa.Integer(), nullable=True),
        sa.Column('res_packet_id', sa.Integer(), nullable=True),
        sa.Column('data', postgresql.BYTEA(), nullable=True),
        sa.Column('anomaly', sa.String(), nullable=True)
    )


def downgrade() -> None:
    op.drop_table('probe_packets_bulk')
