"""add status column to target_services

Revision ID: 7eb35accb46c
Revises: c1d25efe75ae
Create Date: 2023-06-22 17:27:38.963916

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7eb35accb46c'
down_revision = 'c1d25efe75ae'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute("CREATE TYPE status AS ENUM('OK', 'ERROR', 'UNKNOWN');")
    op.add_column('target_services', sa.Column('status', sa.Enum('OK', 'ERROR', 'UNKNOWN', name='status'), server_default='UNKNOWN', nullable=False))


def downgrade() -> None:
    op.drop_column('target_services', 'status')
