"""add import time to download table

Revision ID: 2ca48531c456
Revises: 09b225b591a7
Create Date: 2023-08-10 09:35:55.628945

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2ca48531c456'
down_revision = '09b225b591a7'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('probe_download', sa.Column('import_time', sa.Float(), nullable=True))


def downgrade() -> None:
    op.drop_column('probe_download', 'import_time')
