"""add latency column to probe_packets tables

Revision ID: e99351b1ca60
Revises: 69e23822146c
Create Date: 2023-08-16 18:33:46.570568

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'e99351b1ca60'
down_revision = '69e23822146c'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('probe_packets', sa.Column('latency', sa.Integer(), nullable=True))
    op.add_column('probe_packets_bulk', sa.Column('latency', sa.Integer(), nullable=True))


def downgrade() -> None:
    op.drop_column('probe_packets_bulk', 'latency')
    op.drop_column('probe_packets', 'latency')
