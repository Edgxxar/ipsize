"""set default values for last_update columns

Revision ID: 6934da83949e
Revises: 5b014abc5959
Create Date: 2023-06-29 09:59:06.985658

"""
from alembic import op
from sqlalchemy import func

# revision identifiers, used by Alembic.
revision = '6934da83949e'
down_revision = '5b014abc5959'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column('target_hosts', 'last_update', server_default=func.now())
    op.alter_column('target_services', 'last_update', server_default=func.now())


def downgrade() -> None:
    op.alter_column('target_hosts', 'last_update', server_default=None)
    op.alter_column('target_services', 'last_update', server_default=None)
