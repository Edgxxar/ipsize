"""added probe_download table

Revision ID: 09b225b591a7
Revises: 2e14f919c47b
Create Date: 2023-08-08 10:03:09.051104

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '09b225b591a7'
down_revision = '2e14f919c47b'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'probe_download',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('probe_series_id', sa.SmallInteger(), nullable=False),
        sa.Column('server_id', sa.SmallInteger(), nullable=False),
        sa.Column('file_name', sa.String(), nullable=False),
        sa.Column('file_size', sa.Integer(), nullable=False),
        sa.Column('downloaded_at', sa.DateTime(timezone=True), nullable=False),
        sa.Column('imported_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('packet_count', sa.Integer(), nullable=True),
        sa.Column('error', sa.Boolean(), nullable=True),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_foreign_key(None, 'probe_download', 'probe_series', ['probe_series_id'], ['id'])
    op.create_foreign_key(None, 'probe_download', 'probe_servers', ['server_id'], ['id'])


def downgrade() -> None:
    op.drop_constraint(None, 'probe_download', type_='foreignkey')
    op.drop_constraint(None, 'probe_download', type_='foreignkey')
    op.drop_table('probe_download')
