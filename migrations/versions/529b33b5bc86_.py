"""add next_update column to target services

Revision ID: 529b33b5bc86
Revises: 96e5cd99ebe8
Create Date: 2023-06-24 17:36:20.745344

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '529b33b5bc86'
down_revision = '96e5cd99ebe8'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('target_services', sa.Column('next_update', sa.DateTime(timezone=True), nullable=True))


def downgrade() -> None:
    op.drop_column('target_services', 'next_update')
