"""add enabled column to target hosts

Revision ID: 5b014abc5959
Revises: 56d2e0606397
Create Date: 2023-06-26 12:29:53.854562

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5b014abc5959'
down_revision = '56d2e0606397'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('target_hosts', sa.Column('enabled', sa.Boolean(), server_default=sa.text('true'), nullable=False))


def downgrade() -> None:
    op.drop_column('target_hosts', 'enabled')
