"""alter probe_download table file_size column

Revision ID: 6fa1f286a5c7
Revises: 8c8087dd76a3
Create Date: 2023-08-24 09:11:27.966065

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '6fa1f286a5c7'
down_revision = '8c8087dd76a3'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column('probe_download', 'file_size', type_=sa.BigInteger, nullable=False)


def downgrade() -> None:
    op.alter_column('probe_download', 'file_size', type_=sa.Integer, nullable=False)
