"""add flapping state to service targets

Revision ID: 56d2e0606397
Revises: 529b33b5bc86
Create Date: 2023-06-25 12:37:28.870192

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '56d2e0606397'
down_revision = '529b33b5bc86'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('target_services', sa.Column('flapping', sa.Boolean(), server_default=sa.text('false'), nullable=False))
    op.add_column('target_services_history', sa.Column('flapping', sa.Boolean(), server_default=sa.text('false'), nullable=False))


def downgrade() -> None:
    op.drop_column('target_services_history', 'flapping')
    op.drop_column('target_services', 'flapping')
