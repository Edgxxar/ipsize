"""change probe_servers status column to JSON

Revision ID: c1d25efe75ae
Revises: 4f1d9e0df21f
Create Date: 2023-06-15 12:15:29.521834

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'c1d25efe75ae'
down_revision = '4f1d9e0df21f'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute("ALTER TABLE public.probe_servers ALTER COLUMN status DROP NOT NULL;")
    op.execute("ALTER TABLE public.probe_servers ALTER COLUMN status TYPE jsonb USING NULL;")


def downgrade() -> None:
    op.alter_column('probe_servers', 'status',
               existing_type=postgresql.ENUM('IDLE', 'UPLOAD', 'PROBING', name='probeserverstatus'),
               nullable=False)
