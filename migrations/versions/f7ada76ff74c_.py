"""drop connection column, add request_received and probe_time columns

Revision ID: f7ada76ff74c
Revises: 97ab66cb97c2
Create Date: 2023-10-18 13:20:01.748299

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'f7ada76ff74c'
down_revision = '97ab66cb97c2'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        'probe_packets_public',
        sa.Column('probe_time', sa.DateTime(timezone=True), nullable=False)
    )
    op.add_column(
        'probe_packets_public',
        sa.Column('request_received', sa.Boolean(), server_default=sa.false(), nullable=False)
    )
    op.drop_column('probe_packets_public', 'connection')


def downgrade() -> None:
    op.add_column('probe_packets_public', sa.Column('connection', sa.VARCHAR(), autoincrement=False, nullable=True))
    op.drop_column('probe_packets_public', 'request_received')
    op.drop_column('probe_packets_public', 'probe_time')
