"""add url column to probe servers

Revision ID: 23cfb7c008b9
Revises: 6934da83949e
Create Date: 2023-06-29 18:56:37.606840

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import sql

# revision identifiers, used by Alembic.
revision = '23cfb7c008b9'
down_revision = '6934da83949e'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('probe_servers', sa.Column('url', sa.String(), nullable=False, server_default=sql.literal("https://probe.ipsize.de")))


def downgrade() -> None:
    op.drop_column('probe_servers', 'url')
