"""add last_download column to probe server

Revision ID: aa390c267486
Revises: 23cfb7c008b9
Create Date: 2023-07-04 16:20:50.839411

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'aa390c267486'
down_revision = '23cfb7c008b9'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('probe_servers', sa.Column('last_download', sa.DateTime(timezone=True), nullable=True))


def downgrade() -> None:
    op.drop_column('probe_servers', 'last_download')
