"""create private probe series tables

Revision ID: ca1971769b1c
Revises: 6fa1f286a5c7
Create Date: 2023-08-31 16:32:42.237743

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'ca1971769b1c'
down_revision = '6fa1f286a5c7'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'probe_packets_received',
        sa.Column('probe_series_id', sa.SmallInteger(), nullable=False),
        sa.Column('sequence_number', sa.Integer(), nullable=False),
        sa.Column('server_id', sa.SmallInteger(), nullable=False),
        sa.Column('target_service_id', sa.Integer(), nullable=False),
        sa.Column('recv_time', sa.DateTime(timezone=True), nullable=False),
        sa.Column('size', sa.SmallInteger(), nullable=False)
    )
    op.create_table(
        'probe_packets_sent',
        sa.Column('probe_series_id', sa.SmallInteger(), nullable=False),
        sa.Column('sequence_number', sa.Integer(), nullable=False),
        sa.Column('server_id', sa.SmallInteger(), nullable=False),
        sa.Column('target_service_id', sa.Integer(), nullable=False),
        sa.Column('send_time', sa.DateTime(timezone=True), nullable=False),
        sa.Column('size', sa.SmallInteger(), nullable=False)
    )


def downgrade() -> None:
    op.drop_table('probe_packets_sent')
    op.drop_table('probe_packets_recieved')
