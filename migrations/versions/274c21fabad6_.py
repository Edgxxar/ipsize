"""remove all foreign keys and indexes except the primary index from probe_packets table, to speed up data insertion

Revision ID: 274c21fabad6
Revises: 00b94651d10e
Create Date: 2023-07-07 14:44:35.702360

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '274c21fabad6'
down_revision = '00b94651d10e'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.drop_index('ix_probe_packets_dropped', table_name='probe_packets')
    op.drop_index('ix_probe_packets_probe_series_id', table_name='probe_packets')
    op.drop_index('ix_probe_packets_req_packet_id', table_name='probe_packets')
    op.drop_index('ix_probe_packets_res_packet_id', table_name='probe_packets')
    op.drop_index('ix_probe_packets_sequence_number', table_name='probe_packets')
    op.drop_index('ix_probe_packets_server_id', table_name='probe_packets')
    op.drop_index('ix_probe_packets_size', table_name='probe_packets')
    op.drop_index('ix_probe_packets_target_service_id', table_name='probe_packets')
    op.drop_constraint('probe_packets_server_id_fkey', 'probe_packets', type_='foreignkey')
    op.drop_constraint('probe_packets_target_service_id_fkey', 'probe_packets', type_='foreignkey')
    op.drop_constraint('probe_packets_probe_series_id_fkey', 'probe_packets', type_='foreignkey')


def downgrade() -> None:
    op.create_foreign_key('probe_packets_probe_series_id_fkey', 'probe_packets', 'probe_series', ['probe_series_id'], ['id'])
    op.create_foreign_key('probe_packets_target_service_id_fkey', 'probe_packets', 'target_services', ['target_service_id'], ['id'])
    op.create_foreign_key('probe_packets_server_id_fkey', 'probe_packets', 'probe_servers', ['server_id'], ['id'])
    op.create_index('ix_probe_packets_target_service_id', 'probe_packets', ['target_service_id'], unique=False)
    op.create_index('ix_probe_packets_size', 'probe_packets', ['size'], unique=False)
    op.create_index('ix_probe_packets_server_id', 'probe_packets', ['server_id'], unique=False)
    op.create_index('ix_probe_packets_sequence_number', 'probe_packets', ['sequence_number'], unique=False)
    op.create_index('ix_probe_packets_res_packet_id', 'probe_packets', ['res_packet_id'], unique=False)
    op.create_index('ix_probe_packets_req_packet_id', 'probe_packets', ['req_packet_id'], unique=False)
    op.create_index('ix_probe_packets_probe_series_id', 'probe_packets', ['probe_series_id'], unique=False)
    op.create_index('ix_probe_packets_dropped', 'probe_packets', ['dropped'], unique=False)
