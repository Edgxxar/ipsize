"""add probe_server_addresses table

Revision ID: 8c8087dd76a3
Revises: e99351b1ca60
Create Date: 2023-08-23 13:15:36.043150

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '8c8087dd76a3'
down_revision = 'e99351b1ca60'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'probe_server_addresses',
        sa.Column('address', postgresql.INET(), nullable=False),
        sa.Column('server_id', sa.SmallInteger(), nullable=False),
        sa.ForeignKeyConstraint(['server_id'], ['probe_servers.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('address')
    )
    op.create_index(op.f('ix_probe_server_addresses_address'), 'probe_server_addresses', ['address'], unique=False)
    op.execute("INSERT INTO probe_server_addresses (address, server_id) SELECT address, id FROM probe_servers")
    op.drop_index('ix_probe_servers_address', table_name='probe_servers')
    op.drop_column('probe_servers', 'address')
    op.drop_column('probe_servers', 'max_download')
    op.drop_column('probe_servers', 'max_upload')


def downgrade() -> None:
    op.add_column(
        'probe_servers',
        sa.Column('max_upload', sa.INTEGER(), autoincrement=False, nullable=False, server_default=sql.literal(0))
    )
    op.add_column(
        'probe_servers',
        sa.Column('max_download', sa.INTEGER(), autoincrement=False, nullable=False, server_default=sql.literal(0))
    )
    op.add_column(
        'probe_servers',
        sa.Column('address', postgresql.INET(), autoincrement=False, nullable=False,
                  server_default=sql.literal("127.0.0.1"))
    )
    op.create_index('ix_probe_servers_address', 'probe_servers', ['address'], unique=True)
    op.execute("""
    UPDATE probe_servers SET address=sa.address FROM (
        SELECT DISTINCT ON (server_id) server_id, address FROM probe_server_addresses
    ) AS sa WHERE id=sa.server_id
    """)
    op.drop_index(op.f('ix_probe_server_addresses_address'), table_name='probe_server_addresses')
    op.drop_table('probe_server_addresses')
