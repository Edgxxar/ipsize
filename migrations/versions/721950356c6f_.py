"""add new probe types

Revision ID: 721950356c6f
Revises: 274c21fabad6
Create Date: 2023-07-08 09:54:35.306126

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '721950356c6f'
down_revision = '274c21fabad6'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute("ALTER TYPE probeseriestype ADD VALUE 'PUBLIC_TCP_SMTP';")
    op.execute("ALTER TYPE probeseriestype ADD VALUE 'PRIVATE_TCP';")


def downgrade() -> None:
    pass
