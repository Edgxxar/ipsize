"""set default value for probe packets dropped column to true

Revision ID: 00b94651d10e
Revises: aa390c267486
Create Date: 2023-07-05 19:13:16.891950

"""
from alembic import op
from sqlalchemy import sql

# revision identifiers, used by Alembic.
revision = '00b94651d10e'
down_revision = 'aa390c267486'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column('probe_packets', 'dropped', server_default=sql.true())


def downgrade() -> None:
    op.alter_column('probe_packets', 'dropped', server_default=None)
