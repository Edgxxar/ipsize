"""empty message

Revision ID: 98fd64e2c454
Revises: ca1971769b1c
Create Date: 2023-09-03 16:06:15.067067

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '98fd64e2c454'
down_revision = 'ca1971769b1c'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'probe_packets_public',
        sa.Column('probe_series_id', sa.SmallInteger(), nullable=False),
        sa.Column('sequence_number', sa.Integer(), nullable=False),
        sa.Column('server_id', sa.SmallInteger(), nullable=False),
        sa.Column('target_service_id', sa.Integer(), nullable=False),
        sa.Column('send_time', sa.DateTime(timezone=True), nullable=True),
        sa.Column('recv_time', sa.DateTime(timezone=True), nullable=True),
        sa.Column('latency', sa.Integer(), nullable=True),
        sa.Column('delta', sa.Integer(), nullable=True),
        sa.Column('size', sa.SmallInteger(), nullable=False),
        sa.Column('dropped', sa.Boolean(), server_default=sa.text('true'), nullable=False),
        sa.Column('data', postgresql.BYTEA(), nullable=True),
        sa.Column('anomaly', sa.String(), nullable=True)
    )


def downgrade() -> None:
    op.drop_table('probe_packets_public')
