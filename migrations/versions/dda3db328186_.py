"""Create basic tables

Revision ID: dda3db328186
Revises: 
Create Date: 2023-05-15 16:57:03.535523

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'dda3db328186'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table('users',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('email', sa.String(), nullable=False),
                    sa.Column('hashed_password', sa.String(), nullable=False),
                    sa.Column('is_active', sa.Boolean(), nullable=False),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_index(op.f('ix_users_email'), 'users', ['email'], unique=True)
    op.create_index(op.f('ix_users_id'), 'users', ['id'], unique=False)

    op.create_table('target_hosts',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('address', postgresql.INET(), nullable=False),
                    sa.Column('last_update', sa.DateTime(timezone=True), nullable=False),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_index(op.f('ix_target_hosts_address'), 'target_hosts', ['address'], unique=True)
    op.create_index(op.f('ix_target_hosts_id'), 'target_hosts', ['id'], unique=False)

    op.create_table('target_services',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('target_host_id', sa.Integer(), nullable=False),
                    sa.Column('type', sa.Enum('PUBLIC_ICMP_PING', name='probeseriestype'), nullable=False),
                    sa.Column('port', sa.Integer(), nullable=True),
                    sa.Column('last_update', sa.DateTime(timezone=True), nullable=False),
                    sa.ForeignKeyConstraint(['target_host_id'], ['target_hosts.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_index(op.f('ix_target_services_id'), 'target_services', ['id'], unique=False)
    op.create_index(op.f('ix_target_services_target_host_id'), 'target_services', ['target_host_id'], unique=False)

    op.create_table('probe_servers',
                    sa.Column('id', sa.SmallInteger(), nullable=False),
                    sa.Column('name', sa.String(), nullable=False),
                    sa.Column('address', postgresql.INET(), nullable=False),
                    sa.Column('max_upload', sa.Integer(), nullable=False),
                    sa.Column('max_download', sa.Integer(), nullable=False),
                    sa.Column('connection_type', sa.Enum('ETHERNET', 'FIBRE', 'WIFI', 'DSL', 'MOBILE_4G', 'MOBILE_5G',
                                                         name='connectiontype'), nullable=False),
                    sa.Column('status', sa.Enum('IDLE', 'UPLOAD', 'PROBING', name='probeserverstatus'), nullable=False),
                    sa.Column('last_update', sa.DateTime(timezone=True), nullable=False),
                    sa.Column('server_secret', sa.String(), nullable=False),
                    sa.Column('control_secret', sa.String(), nullable=False),
                    sa.Column('control_secret_salt', sa.String(), nullable=False),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_index(op.f('ix_probe_servers_id'), 'probe_servers', ['id'], unique=False)
    op.create_index(op.f('ix_probe_servers_address'), 'probe_servers', ['address'], unique=True)
    op.create_index(op.f('ix_probe_servers_name'), 'probe_servers', ['name'], unique=True)

    op.create_table('probe_series',
                    sa.Column('id', sa.SmallInteger(), nullable=False),
                    sa.Column('type', sa.Enum('PUBLIC_ICMP_PING', 'PUBLIC_UDP_DNS', 'PUBLIC_TCP_DNS', 'PRIVATE_UDP', name='probeseriestype'), nullable=False),
                    sa.Column('config', postgresql.JSONB(astext_type=sa.Text()), nullable=False),
                    sa.Column('status',
                              sa.Enum('NEW', 'PENDING', 'ONGOING', 'PAUSED', 'FINISHED', 'CLOSED', 'CANCELLED',
                                      name='probeseriesstatus'), nullable=False),
                    sa.Column('start_time', sa.DateTime(timezone=True), nullable=True),
                    sa.Column('finish_time', sa.DateTime(timezone=True), nullable=True),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_index(op.f('ix_probe_series_id'), 'probe_series', ['id'], unique=False)

    op.create_table('probe_series_targets',
                    sa.Column('probe_series_id', sa.SmallInteger(), nullable=False),
                    sa.Column('target_service_id', sa.Integer(), nullable=False),
                    sa.ForeignKeyConstraint(['probe_series_id'], ['probe_series.id'], ),
                    sa.ForeignKeyConstraint(['target_service_id'], ['target_services.id'], ),
                    sa.PrimaryKeyConstraint('probe_series_id', 'target_service_id')
                    )
    op.create_index(op.f('ix_probe_series_targets_probe_series_id'), 'probe_series_targets', ['probe_series_id'],
                    unique=False)
    op.create_index(op.f('ix_probe_series_targets_target_service_id'), 'probe_series_targets', ['target_service_id'],
                    unique=False)

    op.create_table('probe_packets',
                    sa.Column('probe_series_id', sa.SmallInteger(), nullable=False),
                    sa.Column('sequence_number', sa.Integer(), nullable=False),
                    sa.Column('server_id', sa.SmallInteger(), nullable=False),
                    sa.Column('target_service_id', sa.Integer(), nullable=False),
                    sa.Column('send_time', sa.DateTime(timezone=True), nullable=True),
                    sa.Column('recv_time', sa.DateTime(timezone=True), nullable=True),
                    sa.Column('delta', sa.Integer(), nullable=True),
                    sa.Column('size', sa.SmallInteger(), nullable=False),
                    sa.Column('dropped', sa.Boolean(), nullable=False),
                    sa.Column('req_packet_id', sa.Integer(), nullable=True),
                    sa.Column('res_packet_id', sa.Integer(), nullable=True),
                    sa.Column('data', postgresql.BYTEA(), nullable=True),
                    sa.Column('anomaly', sa.String(), nullable=True),
                    sa.ForeignKeyConstraint(['probe_series_id'], ['probe_series.id'], ),
                    sa.ForeignKeyConstraint(['server_id'], ['probe_servers.id'], ),
                    sa.ForeignKeyConstraint(['target_service_id'], ['target_services.id'], ),
                    sa.PrimaryKeyConstraint('probe_series_id', 'sequence_number', 'server_id', 'target_service_id')
                    )
    op.create_index(op.f('ix_probe_packets_dropped'), 'probe_packets', ['dropped'], unique=False)
    op.create_index(op.f('ix_probe_packets_probe_series_id'), 'probe_packets', ['probe_series_id'], unique=False)
    op.create_index(op.f('ix_probe_packets_server_id'), 'probe_packets', ['server_id'], unique=False)
    op.create_index(op.f('ix_probe_packets_req_packet_id'), 'probe_packets', ['req_packet_id'], unique=False)
    op.create_index(op.f('ix_probe_packets_res_packet_id'), 'probe_packets', ['res_packet_id'], unique=False)
    op.create_index(op.f('ix_probe_packets_sequence_number'), 'probe_packets', ['sequence_number'], unique=False)
    op.create_index(op.f('ix_probe_packets_size'), 'probe_packets', ['size'], unique=False)
    op.create_index(op.f('ix_probe_packets_target_service_id'), 'probe_packets', ['target_service_id'], unique=False)


def downgrade() -> None:
    op.drop_index(op.f('ix_probe_packets_target_service_id'), table_name='probe_packets')
    op.drop_index(op.f('ix_probe_packets_size'), table_name='probe_packets')
    op.drop_index(op.f('ix_probe_packets_sequence_number'), table_name='probe_packets')
    op.drop_index(op.f('ix_probe_packets_res_packet_id'), table_name='probe_packets')
    op.drop_index(op.f('ix_probe_packets_req_packet_id'), table_name='probe_packets')
    op.drop_index(op.f('ix_probe_packets_server_id'), table_name='probe_packets')
    op.drop_index(op.f('ix_probe_packets_probe_series_id'), table_name='probe_packets')
    op.drop_index(op.f('ix_probe_packets_dropped'), table_name='probe_packets')
    op.drop_table('probe_packets')

    op.drop_index(op.f('ix_probe_series_targets_target_service_id'), table_name='probe_series_targets')
    op.drop_index(op.f('ix_probe_series_targets_probe_series_id'), table_name='probe_series_targets')
    op.drop_table('probe_series_targets')

    op.drop_index(op.f('ix_probe_series_id'), table_name='probe_series')
    op.drop_table('probe_series')

    op.drop_index(op.f('ix_probe_servers_name'), table_name='probe_servers')
    op.drop_index(op.f('ix_probe_servers_address'), table_name='probe_servers')
    op.drop_index(op.f('ix_probe_servers_id'), table_name='probe_servers')
    op.drop_table('probe_servers')

    op.drop_index(op.f('ix_target_services_target_host_id'), table_name='target_services')
    op.drop_index(op.f('ix_target_services_id'), table_name='target_services')
    op.drop_table('target_services')

    op.drop_index(op.f('ix_target_hosts_id'), table_name='target_hosts')
    op.drop_index(op.f('ix_target_hosts_address'), table_name='target_hosts')
    op.drop_table('target_hosts')

    op.drop_index(op.f('ix_users_id'), table_name='users')
    op.drop_index(op.f('ix_users_email'), table_name='users')
    op.drop_table('users')
