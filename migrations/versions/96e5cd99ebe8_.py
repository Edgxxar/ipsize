"""Add target_services_history Table

Revision ID: 96e5cd99ebe8
Revises: 7eb35accb46c
Create Date: 2023-06-24 10:07:45.206370

"""
import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pgsql
from alembic import op

# revision identifiers, used by Alembic.
revision = '96e5cd99ebe8'
down_revision = '7eb35accb46c'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table('target_services_history',
                    sa.Column('target_service_id', sa.Integer(), nullable=False),
                    sa.Column('timestamp', sa.DateTime(timezone=True), nullable=False),
                    sa.Column('status', pgsql.ENUM('OK', 'ERROR', 'UNKNOWN', name='status', create_type=False), nullable=False),
                    sa.ForeignKeyConstraint(['target_service_id'], ['target_services.id'], ),
                    sa.PrimaryKeyConstraint('target_service_id', 'timestamp')
                    )
    op.create_index(op.f('ix_target_services_history_target_service_id'), 'target_services_history', ['target_service_id'], unique=False)
    op.create_index(op.f('ix_target_services_history_timestamp'), 'target_services_history', ['timestamp'], unique=False)


def downgrade() -> None:
    op.drop_index(op.f('ix_target_services_history_timestamp'), table_name='target_services_history')
    op.drop_index(op.f('ix_target_services_history_target_service_id'), table_name='target_services_history')
    op.drop_table('target_services_history')
