"""add unique constraint to target_services id and type

Revision ID: 2e14f919c47b
Revises: 721950356c6f
Create Date: 2023-08-03 19:46:22.586438

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = '2e14f919c47b'
down_revision = '721950356c6f'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_unique_constraint('target_services_id_type_key', 'target_services', ['id', 'type'])


def downgrade() -> None:
    op.drop_constraint('target_services_id_type_key', 'target_services', type_='unique')
