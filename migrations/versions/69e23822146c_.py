"""add unique constraint to target_services host_id and type

Revision ID: 69e23822146c
Revises: 0b74d3b494c7
Create Date: 2023-08-15 14:59:35.036029

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '69e23822146c'
down_revision = '0b74d3b494c7'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.drop_constraint('target_services_id_type_key', 'target_services', type_='unique')
    # delete duplicate services, keep the service with the lowest port number for each type
    op.execute("CREATE TEMPORARY TABLE duplicate_service_types(id INT)")
    op.execute("""
    INSERT INTO duplicate_service_types (id) SELECT id FROM (
        SELECT id, ROW_NUMBER() OVER(PARTITION BY target_host_id, type ORDER BY port) AS row_num FROM target_services
    ) t WHERE t.row_num > 1
    """)
    op.execute("DELETE FROM probe_series_targets WHERE target_service_id IN (SELECT id FROM duplicate_service_types)")
    op.execute("DELETE FROM target_services_history WHERE target_service_id IN (SELECT id FROM duplicate_service_types)")
    op.execute("DELETE FROM target_services WHERE id IN (SELECT id FROM duplicate_service_types)")
    op.execute("DROP TABLE duplicate_service_types")
    op.create_unique_constraint('target_services_host_type_key', 'target_services', ['target_host_id', 'type'])


def downgrade() -> None:
    op.drop_constraint('target_services_host_type_key', 'target_services', type_='unique')
    op.create_unique_constraint('target_services_id_type_key', 'target_services', ['id', 'type'])
