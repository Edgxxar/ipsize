import ipaddress
import os
import tempfile
import time
from datetime import datetime
from typing import Union
from unittest.mock import MagicMock

import anyio
import hypothesis
import psycopg
import pytest
from anyio.abc import TaskStatus
from anyio.from_thread import BlockingPortal
from fastapi import Request
from httpx import AsyncClient
from pytest_postgresql import factories as postgresql_factories
from pytest_redis import factories as redis_factories
from redis.asyncio.client import Redis
from scapy.interfaces import NetworkInterface, get_working_if
from scapy.packet import Packet
from scapy.supersocket import SuperSocket
from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession, async_sessionmaker, create_async_engine
from sqlalchemy_utils import create_database, database_exists

import ipsize.control.api.deps as control_server_deps
from ipsize import models, security
from ipsize.config import settings
from ipsize.control import main as control_server_main
from ipsize.control.persistence import crud, entities
from ipsize.control.persistence.database import Base
from ipsize.models.enum import ProbeSeriesStatus, ProbeSeriesType, Status, TCPState
from ipsize.probe import main as probe_server_main
from ipsize.probe.config import settings as probe_settings
from ipsize.probe.io.base_series import ProbeSeries
from ipsize.probe.io.icmp import ICMPProbe, ICMPProbeSeries
from ipsize.probe.io.monitor import GuardedTestMonitor, TCPStateMachine
from ipsize.probe.io.tcp_dns import TCPDNSProbe, TCPDNSProbeSeries
from ipsize.probe.io.tcp_private import TCPProbe, TCPProbeSeries
from ipsize.probe.io.tcp_smtp import TCPSMTPProbe, TCPSMTPProbeSeries
from ipsize.probe.io.udp_dns import UDPDNSProbe, UDPDNSProbeSeries
from ipsize.probe.io.udp_private import UDPProbe, UDPProbeSeries
from ipsize.probe.server import probe_server
from ipsize.rest import limiter

redis_factories.redis_proc(port=None)
postgresql_factories.postgresql_proc(port=None, unixsocketdir='/tmp')


def pytest_configure(config):
    """
    Load hypothesis ci profile if we are running in CI.
    Disable the rate limiter for testing. Individual tests may enable it again to test the rate limiter itself.
    Disable lifecycle of servers.
    """
    hypothesis.settings.register_profile("ci", max_examples=10000)
    if os.getenv("CI", False):
        hypothesis.settings.load_profile("ci")

    limiter.enabled = False

    control_server_main.do_lifecycle = False
    probe_server_main.do_lifecycle = False


@pytest.fixture(scope="session")
def anyio_backend():
    return "asyncio"


# ===== Database Fixtures ===== #


@pytest.fixture()
async def redis(redis_proc, mocker) -> Redis:
    redis = Redis(unix_socket_path=redis_proc.unixsocket, decode_responses=True)

    mock_redis = mocker.patch("ipsize.probe.persistence.database.get_redis")
    mock_redis.return_value = redis

    yield redis

    await redis.flushall()


@pytest.fixture()
async def redis_binary(redis_proc, mocker) -> Redis:
    redis = Redis(unix_socket_path=redis_proc.unixsocket, decode_responses=False)

    mock_redis = mocker.patch("ipsize.probe.persistence.database.get_redis_binary")
    mock_redis.return_value = redis

    yield redis

    await redis.flushall()


@pytest.fixture(scope="session")
async def db_engine(postgresql_proc) -> AsyncEngine:
    """
    Provides a session-wide postgres database.
    """
    conn_str = f"postgresql+psycopg://{postgresql_proc.user}:{postgresql_proc.password}@" \
               f"{postgresql_proc.host}:{postgresql_proc.port}/{postgresql_proc.dbname}"
    # sqlalchemy utils are not available sync
    if not database_exists(conn_str):
        create_database(conn_str)
    # create async test engine for all tests
    engine = create_async_engine(conn_str)
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)

    yield engine


@pytest.fixture(scope="session")
async def db_session(db_engine) -> AsyncSession:
    """
    Provides a database session for the whole test session scope, to set up database entries which should be
    available to all tests.
    """
    session = async_sessionmaker(bind=db_engine, autoflush=False, autocommit=False, expire_on_commit=False)
    async with session() as db:
        yield db


@pytest.fixture()
async def db_maker(db_engine) -> async_sessionmaker:
    """
    Provides a database session maker which rolls back any changes after test execution. Multiple sessions can be
    created from this session maker which will see changes made to the database by other sessions of this session maker.
    """
    # use the same connection for the whole test
    async with db_engine.connect() as conn:
        # begin a non-ORM transaction
        trans = await conn.begin()
        # create a new session within
        session = async_sessionmaker(bind=conn, autoflush=False, autocommit=False, expire_on_commit=False)
        yield session

        # roll back all changes
        await trans.rollback()


@pytest.fixture()
async def db(db_maker) -> AsyncSession:
    """
    Provides a database session which rolls back any changes after test execution.
    """
    async with db_maker() as db:
        yield db


@pytest.fixture()
async def db_psycopg(postgresql_proc) -> psycopg.AsyncConnection:
    """
    Provides a database connection using the psycopg driver directly, which rolls back any changes after test execution.
    Mocks the psycopg_connect method to also use this connection.
    """
    async with await psycopg.AsyncConnection.connect(
            host=postgresql_proc.host,
            port=postgresql_proc.port,
            user=postgresql_proc.user,
            password=postgresql_proc.password,
            dbname=postgresql_proc.dbname
    ) as conn:
        yield conn
        await conn.rollback()


# ===== Persistent Test Data Fixtures ===== #


@pytest.fixture(scope="session")
async def admin_user_entity(db_session) -> entities.User:
    """
    Creates an admin user in the database.
    """
    yield await crud.user.create(db_session, models.UserCreate(
        email="admin@ipsize.de",
        password="admin_password"
    ))


@pytest.fixture(scope="session")
async def probe_server_entity(db_session) -> entities.Server:
    """
    Creates a probe server in the database.
    """
    server = await crud.server.create(db_session, models.ServerCreate(
        name="probe001",
        url="https://probe001.local",
        server_secret="server_secret",
        control_secret="control_secret"
    ))
    await crud.server.add_address(db_session, server.id, "10.0.0.1")
    await crud.server.add_address(db_session, server.id, "fe80::1")
    yield await crud.server.get_join_addresses(db_session, server.id)


# ===== Authentication Token Fixtures ===== #


@pytest.fixture(scope="session")
async def token_cs_user(admin_user_entity) -> str:
    """
    Creates a valid user access token for the control server.
    """
    token = security.create_access_token(
        models.TokenPayload.from_user_id(admin_user_entity.id),
        settings.ACCESS_TOKEN_EXPIRE_MINUTES,
        settings.SECRET_KEY
    )
    yield token


@pytest.fixture(scope="session")
async def token_cs_server(probe_server_entity) -> str:
    """
    Creates a valid server access token for the control server.
    """
    token = security.create_access_token(
        models.TokenPayload.from_server_id(probe_server_entity.id),
        settings.ACCESS_TOKEN_EXPIRE_MINUTES,
        settings.SECRET_KEY
    )
    yield token


@pytest.fixture(scope="session")
async def token_ps() -> str:
    """
    Creates a valid access token for the probe server.
    """
    token = security.create_access_token(
        "This is an access token",
        settings.ACCESS_TOKEN_EXPIRE_MINUTES,
        settings.SECRET_KEY
    )
    yield token


# ===== Test Client Fixtures ===== #


@pytest.fixture()
async def client_cs(db) -> AsyncClient:
    """
    Provides a test client for the control server, using the test database.
    """
    # override the database dependency
    control_server_main.app.dependency_overrides[control_server_deps.get_db] = lambda: db
    async with AsyncClient(app=control_server_main.app, base_url="http://controlserver.test") as c:
        yield c


@pytest.fixture(params=["user", "server"])
async def client_cs_auth(request, client_cs, token_cs_user, token_cs_server):
    """
    Provides a test client for the control server, using the test database and both a user and a server access token.
    """
    if request.param == "user":
        client_cs.headers["Authorization"] = f"Bearer {token_cs_user}"
    else:
        client_cs.headers["Authorization"] = f"Bearer {token_cs_server}"
    yield client_cs


@pytest.fixture()
async def client_cs_user(client_cs, token_cs_user) -> AsyncClient:
    """
    Provides a test client for the control server, using the test database and a user access token.
    """
    client_cs.headers["Authorization"] = f"Bearer {token_cs_user}"
    yield client_cs


@pytest.fixture()
async def client_cs_server(client_cs, token_cs_server) -> AsyncClient:
    """
    Provides a test client for the control server, using the test database and a server access token.
    """
    client_cs.headers["Authorization"] = f"Bearer {token_cs_server}"
    yield client_cs


@pytest.fixture()
async def client_ps(redis) -> AsyncClient:
    """
    Provides a test client for the probe server, using the test redis instance.
    """
    async with AsyncClient(app=probe_server_main.app, base_url="http://probeserver.test") as c:
        yield c


@pytest.fixture()
async def client_ps_auth(client_ps, token_ps) -> AsyncClient:
    """
    Provides a test client for the probe server, using the test redis instance and an access token.
    """
    client_ps.headers["Authorization"] = f"Bearer {token_ps}"
    yield client_ps


# ===== Probe Series IO Fixtures ===== #


@pytest.fixture()
def mock_request() -> Request:
    """
    Provides a HTTP request object that receives empty messages
    """

    async def get_message():
        return {}

    return Request({"type": "http"}, lambda: get_message())


@pytest.fixture(scope="session")
def mock_probe_servers() -> list[models.ServerModel]:
    """
    Two probe servers that a probe series could be executed on
    """
    return [
        models.ServerModel(
            id=1,
            name="probe001",
            addresses=["10.0.0.1"],
            url="https://probe001.local"
        ),
        models.ServerModel(
            id=2,
            name="probe002",
            addresses=["10.0.0.2"],
            url="https://probe002.local"
        ),
    ]


@pytest.fixture(scope="session")
def interface() -> NetworkInterface:
    return get_working_if()


@pytest.fixture(params=[
    (ProbeSeriesType.PUBLIC_ICMP_PING, ICMPProbeSeries, ICMPProbe, None, None),
    (ProbeSeriesType.PUBLIC_UDP_DNS, UDPDNSProbeSeries, UDPDNSProbe, 53, 10053),
    # (ProbeSeriesType.PUBLIC_TCP_DNS, TCPDNSProbeSeries, TCPDNSProbe, 53, (11000, 11999)),
    # (ProbeSeriesType.PUBLIC_TCP_DNS, TCPSMTPProbeSeries, TCPSMTPProbe, 25, (12000, 12999)),
    (ProbeSeriesType.PRIVATE_UDP, UDPProbeSeries, UDPProbe, 10042, 10042),
    (ProbeSeriesType.PRIVATE_TCP, TCPProbeSeries, TCPProbe, 11337, 11337),
])
async def mock_probe_series(request, mocker, redis, redis_binary, mock_probe_servers, interface) -> ProbeSeries:
    """
    Provides a probe series that is registered with the probe server and can be executed, using the test redis instance.
    """
    probe_series_type, probe_series_class, probe_class, dport, sport = request.param
    # probe series model with some default parameters
    model = models.ProbeSeries(
        id=1,
        type=probe_series_type,
        config=models.ProbeSeriesConfig(
            interval=0.1,
            timeout=0.5,  # short timeout to speed up tests, packet sending is mocked anyway
            port=sport
        ),
        status=ProbeSeriesStatus.ONGOING
    )
    # target host representing the probe server, with service
    host = models.TargetHost(
        id=1,
        address="10.0.0.1",
        services=[
            models.TargetService(
                id=1,
                type=probe_series_type,
                port=dport,
                target_host_id=1,
                status=Status.OK,
                flapping=False,
                last_update=datetime.now()
            )
        ]
    )
    series = probe_series_class(
        probe_class, model, [host], mock_probe_servers, interface, anyio.to_thread.current_default_thread_limiter(),
        ipaddress.ip_address("127.0.0.1"), ipaddress.ip_address("::1")
    )  # type: ProbeSeries

    # mock netfilter methods to do nothing
    mocker.patch("ipsize.netfilter.add_chain")
    mocker.patch("ipsize.netfilter.add_rule")
    mocker.patch("ipsize.netfilter.delete_chain")

    # mock sending method to just set the sent_time attribute

    def mock_send_blocking(p: Union[list[Packet], Packet], iface, socket):
        if isinstance(p, Packet):
            p.sent_time = time.time()
        elif isinstance(p, list):
            for p_ in p:
                p_.sent_time = time.time()

    mock_send = mocker.patch("ipsize.probe.io.base_probe.send_blocking")
    mock_send.side_effect = mock_send_blocking
    mocker.patch("ipsize.probe.io.base_series.open_ipv4_socket")
    mocker.patch("ipsize.probe.io.base_series.open_ipv6_socket")

    # mock monitor to test monitor

    def mock_create_monitor(portal: BlockingPortal, task_status: TaskStatus[None]):
        return GuardedTestMonitor(portal, series.incoming, task_status)

    series.create_monitor = MagicMock(side_effect=mock_create_monitor)

    await series.add_targets([
        # some target that is not one of the probe servers
        models.TargetServiceModel(
            id=42,
            target_host_id=42,
            type=probe_series_type,
            address="10.0.0.42",
            port=dport
        )
    ])

    # mock TCP open so the tests don't wait for connection establishment
    def mock_tcp_open(self, iface: NetworkInterface, socket: SuperSocket):
        self.tcp._state = TCPState.ESTABLISHED

    mock_tcp_establish_connection = mocker.patch(
        "ipsize.probe.io.base_probe.PublicTCPProbe.establish_connection", autospec=True
    )
    mock_tcp_establish_connection.side_effect = mock_tcp_open
    mock_smtp_establish_connection = mocker.patch(
        "ipsize.probe.io.tcp_smtp.TCPSMTPProbe.establish_connection", autospec=True
    )
    mock_smtp_establish_connection.side_effect = mock_tcp_open

    # add series to probe server
    probe_server.series[series.id] = series

    # yield series with a temporary save directory for data files generated by the tests
    with tempfile.TemporaryDirectory() as save_dir:
        probe_settings.SAVE_DIR = save_dir
        yield series

    # remove again, if not already done by the test
    if series.id in probe_server.series:
        del probe_server.series[series.id]
