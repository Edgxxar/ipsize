import pytest
from fastapi import status

from ipsize import models
from ipsize.control.persistence import crud, entities
from ipsize.models.enum import ProbeSeriesType, Status
from ipsize.rest import control_paths, format_path
from ipsize.util import encode_integers


@pytest.fixture
async def target_host_entity(db) -> entities.TargetHost:
    yield await crud.target_host.create(db, models.TargetHostCreate(
        address="1.1.1.1"
    ))


@pytest.fixture
async def target_service_entity(db, target_host_entity) -> entities.TargetService:
    yield await crud.target_service.create(db, models.TargetServiceCreate(
        target_host_id=target_host_entity.id,
        type=ProbeSeriesType.PUBLIC_ICMP_PING
    ))


@pytest.fixture
async def target_service_entities(db, target_host_entity) -> list[entities.TargetService]:
    icmp = await crud.target_service.create(db, models.TargetServiceCreate(
        target_host_id=target_host_entity.id,
        type=ProbeSeriesType.PUBLIC_ICMP_PING
    ))
    dns = await crud.target_service.create(db, models.TargetServiceCreate(
        target_host_id=target_host_entity.id,
        type=ProbeSeriesType.PUBLIC_UDP_DNS,
        port=53
    ))
    smtp = await crud.target_service.create(db, models.TargetServiceCreate(
        target_host_id=target_host_entity.id,
        type=ProbeSeriesType.PUBLIC_TCP_SMTP,
        port=25
    ))
    yield [icmp, dns, smtp]


@pytest.mark.anyio
async def test_create_target_host_authenticated(db, client_cs_auth):
    new_host = models.TargetHostCreate(
        address="8.8.8.8"
    )
    response = await client_cs_auth.post(
        control_paths.TARGET_HOST_CREATE,
        headers={"Content-Type": "application/json"},
        content=new_host.model_dump_json()
    )
    assert response.status_code == status.HTTP_200_OK
    host = models.TargetHost(**response.json())
    assert host.address == new_host.address
    assert host.enabled is True
    assert len(host.services) == 0
    db_host = await crud.target_host.get(db, host.id)
    assert db_host is not None
    assert db_host.address == new_host.address


@pytest.mark.anyio
async def test_create_target_host_duplicate(client_cs_user, target_host_entity):
    response = await client_cs_user.post(
        control_paths.TARGET_HOST_CREATE,
        json={"address": str(target_host_entity.address)}
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.anyio
async def test_create_target_host_unauthorized(client_cs, target_host_entity):
    response = await client_cs.post(
        control_paths.TARGET_HOST_CREATE,
        json={"address": str(target_host_entity.address)}
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_get_target_host_authenticated(client_cs_auth, target_host_entity, target_service_entities):
    response = await client_cs_auth.get(
        format_path(control_paths.TARGET_HOST_GET, host_id=target_host_entity.id)
    )
    assert response.status_code == status.HTTP_200_OK
    host = models.TargetHost(**response.json())
    assert host.id == target_host_entity.id
    assert host.address == target_host_entity.address
    assert len(host.services) == len(target_service_entities)
    assert [s.id for s in host.services] == [s.id for s in target_service_entities]


@pytest.mark.anyio
async def test_get_target_host_unauthorized(client_cs, target_host_entity):
    response = await client_cs.get(
        format_path(control_paths.TARGET_HOST_GET, host_id=target_host_entity.id)
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_get_target_host_unknown(client_cs_auth):
    response = await client_cs_auth.get(
        format_path(control_paths.TARGET_HOST_GET, host_id=1)
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.anyio
async def test_get_target_host_by_address_authenticated(client_cs_auth, target_host_entity, target_service_entities):
    response = await client_cs_auth.get(
        format_path(control_paths.TARGET_HOST_GET_BY_ADDRESS, address=target_host_entity.address)
    )
    assert response.status_code == status.HTTP_200_OK
    host = models.TargetHost(**response.json())
    assert host.id == target_host_entity.id
    assert host.address == target_host_entity.address
    assert len(host.services) == len(target_service_entities)
    assert [s.id for s in host.services] == [s.id for s in target_service_entities]


@pytest.mark.anyio
async def test_get_target_host_by_address_unauthorized(client_cs, target_host_entity):
    response = await client_cs.get(
        format_path(control_paths.TARGET_HOST_GET_BY_ADDRESS, address=target_host_entity.address)
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_get_target_host_by_address_unknown(client_cs_auth):
    response = await client_cs_auth.get(
        format_path(control_paths.TARGET_HOST_GET_BY_ADDRESS, address="127.0.0.1")
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.anyio
async def test_check_target_host_authenticated(client_cs_auth, target_host_entity):
    response = await client_cs_auth.get(
        format_path(control_paths.TARGET_HOST_CHECK, address=target_host_entity.address)
    )
    assert response.status_code == status.HTTP_200_OK
    host = models.TargetHostEnabled(**response.json())
    assert host.address == target_host_entity.address
    assert host.enabled


@pytest.mark.anyio
async def test_check_target_host_as_host(mocker, client_cs, target_host_entity):
    # mock the address with which the client requests the resource
    mock_client = mocker.patch("fastapi.Request.client")
    mock_client.host = str(target_host_entity.address)
    response = await client_cs.get(
        format_path(control_paths.TARGET_HOST_CHECK, address=target_host_entity.address)
    )
    assert response.status_code == status.HTTP_200_OK
    host = models.TargetHostEnabled(**response.json())
    assert host.address == target_host_entity.address
    assert host.enabled


@pytest.mark.anyio
async def test_check_target_host_as_host_proxy(client_cs, target_host_entity):
    response = await client_cs.get(
        format_path(control_paths.TARGET_HOST_CHECK, address=target_host_entity.address),
        headers={"X-Real-IP": str(target_host_entity.address)}
    )
    assert response.status_code == status.HTTP_200_OK
    host = models.TargetHostEnabled(**response.json())
    assert host.address == target_host_entity.address
    assert host.enabled


@pytest.mark.anyio
async def test_check_target_host_unauthorized(client_cs, target_host_entity):
    response = await client_cs.get(
        format_path(control_paths.TARGET_HOST_CHECK, address=target_host_entity.address)
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_enable_target_host_authenticated(client_cs_auth, target_host_entity):
    response = await client_cs_auth.post(
        format_path(control_paths.TARGET_HOST_ENABLE, address=target_host_entity.address)
    )
    assert response.status_code == status.HTTP_200_OK
    host = models.TargetHostEnabled(**response.json())
    assert host.address == target_host_entity.address
    assert host.enabled


@pytest.mark.anyio
async def test_enable_target_host_as_host(mocker, client_cs, target_host_entity):
    # mock the address with which the client requests the resource
    mock_client = mocker.patch("fastapi.Request.client")
    mock_client.host = str(target_host_entity.address)
    response = await client_cs.post(
        format_path(control_paths.TARGET_HOST_ENABLE, address=target_host_entity.address)
    )
    assert response.status_code == status.HTTP_200_OK
    host = models.TargetHostEnabled(**response.json())
    assert host.address == target_host_entity.address
    assert host.enabled


@pytest.mark.anyio
async def test_enable_target_host_as_host_proxy(client_cs, target_host_entity):
    response = await client_cs.post(
        format_path(control_paths.TARGET_HOST_ENABLE, address=target_host_entity.address),
        headers={"X-Real-IP": str(target_host_entity.address)}
    )
    assert response.status_code == status.HTTP_200_OK
    host = models.TargetHostEnabled(**response.json())
    assert host.address == target_host_entity.address
    assert host.enabled


@pytest.mark.anyio
async def test_enable_target_host_unauthorized(client_cs, target_host_entity):
    response = await client_cs.post(
        format_path(control_paths.TARGET_HOST_ENABLE, address=target_host_entity.address)
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_disable_target_host_authenticated(client_cs_auth, target_host_entity):
    response = await client_cs_auth.post(
        format_path(control_paths.TARGET_HOST_DISABLE, address=target_host_entity.address)
    )
    assert response.status_code == status.HTTP_200_OK
    host = models.TargetHostEnabled(**response.json())
    assert host.address == target_host_entity.address
    assert not host.enabled


@pytest.mark.anyio
async def test_disable_target_host_as_host(mocker, client_cs, target_host_entity):
    # mock the address with which the client requests the resource
    mock_client = mocker.patch("fastapi.Request.client")
    mock_client.host = str(target_host_entity.address)
    response = await client_cs.post(
        format_path(control_paths.TARGET_HOST_DISABLE, address=target_host_entity.address)
    )
    assert response.status_code == status.HTTP_200_OK
    host = models.TargetHostEnabled(**response.json())
    assert host.address == target_host_entity.address
    assert not host.enabled


@pytest.mark.anyio
async def test_disable_target_host_as_host_proxy(client_cs, target_host_entity):
    response = await client_cs.post(
        format_path(control_paths.TARGET_HOST_DISABLE, address=target_host_entity.address),
        headers={"X-Real-IP": str(target_host_entity.address)}
    )
    assert response.status_code == status.HTTP_200_OK
    host = models.TargetHostEnabled(**response.json())
    assert host.address == target_host_entity.address
    assert not host.enabled


@pytest.mark.anyio
async def test_disable_target_host_unauthorized(client_cs, target_host_entity):
    response = await client_cs.post(
        format_path(control_paths.TARGET_HOST_DISABLE, address=target_host_entity.address)
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_create_target_service_authenticated(db, client_cs_auth, target_host_entity):
    new_service = models.TargetServiceCreate(
        target_host_id=target_host_entity.id,
        type=ProbeSeriesType.PUBLIC_ICMP_PING
    )
    response = await client_cs_auth.post(
        control_paths.TARGET_SERVICE_CREATE,
        json=new_service.model_dump()
    )
    assert response.status_code == status.HTTP_200_OK
    service = models.TargetService(**response.json())
    db_service = await crud.target_service.get_join_host(db, service.id)
    assert db_service is not None
    assert db_service.host == target_host_entity
    assert db_service.type == ProbeSeriesType.PUBLIC_ICMP_PING
    assert db_service.status == Status.UNKNOWN


@pytest.mark.anyio
async def test_create_target_service_authenticated_with_status(db, client_cs_auth, target_host_entity):
    new_service = models.TargetServiceCreate(
        target_host_id=target_host_entity.id,
        type=ProbeSeriesType.PUBLIC_ICMP_PING,
        status=Status.OK
    )
    response = await client_cs_auth.post(
        control_paths.TARGET_SERVICE_CREATE,
        json=new_service.model_dump()
    )
    assert response.status_code == status.HTTP_200_OK
    service = models.TargetService(**response.json())
    db_service = await crud.target_service.get_join_host(db, service.id)
    assert db_service is not None
    assert db_service.host == target_host_entity
    assert db_service.type == ProbeSeriesType.PUBLIC_ICMP_PING
    assert db_service.status == Status.OK


@pytest.mark.anyio
async def test_create_target_service_unauthorized(client_cs, target_host_entity):
    new_service = models.TargetServiceCreate(
        target_host_id=target_host_entity.id,
        type=ProbeSeriesType.PUBLIC_ICMP_PING
    )
    response = await client_cs.post(
        control_paths.TARGET_SERVICE_CREATE,
        json=new_service.model_dump()
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_create_target_service_unknown_host(client_cs_user):
    new_service = models.TargetServiceCreate(
        target_host_id=42,
        type=ProbeSeriesType.PUBLIC_ICMP_PING
    )
    response = await client_cs_user.post(
        control_paths.TARGET_SERVICE_CREATE,
        json=new_service.model_dump()
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.anyio
async def test_put_target_service_status_ok(db, client_cs_auth, target_service_entity):
    response = await client_cs_auth.put(
        format_path(control_paths.TARGET_SERVICE_PUT_STATUS, service_id=target_service_entity.id),
        json=Status.OK
    )
    assert response.status_code == status.HTTP_200_OK
    db_service = await crud.target_service.get(db, target_service_entity.id)
    assert db_service.status == Status.OK


@pytest.mark.anyio
async def test_put_target_service_status_error(db, client_cs_auth, target_service_entity):
    response = await client_cs_auth.put(
        format_path(control_paths.TARGET_SERVICE_PUT_STATUS, service_id=target_service_entity.id),
        json=Status.ERROR
    )
    assert response.status_code == status.HTTP_200_OK
    db_service = await crud.target_service.get(db, target_service_entity.id)
    assert db_service.status == Status.ERROR


@pytest.mark.anyio
async def test_put_target_service_status_unauthorized(client_cs, target_service_entity):
    response = await client_cs.put(
        format_path(control_paths.TARGET_SERVICE_PUT_STATUS, service_id=target_service_entity.id),
        json=Status.OK
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_put_target_service_status_unknown_service(client_cs_auth):
    response = await client_cs_auth.put(
        format_path(control_paths.TARGET_SERVICE_PUT_STATUS, service_id=1),
        json=Status.OK
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.anyio
async def test_get_target_services_authenticated(client_cs_auth, target_service_entities):
    ids = [s.id for s in target_service_entities]
    ids.sort()
    response = await client_cs_auth.post(
        control_paths.TARGET_SERVICE_GET_ALL,
        headers={"Content-Type": "application/octet-stream"},
        content=encode_integers(ids)
    )
    assert response.status_code == status.HTTP_200_OK
    services = [models.TargetServiceModel(**s) for s in response.json()]
    assert set([s.id for s in services]) == set([s.id for s in target_service_entities])


@pytest.mark.anyio
async def test_get_target_services_empty_body(client_cs_user):
    response = await client_cs_user.post(
        control_paths.TARGET_SERVICE_GET_ALL,
        headers={"Content-Type": "application/octet-stream"},
        content=bytes(0)
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.anyio
async def test_get_target_services_unauthorized(client_cs):
    response = await client_cs.post(
        control_paths.TARGET_SERVICE_GET_ALL
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
