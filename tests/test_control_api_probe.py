import pytest
from fastapi import status

from ipsize import models
from ipsize.control.persistence import crud, entities
from ipsize.models.enum import ProbeSeriesStatus, ProbeSeriesType
from ipsize.rest import control_paths, format_path
from ipsize.util import decode_integers


@pytest.fixture()
def probe_series_create():
    return models.ProbeSeriesCreate(
        type=ProbeSeriesType.PUBLIC_ICMP_PING,
    )


@pytest.fixture
async def probe_series_entity(db, probe_series_create) -> entities.ProbeSeries:
    yield await crud.probe_series.create(db, probe_series_create)


@pytest.fixture
async def target_host_entity(db) -> entities.TargetHost:
    yield await crud.target_host.create(db, models.TargetHostCreate(
        address="1.1.1.1"
    ))


@pytest.fixture
async def target_service_entity(db, target_host_entity, probe_series_entity) -> entities.TargetService:
    service = await crud.target_service.create(db, models.TargetServiceCreate(
        target_host_id=target_host_entity.id,
        type=ProbeSeriesType.PUBLIC_ICMP_PING
    ))
    await crud.target_service.insert_target(db, probe_series_entity.id, service.id)
    yield service


@pytest.mark.anyio
async def test_create_probe_series_as_user(db, client_cs_user, probe_series_create):
    response = await client_cs_user.post(
        control_paths.PROBE_SERIES_CREATE,
        json=probe_series_create.model_dump()
    )
    assert response.status_code == status.HTTP_200_OK
    model = models.ProbeSeries(**response.json())
    assert model.type == probe_series_create.type
    db_series = await crud.probe_series.get(db, model.id)
    assert db_series is not None
    assert db_series.type == probe_series_create.type


@pytest.mark.anyio
async def test_create_probe_series_as_server(client_cs_server, probe_series_create):
    response = await client_cs_server.post(
        control_paths.PROBE_SERIES_CREATE,
        json=probe_series_create.model_dump()
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.anyio
async def test_create_probe_series_unauthorized(client_cs, probe_series_create):
    response = await client_cs.post(
        control_paths.PROBE_SERIES_CREATE,
        json=probe_series_create.model_dump()
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_get_probe_series_available_authenticated(client_cs_auth, probe_series_entity):
    response = await client_cs_auth.get(control_paths.PROBE_SERIES_GET_AVAILABLE)
    assert response.status_code == status.HTTP_200_OK
    available = response.json()
    assert len(available) == 1
    series = models.ProbeSeries(**available[0])
    assert series.id == probe_series_entity.id


@pytest.mark.anyio
async def test_get_probe_series_available_closed(db, client_cs_auth, probe_series_entity):
    # close probe series
    await crud.probe_series.update_status(db, probe_series_entity, ProbeSeriesStatus.CLOSED)

    # assert series is no longer available
    response = await client_cs_auth.get(control_paths.PROBE_SERIES_GET_AVAILABLE)
    assert response.status_code == status.HTTP_200_OK
    available = response.json()
    assert len(available) == 0


@pytest.mark.anyio
async def test_get_probe_series_available_filtered_in(db, client_cs_server, probe_server_entity, probe_series_entity):
    # add server to config
    config = models.ProbeSeriesConfig.model_validate(probe_series_entity.config)
    config.servers = [probe_server_entity.id]
    await crud.probe_series.update_config(db, probe_series_entity, config)

    # assert series is available
    response = await client_cs_server.get(control_paths.PROBE_SERIES_GET_AVAILABLE)
    assert response.status_code == status.HTTP_200_OK
    available = response.json()
    assert len(available) == 1


@pytest.mark.anyio
async def test_get_probe_series_available_filtered_out(db, client_cs_server, probe_server_entity, probe_series_entity):
    # exclude server from config
    config = models.ProbeSeriesConfig.model_validate(probe_series_entity.config)
    config.servers = []
    await crud.probe_series.update_config(db, probe_series_entity, config)

    # assert series is no longer available
    response = await client_cs_server.get(control_paths.PROBE_SERIES_GET_AVAILABLE)
    assert response.status_code == status.HTTP_200_OK
    available = response.json()
    assert len(available) == 0


@pytest.mark.anyio
async def test_get_probe_series_authenticated(client_cs_auth, probe_series_entity):
    response = await client_cs_auth.get(
        format_path(control_paths.PROBE_SERIES_GET, series_id=probe_series_entity.id)
    )
    assert response.status_code == status.HTTP_200_OK
    series = models.ProbeSeries(**response.json())
    assert series.id == probe_series_entity.id


@pytest.mark.anyio
async def test_get_probe_series_unauthorized(client_cs, probe_series_entity):
    response = await client_cs.get(
        format_path(control_paths.PROBE_SERIES_GET, series_id=probe_series_entity.id)
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_get_probe_series_targets_authenticated(client_cs_auth, probe_series_entity, target_service_entity):
    response = await client_cs_auth.get(
        format_path(control_paths.PROBE_SERIES_GET_TARGETS, series_id=probe_series_entity.id)
    )
    assert response.status_code == status.HTTP_200_OK
    targets = decode_integers(response.content)
    assert len(targets) == 1
    assert targets[0] == target_service_entity.id


@pytest.mark.anyio
async def test_get_probe_series_targets_unauthorized(client_cs, probe_series_entity):
    response = await client_cs.get(
        format_path(control_paths.PROBE_SERIES_GET_TARGETS, series_id=probe_series_entity.id)
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_put_probe_series_config_as_user(db, client_cs_user, probe_series_entity):
    config = models.ProbeSeriesConfig(
        interval=1
    )
    response = await client_cs_user.put(
        format_path(control_paths.PROBE_SERIES_PUT_CONFIG, series_id=probe_series_entity.id),
        json=config.model_dump()
    )
    assert response.status_code == status.HTTP_200_OK
    series = models.ProbeSeries(**response.json())
    assert series.id == probe_series_entity.id
    assert series.config == config
    db_series = await crud.probe_series.get(db, probe_series_entity.id)
    db_config = models.ProbeSeriesConfig(**db_series.config)
    assert db_config == config


@pytest.mark.anyio
async def test_put_probe_series_config_as_server(client_cs_server, probe_series_entity):
    config = models.ProbeSeriesConfig(
        interval=1
    )
    response = await client_cs_server.put(
        format_path(control_paths.PROBE_SERIES_PUT_CONFIG, series_id=probe_series_entity.id),
        json=config.model_dump()
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.anyio
async def test_put_probe_series_config_unauthorized(client_cs, probe_series_entity):
    config = models.ProbeSeriesConfig(
        interval=1
    )
    response = await client_cs.put(
        format_path(control_paths.PROBE_SERIES_PUT_CONFIG, series_id=probe_series_entity.id),
        json=config.model_dump()
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_put_probe_series_status_as_user(db, client_cs_user, probe_series_entity):
    new_status = ProbeSeriesStatus.CLOSED
    response = await client_cs_user.put(
        format_path(control_paths.PROBE_SERIES_PUT_STATUS, series_id=probe_series_entity.id),
        json=new_status
    )
    assert response.status_code == status.HTTP_200_OK
    series = models.ProbeSeries(**response.json())
    assert series.id == probe_series_entity.id
    assert series.status == new_status
    db_series = await crud.probe_series.get(db, probe_series_entity.id)
    assert db_series.status == new_status


@pytest.mark.anyio
async def test_put_probe_series_status_as_server(client_cs_server, probe_series_entity):
    new_status = ProbeSeriesStatus.CLOSED
    response = await client_cs_server.put(
        format_path(control_paths.PROBE_SERIES_PUT_STATUS, series_id=probe_series_entity.id),
        json=new_status
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.anyio
async def test_put_probe_series_status_unauthorized(client_cs, probe_series_entity):
    new_status = ProbeSeriesStatus.CLOSED
    response = await client_cs.put(
        format_path(control_paths.PROBE_SERIES_PUT_STATUS, series_id=probe_series_entity.id),
        json=new_status
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
