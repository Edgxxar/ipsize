import ipaddress

import pytest
from hypothesis import given, strategies as st
from scapy.layers.inet import IP
from scapy.layers.inet6 import IPv6

from ipsize.probe.io.target import Target, TargetDict


def create_target(data):
    service_id, address = data
    return Target(service_id, address)


targets_ipv4 = st.tuples(st.integers(), st.ip_addresses(v=4)).map(create_target)
targets_ipv6 = st.tuples(st.integers(), st.ip_addresses(v=6)).map(create_target)
target_lists = st.lists(  # lists of targets with unique ids and addresses
    st.tuples(st.integers(), st.ip_addresses()),
    unique_by=(lambda x: x[0], lambda x: x[1])
).map(lambda l: [create_target(x) for x in l])


@given(targets_ipv4)
@pytest.mark.anyio
async def test_get_ipv4_headers(target):
    """
    Target ip headers should contain correct destination address
    """
    ip = target.get_ip_headers(ipaddress.ip_address("127.0.0.1"))

    assert IP in ip
    assert ipaddress.ip_address(ip.dst) == target.address


@given(targets_ipv6)
@pytest.mark.anyio
async def test_get_ipv6_headers(target):
    """
    Target ip headers should contain correct destination address
    """
    ip = target.get_ip_headers(ipaddress.ip_address("::1"))

    assert IPv6 in ip
    assert ipaddress.ip_address(ip.dst) == target.address


@given(target_lists)
def test_target_dict_insert(target_list):
    d = TargetDict()
    for target in target_list:
        d.add(target)

    assert len(d) == len(target_list)
    for target in target_list:
        assert d[target.service_id] == target
        assert d[target.address] == target
        assert d[str(target.address)] == target


@given(target_lists)
def test_target_dict_random_access(target_list):
    d = TargetDict()
    for target in target_list:
        d.add(target)

    random_set = set()
    for _ in range(len(target_list)):
        random_set.add(d.get_random())

    # each target should be contained exactly once
    assert len(random_set) == len(target_list)

    random_set2 = set()
    for _ in range(len(target_list)):
        random_set2.add(d.get_random())

    # each target should be contained exactly once, again
    assert len(random_set2) == len(target_list)


@given(target_lists, st.data())
def test_target_dict_remove(target_list, data):
    d = TargetDict()
    for target in target_list:
        d.add(target)

    remove_count = data.draw(st.integers(min_value=0, max_value=len(target_list)))
    remove_list = []
    for i in range(remove_count):
        remove_list.append(target_list[i])

    d.remove_all([target.service_id for target in remove_list])

    assert len(d) == len(target_list) - remove_count
    for target in remove_list:
        assert d[target.service_id] is None
        assert d[target.address] is None
        assert d[str(target.address)] is None
