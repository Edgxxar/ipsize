import ipaddress

import pytest
from hypothesis import given
from scapy.layers.inet import TCP

from ipsize.probe.io.tcp_private import TCPProbe, TCPProbeSeries
from .strategies import probe_arguments

PORT = 12345


@given(probe_arguments(TCPProbe, port=PORT))
@pytest.mark.anyio
async def test_assemble_request(args):
    """
    UDP probes should contain series id and target sequence number
    """
    series_id, server_id, target, size = args
    probe = TCPProbe(series_id, server_id, target, None, PORT, size)
    packet = probe.assemble_request()

    assert ipaddress.ip_address(packet.dst) == target.address
    assert TCP in packet
    assert packet[TCP].dport == target.port
    assert packet[TCP].ack == series_id
    assert packet[TCP].seq == probe.sequence_number

    assert len(packet) == size


@given(probe_arguments(TCPProbe, port=PORT))
@pytest.mark.anyio
async def test_extract_identifiers(args):
    series_id, server_id, target, size = args
    probe = TCPProbe(series_id, server_id, target, None, PORT, size)
    packet = probe.assemble_request()

    series, sequence_number = TCPProbeSeries.extract_identifiers(packet)
    assert series == series_id
    assert sequence_number == probe.sequence_number
