import ipaddress
import re

import pytest
from hypothesis import assume, given
from scapy.layers.dns import DNS, DNSRR
from scapy.layers.inet import IP, UDP
from scapy.layers.inet6 import IPv6

from ipsize.probe.io.udp_dns import UDPDNSProbe, get_text_record_length_bytes
from .strategies import probe_arguments

PORT = 10053
DOMAIN_REGEX = re.compile(r"txt([0-9]{4})\.ipsize\.de\.")


@given(probe_arguments(UDPDNSProbe, port=PORT))
@pytest.mark.anyio
async def test_assemble_request(args):
    series_id, server_id, target, size = args
    ip_len = 20 if target.address.version == 4 else 40
    assume((size - ip_len - UDPDNSProbe.PREAMBLE) % 256 != 1)  # some sizes are impossible due to the TXT encoding

    probe = UDPDNSProbe(series_id, server_id, target, None, PORT, size)
    request = probe.assemble_request()

    assert ipaddress.ip_address(request.dst) == target.address
    assert UDP in request
    assert request[UDP].sport == PORT
    assert request[UDP].dport == target.port

    assert DNS in request
    dns = request[DNS]
    assert dns.id == probe.sequence_number
    assert dns.rd == 1
    assert dns.qdcount == 1  # packet has one query
    assert dns.ancount == 0  # packet has no answers
    assert dns.nscount == 0  # packet has no authority records
    assert dns.arcount == 1  # packet has one additional record

    query = dns.qd[0]
    qname_match = DOMAIN_REGEX.fullmatch(query.qname.decode())
    assert qname_match  # domain in query is correct
    txt_len = int(qname_match.group(1))
    assert 0 <= txt_len <= 1500  # text record length in domain is valid
    assert query.qtype == 16  # TXT query type
    assert query.qclass == 1  # IN query class

    opt = dns.ar[0]
    assert opt.type == 41  # OPT resource record
    assert opt.rclass == size  # UDP payload size

    assert ip_len + UDPDNSProbe.PREAMBLE + txt_len + get_text_record_length_bytes(txt_len) == size


@given(probe_arguments(UDPDNSProbe, port=PORT, ip_version=4))
@pytest.mark.anyio
async def test_match_response_ipv4(args):
    series_id, server_id, target, size = args
    probe = UDPDNSProbe(series_id, server_id, target, None, PORT, size)
    request = probe.assemble_request()

    # get DNS data from request
    query = request[DNS].qd[0]
    answer = DNSRR(rrname=query.qname, type="TXT", rdata="A")
    dns = DNS(id=probe.sequence_number, qd=query, an=answer, ar=request[DNS].ar[0])
    response = IP(src=str(target.address)) / UDP(dport=PORT) / dns.compress()

    # fake sending
    probe.request = request

    match = probe.match_response(response)
    assert match
    assert probe.t_recv is not None
    assert probe.response == response


@given(probe_arguments(UDPDNSProbe, port=PORT, ip_version=6))
@pytest.mark.anyio
async def test_match_response_ipv6(args):
    series_id, server_id, target, size = args
    probe = UDPDNSProbe(series_id, server_id, target, None, PORT, size)
    request = probe.assemble_request()

    # get DNS data from request
    query = request[DNS].qd[0]
    answer = DNSRR(rrname=query.qname, type="TXT", rdata="A")
    dns = DNS(id=probe.sequence_number, qd=query, an=answer, ar=request[DNS].ar[0])
    response = IPv6(src=str(target.address)) / UDP(dport=PORT) / dns.compress()

    # fake sending
    probe.request = request

    match = probe.match_response(response)
    assert match
    assert probe.t_recv is not None
    assert probe.response == response
