import fastapi
import pytest

from ipsize import models
from ipsize.models.enum import Status
from ipsize.rest import probe_paths


@pytest.mark.anyio
async def test_status(client_ps):
    response = await client_ps.get(probe_paths.STATUS)
    assert response.status_code == fastapi.status.HTTP_200_OK
    status = models.ProbeServerStatus(**response.json())
    assert status.status == Status.UNKNOWN  # lifecycle events are not executed during tests, so this will be UNKNOWN
