import anyio
import pytest
from hypothesis import HealthCheck, given, settings, strategies as st

from ipsize.probe.io.base_series import OutstandingDict
from .strategies import targets


@given(target_list=st.lists(targets(), min_size=1, unique_by=lambda t: t.address))
@settings(suppress_health_check=[HealthCheck.function_scoped_fixture])
@pytest.mark.anyio
async def test_outstanding_dict_unique(target_list, mock_probe_series):
    outstanding = OutstandingDict()
    # create list of target-probe-tuples
    tpl = [(t, mock_probe_series.create_next_probe(t)) for t in target_list]
    # add all probes and assert target is contained in the dict
    for target, probe in tpl:
        outstanding.add(probe)
        assert target.address in outstanding

    # remove probes and assert target is not contained anymore
    for target, probe in tpl:
        outstanding.remove(probe)
        assert target.address not in outstanding


@given(target_list=st.lists(
    st.tuples(targets(), st.integers(min_value=1, max_value=5)),  # create 1-5 probes per target
    min_size=1,
    unique_by=lambda t: t[0].address
))
@settings(suppress_health_check=[HealthCheck.function_scoped_fixture])
@pytest.mark.anyio
async def test_outstanding_dict_full(target_list, mock_probe_series):
    outstanding = OutstandingDict()
    # create list of target-probe-tuples with n probes per target
    tpl = [(t, [mock_probe_series.create_next_probe(t) for _ in range(n)]) for t, n in target_list]
    # add all probes and assert target is contained in the dict
    for target, probes in tpl:
        for probe in probes:
            outstanding.add(probe)
            assert target.address in outstanding

    # remove one at a time and assert the target is not contained anymore only after removing the last
    for target, probes in tpl:
        for i, probe in enumerate(probes):
            outstanding.remove(probe)
            if i < len(probes) - 1:
                assert target.address in outstanding
            else:
                assert target.address not in outstanding


@pytest.mark.anyio
@pytest.mark.timeout(10)
async def test_probe_series_lifecycle(mock_probe_series):
    run_for = 1  # run for one second, one probe every 0.1 seconds, should result in 10 probes sent
    async with anyio.create_task_group() as tg:
        mock_probe_series.start()
        assert mock_probe_series.can_run

        await tg.start(mock_probe_series.run)
        assert mock_probe_series.running

        await anyio.sleep(run_for)

        await mock_probe_series.stop()
        assert not mock_probe_series.can_run
        assert not mock_probe_series.running

    assert mock_probe_series.t_start > 0
    assert mock_probe_series.t_stop > 0
    assert mock_probe_series.t_stop - mock_probe_series.t_start >= run_for
    assert mock_probe_series.sent == 10
    assert mock_probe_series.received == 0
