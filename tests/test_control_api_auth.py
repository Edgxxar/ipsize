from typing import Any

import pytest
from fastapi import status
from jose import jwt

from ipsize import models, security
from ipsize.control.config import settings
from ipsize.rest import control_paths


@pytest.fixture(params=["basic", "form"])
def server_credentials(request, probe_server_entity) -> tuple[dict[str, Any], tuple[str, str]]:
    if request.param == "basic":
        data = {"grant_type": "client_credentials"}
        auth = (str(probe_server_entity.id), "server_secret")
    else:
        data = {
            "grant_type": "client_credentials",
            "client_id": probe_server_entity.id,
            "client_secret": "server_secret"
        }
        auth = None
    return data, auth


@pytest.mark.anyio
async def test_auth(client_cs, server_credentials, probe_server_entity):
    """
    client credentials authentication should return valid server token
    """
    data, auth = server_credentials
    response = await client_cs.post(
        control_paths.AUTH,
        data=data,
        auth=auth
    )
    assert response.status_code == status.HTTP_200_OK
    token = models.AccessToken(**response.json())
    assert token.token_type == "bearer"
    payload = jwt.decode(
        token.access_token, settings.SECRET_KEY, algorithms=[security.ALGORITHM]
    )
    token_data = models.TokenPayload(**payload)
    assert token_data.is_server_subject()
    assert token_data.get_id() == probe_server_entity.id


@pytest.mark.anyio
async def test_auth_missing_grant_type(client_cs, server_credentials):
    """
    client credentials authentication with missing grant type should result in an error 422
    """
    data, auth = server_credentials
    del data["grant_type"]
    response = await client_cs.post(
        control_paths.AUTH,
        data=data,
        auth=auth
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.anyio
async def test_auth_invalid_grant_type(client_cs, server_credentials):
    """
    client credentials authentication with invalid grant type should result in an error 422
    """
    data, auth = server_credentials
    data["grant_type"] = "password"
    response = await client_cs.post(
        control_paths.AUTH,
        data=data,
        auth=auth
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.anyio
async def test_auth_basic_invalid_secret(client_cs, probe_server_entity):
    """
    client credentials authentication using basic auth with invalid client secret should result in an error 401
    """
    response = await client_cs.post(
        control_paths.AUTH,
        data={"grant_type": "client_credentials"},
        auth=(str(probe_server_entity.id), "wrong_secret")
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_auth_form_invalid_secret(client_cs, probe_server_entity):
    """
    client credentials authentication using form data with invalid client secret should result in an error 401
    """
    response = await client_cs.post(
        control_paths.AUTH,
        data={
            "grant_type": "client_credentials",
            "client_id": probe_server_entity.id,
            "client_secret": "wrong_secret"
        }
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_auth_basic_invalid_id(client_cs, probe_server_entity):
    """
    client credentials authentication using basic auth with invalid client id should result in an error 401
    """
    response = await client_cs.post(
        control_paths.AUTH,
        data={"grant_type": "client_credentials"},
        auth=("foo", "server_secret")
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_auth_form_invalid_id(client_cs, probe_server_entity):
    """
    client credentials authentication using form data with invalid client id should result in an error 401
    """
    response = await client_cs.post(
        control_paths.AUTH,
        data={
            "grant_type": "client_credentials",
            "client_id": "foo",
            "client_secret": "server_secret"
        }
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_auth_basic_unknown_id(client_cs, probe_server_entity):
    """
    client credentials authentication using basic auth with invalid client id should result in an error 401
    """
    response = await client_cs.post(
        control_paths.AUTH,
        data={"grant_type": "client_credentials"},
        auth=("42", "server_secret")
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_auth_form_unknown_id(client_cs, probe_server_entity):
    """
    client credentials authentication using form data with invalid client id should result in an error 401
    """
    response = await client_cs.post(
        control_paths.AUTH,
        data={
            "grant_type": "client_credentials",
            "client_id": "42",
            "client_secret": "server_secret"
        }
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_test_token(client_cs_server, probe_server_entity):
    response = await client_cs_server.post(control_paths.TEST_TOKEN)
    assert response.status_code == status.HTTP_200_OK
    server = models.ServerModel(**response.json())
    assert server.id == probe_server_entity.id


@pytest.mark.anyio
async def test_test_token_unauthorized(client_cs):
    response = await client_cs.post(control_paths.TEST_TOKEN)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_test_token_as_user(client_cs_user):
    response = await client_cs_user.post(control_paths.TEST_TOKEN)
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.anyio
async def test_test_token_unknown_server(client_cs):
    token = security.create_access_token(
        models.TokenPayload.from_server_id(42),
        settings.ACCESS_TOKEN_EXPIRE_MINUTES,
        settings.SECRET_KEY
    )
    response = await client_cs.post(
        control_paths.TEST_TOKEN,
        headers={"Authorization": f"Bearer {token}"}
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.anyio
async def test_authentication_invalid_signing_key(client_cs, probe_server_entity):
    token = security.create_access_token(
        models.TokenPayload.from_server_id(probe_server_entity.id),
        settings.ACCESS_TOKEN_EXPIRE_MINUTES,
        "invalid_signing_key"
    )
    response = await client_cs.post(
        control_paths.TEST_TOKEN,
        headers={"Authorization": f"Bearer {token}"}
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_authentication_invalid_token_subject(client_cs, probe_server_entity):
    token = security.create_access_token(
        "invalid_token_subject",
        settings.ACCESS_TOKEN_EXPIRE_MINUTES,
        settings.SECRET_KEY
    )
    response = await client_cs.post(
        control_paths.TEST_TOKEN,
        headers={"Authorization": f"Bearer {token}"}
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_login(client_cs, admin_user_entity):
    """
    password authentication should return valid user token
    """
    response = await client_cs.post(control_paths.USER_LOGIN, data={
        "grant_type": "password",
        "username": admin_user_entity.email,
        "password": "admin_password"
    })
    assert response.status_code == status.HTTP_200_OK
    token = models.AccessToken(**response.json())
    assert token.token_type == "bearer"
    payload = jwt.decode(
        token.access_token, settings.SECRET_KEY, algorithms=[security.ALGORITHM]
    )
    token_data = models.TokenPayload(**payload)
    assert token_data.is_user_subject()
    assert token_data.get_id() == admin_user_entity.id


@pytest.mark.anyio
async def test_login_invalid_grant_type(client_cs, admin_user_entity):
    """
    password authentication with invalid grant type should result in an error 422
    """
    response = await client_cs.post(control_paths.USER_LOGIN, data={
        "grant_type": "client_credentials",
        "username": admin_user_entity.email,
        "password": "admin_password"
    })
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.anyio
async def test_login_invalid_password(client_cs, admin_user_entity):
    """
    password authentication with wrong password should result in an error 401
    """
    response = await client_cs.post(control_paths.USER_LOGIN, data={
        "grant_type": "password",
        "username": admin_user_entity.email,
        "password": "wrong_password"
    })
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_login_unknown_email(client_cs, admin_user_entity):
    """
    password authentication with wrong email should result in an error 401
    """
    response = await client_cs.post(control_paths.USER_LOGIN, data={
        "grant_type": "password",
        "username": "unknown@ipsize.de",
        "password": "admin_password"
    })
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_user_test_token(client_cs_user, admin_user_entity):
    response = await client_cs_user.post(control_paths.USER_TEST_TOKEN)
    assert response.status_code == status.HTTP_200_OK
    user = models.UserModel(**response.json())
    assert user.id == admin_user_entity.id


@pytest.mark.anyio
async def test_user_test_token_unauthorized(client_cs):
    response = await client_cs.post(control_paths.USER_TEST_TOKEN)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_user_test_token_as_server(client_cs_server):
    response = await client_cs_server.post(control_paths.USER_TEST_TOKEN)
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.anyio
async def test_user_test_token_unknown_user(client_cs):
    token = security.create_access_token(
        models.TokenPayload.from_user_id(42),
        settings.ACCESS_TOKEN_EXPIRE_MINUTES,
        settings.SECRET_KEY
    )
    response = await client_cs.post(
        control_paths.TEST_TOKEN,
        headers={"Authorization": f"Bearer {token}"}
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.anyio
async def test_register(client_cs_user):
    """
    user registration should be allowed for users
    """
    response = await client_cs_user.post(control_paths.USER_REGISTER, json={
        "email": "test@example.com",
        "password": "123456",
    })
    assert response.status_code == status.HTTP_200_OK
    user = models.UserModel(**response.json())
    assert user.email == "test@example.com"
    assert user.is_active


@pytest.mark.anyio
async def test_register_unauthorized(client_cs):
    """
    user registration should not be allowed without authentication
    """
    response = await client_cs.post(control_paths.USER_REGISTER, json={
        "email": "test@example.com",
        "password": "123456",
    })
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_register_as_server(client_cs_server):
    """
    user registration should not be allowed for probe servers
    """
    response = await client_cs_server.post(control_paths.USER_REGISTER, json={
        "email": "test@example.com",
        "password": "123456",
    })
    assert response.status_code == status.HTTP_403_FORBIDDEN
