import ipaddress

import pytest
from hypothesis import given, strategies as st
from scapy.layers.inet import UDP
from scapy.packet import Raw

from ipsize.probe.io.udp_private import UDPProbe, UDPProbeSeries
from .strategies import probe_arguments

PORT = 12345


@given(probe_arguments(UDPProbe, port=PORT))
@pytest.mark.anyio
async def test_assemble_request(args):
    """
    UDP probes should contain series id and target sequence number
    """
    series_id, server_id, target, size = args
    probe = UDPProbe(series_id, server_id, target, None, PORT, size)
    packet = probe.assemble_request()

    assert ipaddress.ip_address(packet.dst) == target.address
    assert UDP in packet
    assert packet[UDP].dport == target.port

    payload = bytes(packet[UDP].payload)
    assert int.from_bytes(payload[0:2], "big") == series_id
    assert int.from_bytes(payload[2:6], "big") == probe.sequence_number

    assert len(packet) == size


@given(probe_arguments(UDPProbe, port=PORT))
@pytest.mark.anyio
async def test_extract_identifiers(args):
    series_id, server_id, target, size = args
    probe = UDPProbe(series_id, server_id, target, None, PORT, size)
    packet = probe.assemble_request()

    series, sequence_number = UDPProbeSeries.extract_identifiers(packet)
    assert series == series_id
    assert sequence_number == probe.sequence_number


@given(probe_arguments(UDPProbe, port=PORT), st.integers(0, 5))
@pytest.mark.anyio
async def test_extract_identifiers_missing_payload(args, new_size):
    series_id, server_id, target, size = args
    probe = UDPProbe(series_id, server_id, target, None, PORT, size)
    packet = probe.assemble_request()
    # force payload that is too short
    packet[UDP].payload = Raw(bytes(new_size))

    series, sequence_number = UDPProbeSeries.extract_identifiers(packet)
    assert series == 0
    assert sequence_number == 0
