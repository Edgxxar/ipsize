import anyio
import pytest
from fastapi import status
from httpx import Response

from ipsize import models
from ipsize.control.download import decompress_brotli, read_probe_packets_sequence
from ipsize.models.enum import ProbeSeriesStatus
from ipsize.probe.config import settings
from ipsize.probe.io.base_series import is_download_available, get_downloads_available
from ipsize.probe.server import probe_server
from ipsize.rest import format_path, probe_paths


@pytest.mark.anyio
async def test_post_probe_series_targets_authenticated(mocker, client_ps_auth):
    update = models.ProbeSeriesTargetUpdate(updates={
        1: models.ProbeSeriesTargetChanges(
            added=[42],
            removed=[69],
            total=1337
        )
    })
    # patch the background job, we just want to test the API call here
    update_task = mocker.patch("ipsize.probe.api.probe.update_probe_series")
    response = await client_ps_auth.post(
        probe_paths.PROBE_SERIES_POST_TARGETS,
        json=update.model_dump()
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert update_task.call_count == 1


@pytest.mark.anyio
async def test_post_probe_series_targets_unauthorized(client_ps):
    update = models.ProbeSeriesTargetUpdate(updates={
        1: models.ProbeSeriesTargetChanges(
            added=[42],
            removed=[69],
            total=1337
        )
    })
    response = await client_ps.post(
        probe_paths.PROBE_SERIES_POST_TARGETS,
        json=update.model_dump()
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_get_probe_series_available_authenticated(client_ps_auth):
    response = await client_ps_auth.get(probe_paths.PROBE_SERIES_GET_AVAILABLE)
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.anyio
async def test_get_probe_series_available_unauthorized(client_ps):
    response = await client_ps.get(probe_paths.PROBE_SERIES_GET_AVAILABLE)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_post_probe_series_pause(client_ps_auth, mock_probe_series):
    response = await client_ps_auth.post(probe_paths.PROBE_SERIES_PAUSE)
    assert response.status_code == status.HTTP_200_OK
    assert mock_probe_series.status == ProbeSeriesStatus.PAUSED


@pytest.mark.anyio
async def test_post_probe_series_pause_unauthorized(client_ps, mock_probe_series):
    response = await client_ps.post(probe_paths.PROBE_SERIES_PAUSE)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert mock_probe_series.status == ProbeSeriesStatus.ONGOING


@pytest.mark.anyio
async def test_post_probe_series_resume(client_ps_auth, mock_probe_series):
    await mock_probe_series.pause()
    response = await client_ps_auth.post(probe_paths.PROBE_SERIES_RESUME)
    assert response.status_code == status.HTTP_200_OK
    assert mock_probe_series.status == ProbeSeriesStatus.ONGOING


@pytest.mark.anyio
async def test_post_probe_series_resume_unauthorized(client_ps, mock_probe_series):
    await mock_probe_series.pause()
    response = await client_ps.post(probe_paths.PROBE_SERIES_RESUME)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert mock_probe_series.status == ProbeSeriesStatus.PAUSED


@pytest.mark.anyio
@pytest.mark.timeout(10)
async def test_download_probe_series_data_authenticated(redis, client_ps_auth, mock_probe_series):
    # create save file by running probe series
    async with anyio.create_task_group() as tg:
        mock_probe_series.start()
        await tg.start(mock_probe_series.run)
        await anyio.sleep(0.5)
        await mock_probe_series.stop()

    downloads = 0
    async for download in get_downloads_available(redis):
        downloads += 1
        assert download.packet_count > 0
        packets = 0
        async with client_ps_auth.stream(
                "GET", format_path(probe_paths.PROBE_SERIES_DOWNLOAD, file_name=download.file_name)
        ) as stream:  # type: Response
            assert stream.status_code == status.HTTP_200_OK
            # download them all
            async for _ in read_probe_packets_sequence(decompress_brotli(stream.aiter_bytes())):
                packets += 1
        assert packets == download.packet_count
    assert downloads == 1


@pytest.mark.anyio
async def test_download_probe_series_data_not_found(client_ps_auth, mock_probe_series):
    del probe_server.series[mock_probe_series.id]
    async with client_ps_auth.stream(
            "GET", format_path(probe_paths.PROBE_SERIES_DOWNLOAD, file_name="unknown.json")
    ) as stream:
        assert stream.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.anyio
async def test_download_probe_series_data_unauthorized(client_ps, mock_probe_series):
    async with client_ps.stream(
            "GET", format_path(probe_paths.PROBE_SERIES_DOWNLOAD, file_name="unknown.json")
    ) as stream:
        assert stream.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
@pytest.mark.timeout(10)
async def test_delete_probe_series_data_authenticated(redis, client_ps_auth, mock_probe_series):
    # create save file by running probe series
    async with anyio.create_task_group() as tg:
        mock_probe_series.start()
        await tg.start(mock_probe_series.run)
        await anyio.sleep(0.5)
        await mock_probe_series.stop()

    downloads = 0
    async for download in get_downloads_available(redis):
        downloads += 1
        assert download.packet_count > 0
        await client_ps_auth.delete(format_path(probe_paths.PROBE_SERIES_DOWNLOAD, file_name=download.file_name))
        assert not await is_download_available(redis, str(download.file_name))
        path = anyio.Path(settings.SAVE_DIR, download.file_name)
        assert not await path.exists()
    assert downloads == 1


@pytest.mark.anyio
async def test_delete_probe_series_data_unauthorized(client_ps):
    response = await client_ps.delete(format_path(probe_paths.PROBE_SERIES_DOWNLOAD, file_name="unknown.json"))
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_get_probe_series_state_authenticated(client_ps_auth, mock_probe_series):
    response = await client_ps_auth.get(
        format_path(probe_paths.PROBE_SERIES_GET_STATE, series_id=mock_probe_series.id)
    )
    assert response.status_code == status.HTTP_200_OK
    state = models.ProbeSeriesLocalState(**response.json())
    assert state.status == mock_probe_series.status
    assert state.targets == len(mock_probe_series.targets)
    assert state.sent == 0
    assert state.received == 0


@pytest.mark.anyio
async def test_get_probe_series_state_not_found(client_ps_auth, mock_probe_series):
    del probe_server.series[mock_probe_series.id]
    response = await client_ps_auth.get(
        format_path(probe_paths.PROBE_SERIES_GET_STATE, series_id=mock_probe_series.id)
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.anyio
async def test_get_probe_series_state_unauthorized(client_ps, mock_probe_series):
    response = await client_ps.get(
        format_path(probe_paths.PROBE_SERIES_GET_STATE, series_id=mock_probe_series.id)
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_put_probe_series_status_authenticated(client_ps_auth, mock_probe_series):
    response = await client_ps_auth.put(
        format_path(probe_paths.PROBE_SERIES_PUT_STATUS, series_id=mock_probe_series.id),
        json=ProbeSeriesStatus.PAUSED
    )
    assert response.status_code == status.HTTP_200_OK
    # probe series is registered at probe server, object should be changed now
    assert mock_probe_series.status == ProbeSeriesStatus.PAUSED


@pytest.mark.anyio
async def test_put_probe_series_status_not_found(client_ps_auth, mock_probe_series):
    del probe_server.series[mock_probe_series.id]
    response = await client_ps_auth.put(
        format_path(probe_paths.PROBE_SERIES_PUT_STATUS, series_id=mock_probe_series.id),
        json=ProbeSeriesStatus.PAUSED
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.anyio
async def test_put_probe_series_status_unauthorized(client_ps, mock_probe_series):
    response = await client_ps.put(
        format_path(probe_paths.PROBE_SERIES_PUT_STATUS, series_id=mock_probe_series.id),
        json=ProbeSeriesStatus.PAUSED
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
