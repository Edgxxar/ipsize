from typing import Any

import pytest
from fastapi import status
from jose import jwt

from ipsize import models, security
from ipsize.probe.config import settings
from ipsize.rest import probe_paths


@pytest.fixture(params=["basic", "form"])
def server_credentials(request) -> tuple[dict[str, Any], tuple[str, str]]:
    if request.param == "basic":
        data = {"grant_type": "client_credentials"}
        auth = ("1", "control_secret")
    else:
        data = {
            "grant_type": "client_credentials",
            "client_id": "1",
            "client_secret": "control_secret"
        }
        auth = None
    return data, auth


@pytest.mark.anyio
async def test_auth(mocker, client_ps, server_credentials):
    # set cached control secret, such that the control server is not queried in this test
    mocker.patch("ipsize.probe.rest.hashed_control_secret", security.hash_password("control_secret"))
    data, auth = server_credentials
    response = await client_ps.post(
        probe_paths.AUTH,
        data=data,
        auth=auth
    )
    assert response.status_code == status.HTTP_200_OK
    token = models.AccessToken(**response.json())
    assert token.token_type == "bearer"
    payload = jwt.decode(
        token.access_token, settings.SECRET_KEY, algorithms=[security.ALGORITHM]
    )
    assert payload is not None


@pytest.mark.anyio
async def test_auth_missing_grant_type(client_ps, server_credentials):
    data, auth = server_credentials
    del data["grant_type"]
    response = await client_ps.post(
        probe_paths.AUTH,
        data=data,
        auth=auth
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.anyio
async def test_auth_invalid_grant_type(client_ps, server_credentials):
    data, auth = server_credentials
    data["grant_type"] = "password"
    response = await client_ps.post(
        probe_paths.AUTH,
        data=data,
        auth=auth
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.anyio
async def test_auth_basic_invalid_secret(mocker, client_ps):
    # set cached control secret, such that the control server is not queried in this test
    mocker.patch("ipsize.probe.rest.hashed_control_secret", security.hash_password("control_secret"))
    response = await client_ps.post(
        probe_paths.AUTH,
        data={"grant_type": "client_credentials"},
        auth=("1", "wrong_secret")
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_auth_form_invalid_secret(mocker, client_ps):
    # set cached control secret, such that the control server is not queried in this test
    mocker.patch("ipsize.probe.rest.hashed_control_secret", security.hash_password("control_secret"))
    response = await client_ps.post(
        probe_paths.AUTH,
        data={
            "grant_type": "client_credentials",
            "client_id": "1",
            "client_secret": "wrong_secret"
        }
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_test_token(client_ps_auth):
    response = await client_ps_auth.post(probe_paths.TEST_TOKEN)
    assert response.status_code == status.HTTP_200_OK
    token = response.json()
    assert token is not None
    assert isinstance(token, dict)


@pytest.mark.anyio
async def test_test_token_unauthorized(client_ps):
    response = await client_ps.post(probe_paths.TEST_TOKEN)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_authentication_invalid_signing_key(client_ps):
    token = security.create_access_token(
        "You're a Wizard, Harry!",
        settings.ACCESS_TOKEN_EXPIRE_MINUTES,
        "invalid_signing_key"
    )
    response = await client_ps.post(
        probe_paths.TEST_TOKEN,
        headers={"Authorization": f"Bearer {token}"}
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
