import random
import string
from contextlib import asynccontextmanager
from datetime import datetime
from typing import AsyncIterator

import orjson
import pbjson
import psycopg
import pytest as pytest

from ipsize import models
from ipsize.control.download import ProbeDownloadImporter, read_probe_packets_binary, read_probe_packets_sequence
from ipsize.control.persistence import crud, entities
from ipsize.models.enum import ProbeSeriesType
from ipsize.util import default_encode


@pytest.fixture()
def probe_packet_data():
    """
    Create random packet data
    """
    count = random.randint(0, 100)
    return [{
        "sequence_number": random.randint(0, 2 ** 16 - 1),
        "server_id": random.randint(0, 2 ** 15 - 1),
        "target_service_id": random.randint(0, 2 ** 31 - 1),
        "probe_time": datetime.now().astimezone().isoformat(),
        "send_time": datetime.now().astimezone().isoformat(),
        "recv_time": datetime.now().astimezone().isoformat(),
        "request_received": bool(random.randint(0, 1)),
        "latency": random.randint(0, 2 ** 31 - 1),
        "delta": random.randint(0, 2 ** 31 - 1),
        "size": random.randint(0, 1500),
        "data": random.randbytes(random.randint(0, 1500)),
        "anomaly": "".join(random.choice(string.ascii_letters) for _ in range(random.randint(0, 100)))
    } for _ in range(count)]


def split_into_chunks(data: bytes, min_length: int = 100, max_length: int = 1000) -> list[bytes]:
    """
    Split bytes into random chunks of different length
    """
    total = len(data)
    split = []
    i = 0
    while i < len(data):
        part = random.randint(min(min_length, total - i), min(max_length, total - i))
        split.append(data[i:i + part])
        i += part
    return split


@pytest.fixture()
def probe_packet_data_binary(probe_packet_data) -> (list[bytes], int):
    """
    Create random, packed binary JSON encoded, packet data and split it into random chunks.
    """
    data = bytearray()
    for packet in probe_packet_data:
        encoded = pbjson.dumps(packet, check_circular=False)
        data.extend(len(encoded).to_bytes(2, "big", signed=False))
        data.extend(encoded)
    return split_into_chunks(data), len(probe_packet_data)


@pytest.fixture()
def probe_packet_data_sequence(probe_packet_data) -> (list[bytes], int):
    """
    Create random, JSON text sequence encoded, packet data and split it into random chunks.
    """
    data = bytearray()
    for packet in probe_packet_data:
        encoded = orjson.dumps(
            packet,
            default=default_encode,
            option=orjson.OPT_APPEND_NEWLINE
        )
        data.append(0x1E)
        data.extend(encoded)
    return split_into_chunks(data), len(probe_packet_data)


async def stream_data(data: list[bytes], extra_data: bytes = None) -> AsyncIterator[bytes]:
    for d in data:
        yield d
    if extra_data:
        yield extra_data


@pytest.fixture
async def probe_series_entity_public(db) -> entities.ProbeSeries:
    yield await crud.probe_series.create(db, models.ProbeSeriesCreate(
        type=ProbeSeriesType.PUBLIC_ICMP_PING,
    ))


@pytest.fixture()
async def probe_download_public(db, probe_series_entity_public, probe_server_entity) -> entities.ProbeDownload:
    yield await crud.probe_download.create(db, models.ProbeDownloadCreate(
        probe_series_id=probe_series_entity_public.id,
        server_id=probe_server_entity.id,
        packet_count=42,
        file_name="download.json",
        file_size=1337,
        downloaded_at=datetime.now()
    ))


@pytest.fixture
async def probe_series_entity_private(db) -> entities.ProbeSeries:
    yield await crud.probe_series.create(db, models.ProbeSeriesCreate(
        type=ProbeSeriesType.PRIVATE_UDP,
        config=models.ProbeSeriesConfig(
            port=12345
        )
    ))


@pytest.fixture()
async def probe_download_private(db, probe_series_entity_private, probe_server_entity) -> entities.ProbeDownload:
    yield await crud.probe_download.create(db, models.ProbeDownloadCreate(
        probe_series_id=probe_series_entity_private.id,
        server_id=probe_server_entity.id,
        packet_count=42,
        file_name="download.json",
        file_size=1337,
        downloaded_at=datetime.now()
    ))


@pytest.mark.anyio
async def test_read_probe_packets_binary(probe_packet_data_binary):
    packets, count = probe_packet_data_binary
    downloaded = 0
    async for _ in read_probe_packets_binary(stream_data(packets)):
        downloaded += 1
    assert downloaded == count


@pytest.mark.anyio
async def test_read_probe_packets_sequence(probe_packet_data_sequence):
    packets, count = probe_packet_data_sequence
    downloaded = 0
    async for _ in read_probe_packets_sequence(stream_data(packets)):
        downloaded += 1
    assert downloaded == count


@pytest.mark.anyio
async def test_import_missing_file(db_maker, probe_download_public):
    importer = ProbeDownloadImporter(db_maker, probe_download_public)

    await importer.import_downloaded_packets()

    # check metadata was set correctly
    async with db_maker() as db:
        db_download = await crud.probe_download.get(db, probe_download_public.id)
    assert db_download.imported_at is None
    assert db_download.import_time is None
    assert db_download.error is True


@asynccontextmanager
async def psycopg_connect(postgresql_proc) -> psycopg.AsyncConnection:
    """
    Provides a database connection using the psycopg driver directly, which rolls back any changes after test execution.
    """
    async with await psycopg.AsyncConnection.connect(
            host=postgresql_proc.host,
            port=postgresql_proc.port,
            user=postgresql_proc.user,
            password=postgresql_proc.password,
            dbname=postgresql_proc.dbname
    ) as conn:
        yield conn
        await conn.rollback()


@pytest.mark.anyio
async def test_import_public(
        mocker, postgresql_proc, db_maker,
        probe_download_public, probe_packet_data_sequence
):
    importer = ProbeDownloadImporter(db_maker, probe_download_public)
    packets, count = probe_packet_data_sequence

    # mock file access
    mock_is_file = mocker.patch("anyio.Path.is_file")
    mock_is_file.return_value = True
    mock_open_file = mocker.patch("ipsize.control.download.ProbeDownloadImporter.open_file")
    mock_open_file.return_value.__aenter__.return_value = stream_data(packets)

    # execute import with mocked connection
    async with psycopg_connect(postgresql_proc) as conn:
        mock_connect = mocker.patch("ipsize.control.persistence.database.psycopg_connect")
        mock_connect.return_value.__aenter__.return_value = conn

        await importer.import_downloaded_packets()

        # check data was inserted completely
        async with conn.cursor() as cur:
            await cur.execute(
                "SELECT COUNT(*) FROM probe_packets_public WHERE probe_series_id = %s",
                (probe_download_public.probe_series_id,)
            )
            db_count = (await cur.fetchone())[0]
    assert db_count == count

    # check metadata was set correctly
    async with db_maker() as db:
        db_download = await crud.probe_download.get(db, probe_download_public.id)
    assert db_download.imported_at is not None
    assert db_download.import_time is not None
    assert db_download.packet_count == count
    assert db_download.error is False


@pytest.mark.anyio
async def test_import_private(
        mocker, postgresql_proc, db_maker,
        probe_download_private, probe_packet_data_sequence
):
    importer = ProbeDownloadImporter(db_maker, probe_download_private)
    packets, count = probe_packet_data_sequence

    # mock file access
    mock_is_file = mocker.patch("anyio.Path.is_file")
    mock_is_file.return_value = True
    mock_open_file = mocker.patch("ipsize.control.download.ProbeDownloadImporter.open_file")
    mock_open_file.return_value.__aenter__.return_value = stream_data(packets)

    # execute import with mocked connections
    async with psycopg_connect(postgresql_proc) as conn1:
        async with psycopg_connect(postgresql_proc) as conn2:
            mock_connect = mocker.patch("ipsize.control.persistence.database.psycopg_connect")
            mock_connect.return_value.__aenter__.side_effect = [conn1, conn2]  # return two different connections

            await importer.import_downloaded_packets()

            # check data was inserted completely
            async with conn1.cursor() as cur:
                await cur.execute(
                    "SELECT COUNT(*) FROM probe_packets_sent WHERE probe_series_id = %s",
                    (probe_download_private.probe_series_id,)
                )
                count_sent = (await cur.fetchone())[0]
            async with conn2.cursor() as cur:
                await cur.execute(
                    "SELECT COUNT(*) FROM probe_packets_received WHERE probe_series_id = %s",
                    (probe_download_private.probe_series_id,)
                )
                count_recv = (await cur.fetchone())[0]
    assert count_sent + count_recv == count

    # check metadata was set correctly
    async with db_maker() as db:
        db_download = await crud.probe_download.get(db, probe_download_private.id)
    assert db_download.imported_at is not None
    assert db_download.import_time is not None
    assert db_download.packet_count == count
    assert db_download.error is False
