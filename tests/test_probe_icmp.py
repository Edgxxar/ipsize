import ipaddress

import pytest
from hypothesis import assume, given, strategies as st
from scapy.layers.inet import ICMP, IP
from scapy.layers.inet6 import ICMPv6EchoReply, ICMPv6EchoRequest, IPv6

from ipsize.probe.io.icmp import ICMPProbe
from .strategies import probe_arguments, sequences, shorts


@given(probe_arguments(ICMPProbe, ip_version=4))
@pytest.mark.anyio
async def test_assemble_request_ipv4(args):
    """
    ICMP echo request packets should contain series id and target sequence number
    """
    series_id, server_id, target, size = args
    probe = ICMPProbe(series_id, server_id, target, None, None, size)
    request = probe.assemble_request()

    assert ipaddress.ip_address(request.dst) == target.address

    assert ICMP in request
    assert request[ICMP].type == 8
    assert request[ICMP].code == 0
    assert request[ICMP].id == series_id
    assert request[ICMP].seq == probe.sequence_number

    assert len(request) == size


@given(probe_arguments(ICMPProbe, ip_version=6))
@pytest.mark.anyio
async def test_assemble_request_ipv6(args):
    """
    ICMP echo request packets should contain series id and target sequence number
    """
    series_id, server_id, target, size = args
    probe = ICMPProbe(series_id, server_id, target, None, None, size)
    request = probe.assemble_request()

    assert ipaddress.ip_address(request.dst) == target.address

    assert ICMPv6EchoRequest in request
    assert request[ICMPv6EchoRequest].type == 128
    assert request[ICMPv6EchoRequest].code == 0
    assert request[ICMPv6EchoRequest].id == series_id
    assert request[ICMPv6EchoRequest].seq == probe.sequence_number

    assert len(request) == size


@given(probe_arguments(ICMPProbe, ip_version=4))
@pytest.mark.anyio
async def test_match_response_ipv4(args):
    """
    ICMP probes match on id and sequence number. Also, test that state values are set correctly by the match.
    """
    series_id, server_id, target, size = args
    probe = ICMPProbe(series_id, server_id, target, None, None, size)
    request = probe.assemble_request()

    # get ICMP data from request
    payload = request[ICMP].payload
    response = IP(src=str(target.address)) / ICMP(type=0, id=series_id, seq=probe.sequence_number) / payload

    # fake sending
    probe.request = request

    match = probe.match_response(response)
    assert match
    assert probe.t_recv is not None
    assert probe.response == response
    assert probe.anomaly is None


@given(probe_arguments(ICMPProbe, ip_version=6))
@pytest.mark.anyio
async def test_match_response_ipv6(args):
    """
    ICMP probes match on id and sequence number. Also, test that state values are set correctly by the match.
    """
    series_id, server_id, target, size = args
    probe = ICMPProbe(series_id, server_id, target, None, None, size)
    request = probe.assemble_request()
    # get ICMP data from request
    payload = request[ICMPv6EchoRequest].data
    response = IPv6(src=str(target.address)) / ICMPv6EchoReply(id=series_id, seq=probe.sequence_number, data=payload)

    # fake sending
    probe.request = request

    match = probe.match_response(response)
    assert match
    assert probe.t_recv is not None
    assert probe.response == response
    assert probe.anomaly is None


@given(probe_arguments(ICMPProbe, ip_version=4), st.data())
@pytest.mark.anyio
async def test_match_response_anomaly_v4(args, data):
    """
    ICMP probes match on id and sequence number, a different payload should be marked. Also, test that
    state values are set correctly by the match.
    """
    series_id, server_id, target, size = args
    probe = ICMPProbe(series_id, server_id, target, None, None, size)
    request = probe.assemble_request()
    # get ICMP data from request, but generate a different payload for response
    payload = request[ICMP].payload.load
    mismatched = data.draw(st.binary(min_size=len(payload), max_size=len(payload)))
    # make sure they are different
    assume(payload != mismatched)
    response = IP(src=str(target.address)) / ICMP(type=0, id=series_id, seq=probe.sequence_number) / mismatched

    # fake sending
    probe.request = request

    match = probe.match_response(response)
    assert match
    assert probe.t_recv is not None
    assert probe.response == response
    assert probe.anomaly == "not matching"


@given(probe_arguments(ICMPProbe, ip_version=6), st.data())
@pytest.mark.anyio
async def test_match_response_anomaly_v6(args, data):
    """
    ICMP probes match on id and sequence number, a different payload should be marked. Also, test that
    state values are set correctly by the match.
    """
    series_id, server_id, target, size = args
    probe = ICMPProbe(series_id, server_id, target, None, None, size)
    request = probe.assemble_request()
    # get ICMP data from request, but generate a different payload for response
    payload = request[ICMPv6EchoRequest].data
    mismatched = data.draw(st.binary(min_size=len(payload), max_size=len(payload)))
    # make sure they are different
    assume(payload != mismatched)
    response = IPv6(src=str(target.address)) / ICMPv6EchoReply(id=series_id, seq=probe.sequence_number, data=mismatched)

    # fake sending
    probe.request = request

    match = probe.match_response(response)
    assert match
    assert probe.t_recv is not None
    assert probe.response == response
    assert probe.anomaly == "not matching"


@given(probe_arguments(ICMPProbe, ip_version=4), shorts)
@pytest.mark.anyio
async def test_match_response_wrong_id_ipv4(args, id):
    """
    ICMP probes match on id and sequence number. Also, test that state values are left unchanged.
    """
    series_id, server_id, target, size = args
    # make sure we get a different series id
    assume(series_id != id)
    probe = ICMPProbe(series_id, server_id, target, None, None, size)
    request = probe.assemble_request()
    # get ICMP data from request
    payload = request[ICMP].payload
    response = IP(src=str(target.address)) / ICMP(type=0, id=id, seq=probe.sequence_number) / payload

    # fake sending
    probe.request = request

    match = probe.match_response(response)
    assert not match
    assert probe.t_recv is None
    assert probe.response is None
    assert probe.anomaly is None


@given(probe_arguments(ICMPProbe, ip_version=6), shorts)
@pytest.mark.anyio
async def test_match_response_wrong_id_ipv6(args, id):
    """
    ICMP probes match on id and sequence number. Also, test that state values are left unchanged.
    """
    series_id, server_id, target, size = args
    # make sure we get a different series id
    assume(series_id != id)
    probe = ICMPProbe(series_id, server_id, target, None, None, size)
    request = probe.assemble_request()
    # get ICMP data from request
    payload = request[ICMPv6EchoRequest].data
    response = IPv6(src=str(target.address)) / ICMPv6EchoReply(id=id, seq=probe.sequence_number, data=payload)

    # fake sending
    probe.request = request

    match = probe.match_response(response)
    assert not match
    assert probe.t_recv is None
    assert probe.response is None
    assert probe.anomaly is None


@given(probe_arguments(ICMPProbe, ip_version=4), sequences)
@pytest.mark.anyio
async def test_match_response_wrong_sequence_ipv4(args, sequence):
    """
    ICMP probes match on id and sequence number. Also, test that state values are left unchanged.
    """
    series_id, server_id, target, size = args
    # make sure we get a different sequence
    assume(target.next_sequence_number != sequence)
    probe = ICMPProbe(series_id, server_id, target, None, None, size)
    request = probe.assemble_request()
    # get ICMP data from request
    payload = request[ICMP].payload
    response = IP(src=str(target.address)) / ICMP(type=0, id=series_id, seq=sequence) / payload

    # fake sending
    probe.request = request

    match = probe.match_response(response)
    assert not match
    assert probe.t_recv is None
    assert probe.response is None
    assert probe.anomaly is None


@given(probe_arguments(ICMPProbe, ip_version=6), sequences)
@pytest.mark.anyio
async def test_match_response_wrong_sequence_ipv6(args, sequence):
    """
    ICMP probes match on id and sequence number. Also, test that state values are left unchanged.
    """
    series_id, server_id, target, size = args
    # make sure we get a different sequence
    assume(target.next_sequence_number != sequence)
    probe = ICMPProbe(series_id, server_id, target, None, None, size)
    request = probe.assemble_request()
    # get ICMP data from request
    payload = request[ICMPv6EchoRequest].data
    response = IPv6(src=str(target.address)) / ICMPv6EchoReply(id=series_id, seq=sequence, data=payload)

    # fake sending
    probe.request = request

    match = probe.match_response(response)
    assert not match
    assert probe.t_recv is None
    assert probe.response is None
    assert probe.anomaly is None
