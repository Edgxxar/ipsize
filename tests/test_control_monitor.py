from datetime import datetime

import pytest
from hypothesis import assume, given, strategies as st
from pytest import approx

from ipsize import models
from ipsize.control.config import settings
from ipsize.control.monitor import MONITOR_HISTORY_FETCH, MONITOR_MAX_REPEAT_ERROR, MONITOR_MAX_REPEAT_OK, \
    TargetServiceContainer, \
    get_change_value, get_next_check_interval, get_status_sticky
from ipsize.control.persistence import entities
from ipsize.models.enum import Status

HISTORY_LEN = MONITOR_HISTORY_FETCH + 2


def create_history(data, initial: Status, length: int, consecutive: int) -> list[models.TargetServiceHistory]:
    history = []
    other = Status.ERROR if initial == Status.OK else Status.OK
    for i in range(0, length):
        history.append(models.TargetServiceHistory(
            target_service_id=1,
            timestamp=datetime.now(),
            status=initial if i < consecutive else other if i == consecutive else data.draw(st.sampled_from(Status)),
            flapping=False
        ))
    return history


@given(
    st.data(),
    st.integers(min_value=0, max_value=HISTORY_LEN),
    st.integers(min_value=0, max_value=HISTORY_LEN),
)
def test_get_next_check_interval_ok(data, length: int, consecutive: int):
    # make sure history length is valid
    assume(length >= consecutive)
    # create history with consecutive OK, then ERROR, then random
    history = create_history(data, Status.OK, length, consecutive)
    interval = get_next_check_interval(Status.OK, history, False)
    expected = min(settings.MONITOR_INTERVAL_STEP * (2 ** consecutive), settings.MONITOR_INTERVAL_MAX_OK)
    assert interval == expected


@given(
    st.data(),
    st.integers(min_value=0, max_value=HISTORY_LEN),
    st.integers(min_value=0, max_value=HISTORY_LEN),
)
def test_get_next_check_interval_error(data, length: int, consecutive: int):
    # make sure history length is valid
    assume(length >= consecutive)
    # create history with consecutive ERROR, then OK, then random
    history = create_history(data, Status.ERROR, length, consecutive)
    interval = get_next_check_interval(Status.ERROR, history, False)
    expected = min(settings.MONITOR_INTERVAL_STEP * (2 ** consecutive), settings.MONITOR_INTERVAL_MAX_ERROR)
    assert interval == expected


@given(
    st.data(),
    st.integers(min_value=0, max_value=HISTORY_LEN),
    st.integers(min_value=1, max_value=MONITOR_MAX_REPEAT_OK),
)
def test_get_next_check_interval_randomize_ok(data, length: int, consecutive: int):
    # make sure history length is valid
    assume(length >= consecutive)
    # create history with consecutive OK, then ERROR, then random
    history = create_history(data, Status.OK, length, consecutive)
    interval = get_next_check_interval(Status.OK, history, True)

    interval_min = settings.MONITOR_INTERVAL_STEP * (2 ** (consecutive - 1))
    interval_max = settings.MONITOR_INTERVAL_STEP * (2 ** consecutive)
    assert interval_min <= interval <= interval_max


@given(
    st.data(),
    st.integers(min_value=0, max_value=HISTORY_LEN),
    st.integers(min_value=1, max_value=MONITOR_MAX_REPEAT_ERROR),
)
def test_get_next_check_interval_randomize_error(data, length: int, consecutive: int):
    # make sure history length is valid
    assume(length >= consecutive)
    # create history with consecutive ERROR, then OK, then random
    history = create_history(data, Status.ERROR, length, consecutive)
    interval = get_next_check_interval(Status.ERROR, history, True)

    interval_min = settings.MONITOR_INTERVAL_STEP * (2 ** (consecutive - 1))
    interval_max = settings.MONITOR_INTERVAL_STEP * (2 ** consecutive)
    assert interval_min <= interval <= interval_max


@given(
    st.data(),
    st.integers(min_value=MONITOR_MAX_REPEAT_OK + 1, max_value=HISTORY_LEN),
    st.integers(min_value=MONITOR_MAX_REPEAT_OK + 1, max_value=HISTORY_LEN),
)
def test_get_next_check_interval_randomize_max_ok(data, length: int, consecutive: int):
    # make sure history length is valid
    assume(length >= consecutive)
    # create history with consecutive OK, then ERROR, then random
    history = create_history(data, Status.OK, length, consecutive)
    interval = get_next_check_interval(Status.OK, history, True)

    interval_min = settings.MONITOR_INTERVAL_STEP * (2 ** MONITOR_MAX_REPEAT_OK)
    interval_max = settings.MONITOR_INTERVAL_MAX_OK
    assert interval_min <= interval <= interval_max


@given(
    st.data(),
    st.integers(min_value=MONITOR_MAX_REPEAT_ERROR + 1, max_value=HISTORY_LEN),
    st.integers(min_value=MONITOR_MAX_REPEAT_ERROR + 1, max_value=HISTORY_LEN),
)
def test_get_next_check_interval_randomize_max_error(data, length: int, consecutive: int):
    # make sure history length is valid
    assume(length >= consecutive)
    # create history with consecutive ERROR, then OK, then random
    history = create_history(data, Status.ERROR, length, consecutive)
    interval = get_next_check_interval(Status.ERROR, history, True)

    interval_min = settings.MONITOR_INTERVAL_STEP * (2 ** MONITOR_MAX_REPEAT_ERROR)
    interval_max = settings.MONITOR_INTERVAL_MAX_ERROR
    assert interval_min <= interval <= interval_max


@given(
    st.data(),
    st.sampled_from(Status),
    st.integers(min_value=0, max_value=HISTORY_LEN),
)
def test_get_next_check_interval_randomize_zero(data, initial: Status, length: int):
    # create history that starts with other status, then random
    history = create_history(data, initial, length, 0)
    interval = get_next_check_interval(initial, history, True)
    assert interval == settings.MONITOR_INTERVAL_STEP


@given(
    st.data(),
    st.sampled_from(Status),
    st.integers(min_value=1, max_value=HISTORY_LEN),
    st.floats(min_value=0, max_value=2)
)
def test_get_change_value_boundaries(data, status, length, weight):
    history = []
    for i in range(0, length):
        history.append(models.TargetServiceHistory(
            target_service_id=1,
            timestamp=datetime.now(),
            status=data.draw(st.sampled_from(Status)),
            flapping=False
        ))
    change = get_change_value(status, history, weight)
    assert 0 <= change <= 1


@given(
    st.sampled_from(Status),
    st.integers(min_value=1, max_value=HISTORY_LEN),
    st.floats(min_value=0, max_value=2)
)
def test_get_change_value_min(status, length, weight):
    history = []
    for i in range(0, length):
        history.append(models.TargetServiceHistory(
            target_service_id=1,
            timestamp=datetime.now(),
            status=status,
            flapping=False
        ))
    change = get_change_value(status, history, weight)
    assert change == 0


@given(
    st.sampled_from(Status),
    st.integers(min_value=1, max_value=HISTORY_LEN),
    st.floats(min_value=0, max_value=2)
)
def test_get_change_value_max(initial, length, weight):
    history = []
    s = initial
    for i in range(0, length):
        s = Status.ERROR if s == Status.OK else Status.OK
        history.append(models.TargetServiceHistory(
            target_service_id=1,
            timestamp=datetime.now(),
            status=s,
            flapping=False
        ))
    change = get_change_value(initial, history, weight)
    assert change == 1


@given(
    st.data(),
    st.sampled_from(Status),
    st.integers(min_value=1, max_value=HISTORY_LEN),
)
def test_get_change_value_unweighted(data, status, length):
    history = []
    for i in range(0, length):
        history.append(models.TargetServiceHistory(
            target_service_id=1,
            timestamp=datetime.now(),
            status=data.draw(st.sampled_from(Status)),
            flapping=False
        ))
    changes = 0
    last = status
    for s in history:
        if s.status != last:
            changes += 1
        last = s.status
    expected = changes / length
    change = get_change_value(status, history, 0)
    assert expected == approx(change)


@given(
    st.sampled_from(Status),
    st.integers(min_value=1, max_value=HISTORY_LEN).filter(lambda x: x % 2 == 1),
    st.floats(min_value=0, max_value=2)
)
def test_get_change_value_middle(initial, length, weight):
    history = []
    other = Status.ERROR if initial == Status.OK else Status.OK
    for i in range(0, length):
        history.append(models.TargetServiceHistory(
            target_service_id=1,
            timestamp=datetime.now(),
            status=initial if i < length // 2 else other,
            flapping=False
        ))
    change = get_change_value(initial, history, weight)
    expected = 1 / length
    assert expected == approx(change)


@given(
    st.sampled_from(Status),
    st.integers(min_value=2, max_value=HISTORY_LEN).filter(lambda x: x % 2 == 0),
    st.floats(min_value=0, max_value=2).filter(lambda x: x > 0),
    st.integers(min_value=0, max_value=HISTORY_LEN)
)
def test_get_change_value_symmetry(initial, length, weight, switch):
    # force status change somewhere in the first half of the history
    assume(switch < length / 2)
    history = []
    other = Status.ERROR if initial == Status.OK else Status.OK
    for i in range(0, length):
        history.append(models.TargetServiceHistory(
            target_service_id=1,
            timestamp=datetime.now(),
            status=initial if i < switch else other,
            flapping=False
        ))
    change1 = get_change_value(initial, history, weight)
    # reverse the history and invert status after the switch to account for swapping of new_status
    history[switch].status = initial
    history.reverse()
    change2 = get_change_value(other, history, weight)
    # difference to the unweighted change value should be symmetrical
    unweighted = 1 / length
    assert approx(change1 - unweighted, abs=1e-8) == approx(unweighted - change2, abs=1e-8)


@given(
    st.data(),
    st.sampled_from(Status),
    st.sampled_from(Status),
    st.integers(min_value=1, max_value=HISTORY_LEN),
    st.integers(min_value=1, max_value=HISTORY_LEN)
)
def test_get_status_sticky(data, current_status, new_status, length, sticky):
    # only test cases with enough history
    assume(sticky <= length)
    other = Status.ERROR if new_status == Status.OK else Status.OK
    history = []
    for i in range(0, length):
        history.append(models.TargetServiceHistory(
            target_service_id=1,
            timestamp=datetime.now(),
            status=new_status if i < sticky else other if i == sticky else data.draw(st.sampled_from(Status)),
            flapping=False
        ))
    assert get_status_sticky(current_status, new_status, history, sticky) == new_status
    assert get_status_sticky(current_status, new_status, history, sticky + 1) == current_status


@given(
    st.data(),
    st.sampled_from(Status),
    st.sampled_from(Status),
    st.integers(min_value=0, max_value=HISTORY_LEN),
    st.integers(min_value=1, max_value=HISTORY_LEN)
)
def test_get_status_sticky_unknown(data, current_status, new_status, length, sticky):
    # test cases where we don't have enough history
    assume(sticky > length)
    history = []
    for i in range(0, length):
        history.append(models.TargetServiceHistory(
            target_service_id=1,
            timestamp=datetime.now(),
            status=data.draw(st.sampled_from(Status)),
            flapping=False
        ))
    assert get_status_sticky(current_status, new_status, history, sticky) == current_status


@given(
    st.lists(st.integers(), min_size=1, unique=True).map(
        lambda ids: [entities.TargetService(id=i) for i in ids]
    )
)
@pytest.mark.anyio
async def test_target_service_container(services):
    container = TargetServiceContainer()
    container.add_ready(services)

    # assert counts are correct
    assert len(container) == len(services)
    assert container.count_ready() == len(services)
    assert container.count_done() == 0

    # get one service, assert counts remain correct
    ready = await container.get_next_ready()
    assert len(container) == len(services)
    assert container.count_ready() == len(services) - 1
    assert container.count_done() == 0

    # mark done, check counts again
    container.set_done(ready, Status.OK)
    assert len(container) == len(services)
    assert container.count_ready() == len(services) - 1
    assert container.count_done() == 1

    # get done, check counts again
    done = container.get_all_done()
    assert len(done) == 1
    assert len(container) == len(services)
    assert container.count_ready() == len(services) - 1
    assert container.count_done() == 0

    # remove fully, check counts
    container.remove_all([s.id for s, _, _ in done])
    assert len(container) == len(services) - 1
    assert container.count_ready() == len(services) - 1
    assert container.count_done() == 0
