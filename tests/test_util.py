import math

from hypothesis import given, strategies as st

from ipsize.util import decode_integers, encode_integers


@given(st.lists(st.integers(min_value=1)))
def test_encode_integers(data: list[int]):
    data.sort()
    encoded = encode_integers(data)
    bits_per = encoded[0]
    length = math.ceil((len(data) * bits_per) / 8) + 1
    assert len(encoded) == length
    decoded = decode_integers(encoded)
    assert decoded == data
