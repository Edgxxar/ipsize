import ipaddress

import pytest
from fastapi import status

from ipsize import models, security
from ipsize.control.config import settings
from ipsize.control.persistence import crud
from ipsize.models.enum import ConnectionType
from ipsize.rest import control_paths, format_path


@pytest.fixture()
def server_json():
    return {
        "name": "probe042",
        "url": "https://probe042.local",
        "connection_type": ConnectionType.ETHERNET,
        "server_secret": "SuperServerSecret",
        "control_secret": "SuperControlSecret"
    }


@pytest.mark.anyio
async def test_create_server_as_user(client_cs_user, server_json):
    response = await client_cs_user.post(
        control_paths.SERVER_CREATE,
        json=server_json
    )
    assert response.status_code == status.HTTP_200_OK
    server = models.ServerModel(**response.json())
    assert server.name == server_json["name"]


@pytest.mark.anyio
async def test_create_server_as_server(client_cs_server, server_json):
    response = await client_cs_server.post(
        control_paths.SERVER_CREATE,
        json=server_json
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.anyio
async def test_create_server_duplicate_name(client_cs_user, probe_server_entity, server_json):
    server_json["name"] = probe_server_entity.name
    response = await client_cs_user.post(
        control_paths.SERVER_CREATE,
        json=server_json
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.anyio
async def test_create_server_unauthorized(client_cs, server_json):
    response = await client_cs.post(
        control_paths.SERVER_CREATE,
        json=server_json
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_post_server_address_as_server(db, client_cs_server, probe_server_entity):
    address = ipaddress.ip_address("10.0.0.2")
    response = await client_cs_server.post(
        format_path(control_paths.SERVER_POST_ADDRESS, server_id=probe_server_entity.id),
        json=str(address)
    )
    assert response.status_code == status.HTTP_201_CREATED
    db_address = await crud.server.get_address(db, address)
    assert db_address is not None
    assert db_address.address == address
    assert db_address.server.id == probe_server_entity.id


@pytest.mark.anyio
async def test_post_server_address_as_user(db, client_cs_user, probe_server_entity):
    address = ipaddress.ip_address("10.0.0.2")
    response = await client_cs_user.post(
        format_path(control_paths.SERVER_POST_ADDRESS, server_id=probe_server_entity.id),
        json=str(address)
    )
    assert response.status_code == status.HTTP_201_CREATED
    db_address = await crud.server.get_address(db, address)
    assert db_address is not None
    assert db_address.address == address
    assert db_address.server.id == probe_server_entity.id


@pytest.mark.anyio
async def test_post_server_address_unauthorized(client_cs, probe_server_entity):
    response = await client_cs.post(
        format_path(control_paths.SERVER_POST_ADDRESS, server_id=probe_server_entity.id),
        json="10.0.0.2"
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_post_server_address_wrong_server(db, client_cs_server, probe_server_entity, server_json):
    other_server = await crud.server.create(db, models.ServerCreate(**server_json))
    response = await client_cs_server.post(
        format_path(control_paths.SERVER_POST_ADDRESS, server_id=other_server.id),
        json="10.0.0.2"
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.anyio
async def test_delete_server_address_as_server(db, client_cs_server, probe_server_entity):
    address = ipaddress.ip_address("10.0.0.2")
    await crud.server.add_address(db, probe_server_entity.id, address)
    response = await client_cs_server.delete(
        format_path(control_paths.SERVER_DELETE_ADDRESS, server_id=probe_server_entity.id, address=address)
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT
    db_address = await crud.server.get_address(db, address)
    assert db_address is None


@pytest.mark.anyio
async def test_delete_server_address_as_user(db, client_cs_user, probe_server_entity):
    address = ipaddress.ip_address("10.0.0.2")
    await crud.server.add_address(db, probe_server_entity.id, address)
    response = await client_cs_user.delete(
        format_path(control_paths.SERVER_DELETE_ADDRESS, server_id=probe_server_entity.id, address=address)
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT
    db_address = await crud.server.get_address(db, address)
    assert db_address is None


@pytest.mark.anyio
async def test_delete_server_address_unauthorized(db, client_cs, probe_server_entity):
    response = await client_cs.delete(
        format_path(
            control_paths.SERVER_DELETE_ADDRESS,
            server_id=probe_server_entity.id,
            address=probe_server_entity.addresses[0].address
        )
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_delete_server_address_wrong_server(db, client_cs_server, probe_server_entity, server_json):
    other_server = await crud.server.create(db, models.ServerCreate(**server_json))
    response = await client_cs_server.delete(
        format_path(
            control_paths.SERVER_DELETE_ADDRESS,
            server_id=other_server.id,
            address=probe_server_entity.addresses[0].address
        )
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.anyio
async def test_delete_server_address_unknown(client_cs_server, probe_server_entity):
    address = ipaddress.ip_address("10.0.0.2")
    response = await client_cs_server.delete(
        format_path(control_paths.SERVER_DELETE_ADDRESS, server_id=probe_server_entity.id, address=address)
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.anyio
async def test_update_server_secret_as_user(client_cs_user, probe_server_entity, db):
    response = await client_cs_user.post(
        format_path(control_paths.SERVER_UPDATE_SERVER_SECRET, server_id=probe_server_entity.id),
        json="NewServerSecret"
    )
    assert response.status_code == status.HTTP_200_OK
    server = models.ServerModel(**response.json())
    assert server.last_update > probe_server_entity.last_update
    # check server secret was changed in the database
    db_server = await crud.server.get(db, probe_server_entity.id)
    assert security.verify_password("NewServerSecret", db_server.server_secret)


@pytest.mark.anyio
async def test_update_server_secret_as_server(client_cs_server, probe_server_entity):
    response = await client_cs_server.post(
        format_path(control_paths.SERVER_UPDATE_SERVER_SECRET, server_id=probe_server_entity.id),
        json="NewServerSecret"
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.anyio
async def test_update_server_secret_unauthorized(client_cs, probe_server_entity):
    response = await client_cs.post(
        format_path(control_paths.SERVER_UPDATE_SERVER_SECRET, server_id=probe_server_entity.id),
        json="NewServerSecret"
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_update_control_secret_as_user(client_cs_user, probe_server_entity, db):
    response = await client_cs_user.post(
        format_path(control_paths.SERVER_UPDATE_CONTROL_SECRET, server_id=probe_server_entity.id),
        json="NewControlSecret"
    )
    assert response.status_code == status.HTTP_200_OK
    server = models.ServerModel(**response.json())
    assert server.last_update > probe_server_entity.last_update
    # check control secret was changed in the database
    db_server = await crud.server.get(db, probe_server_entity.id)
    decrypted = security.decrypt_string(db_server.control_secret, settings.SECRET_KEY, db_server.control_secret_salt)
    assert decrypted == "NewControlSecret"


@pytest.mark.anyio
async def test_update_control_secret_as_server(client_cs_server, probe_server_entity):
    response = await client_cs_server.post(
        format_path(control_paths.SERVER_UPDATE_CONTROL_SECRET, server_id=probe_server_entity.id),
        json="NewControlSecret"
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.anyio
async def test_update_control_secret_unauthorized(client_cs, probe_server_entity):
    response = await client_cs.post(
        format_path(control_paths.SERVER_UPDATE_CONTROL_SECRET, server_id=probe_server_entity.id),
        json="NewControlSecret"
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_get_server_authenticated(client_cs_auth, probe_server_entity):
    response = await client_cs_auth.get(
        format_path(control_paths.SERVER_GET, server_id=probe_server_entity.id)
    )
    assert response.status_code == status.HTTP_200_OK
    server = models.ServerModel(**response.json())
    assert server.id == probe_server_entity.id
    assert len(server.addresses) == len(probe_server_entity.addresses)


@pytest.mark.anyio
async def test_get_server_unauthorized(client_cs, probe_server_entity):
    response = await client_cs.get(
        format_path(control_paths.SERVER_GET, server_id=probe_server_entity.id)
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_get_servers_authenticated(client_cs_auth, probe_server_entity):
    response = await client_cs_auth.get(control_paths.SERVER_GET_ALL)
    assert response.status_code == status.HTTP_200_OK
    servers = [models.ServerModel(**s) for s in response.json()]
    assert len(servers) == 1
    assert servers[0].id == probe_server_entity.id
    assert len(servers[0].addresses) == len(probe_server_entity.addresses)


@pytest.mark.anyio
async def test_get_servers_unauthorized(client_cs, probe_server_entity):
    response = await client_cs.get(control_paths.SERVER_GET_ALL)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_get_server_by_name_authenticated(client_cs_auth, probe_server_entity):
    response = await client_cs_auth.get(
        format_path(control_paths.SERVER_GET_BY_NAME, name=probe_server_entity.name)
    )
    assert response.status_code == status.HTTP_200_OK
    server = models.ServerModel(**response.json())
    assert server.id == probe_server_entity.id
    assert len(server.addresses) == len(probe_server_entity.addresses)


@pytest.mark.anyio
async def test_get_server_by_name_unauthorized(client_cs, probe_server_entity):
    response = await client_cs.get(
        format_path(control_paths.SERVER_GET_BY_NAME, name=probe_server_entity.name)
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_get_server_by_address_authenticated(client_cs_auth, probe_server_entity):
    response = await client_cs_auth.get(
        format_path(control_paths.SERVER_GET_BY_ADDRESS, address=probe_server_entity.addresses[0].address)
    )
    assert response.status_code == status.HTTP_200_OK
    server = models.ServerModel(**response.json())
    assert server.id == probe_server_entity.id
    assert len(server.addresses) == len(probe_server_entity.addresses)


@pytest.mark.anyio
async def test_get_server_by_address_unauthorized(client_cs, probe_server_entity):
    response = await client_cs.get(format_path(
        control_paths.SERVER_GET_BY_ADDRESS, address=probe_server_entity.addresses[0].address)
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_get_server_control_secret_as_server(client_cs_server, probe_server_entity):
    response = await client_cs_server.get(
        format_path(control_paths.SERVER_GET_CONTROL_SECRET, server_id=probe_server_entity.id)
    )
    assert response.status_code == status.HTTP_200_OK
    secret = response.json()
    assert security.verify_password("control_secret", secret)


@pytest.mark.anyio
async def test_get_server_control_secret_as_user(client_cs_user, probe_server_entity):
    response = await client_cs_user.get(
        format_path(control_paths.SERVER_GET_CONTROL_SECRET, server_id=probe_server_entity.id)
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.anyio
async def test_get_server_control_secret_unauthorized(client_cs, probe_server_entity):
    response = await client_cs.get(
        format_path(control_paths.SERVER_GET_CONTROL_SECRET, server_id=probe_server_entity.id)
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_get_server_control_secret_wrong_server(db, client_cs_server, probe_server_entity, server_json):
    other_server = await crud.server.create(db, models.ServerCreate(**server_json))
    response = await client_cs_server.get(
        format_path(control_paths.SERVER_GET_CONTROL_SECRET, server_id=other_server.id)
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN
