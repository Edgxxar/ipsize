from ipaddress import IPv4Address, IPv6Address
from typing import Literal, Optional, Type, Union

from hypothesis import strategies as st

from ipsize.probe.io.base_series import Probe
from ipsize.probe.io.target import Target

# positive two-byte signed integers
shorts = st.integers(0, 2 ** 15 - 1)
# positive two-byte unsigned integers
sequences = st.integers(0, 2 ** 16 - 1)


@st.composite
def targets(
        draw,
        *,
        port: Optional[int] = None,
        ip_version: Optional[Literal[4, 6]] = None
):
    return draw(st.ip_addresses(v=ip_version).map(lambda a: Target(1, a, port)))


@st.composite
def probe_arguments(
        draw,
        probe_class: Type[Probe],
        *,
        port: Optional[int] = None,
        ip_version: Optional[Literal[4, 6]] = None
):
    """
    Draws arguments to construct a valid probe, taking into account the size constraints of the probe type and target.
    :param draw: hypothesis data object
    :param probe_class: probe class to use for size constraint
    :param port: optional port to use for target
    :param ip_version: optional ip version for targets, leave unspecified to generate both IPv4 and IPv6 addresses
    :return: tuple of (probe series id, server id, target service, target state, packet size)
    """
    series = draw(shorts)
    server = draw(shorts)
    target = Target(draw(st.integers()), draw(st.ip_addresses(v=ip_version)), port, draw(sequences))
    # minimum size depends on generated target address header length, and probe type
    size = draw(st.integers(get_header_length(target.address) + probe_class.MIN_SIZE, probe_class.MAX_SIZE))
    return series, server, target, size


def get_header_length(address: Union[IPv4Address, IPv6Address]) -> int:
    if address.version == 4:
        return 20
    if address.version == 6:
        return 40
    raise ValueError("invalid address version")
