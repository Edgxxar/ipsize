import re

import pytest
from hypothesis import assume, given
from scapy.layers.dns import DNS, DNSRR
from scapy.layers.inet import IP, TCP
from scapy.layers.inet6 import IPv6

from ipsize.probe.io.monitor import TCPStateMachine
from ipsize.probe.io.tcp_dns import TCPDNSProbe
from ipsize.probe.io.udp_dns import get_text_record_length_bytes
from .strategies import probe_arguments

PORT = 10053


@given(probe_arguments(TCPDNSProbe, port=PORT))
@pytest.mark.anyio
async def test_assemble_payload(args):
    series_id, server_id, target, size = args
    ip_len = 20 if target.address.version == 4 else 40
    assume((size - ip_len - TCPDNSProbe.PREAMBLE) % 256 != 1)  # some sizes are impossible due to the TXT encoding

    tcp = TCPStateMachine(None, PORT, target.address, target.port, None, None, 10)
    tcp._tsopt = True  # force use of TCP timestamps
    probe = TCPDNSProbe(series_id, server_id, target, tcp, None, PORT, size)
    payload, length = probe.assemble_payload()

    assert DNS in payload
    dns = payload[DNS]
    assert dns.id == probe.sequence_number
    assert dns.rd == 1
    assert dns.qdcount == 1  # packet has one query
    assert dns.ancount == 0  # packet has no answers
    assert dns.nscount == 0  # packet has no authority records
    assert dns.arcount == 0  # packet has no additional records

    query = dns.qd[0]
    qname_regex = re.compile(r"txt([0-9]{4})\.ipsize\.de\.")
    qname_match = qname_regex.fullmatch(query.qname.decode())
    assert qname_match  # domain in query is correct
    txt_len = int(qname_match.group(1))
    assert 0 <= txt_len <= 1500  # text record length in domain is valid
    assert query.qtype == 16  # TXT query type
    assert query.qclass == 1  # IN query class

    assert ip_len + TCPDNSProbe.PREAMBLE + txt_len + get_text_record_length_bytes(txt_len) == size


@given(probe_arguments(TCPDNSProbe, port=PORT, ip_version=4))
@pytest.mark.anyio
async def test_match_response_ipv4(args):
    series_id, server_id, target, size = args
    tcp = TCPStateMachine(None, PORT, target.address, target.port, None, None, 10)
    probe = TCPDNSProbe(series_id, server_id, target, tcp, None, PORT, size)
    payload, length = probe.assemble_payload()

    # get DNS data from request
    query = payload.qd[0]
    answer = DNSRR(rrname=query.qname, type="TXT", rdata="A")
    dns = DNS(id=probe.sequence_number, qd=query, an=answer)
    response = IP(src=str(target.address)) / TCP(dport=PORT, flags="A") / dns.compress()

    # fake sending
    probe.request, probe.request_ack = tcp.send_segment(payload, length)

    match = probe.match_response(response)
    assert match
    assert probe.t_recv is not None
    assert probe.response == response


@given(probe_arguments(TCPDNSProbe, port=PORT, ip_version=6))
@pytest.mark.anyio
async def test_match_response_ipv6(args):
    series_id, server_id, target, size = args
    tcp = TCPStateMachine(None, PORT, target.address, target.port, None, None, 10)
    probe = TCPDNSProbe(series_id, server_id, target, tcp, None, PORT, size)
    payload, length = probe.assemble_payload()

    # get DNS data from request
    query = payload.qd[0]
    answer = DNSRR(rrname=query.qname, type="TXT", rdata="A")
    dns = DNS(id=probe.sequence_number, qd=query, an=answer)
    response = IPv6(src=str(target.address)) / TCP(dport=PORT, flags="A") / dns.compress()

    # fake sending
    probe.request, probe.request_ack = tcp.send_segment(payload, length)

    match = probe.match_response(response)
    assert match
    assert probe.t_recv is not None
    assert probe.response == response
