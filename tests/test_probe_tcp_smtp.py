import re

import pytest
from hypothesis import assume, given
from scapy.layers.inet import IP, TCP
from scapy.layers.inet6 import IPv6

from ipsize.probe.io.monitor import TCPStateMachine
from ipsize.probe.io.tcp_smtp import TCPSMTPProbe
from .strategies import probe_arguments

PORT = 10025


@given(probe_arguments(TCPSMTPProbe, port=PORT))
@pytest.mark.anyio
async def test_assemble_payload(args):
    series_id, server_id, target, size = args
    ip_len = 20 if target.address.version == 4 else 40
    assume((size - ip_len - TCPSMTPProbe.PREAMBLE) % 256 != 1)  # some sizes are impossible due to the TXT encoding

    tcp = TCPStateMachine(None, PORT, target.address, target.port, None, None, 10)
    probe = TCPSMTPProbe(series_id, server_id, target, tcp, None, PORT, size)
    payload, length = probe.assemble_payload()

    noop = re.compile("^NOOP *\r\n$")
    assert noop.fullmatch(payload.load.decode("ascii"))
    assert ip_len + 20 + len(payload) == size


@given(probe_arguments(TCPSMTPProbe, port=PORT, ip_version=4))
@pytest.mark.anyio
async def test_match_response_ipv4(args):
    series_id, server_id, target, size = args
    tcp = TCPStateMachine(None, PORT, target.address, target.port, None, None, 10)
    probe = TCPSMTPProbe(series_id, server_id, target, tcp, None, PORT, size)
    payload, length = probe.assemble_payload()

    # fake sending
    probe.request, probe.request_ack = tcp.send_segment(payload, length)

    response = IP(src=str(target.address)) / TCP(dport=PORT, ack=probe.request[TCP].seq + length, flags="A")

    match = probe.match_response(response)
    assert match
    assert probe.t_recv is not None
    assert probe.response == response


@given(probe_arguments(TCPSMTPProbe, port=PORT, ip_version=6))
@pytest.mark.anyio
async def test_match_response_ipv6(args):
    series_id, server_id, target, size = args
    tcp = TCPStateMachine(None, PORT, target.address, target.port, None, None, 10)
    probe = TCPSMTPProbe(series_id, server_id, target, tcp, None, PORT, size)
    payload, length = probe.assemble_payload()

    # fake sending
    probe.request, probe.request_ack = tcp.send_segment(payload, length)

    response = IPv6(src=str(target.address)) / TCP(dport=PORT, ack=probe.request[TCP].seq + length, flags="A")

    match = probe.match_response(response)
    assert match
    assert probe.t_recv is not None
    assert probe.response == response
