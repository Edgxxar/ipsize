#!/bin/bash

# make sure the PYTHONPATH environment variable contains the ipsize source

source .venv/bin/activate
sudo "$(which python)" "$@"
