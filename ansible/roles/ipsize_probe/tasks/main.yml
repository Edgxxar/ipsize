---
- name: Add redis repository key
  apt_key:
    url: https://packages.redis.io/gpg

- name: Add redis repository
  apt_repository:
    repo: "deb https://packages.redis.io/deb {{ ansible_facts['distribution_release'] }} main"
    filename: redis

- name: Install redis
  package:
    name:
      - redis
      - redis-stack-server

- name: Configure redis
  copy:
    src: files/etc/redis/redis.conf
    dest: /etc/redis/redis.conf
  notify: redis_restart

- name: Allow overcommit memory
  ansible.posix.sysctl:
    name: vm.overcommit_memory
    value: "1"
  notify: redis_restart

- name: Make sure redis is running
  service:
    name: redis-server
    state: started
    enabled: true

- name: Deploy nginx site
  template:
    src: etc/nginx/sites-available/probe.conf.j2
    dest: "/etc/nginx/sites-available/{{ inventory_hostname }}.conf"
  notify: nginx_reload

- name: Enable nginx site
  file:
    path: "/etc/nginx/sites-enabled/{{ inventory_hostname }}.conf"
    src: "../sites-available/{{ inventory_hostname }}.conf"
    state: link
  notify: nginx_reload

- name: Create default .env file
  template:
    src: var/www/ipsize/probeserver.env.j2
    dest: "{{ install_dir }}/.env"
    owner: ipsize
    group: ipsize
    mode: u=rw,g=r,o=
    force: false

- name: Deploy systemd service
  template:
    src: "etc/systemd/system/{{ item }}.j2"
    dest: "/etc/systemd/system/{{ item }}"
  loop:
    - ipsize-probe.service
    - ipsize-probe.socket
    - ipsize-monitor.service
  notify:
    - systemd_daemon_reload
    - ipsize-probe_restart

- name: Force all notified handlers to run
  meta: flush_handlers

- name: Enable and start probe server
  systemd:
    name: ipsize-probe.service
    state: started
    enabled: true

- name: Disable monitoring
  systemd:
    name: ipsize-monitor.service
    state: stopped
    enabled: false
