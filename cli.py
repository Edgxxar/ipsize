#!/usr/bin/env python3
import json
from pathlib import Path
from typing import Annotated, Any

import typer
import yaml
from rich import print

from ipsize.control.cli import data, server, target, user
from ipsize.control.main import app as control_app
from ipsize.probe.cli import probe
from ipsize.probe.main import app as probe_app

api = typer.Typer()


def build_docs(docs: dict[str, Any], output: Path, force: bool):
    if output.exists() and not force:
        print("Output file exists, use --force to overwrite")
        raise typer.Abort()
    with open(output, "w") as file:
        if output.suffix == ".json":
            file.write(json.dumps(docs))
        elif output.suffix == ".yml":
            yaml.dump(docs, file)
        else:
            print("Unknown file suffix, only json and yml supported")
            raise typer.Abort()


@api.command()
def build_control_docs(output: Path, force: Annotated[bool, typer.Option("--force")] = False):
    build_docs(control_app.openapi(), output, force)


@api.command()
def build_probe_docs(output: Path, force: Annotated[bool, typer.Option("--force")] = False):
    build_docs(probe_app.openapi(), output, force)


cli = typer.Typer()
cli.add_typer(api, name="api")
cli.add_typer(data.cli, name="data")
cli.add_typer(server.cli, name="server")
cli.add_typer(probe.cli, name="probe")
cli.add_typer(target.cli, name="target")
cli.add_typer(user.cli, name="user")

if __name__ == "__main__":
    try:
        cli()
    except ConnectionRefusedError as e:
        print("ConnectionRefusedError while trying to execute the command:")
        print(e)
