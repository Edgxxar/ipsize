## New API endpoints
Write tests for new API endpoints:

- [ ] test authorized access with valid request data
- [ ] test authorized access with invalid request data
- [ ] test unauthorized access

## New Probe Types

Write tests for new probes and probe series:

- [ ] All Probes: ``assemble_request``
- [ ] Public Probes: ``match_response`` (test valid and invalid responses)
- [ ] Private Probe Series: ``validate_incoming`` (test valid and invalid packets)

## Deployment

Verify the following:

- [ ] Ansible deployment works without errors
- [ ] Services start successfully
