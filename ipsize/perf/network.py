import time

import psutil

from ipsize.perf.base import PerformanceMonitor, TimeseriesConfig


class NetworkPerformanceMonitor(PerformanceMonitor):

    def __init__(self, interval: float = 1, retention: int = 0):
        super().__init__("IPSize-NetworkMonitor", interval, retention)

    def get_keys(self) -> dict[str, TimeseriesConfig]:
        return {
            "perf:network:sent:bytes": TimeseriesConfig("max", {"type": "network", "metric": "bytes"}),
            "perf:network:recv:bytes": TimeseriesConfig("max", {"type": "network", "metric": "bytes"}),
            "perf:network:sent:packets": TimeseriesConfig("max", {"type": "network", "metric": "count"}),
            "perf:network:recv:packets": TimeseriesConfig("max", {"type": "network", "metric": "count"})
        }

    def get_data(self) -> list[tuple[str, int, float]]:
        timestamp = round(time.time() * 1000)
        network = psutil.net_io_counters()
        return [
            ("perf:network:sent:bytes", timestamp, network.bytes_sent),
            ("perf:network:recv:bytes", timestamp, network.bytes_recv),
            ("perf:network:sent:packets", timestamp, network.packets_sent),
            ("perf:network:recv:packets", timestamp, network.packets_recv),
        ]
