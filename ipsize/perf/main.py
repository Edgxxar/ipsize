import logging
import signal
import time
from logging.config import dictConfig
from pathlib import Path

import redis

from ipsize.config import LogConfig
from ipsize.perf import database
from ipsize.perf.config import settings
from ipsize.perf.cpu import CPUPerformanceMonitor
from ipsize.perf.disk import DiskPerformanceMonitor
from ipsize.perf.memory import MemoryPerformanceMonitor
from ipsize.perf.network import NetworkPerformanceMonitor

running = True


def shutdown(*args):
    global running
    running = False


def main():
    if settings.LOG_TO_FILE:
        Path(settings.LOG_DIR).mkdir(mode=0o755, parents=True, exist_ok=True)
    dictConfig(LogConfig(settings.LOG_FILENAME))
    logger = logging.getLogger(settings.LOGGER_NAME)

    db = database.get_redis()
    try:
        if not db.ping():
            logger.error("Failed to ping redis server. Make sure redis is reachable via unix socket.")
            return
        ts = db.ts()  # test Time Series availability
        try:
            ts.info("test")
        except redis.exceptions.ResponseError:  # Key should not exist, a response error is expected
            pass
        logger.debug("Successfully connected to redis.")
    except (FileNotFoundError, redis.exceptions.ConnectionError) as e:
        logger.error("Error trying to connect to redis: " + e)
        return
    except redis.exceptions.BusyLoadingError:
        logger.error("Redis is busy loading the database.")
        return

    logger.info("Starting monitor")
    cpu = CPUPerformanceMonitor()
    cpu.start()
    mem = MemoryPerformanceMonitor()
    mem.start()
    net = NetworkPerformanceMonitor()
    net.start()
    disk = DiskPerformanceMonitor()
    disk.start()

    while running:
        time.sleep(0.1)

    logger.info("Stopping monitor")
    cpu.stop()
    mem.stop()
    net.stop()
    disk.stop()
    cpu.join()
    mem.join()
    net.join()
    disk.join()


if __name__ == "__main__":
    signal.signal(signal.SIGTERM, shutdown)
    signal.signal(signal.SIGINT, shutdown)
    main()
