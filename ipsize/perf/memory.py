import time

import psutil

from ipsize.perf.base import PerformanceMonitor, TimeseriesConfig


class MemoryPerformanceMonitor(PerformanceMonitor):

    def __init__(self, interval: float = 1, retention: int = 0):
        super().__init__("IPSize-MemoryMonitor", interval, retention)

    def get_keys(self) -> dict[str, TimeseriesConfig]:
        return {
            "perf:memory:total": TimeseriesConfig("first", {"type": "memory"}),
            "perf:memory:available": TimeseriesConfig("first", {"type": "memory"}),
            "perf:memory:swap": TimeseriesConfig("first", {"type": "memory"})
        }

    def get_data(self) -> list[tuple[str, int, float]]:
        timestamp = round(time.time() * 1000)
        mem = psutil.virtual_memory()
        swap = psutil.virtual_memory()
        return [
            ("perf:memory:total", timestamp, mem.total),
            ("perf:memory:available", timestamp, mem.available),
            ("perf:memory:swap", timestamp, swap.used),
        ]
