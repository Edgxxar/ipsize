from abc import ABC, abstractmethod
from dataclasses import dataclass
from threading import Thread
from time import sleep
from typing import Literal

from ipsize.perf import database


@dataclass
class TimeseriesConfig:
    duplicate_policy: Literal["block", "first", "last", "min", "max", "sum"]
    labels: dict[str, str]


class PerformanceMonitor(Thread, ABC):
    def __init__(
            self,
            name: str,
            interval: float = 1,
            retention: int = 0
    ):
        self.interval = interval
        self.retention = retention
        self.running = True
        super().__init__(target=self._run, name=name, daemon=True)

    def setup(self):
        db = database.get_redis()
        ts = db.ts()
        for key, config in self.get_keys().items():
            if db.exists(key):
                ts.alter(
                    key,
                    retention_msecs=self.retention,
                    duplicate_policy=config.duplicate_policy,
                    labels=config.labels
                )
            else:
                ts.create(
                    key,
                    retention_msecs=self.retention,
                    duplicate_policy=config.duplicate_policy,
                    labels=config.labels
                )

    def _run(self):
        self.setup()
        while self.running:
            ts = database.get_redis().ts()
            ts.madd(self.get_data())
            sleep(self.interval)

    def stop(self):
        self.running = False

    @abstractmethod
    def get_keys(self) -> dict[str, TimeseriesConfig]:
        """
        :return: list of timeseries keys which should be created
        """

    @abstractmethod
    def get_data(self) -> list[tuple[str, int, float]]:
        """
        Measures the current performance and returns the results.
        :return: set of datapoints as a list of tuples as (key, timestamp, value)
        """
