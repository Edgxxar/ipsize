import time

import psutil

from ipsize.perf.base import PerformanceMonitor, TimeseriesConfig


class DiskPerformanceMonitor(PerformanceMonitor):

    def __init__(self, interval: float = 1, retention: int = 0):
        super().__init__("IPSize-DiskMonitor", interval, retention)

    def get_keys(self) -> dict[str, TimeseriesConfig]:
        return {
            "perf:disk:read:count": TimeseriesConfig("max", {"type": "disk", "metric": "count"}),
            "perf:disk:write:count": TimeseriesConfig("max", {"type": "disk", "metric": "count"}),
            "perf:disk:read:bytes": TimeseriesConfig("max", {"type": "disk", "metric": "bytes"}),
            "perf:disk:write:bytes": TimeseriesConfig("max", {"type": "disk", "metric": "bytes"})
        }

    def get_data(self) -> list[tuple[str, int, float]]:
        timestamp = round(time.time() * 1000)
        disk = psutil.disk_io_counters()
        return [
            ("perf:disk:read:count", timestamp, disk.read_count),
            ("perf:disk:write:count", timestamp, disk.write_count),
            ("perf:disk:read:bytes", timestamp, disk.read_bytes),
            ("perf:disk:write:bytes", timestamp, disk.write_bytes),
        ]
