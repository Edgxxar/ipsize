from ipsize.config import Settings


class MonitorSettings(Settings):
    LOG_FILENAME: str = "monitor.log"

    REDIS_SOCKET: str = "/run/redis/redis-server.sock"
    REDIS_DB: int = 0


settings = MonitorSettings()
