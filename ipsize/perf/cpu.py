import time

import psutil

from ipsize.perf.base import PerformanceMonitor, TimeseriesConfig


class CPUPerformanceMonitor(PerformanceMonitor):

    def __init__(self, interval: float = 1, retention: int = 0):
        super().__init__("IPSize-CPUMonitor", interval, retention)

    def get_keys(self) -> dict[str, TimeseriesConfig]:
        cpu = psutil.cpu_percent(interval=None, percpu=True)  # type: list[float]
        keys = {}
        for i in range(len(cpu)):
            keys[f"perf:cpu:{i}"] = TimeseriesConfig("first", {"type": "cpu"})
        return keys

    def get_data(self) -> list[tuple[str, int, float]]:
        timestamp = round(time.time() * 1000)
        cpu = psutil.cpu_percent(interval=None, percpu=True)  # type: list[float]
        return [(f"perf:cpu:{i}", timestamp, v) for i, v in enumerate(cpu)]
