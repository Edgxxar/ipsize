from redis.client import Redis
from redis.connection import ConnectionPool, UnixDomainSocketConnection

from ipsize.perf.config import settings


def _create_redis_connection_pool(decode_responses: bool) -> ConnectionPool:
    return ConnectionPool(
        connection_class=UnixDomainSocketConnection,
        path=settings.REDIS_SOCKET,
        db=settings.REDIS_DB,
        decode_responses=decode_responses
    )


redis_pool = _create_redis_connection_pool(True)


def get_redis() -> Redis:
    """
    Gets a redis client that uses the global connection pool. Responses are decoded to strings.
    :return: redis client
    """
    return Redis(connection_pool=redis_pool)
