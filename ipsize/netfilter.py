import logging
import shutil
import subprocess
from typing import Optional

import anyio

from ipsize.config import settings

logger = logging.getLogger(settings.LOGGER_NAME)
nft_executable: Optional[str] = None
table_name = "ipsize"


def get_nft_executable() -> str:
    global nft_executable
    if nft_executable is None:
        nft_executable = shutil.which("nft")
        if not nft_executable:
            raise RuntimeError("The nft executable could not be found on this system")
    return nft_executable


async def install_table() -> None:
    """
    Installs a new netfilter table, used to process incoming packets before they get filtered by the normal firewall
    rules. Individual probe series will install their own chains and rules. If the table exists already, it will be
    deleted beforehand, to clear out any chains that may not have been removed properly by previous executions.
    """
    # delete table if necessary
    await remove_table()
    try:
        # create new table
        nft = get_nft_executable()
        await anyio.run_process([nft, "add", "table", "inet", table_name])
    except subprocess.CalledProcessError as e:
        logger.error(e.stderr)
        raise e


async def remove_table() -> None:
    """
    Deletes the custom netfilter table if it exists.
    """
    try:
        nft = get_nft_executable()
        tables = await anyio.run_process([nft, "list", "tables"])
        if f"table inet {table_name}" in tables.stdout.decode():
            await anyio.run_process([nft, "delete", "table", "inet", table_name])
    except subprocess.CalledProcessError as e:
        logger.error(e.stderr)
        raise e


async def add_chain(name: str, specification: str) -> None:
    """
    Adds a chain to the netfilter table.
    :param name: the name of the chain
    :param specification: the specification of the chain
    """
    try:
        nft = get_nft_executable()
        await anyio.run_process([nft, "add", "chain", "inet", table_name, name, specification])
    except subprocess.CalledProcessError as e:
        logger.error(e.stderr)
        raise e


async def delete_chain(name: str) -> None:
    """
    Deletes a chain from the netfilter table.
    :param name: the name of the chain
    """
    try:
        nft = get_nft_executable()
        await anyio.run_process([nft, "delete", "chain", "inet", table_name, name])
    except subprocess.CalledProcessError as e:
        logger.error(e.stderr)
        raise e


async def add_rule(chain: str, rule: str) -> None:
    """
    Adds the specified rule to the chain in the netfilter table.
    :param chain: the name of the chain to modify
    :param rule: the rule to add
    """
    try:
        nft = get_nft_executable()
        await anyio.run_process([nft, "add", "rule", "inet", table_name, chain, rule])
    except subprocess.CalledProcessError as e:
        logger.error(e.stderr)
        raise e
