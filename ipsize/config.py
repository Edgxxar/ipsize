import secrets
from ipaddress import IPv4Address, IPv6Address
from pathlib import Path, PurePath
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    LOGGER_NAME: str = "IPSize"
    LOG_LEVEL: str = "INFO"
    LOG_TO_FILE: bool = True
    LOG_DIR: Path = "logs"

    # used to sign access tokens and encrypt control secrets
    SECRET_KEY: str = secrets.token_urlsafe(32)
    # 60 minutes * 24 hours * 30 days
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 30

    REST_CLIENT_TIMEOUT: int = 30  # seconds

    CONNECTIVITY_CHECK_IPV4: set[IPv4Address] = {
        "1.1.1.1",  # Cloudflare
        "1.0.0.1",
        "8.8.8.8",  # Google
        "8.8.4.4",
        "130.83.22.61",  # TU Darmstadt
        "130.83.56.61",
        "134.130.4.1",  # RWTH Aachen
        "134.130.5.1",
        "128.32.136.3",  # UC Berkeley
        "128.32.136.14"
    }
    CONNECTIVITY_CHECK_IPV6: set[IPv6Address] = {
        "2606:4700:4700::1111",  # Cloudflare
        "2606:4700:4700::1001",
        "2001:4860:4860::8888",  # Google
        "2001:4860:4860::8844",
        "2001:41b8:83f:22::61",  # TU Darmstadt
        "2001:41b8:83f:56::61",
        "2a00:8a60:0:4::1",  # RWTH Aachen
        "2a00:8a60:0:5::1",
        "2607:f140:ffff:fffe::3",  # UC Berkeley
        "2607:f140:ffff:fffe::e"
    }

    model_config = SettingsConfigDict(
        extra="allow",
        case_sensitive=True,
        env_file=".env",
        env_file_encoding="utf-8"
    )


settings = Settings()


# Logging config
class LogConfig(dict):

    def __init__(self, filename: str):
        super().__init__()
        self.update({
            "version": 1,
            "disable_existing_loggers": False,
            "formatters": {
                "console": {
                    "format": "%(levelname)-8s %(message)s",
                },
                "file": {
                    "format": "%(asctime)s - %(levelname)-8s %(message)s",
                    "datefmt": "%Y-%m-%d %H:%M:%S",
                },
            },
            "handlers": {
                "console": {
                    "class": "logging.StreamHandler",
                    "stream": "ext://sys.stdout",
                    "formatter": "console",
                },
                "file": {
                    "class": "logging.handlers.TimedRotatingFileHandler",
                    "formatter": "file",
                    "filename": PurePath(settings.LOG_DIR, filename),
                    "when": "midnight",
                    "backupCount": 31,
                    "delay": True
                }
            },
            "loggers": {
                settings.LOGGER_NAME: {
                    "level": settings.LOG_LEVEL,
                    "handlers": ["console", "file"] if settings.LOG_TO_FILE else ["console"],
                    "propagate": False
                },
            }
        })
