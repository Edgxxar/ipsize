import ipaddress
from datetime import datetime
from ipaddress import IPv4Address, IPv6Address
from typing import Annotated, Any, Optional, Union

from pydantic import AnyHttpUrl, BeforeValidator, PlainSerializer

from ipsize.models.base import BaseModel, short
from ipsize.models.enum import ConnectionType
from ipsize.models.status import ServerStatus


class ServerBase(BaseModel):
    """
    Server model base
    """
    name: str
    url: Annotated[AnyHttpUrl, PlainSerializer(lambda v: f"{v}", return_type=str)]
    connection_type: ConnectionType = ConnectionType.ETHERNET


class ServerCreate(ServerBase):
    """
    Server model during creation, contains cleartext secrets
    """
    server_secret: str
    control_secret: str


def validate_server_address(v: Any) -> Union[IPv4Address, IPv6Address]:
    if hasattr(v, "address"):
        return ipaddress.ip_address(v.address)
    if isinstance(v, dict) and "address" in v:
        return ipaddress.ip_address(v["address"])
    return ipaddress.ip_address(v)


class Server(ServerBase):
    """
    Full server model
    """
    id: short
    addresses: list[Annotated[Union[IPv4Address, IPv6Address], BeforeValidator(validate_server_address)]]
    status: Optional[ServerStatus] = None
    last_update: datetime
    # hashed client credentials used to authorize the probe server at the control server
    server_secret: str
    # encrypted client credentials used to authorize the control server at the probe server
    control_secret: str
    control_secret_salt: str


class ServerModel(ServerBase):
    """
    Server model returned in requests, omits secrets
    """
    id: short
    addresses: list[Annotated[Union[IPv4Address, IPv6Address], BeforeValidator(validate_server_address)]]
    status: Optional[ServerStatus] = None
    last_update: datetime = datetime.now()
