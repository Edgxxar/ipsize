"""
Pydantic models that will be used with the API. Base models define common attributes, subclasses are defined for
read and write operations. Data validation is performed automatically using the type annotations of attributes.
"""

from .auth import AccessToken, TokenPayload, User, UserCreate, UserModel
from .probe import ProbeSeries, ProbeSeriesConfig, ProbeSeriesCreate, ProbeSeriesLocalState, ProbeSeriesTargetChanges, \
    ProbeSeriesTargetUpdate, ProbeDownloadAvailable, ProbeDownloadCreate, ProbeDownload
from .server import Server, ServerCreate, ServerModel
from .status import ControlServerStatus, ProbeServerStatus
from .target import TargetHost, TargetHostCreate, TargetHostEnabled, TargetService, TargetServiceCreate, \
    TargetServiceModel, TargetServiceState, TargetServiceHistory
