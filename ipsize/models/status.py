from ipsize.models.base import BaseModel
from ipsize.models.enum import Status


class ServerStatus(BaseModel):
    """
    Server status, only OK if all other attributes are OK
    """
    status: Status = Status.UNKNOWN
    ipv4: Status = Status.UNKNOWN
    ipv6: Status = Status.UNKNOWN


class ControlServerStatus(ServerStatus):
    """
    Control server status
    """
    database: Status = Status.UNKNOWN


class ProbeServerStatus(ServerStatus):
    """
    Probe server status
    """
    redis: Status = Status.UNKNOWN
    control: Status = Status.UNKNOWN
