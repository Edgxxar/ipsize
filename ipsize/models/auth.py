from typing import Annotated

from pydantic import EmailStr, Field

from ipsize.models.base import BaseModel


class UserBase(BaseModel):
    """
    User model base
    """
    email: EmailStr


class UserCreate(UserBase):
    """
    User model during creation, contains cleartext password
    """
    password: str


class User(UserBase):
    """
    Full user model
    """
    id: int
    hashed_password: str
    is_active: bool


class UserModel(UserBase):
    """
    User model returned in requests, omits the password field
    """
    id: int
    is_active: bool


class AccessToken(BaseModel):
    """
    Access token returned by authentication endpoints
    """
    access_token: str
    token_type: str = "bearer"


class TokenPayload(BaseModel):
    """
    JWT payload
    """
    exp: int
    sub: Annotated[str, Field(pattern="^(user|server):([0-9]+)$")]

    @classmethod
    def from_user_id(cls, user_id: int) -> str:
        return f"user:{user_id}"

    @classmethod
    def from_server_id(cls, server_id: int) -> str:
        return f"server:{server_id}"

    def is_user_subject(self) -> bool:
        return self.sub.startswith("user")

    def is_server_subject(self) -> bool:
        return self.sub.startswith("server")

    def get_id(self) -> int:
        return int(self.sub.split(":")[1])
