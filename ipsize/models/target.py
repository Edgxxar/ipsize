from datetime import datetime
from ipaddress import IPv4Address, IPv6Address
from typing import Annotated, Optional, Union

from pydantic import ConfigDict, Field, model_validator

from ipsize.models.base import BaseModel, probe_sequence
from ipsize.models.enum import ProbeSeriesType, Status


class TargetHostBase(BaseModel):
    """
    Target host model base
    """
    address: Union[IPv4Address, IPv6Address]


class TargetHostCreate(TargetHostBase):
    """
    Target host model during creation
    """
    model_config = ConfigDict(
        json_schema_extra={
            "example": {
                "address": "1.1.1.1"
            }
        }
    )


class TargetHost(TargetHostBase):
    """
    Full target host model, with services of this host
    """
    id: int
    location: Optional[dict] = None
    autonomous_system: Optional[Union[str, dict]] = None
    enabled: bool = True
    last_update: datetime = datetime.now()

    services: list["TargetService"] = []

    def has_service_type(self, service_type: ProbeSeriesType) -> bool:
        return any(s.type == service_type for s in self.services)

    def get_service(self, service_type: ProbeSeriesType) -> Optional["TargetService"]:
        return next(s for s in self.services if s.type == service_type)


class TargetHostEnabled(TargetHostBase):
    """
    Target host model for check/enable/disable endpoints, only includes the enabled attribute
    """
    enabled: bool


class TargetServiceBase(BaseModel):
    """
    Target service model base
    """
    type: ProbeSeriesType
    port: Optional[Annotated[int, Field(ge=1, lt=2 ** 16 - 1)]] = None

    @model_validator(mode="after")
    def check_port_semantics(self):
        if self.type == ProbeSeriesType.PUBLIC_ICMP_PING and self.port is not None:
            raise ValueError(self.type + " probe type cannot have a port number")
        if self.type != ProbeSeriesType.PUBLIC_ICMP_PING and self.port is None:
            raise ValueError(self.type + " probe type requires a port number")
        return self


class TargetServiceCreate(TargetServiceBase):
    """
    Target service model during creation.
    """
    target_host_id: int
    status: Status = Status.UNKNOWN

    model_config = ConfigDict(
        json_schema_extra={
            "example": {
                "type": ProbeSeriesType.PUBLIC_UDP_DNS.name,
                "target_host_id": 1,
                "port": 53
            }
        }
    )


class TargetService(TargetServiceBase):
    """
    Full target service model
    """
    id: int
    target_host_id: int
    status: Status
    flapping: bool
    last_update: datetime
    next_update: Optional[datetime] = None


class TargetServiceModel(TargetServiceBase):
    """
    Target service model without status but with host address included
    """
    id: int
    target_host_id: int
    address: Union[IPv4Address, IPv6Address]


class TargetServiceState(BaseModel):
    """
    Target service state for a single probe series and server, only used locally at probe servers
    """
    seq_no: probe_sequence = 0
    last_probe: float = 0


class TargetServiceHistory(BaseModel):
    """
    Full target service history model
    """
    target_service_id: int
    timestamp: datetime
    status: Status
    flapping: bool


# update forward references, see https://docs.pydantic.dev/latest/usage/postponed_annotations/
TargetHost.model_rebuild()
