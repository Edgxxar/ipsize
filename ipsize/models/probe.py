from datetime import datetime
from pathlib import Path
from typing import Annotated, Optional, Union

from pydantic import Field, model_validator

from ipsize.models.base import BaseModel, short
from ipsize.models.enum import ProbeSeriesStatus, ProbeSeriesType

Port = Field(ge=1, lt=2 ** 16 - 1)


class ProbeSeriesConfig(BaseModel):
    """
    Configuration of a probe series
    """
    # seconds between probes
    interval: Annotated[float, Field(gt=0)] = 0.2
    # timeout after which probes are recorded as dropped
    timeout: Annotated[float, Field(gt=0)] = 3.0
    # optional list of servers to restrict probe sending to
    servers: Optional[list[short]] = None
    # port for probe series
    port: Optional[Union[Annotated[int, Port], tuple[Annotated[int, Port], Annotated[int, Port]]]] = None
    # number of target services to include
    targets: Annotated[int, Field(gt=0)] = 25000
    # automatically exclude errored hosts
    exclude_errored: bool = True
    # automatically exclude flapping hosts
    exclude_flapping: bool = False


class ProbeSeriesBase(BaseModel):
    """
    Probe series base model
    """
    type: ProbeSeriesType
    config: ProbeSeriesConfig = ProbeSeriesConfig()
    status: ProbeSeriesStatus = ProbeSeriesStatus.NEW

    @model_validator(mode="after")
    def check_port_semantics(self):
        if self.type == ProbeSeriesType.PUBLIC_ICMP_PING and self.config.port is not None:
            raise ValueError(self.type + " probe type cannot have a port number")
        if self.type != ProbeSeriesType.PUBLIC_ICMP_PING and self.config.port is None:
            raise ValueError(self.type + " probe type requires a port number or range")
        if isinstance(self.config.port, tuple) and self.config.port[0] >= self.config.port[1]:
            raise ValueError("Invalid port range defined")
        return self


class ProbeSeriesCreate(ProbeSeriesBase):
    """
    Probe series model during creation
    """
    pass


class ProbeSeries(ProbeSeriesBase):
    """
    Full probe series model
    """
    id: short
    start_time: Optional[datetime] = None
    finish_time: Optional[datetime] = None


class ProbeSeriesTargetChanges(BaseModel):
    """
    Target Service IDs that changed for a probe series
    """
    added: list[int]
    removed: list[int]
    total: int


class ProbeSeriesTargetUpdate(BaseModel):
    """
    Collection of probe series targets that changed, mapped by probe series ID
    """
    updates: dict[int, ProbeSeriesTargetChanges] = {}


class ProbeSeriesLocalState(BaseModel):
    """
    Local state of a probe series at a probe server
    """
    series_id: int
    status: ProbeSeriesStatus
    can_run: bool
    running: bool
    targets: int
    sent: int
    received: int
    incoming: int
    sending: int
    save_pending: int
    started_at: Optional[datetime] = None
    stopped_at: Optional[datetime] = None


class ProbeDownloadBase(BaseModel):
    """
    Probe download base model
    """
    probe_series_id: short
    server_id: short
    packet_count: int
    file_name: Path
    file_size: int
    downloaded_at: Optional[datetime] = None


class ProbeDownloadCreate(ProbeDownloadBase):
    """
    Probe download model during creation
    """


class ProbeDownloadAvailable(ProbeDownloadBase):
    """
    Probe download available at the probe server
    """
    flushed: bool = False


class ProbeDownload(ProbeDownloadBase):
    """
    Full probe download model
    """
    id: int
    imported_at: Optional[datetime] = None
    error: Optional[bool] = None
