from ipsize.models.base import BaseModel


class PaginatedRequest(BaseModel):
    count: int
    offset: int


class PaginatedResponse(BaseModel):
    """
    Base class for paginated responses
    """
    count: int
    offset: int
    total: int
    results: list
