from typing import Annotated

from pydantic import BaseModel as PydanticBaseModel, ConfigDict, Field

short = Annotated[int, Field(ge=-2 ** 15, le=2 ** 15 - 1)]  # 2-byte integer
probe_sequence = Annotated[int, Field(ge=0, le=2 ** 31 - 1)]  # positive 4-byte integer


class BaseModel(PydanticBaseModel):
    """
    Base class for all models. Don't allow any extra fields, to prevent information leaking from the database due to
    the automatic transformation done by FastAPI. Allow usage of attributes for all models.
    """
    model_config = ConfigDict(
        extra="forbid",
        from_attributes=True
    )
