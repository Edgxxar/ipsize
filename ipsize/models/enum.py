import enum


class ConnectionType(str, enum.Enum):
    # TODO what connection types do we have?
    ETHERNET = "ETHERNET"
    FIBRE = "FIBRE"
    WIFI = "WIFI"
    DSL = "DSL"
    MOBILE_4G = "MOBILE_4G"
    MOBILE_5G = "MOBILE_5G"


class ProbeSeriesType(str, enum.Enum):
    PUBLIC_ICMP_PING = "PUBLIC_ICMP_PING"
    PUBLIC_UDP_DNS = "PUBLIC_UDP_DNS"
    PUBLIC_TCP_DNS = "PUBLIC_TCP_DNS"
    PUBLIC_TCP_SMTP = "PUBLIC_TCP_SMTP"
    PRIVATE_UDP = "PRIVATE_UDP"
    PRIVATE_TCP = "PRIVATE_TCP"

    def is_private(self) -> bool:
        """
        :return: true, if this is a private probe series
        """
        return self == ProbeSeriesType.PRIVATE_UDP or self == ProbeSeriesType.PRIVATE_TCP


class ProbeSeriesStatus(str, enum.Enum):
    NEW = "NEW"                 # created, but not sent to probe server yet
    PENDING = "PENDING"         # probe server has been informed and can start to collect data
    ONGOING = "ONGOING"         # data is currently being collected
    PAUSED = "PAUSED"           # paused, can be resumed
    FINISHED = "FINISHED"       # done, but data was not yet sent to controller
    CLOSED = "CLOSED"           # done, data has been sent to controller
    CANCELLED = "CANCELLED"     # cancelled


class Status(str, enum.Enum):
    OK = "OK"
    ERROR = "ERROR"
    UNKNOWN = "UNKNOWN"


class TCPState(int, enum.Enum):
    """
    TCP states the probe server can be in, with respect to public probe series. The probe server is always the client,
    thus some server states are not implemented.
    """
    CLOSED = 0
    SYN_SENT = 1
    ESTABLISHED = 2
    FIN_WAIT_1 = 3
    FIN_WAIT_2 = 4
    LAST_ACK = 5
