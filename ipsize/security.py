import base64
import binascii
import os
from datetime import datetime, timedelta
from typing import Optional, Union

import bcrypt
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from fastapi import Form, HTTPException, status
from fastapi.openapi.models import OAuthFlows
from fastapi.security import OAuth2
from fastapi.security.utils import get_authorization_scheme_param
from jose import jwt
from starlette.requests import Request
from starlette.status import HTTP_401_UNAUTHORIZED

ALGORITHM = "HS256"


def hash_password(password: str) -> str:
    """
    Hashes a password using a randomly generated salt.
    :param password: the password to hash
    :return: hashed password
    """
    salt = bcrypt.gensalt()
    return bcrypt.hashpw(password.encode(), salt).decode()


def verify_password(password: str, hashed: str) -> bool:
    """
    Verifies a password matches a hashed password.
    :param password: password to verify
    :param hashed: hash to compare against
    :return: true, if the password and hash match
    """
    return bcrypt.checkpw(password.encode(), hashed.encode())


def encrypt_string(string: str, password: str) -> tuple[str, str]:
    """
    Encrypts a string using the Fernet recipe provided by the cryptography library.
    :param string: string to encrypt
    :param password: password to use for encryption. A secure key will be derived from this password.
    :return: the encrypted string and the salt used for key derivation as a string of hexadecimal values
    """
    salt = os.urandom(16)
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=salt,
        iterations=480000,
    )
    key = base64.urlsafe_b64encode(kdf.derive(password.encode()))
    f = Fernet(key)
    token = f.encrypt(string.encode())
    return token.decode(), salt.hex()


def decrypt_string(string: str, password: str, salt: str) -> str:
    """
    Decrypts a string using the Fernet recipe provided by the cryptography library.
    :param string: string to decrypt
    :param password: password to use for decryption. A secure key will be derived from this password.
    :param salt: salt used for key derivation as a string of hexadecimal values
    :return:
    """
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=bytes.fromhex(salt),
        iterations=480000,
    )
    key = base64.urlsafe_b64encode(kdf.derive(password.encode()))
    f = Fernet(key)
    return f.decrypt(string.encode()).decode()


def create_access_token(subject: str, expires_delta: Union[int, timedelta], key: str) -> str:
    """
    Creates a signed JSON Web Token (JWT) used to authenticate requests against protected API endpoints.
    :param subject: locally unique identifier for the principal that is the subject of this JWT
    :param expires_delta: time delta (or number of minutes) after which this JWT becomes invalid, relative to now
    :param key: key used to sign this JWT
    :return: signed JWT containing the `sub` and `exp` claims as specified
    """
    if isinstance(expires_delta, int):
        expires_delta = timedelta(minutes=expires_delta)
    expire = datetime.utcnow() + expires_delta
    to_encode = {"exp": expire, "sub": subject}
    encoded_jwt = jwt.encode(to_encode, key, algorithm=ALGORITHM)
    return encoded_jwt


def unauthorized_response(detail: str):
    return HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail=detail,
        headers={"WWW-Authenticate": "Basic"}
    )


class OAuth2ClientCredentials:
    """
    Dependency class, similar to :class:`fastapi.security.oauth2.OAuth2PasswordRequestForm`. Creates the required form
    request parameters for the OAuth 2.0 client credentials grant type. If the authorization header is present in the
    request, it takes precedence over any form data in the body. The authorization header must contain a valid
    HTTP basic auth string, if present. Any scope values are ignored.
    """

    def __init__(
            self,
            request: Request,
            grant_type: str = Form(regex="client_credentials"),
            scope: str = Form(""),
            client_id: Optional[str] = Form(None),
            client_secret: Optional[str] = Form(None)
    ):
        self.grant_type = grant_type
        self.scopes = scope.split()

        authorization = request.headers.get("Authorization")

        # use HTTP basic auth if authorization header is present
        if authorization:
            scheme, param = get_authorization_scheme_param(authorization)
            if scheme.lower() != "basic":
                raise unauthorized_response("Invalid authorization scheme")
            try:
                data = base64.b64decode(param).decode("ascii")
            except (ValueError, UnicodeDecodeError, binascii.Error):
                raise unauthorized_response("Invalid authentication credentials")
            self.client_id, separator, self.client_secret = data.partition(":")
            if not separator:
                raise unauthorized_response("Invalid authentication credentials")

        # use form data if authorization header is not present
        else:
            self.client_id = client_id
            self.client_secret = client_secret

        # check values are not empty
        if not self.client_id or not self.client_secret:
            raise unauthorized_response("Invalid authentication credentials")


class OAuth2TokenBearer(OAuth2):
    """
    Dependency class, similar to :class:`fastapi.security.oauth2.OAuth2PasswordBearer`. Checks for a bearer token and
    provides the necessary specification for the OAuth 2.0 grant types password and client credentials. Allows
    for two different token endpoints to be specified, one for each grant type.
    """

    def __init__(
            self,
            password_token_url: str = None,
            client_credentials_token_url: str = None,
            auto_error=True
    ):
        flows = {}
        if password_token_url is not None:
            flows["password"] = {"tokenUrl": password_token_url, "scopes": {}}
        if client_credentials_token_url is not None:
            flows["clientCredentials"] = {"tokenUrl": client_credentials_token_url, "scopes": {}}
        super().__init__(
            flows=OAuthFlows(**flows),
            scheme_name="oAuthIPSize",
            description=None,
            auto_error=auto_error,
        )

    async def __call__(self, request: Request) -> Optional[str]:
        authorization: str = request.headers.get("Authorization")
        scheme, param = get_authorization_scheme_param(authorization)
        if not authorization or scheme.lower() != "bearer":
            if self.auto_error:
                raise HTTPException(
                    status_code=HTTP_401_UNAUTHORIZED,
                    detail="Not authenticated",
                    headers={"WWW-Authenticate": "Bearer"},
                )
            else:
                return None
        return param
