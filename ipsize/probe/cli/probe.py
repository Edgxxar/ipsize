import ipaddress
from typing import Annotated

import anyio
import scapy.sendrecv
import typer
from pydantic import IPvAnyAddress

from ipsize.probe.config import settings
from ipsize.probe.io.icmp import ICMPProbe
from ipsize.probe.io.target import Target
from ipsize.probe.io.udp_dns import UDPDNSProbe
from ipsize.probe.io.udp_private import UDPProbe

cli = typer.Typer()


@cli.command()
def send(
        type: str,
        address: Annotated[IPvAnyAddress, typer.Argument(parser=IPvAnyAddress)],
        port: Annotated[int, typer.Option("--port", "-p", min=0, max=2 ** 16 - 1)] = 0,
        series: Annotated[int, typer.Option("--series", "-S")] = 1,
        seq_no: Annotated[int, typer.Option("--seq", "-s")] = 0,
        size: Annotated[int, typer.Option("--size", min=0, max=settings.PROBE_MTU)] = 60
):
    target = Target(
        service_id=1,
        address=ipaddress.ip_address(str(address)),
        port=port,
        sequence_number=seq_no
    )
    if type == "icmp":
        anyio.run(_send, ICMPProbe, series, target, size)
    elif type == "dns":
        anyio.run(_send, UDPDNSProbe, series, target, size)
    elif type == "udp":
        anyio.run(_send, UDPProbe, series, target, size)
    else:
        print("unknown probe type")
        raise typer.Abort()


async def _send(probe_class, series, target, size):
    probe = probe_class(series, 1, target, None, None, size)
    packet = probe.assemble_request()
    scapy.sendrecv.send(packet)
