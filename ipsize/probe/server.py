import ipaddress
import logging
import os
import random
from ipaddress import IPv4Address, IPv6Address
from typing import Literal, Optional, Union

import anyio
import httpx
import redis
from anyio import TASK_STATUS_IGNORED
from anyio.abc import TaskStatus
from fastapi import status
from httpx import AsyncClient
from redis.asyncio.client import Redis
from scapy.interfaces import NetworkInterface, get_working_if, resolve_iface

from ipsize import models, netfilter
from ipsize.models import ProbeServerStatus
from ipsize.models.enum import ProbeSeriesStatus, ProbeSeriesType, Status
from ipsize.probe.config import settings
from ipsize.probe.io.base_series import ProbeSeries, cleanup_download_files
from ipsize.probe.io.icmp import ICMPProbe, ICMPProbeSeries
from ipsize.probe.io.tcp_dns import TCPDNSProbe, TCPDNSProbeSeries
from ipsize.probe.io.tcp_private import TCPProbe, TCPProbeSeries
from ipsize.probe.io.tcp_smtp import TCPSMTPProbe, TCPSMTPProbeSeries
from ipsize.probe.io.udp_dns import UDPDNSProbe, UDPDNSProbeSeries
from ipsize.probe.io.udp_private import UDPProbe, UDPProbeSeries
from ipsize.probe.persistence import database
from ipsize.probe.rest import get_control_client, get_control_token
from ipsize.rest import control_paths, format_path
from ipsize.server import Server
from ipsize.util import decode_integers, encode_integers

logger = logging.getLogger(settings.LOGGER_NAME)


def resolve_ip_address(
        iface: NetworkInterface, v: Literal[4, 6], setting: Union[None, IPv4Address, IPv6Address]
) -> Union[None, IPv4Address, IPv6Address]:
    """
    Resolves the IP address of this host to use for probe sending and matching.
    :param iface: network interface to use
    :param v: IP version
    :param setting: optional preset address
    :return: IP address to use, or none if no global address could be resolved
    :raises RuntimeError: if the setting address is not available at the given interface
    """
    if setting is not None:
        if setting.is_loopback:
            logger.info("Disabling IPv%d for probe matching.", v)
            return None
        if str(setting) not in iface.ips[v]:
            logger.warning("Configured IPv%d address %s is not available at interface %s, this could be an error.",
                           v, setting, iface.name)
        logger.info("Using configured IPv%d address %s for interface %s", v, setting, iface.name)
        return setting

    address = next(filter(lambda a: a.is_global, map(ipaddress.ip_address, iface.ips[v])), None)
    if address is None:
        logger.warning("Could not resolve global IPv%d address for interface %s", v, iface.name)
    else:
        logger.info("Using IPv%d address %s for interface %s", v, address, iface.name)
    return address


async def load_target_host(client: AsyncClient, address: Union[IPv4Address, IPv6Address]) -> models.TargetHost:
    """
    Loads or creates the target host representing this server, and its private probe services
    :param client: control server REST client
    :param address: ip address of this server
    :return: the target host model
    """
    response = await client.get(
        format_path(control_paths.TARGET_HOST_GET_BY_ADDRESS, address=address)
    )
    if response.status_code == status.HTTP_404_NOT_FOUND:
        logger.info("Creating target host for this server with address %s", address)
        response = await client.post(
            control_paths.TARGET_HOST_CREATE,
            json={"address": str(address)}
        )
        response.raise_for_status()
        host = models.TargetHost(**response.json())
    else:
        response.raise_for_status()
        host = models.TargetHost(**response.json())

    # set all services to OK
    for service in host.services:
        await client.put(
            format_path(control_paths.TARGET_SERVICE_PUT_STATUS, service_id=service.id),
            json=Status.OK
        )
    # add private services
    await create_target_service(client, host, ProbeSeriesType.PRIVATE_UDP, settings.PROBE_PRIVATE_UDP_PORT)
    await create_target_service(client, host, ProbeSeriesType.PRIVATE_TCP, settings.PROBE_PRIVATE_TCP_PORT)
    logger.info("Loaded target host %d/%s with %d services", host.id, host.address, len(host.services))
    return host


async def create_target_service(client: AsyncClient, host: models.TargetHost, service_type: ProbeSeriesType, port: int):
    """
    Creates a target service for a host, if that host does not already have a service of the given type.
    :param client: control server REST client
    :param host: target host model
    :param service_type: probe series type
    :param port: port
    """
    if not host.has_service_type(service_type):
        logger.info("Creating target service %s for this server on port %d", service_type.name, port)
        response = await client.post(control_paths.TARGET_SERVICE_CREATE, json=models.TargetServiceCreate(
            target_host_id=host.id,
            type=service_type,
            port=port,
            status=Status.OK
        ).model_dump())
        response.raise_for_status()
        host.services.append(models.TargetService(**response.json()))


async def load_probe_series_targets(series_id: int) -> list[models.TargetServiceModel]:
    """
    Loads all targets required for a probe series from the control server.
    :param series_id: id of the probe series
    :return: list of target services
    """
    db = database.get_redis()
    async with get_control_client() as client:
        response = await client.get(format_path(control_paths.PROBE_SERIES_GET_TARGETS, series_id=series_id))
        response.raise_for_status()

        service_ids = decode_integers(response.content)
        return await load_target_services(db, client, service_ids)


async def load_target_services(
        db: Redis,
        client: AsyncClient,
        service_ids: list[int]
) -> list[models.TargetServiceModel]:
    """
    Loads a list of target services from redis, optionally querying the control server about targets that were never
    loaded before. Shuffles the list before returning them.
    :param db: redis client
    :param client: async http client
    :param service_ids: service IDs to load
    :return: list of target services, shuffled.
    """
    services = []
    missing = []
    for service_id in service_ids:
        service = await load_target_service(db, service_id)
        if service is None:
            missing.append(service_id)
        else:
            services.append(service)
    if len(missing) > 0:
        missing.sort()
        response = await client.post(
            control_paths.TARGET_SERVICE_GET_ALL,
            headers={"Content-Type": "application/octet-stream"},
            content=encode_integers(missing)
        )
        response.raise_for_status()
        for data in response.json():
            service = models.TargetServiceModel(**data)
            await save_target_service(db, service)
            services.append(service)
    random.shuffle(services)
    return services


async def load_target_service(db: Redis, service_id: int) -> Optional[models.TargetServiceModel]:
    """
    Loads a single target service from redis.
    :param db: redis client
    :param service_id: id of the target service
    :return: the target service, or None, if the service was never loaded before
    """
    service = await db.hgetall(f"target:service:{service_id}")
    if len(service) == 0:
        return None
    else:
        return models.TargetServiceModel(**service)


async def save_target_service(db: Redis, service: models.TargetServiceModel) -> None:
    """
    Saves a target service to redis.
    :param db: redis client
    :param service: service to save
    """
    mapping = service.model_dump(exclude_none=True)
    mapping["address"] = str(service.address)  # need to convert address to string
    await db.hset(f"target:service:{service.id}", mapping=mapping)


class ProbeServer(Server):
    """
    Represents the current state of the probe server. A new instance of this class is created at startup and added to
    the fastapi state dictionary, to be available in endpoint functions using the request method.
    """

    def __init__(self):
        super().__init__()
        self._cancel_scope: Optional[anyio.CancelScope] = None
        self._status = ProbeServerStatus()

        self.thread_limiter: Optional[anyio.CapacityLimiter] = None

        # local network interface and addresses
        self.interface: Optional[NetworkInterface] = None
        self.ipv4_address: Optional[IPv4Address] = None
        self.ipv6_address: Optional[IPv6Address] = None

        # local models of all probe servers, target hosts representing this host, and probe series
        self.servers: list[models.ServerModel] = []
        self.target_ipv4: Optional[models.TargetHost] = None
        self.target_ipv6: Optional[models.TargetHost] = None
        self.series: dict[int, ProbeSeries] = {}

    @property
    def status(self) -> ProbeServerStatus:
        return self._status

    async def startup(self, *, task_status: TaskStatus[None] = TASK_STATUS_IGNORED) -> None:
        """
        Starts this server by performing some preliminary checks, and executing all available probe series if the
        checks pass successfully.
        """
        # check permissions
        if os.getuid() != 0:
            raise RuntimeError("The probe server needs to be started as root user!")
        # do this here, after FastAPI has loaded the async libraries
        self.thread_limiter = anyio.CapacityLimiter(settings.WORKER_THREAD_LIMIT)

        self.resolve_networking()
        await anyio.Path(settings.SAVE_DIR).mkdir(mode=0o750, parents=True, exist_ok=True)
        task_status.started()

        async with anyio.create_task_group() as tg:
            self._cancel_scope = tg.cancel_scope
            # check connectivity
            await self.check_connectivity()

            # load this server and target
            await self.load_local_models()

            # install netfilter tables to process incoming raw packets
            await netfilter.install_table()

            # clean up download files from abrupt shutdowns
            await cleanup_download_files()
            # load probe series
            await self.load_probe_series()

            # run probe series
            while True:
                for series in self.series.values():
                    # series that are not running but have the send_probes flag set can be started
                    if series.can_run and not series.running:
                        await tg.start(series.run)
                await anyio.sleep(1)

    async def shutdown(self) -> None:
        """
        Stops this server gracefully, waits for all running probe series to finish and to release their resources.
        """
        if self._cancel_scope is not None:
            await self._cancel_scope.cancel()
        # stop all probe series
        async with anyio.create_task_group() as tg:
            for series in self.series.values():
                tg.start_soon(series.stop)
        # set all services to ERROR
        async with get_control_client() as client:
            if self.target_ipv4 is not None:
                for service in self.target_ipv4.services:
                    await client.put(
                        format_path(control_paths.TARGET_SERVICE_PUT_STATUS, service_id=service.id),
                        json=Status.ERROR
                    )
            if self.target_ipv6 is not None:
                for service in self.target_ipv6.services:
                    await client.put(
                        format_path(control_paths.TARGET_SERVICE_PUT_STATUS, service_id=service.id),
                        json=Status.ERROR
                    )
        # close redis connections
        await database.redis_pool.disconnect(inuse_connections=True)
        # remove netfilter tables
        await netfilter.remove_table()

    async def check_connectivity(self) -> None:
        """
        Checks connectivity with all required components.
        """
        timeout = 1
        tries = 0
        while True:
            logger.info("Checking connectivity...")
            if self.ipv4_address is not None:
                await self.check_ipv4_connectivity(self.ipv4_address)
                if self.status.ipv4 == Status.ERROR:
                    self.ipv4_address = None
            if self.ipv6_address is not None:
                await self.check_ipv6_connectivity(self.ipv6_address)
                if self.status.ipv6 == Status.ERROR:
                    self.ipv6_address = None
            await self.check_redis_connectivity()
            await self.check_control_connectivity()
            self.update_status()

            # require at least redis and control server
            if self.status.control == Status.OK and self.status.redis == Status.OK:
                break
            # else wait a while and try again, with exponential backoff
            tries += 1
            logger.info("Trying again in %d second(s)", timeout)
            await anyio.sleep(timeout)
            timeout = 1 if tries < 2 else 60

    async def check_redis_connectivity(self) -> None:
        """
        Checks connectivity with the redis server.
        """
        try:
            db = database.get_redis()
            if await db.ping():
                logger.info("Successfully connected to redis.")
                self.status.redis = Status.OK
                return
            else:
                logger.error("Failed to ping redis server. Make sure redis is reachable via unix socket.")
        except (FileNotFoundError, redis.exceptions.ConnectionError) as e:
            logger.error("Error trying to connect to redis: %s", e)
        except redis.exceptions.BusyLoadingError:
            logger.error("Redis is busy loading the database.")
        self.status.redis = Status.ERROR

    async def check_control_connectivity(self) -> None:
        """
        Checks connectivity with the control server and loads the authorization token, if the status check was
        successful.
        """
        try:
            async with AsyncClient(
                    base_url=str(settings.CONTROL_SERVER_URL), timeout=settings.REST_CLIENT_TIMEOUT
            ) as client:
                response = await client.get(control_paths.STATUS)
                response.raise_for_status()

                control_status = models.ControlServerStatus(**response.json())
                if control_status.status == Status.OK:
                    logger.info("Successfully connected to control server.")
                else:
                    logger.warning("Control Server reported status %s", control_status.status.name)
                self.status.control = control_status.status

                await get_control_token(client)
        except httpx.HTTPError as e:
            logger.error("Error trying to connect to control server: %s %s - %s", e.request.method, e.request.url, e)
            self.status.control = Status.ERROR

    def resolve_networking(self) -> None:
        """
        Loads the network interface that will be used to send probes from, as well as this hosts IP addresses.
        """
        if settings.PROBE_INTERFACE is not None:
            interface = resolve_iface(settings.PROBE_INTERFACE)
            if not interface.is_valid():
                raise RuntimeError("Cannot resolve interface named " + settings.PROBE_INTERFACE)
        else:
            interface = get_working_if()
            logger.info("No probe interface specified, using %s", interface.name)
        self.interface = interface
        self.ipv4_address = resolve_ip_address(interface, 4, settings.PROBE_IPV4_ADDRESS)
        self.ipv6_address = resolve_ip_address(interface, 6, settings.PROBE_IPV6_ADDRESS)

    def get_interface(self) -> NetworkInterface:
        """
        :return: the network interface to send probes from
        """
        if self.interface is None:
            raise ValueError("interface could not be resolved")
        return self.interface

    async def load_local_models(self) -> None:
        """
        Loads the server and target host models for this control server.
        """
        async with get_control_client() as client:
            # load this server and check addresses are correct
            response = await client.get(format_path(control_paths.SERVER_GET, server_id=settings.SERVER_ID))
            response.raise_for_status()
            server = models.ServerModel(**response.json())
            if self.ipv4_address is not None and self.ipv4_address not in server.addresses:
                response = await client.post(
                    format_path(control_paths.SERVER_POST_ADDRESS, server_id=settings.SERVER_ID),
                    json=str(self.ipv4_address)
                )
                response.raise_for_status()
            if self.ipv6_address is not None and self.ipv6_address not in server.addresses:
                response = await client.post(
                    format_path(control_paths.SERVER_POST_ADDRESS, server_id=settings.SERVER_ID),
                    json=str(self.ipv6_address)
                )
                response.raise_for_status()
            for address in server.addresses:
                if address != self.ipv4_address and address != self.ipv6_address:
                    response = await client.delete(format_path(
                        control_paths.SERVER_DELETE_ADDRESS,
                        server_id=settings.SERVER_ID,
                        address=address
                    ))
                    response.raise_for_status()

            # load all servers
            response = await client.get(control_paths.SERVER_GET_ALL)
            response.raise_for_status()
            self.servers = [models.ServerModel(**s) for s in response.json()]

            # load targets representing this host
            if self.ipv4_address is not None:
                self.target_ipv4 = await load_target_host(client, self.ipv4_address)
            if self.ipv6_address is not None:
                self.target_ipv6 = await load_target_host(client, self.ipv6_address)

    async def load_probe_series(self) -> None:
        """
        Fetches all available probe series from the control server and performs actions as necessary
        """
        async with get_control_client() as client:
            response = await client.get(control_paths.PROBE_SERIES_GET_AVAILABLE)
            response.raise_for_status()
            available = [models.ProbeSeries(**series) for series in response.json()]
            logger.info(f"Fetched {len(available)} available probe series.")

        for series_model in available:
            if series_model.type == ProbeSeriesType.PUBLIC_ICMP_PING:
                series_class = ICMPProbeSeries
                probe_class = ICMPProbe
            elif series_model.type == ProbeSeriesType.PUBLIC_UDP_DNS:
                series_class = UDPDNSProbeSeries
                probe_class = UDPDNSProbe
            elif series_model.type == ProbeSeriesType.PUBLIC_TCP_DNS:
                series_class = TCPDNSProbeSeries
                probe_class = TCPDNSProbe
            elif series_model.type == ProbeSeriesType.PUBLIC_TCP_SMTP:
                series_class = TCPSMTPProbeSeries
                probe_class = TCPSMTPProbe
            elif series_model.type == ProbeSeriesType.PRIVATE_UDP:
                series_class = UDPProbeSeries
                probe_class = UDPProbe
            elif series_model.type == ProbeSeriesType.PRIVATE_TCP:
                series_class = TCPProbeSeries
                probe_class = TCPProbe
            else:
                logger.error("Cannot load probe series %d of type %s, not yet implemented.",
                             series_model.id, series_model.type)
                continue

            local_targets = []
            if self.target_ipv4:
                local_targets.append(self.target_ipv4)
            if self.target_ipv6:
                local_targets.append(self.target_ipv6)
            probe_series = series_class(
                probe_class, series_model, local_targets, self.servers, self.interface, self.thread_limiter,
                self.ipv4_address, self.ipv6_address
            )
            targets = await load_probe_series_targets(series_model.id)
            if targets is not None:
                await probe_series.add_targets(targets)
            self.series[series_model.id] = probe_series
            # start if series is ongoing or paused
            if series_model.status == ProbeSeriesStatus.ONGOING or series_model.status == ProbeSeriesStatus.PAUSED:
                probe_series.start()

    def has_probe_series(self, series_id: int) -> bool:
        """
        Checks if a probe series was loaded.
        :param series_id: probe series id
        :return: true, if this probe server has loaded the probe series
        """
        return series_id in self.series

    def get_probe_series(self, series_id: int) -> ProbeSeries:
        """
        Gets the loaded probe series.
        :param series_id: probe series id
        :return: loaded probe series
        """
        return self.series[series_id]


# global server instance
probe_server = ProbeServer()
