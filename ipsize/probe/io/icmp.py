import os
from typing import Optional, Union

from anyio.abc import TaskStatus
from anyio.from_thread import BlockingPortal

from scapy.layers.inet import ICMP, IP
from scapy.layers.inet6 import ICMPv6EchoReply, ICMPv6EchoRequest, IPv6

from ipsize.probe.io.monitor import GuardedMonitor, GuardedSniffer
from ipsize.probe.io.base_probe import PublicProbe
from ipsize.probe.io.base_series import PublicProbeSeries


class ICMPProbe(PublicProbe):
    """
    Relevant RFCs:
    ICMP:   https://datatracker.ietf.org/doc/html/rfc792
    ICMPv6: https://datatracker.ietf.org/doc/html/rfc4443#section-4
    """

    # four bytes ICMP(v6) header plus four bytes id and sequence number
    MIN_SIZE = 8

    def assemble_request(self) -> Union[IP, IPv6]:
        """
        Assembles an ICMP Echo Request packet with the ID set to the series ID and the sequence number set to this
        probes sequence number.
        :return: the ICMP Echo Request packet
        """
        ip = self.target.get_ip_headers(self.saddr)
        self.data = os.urandom(self.size - len(ip) - self.MIN_SIZE)
        if self.target.address.version == 4:
            request = ip / ICMP(id=self.series_id, seq=self.sequence_number) / self.data
        else:
            request = ip / ICMPv6EchoRequest(id=self.series_id, seq=self.sequence_number, data=self.data)
        return request

    def match_response(self, response: Union[IP, IPv6]) -> bool:
        """
        Checks if the incoming packet contains an ICMP Echo Reply to this probes request. Marks an anomaly if the reply
        payload does not match the request payload.
        :param response: the response packet
        :return: true, if the packet contains an ICMP Echo Reply to this probes request.
        """
        if self.target.address.version == 4:
            sid = response[ICMP].id
            seq = response[ICMP].seq
            data = bytes(response[ICMP].payload)
        else:
            sid = response[ICMPv6EchoReply].id
            seq = response[ICMPv6EchoReply].seq
            data = response[ICMPv6EchoReply].data
        # match on ICMP identifier and sequence number
        if sid != self.series_id or seq != self.sequence_number:
            return False

        self.response = response
        self.t_recv = response.time
        if len(response) != self.size:
            self.anomaly = f"expected {self.size} bytes"
        elif data != self.data:
            self.anomaly = "not matching"
        return True


class ICMPProbeSeries(PublicProbeSeries[ICMPProbe]):

    def get_bpf(self) -> Optional[str]:
        # Echo Reply for ICMP and ICMPv6
        return "icmp[icmptype] = 0 || icmp6[icmp6type] = 129"

    def get_nft_rule(self) -> Optional[str]:
        return None

    def create_monitor(self, portal: BlockingPortal, task_status: TaskStatus[None]) -> Optional[GuardedMonitor]:
        return GuardedSniffer(portal, self.get_bpf(), self.incoming, task_status)

