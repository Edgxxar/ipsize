from ipsize.models.enum import TCPState


class TCPFlowError(BaseException):
    """
    Raised when an error occurs in the TCP state machine.
    """

    def __init__(self, state: TCPState, anomaly: str, abort_probe: bool = True):
        """
        :param state: the state of the TCP state machine as this error occurred
        :param anomaly: description of the error
        :param abort_probe: if set to true, the outstanding probes are aborted
        """
        self.state = state
        self.anomaly = anomaly
        self.packet = None
        self.abort_probe = abort_probe
