import logging
from typing import Union

from scapy.interfaces import NetworkInterface
from scapy.layers.inet import IP
from scapy.layers.inet6 import IPv6
from scapy.sendrecv import send
from scapy.supersocket import SuperSocket

from ipsize.probe.config import settings

logger = logging.getLogger(settings.LOGGER_NAME)


def send_blocking(p: Union[list[Union[IP, IPv6]], IP, IPv6], iface: NetworkInterface, socket: SuperSocket) -> None:
    try:
        send(p, iface, socket=socket, verbose=0)
    except BaseException as e:
        logger.error("Error while trying to send packet with scapy:")
        logger.error(p.summary())
        logger.exception(e)
