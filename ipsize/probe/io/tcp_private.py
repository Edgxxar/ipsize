import os
from typing import Optional, Union

from scapy.layers.inet import IP, TCP
from scapy.layers.inet6 import IPv6

from ipsize.probe.io.base_probe import PrivateProbe
from ipsize.probe.io.base_series import PrivateProbeSeries


class TCPProbe(PrivateProbe):
    # 20 bytes TCP header
    MIN_SIZE = 20

    def assemble_request(self) -> Union[IP, IPv6]:
        # assemble up to TCP part to get the length
        tcp = self.target.get_ip_headers(self.saddr) / TCP(
            sport=self.sport,
            dport=self.target.port,
            seq=self.sequence_number,  # use TCP sequence to carry probe sequence
            ack=self.series_id,  # use TCP ack number to carry series ID
            flags="A"
        )
        # payload is random padding
        if self.size - len(tcp) > 0:
            return tcp / os.urandom(self.size - len(tcp))
        return tcp


class TCPProbeSeries(PrivateProbeSeries[TCPProbe]):

    def get_nft_rule(self) -> Optional[str]:
        return f"tcp dport {self.config.port} queue num {self.id}"

    @classmethod
    def extract_identifiers(cls, packet: Union[IP, IPv6]) -> tuple[int, int]:
        # series_id and sequence number are contained in the ack and seq fields of the TCP header
        return packet[TCP].ack, packet[TCP].seq

    @classmethod
    def get_dport(cls, packet: Union[IP, IPv6]) -> int:
        return packet[TCP].dport
