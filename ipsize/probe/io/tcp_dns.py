from ipaddress import IPv4Address, IPv6Address
from typing import Optional, Union

from scapy.layers.dns import DNS, DNSQR
from scapy.layers.inet import IP, TCP
from scapy.layers.inet6 import IPv6
from scapy.packet import NoPayload, Packet

from ipsize.probe.io.base_probe import PublicTCPProbe
from ipsize.probe.io.base_series import PublicTCPProbeSeries
from ipsize.probe.io.monitor import TCPStateMachine
from ipsize.probe.io.target import Target
from ipsize.probe.io.udp_dns import UDPDNSProbe


class TCPDNSProbe(PublicTCPProbe, UDPDNSProbe):
    """
    Relevant RFCs:
    TCP Encoding:   https://datatracker.ietf.org/doc/html/rfc1035#section-4.2.2
    """

    # 32 bytes TCP, 14 bytes DNS header (incl. length), 23 bytes query,
    # answer: 2 bytes RRname (token), 2 bytes type, 2 bytes class, 4 bytes TTL, 2 bytes data length
    PREAMBLE = 81
    # min size is one byte for the empty text record
    MIN_SIZE = PREAMBLE + 1
    # magic number is 1480, everything above gets truncated
    MAX_SIZE = 1480

    def __init__(
            self,
            series_id: int,
            server_id: int,
            target: Target,
            tcp: TCPStateMachine,
            saddr: Union[None, IPv4Address, IPv6Address] = None,
            sport: Optional[int] = None,
            size: int = 60
    ):
        super().__init__(series_id, server_id, target, tcp, saddr, sport, size)
        self.response_data = bytearray()

    def assemble_payload(self) -> tuple[Packet, int]:
        """
        Assembles a DNS query with the DNS ID set to the sequence number of this probe.
        :return: the DNS query payload
        """
        # decrease size of preamble if this endpoint does not use TCP timestamps
        if not self.tcp.has_timestamps():
            self.PREAMBLE -= 12

        ip = self.target.get_ip_headers(self.saddr)
        query = DNSQR(qname=self.get_qname(self.size, len(ip)), qtype="TXT")
        payload = DNS(id=self.sequence_number, rd=1, qd=query)
        length = len(payload) + 2  # DNS over TCP has two-byte length field
        return payload, length

    def match_response(self, response: Union[IP, IPv6]) -> bool:
        """
        Checks if the incoming packet contains a DNS response and the DNS ID in the header matches the sequence number
        of this probe. Marks an anomaly if the response code is not zero, the message is truncated, or not of the
        expected size.
        :param response: the response packet
        :return: true, if the packet contains a DNS reply to this probes request
        """
        segment = response[TCP]
        if not segment.flags.A:
            return False

        # mark request received if package is acknowledged
        if self.request_ack is not None and not self.request_received:
            if self.tcp.get_segment_ack(segment) >= self.request_ack:
                self.request_received = True

        # need payload to match response
        if isinstance(segment.payload, NoPayload):
            return False

        # if full DNS response is contained, match it
        if DNS in response:
            return self.match_dns_response(response)

        # else try to assemble DNS response from multiple packets
        self.response_data.extend(bytes(segment.payload))
        if len(self.response_data) < 2:
            return False
        length = int.from_bytes(self.response_data[0:2], "big", signed=False)
        if len(self.response_data) - 2 < length:  # wait until the whole response is received
            return False
        # try to reassemble the DNS response without the length field
        try:
            dns = DNS(self.response_data[2:])
        except BaseException:
            return False
        # match on DNS message ID
        if dns.id != self.sequence_number:
            self.response_data.clear()
            return False

        self.response = response
        self.t_recv = response.time
        self.anomaly = "response was fragmented"
        return True


class TCPDNSProbeSeries(PublicTCPProbeSeries[TCPDNSProbe]):
    # most DNS implementations only answer one query per TCP connection
    CLOSE_IMMEDIATELY = True
