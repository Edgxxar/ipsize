import os
from typing import Optional, Union

from scapy.layers.inet import IP, UDP
from scapy.layers.inet6 import IPv6

from ipsize.probe.io.base_probe import PrivateProbe
from ipsize.probe.io.base_series import PrivateProbeSeries


class UDPProbe(PrivateProbe):
    # eight bytes UDP header plus two bytes series id and four bytes sequence number
    MIN_SIZE = 14

    def assemble_request(self) -> Union[IP, IPv6]:
        # assemble up to UDP part to get the length
        udp = self.target.get_ip_headers(self.saddr) / UDP(sport=self.sport, dport=self.target.port)
        # payload is two bytes series id and four bytes sequence number, followed by random padding
        series = self.series_id.to_bytes(2, "big", signed=False)
        seq_no = self.sequence_number.to_bytes(4, "big", signed=False)
        payload = series + seq_no
        return udp / payload / os.urandom(self.size - len(udp) - len(payload))


class UDPProbeSeries(PrivateProbeSeries[UDPProbe]):

    def get_nft_rule(self) -> Optional[str]:
        return f"udp dport {self.config.port} queue num {self.id}"

    @classmethod
    def extract_identifiers(cls, packet: Union[IP, IPv6]) -> tuple[int, int]:
        payload = bytes(packet[UDP].payload)
        # payload has to be at least six bytes
        if len(payload) < 6:
            return 0, 0
        series_id = int.from_bytes(payload[0:2], "big", signed=False)
        sequence_number = int.from_bytes(payload[2:6], "big", signed=False)
        return series_id, sequence_number

    @classmethod
    def get_dport(cls, packet: Union[IP, IPv6]) -> int:
        return packet[UDP].dport
