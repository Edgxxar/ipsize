import asyncio
import collections
import ipaddress
import logging
import random
import re
import time
import uuid
from abc import ABC, abstractmethod
from ipaddress import IPv4Address, IPv6Address
from typing import AsyncIterator, Generic, Optional, Type, TypeVar, Union

import anyio
import brotli
import orjson
from anyio import TASK_STATUS_IGNORED
from anyio.abc import TaskStatus
from anyio.from_thread import BlockingPortal
from anyio.streams.file import FileReadStream, FileWriteStream
from redis.asyncio.client import Redis
from scapy.interfaces import NetworkInterface
from scapy.layers.inet import IP, TCP
from scapy.layers.inet6 import IPv6, L3RawSocket6
from scapy.supersocket import L3RawSocket, SuperSocket

from ipsize import models, netfilter
from ipsize.models.enum import ProbeSeriesStatus, ProbeSeriesType
from ipsize.probe.config import settings
from ipsize.probe.io.base_probe import Probe, PublicProbe, PublicTCPProbe
from ipsize.probe.io.exception import TCPFlowError
from ipsize.probe.io.monitor import GuardedMonitor, GuardedNFQConsumer, GuardedNFQTCPConsumer
from ipsize.probe.io.target import Target, TargetDict
from ipsize.probe.persistence import database
from ipsize.util import default_encode

logger = logging.getLogger(settings.LOGGER_NAME)

global_throttle: anyio.Lock = anyio.Lock()


def open_ipv4_socket(iface: NetworkInterface) -> SuperSocket:
    """
    Opens a raw socket, that con be used to send IPv4 packets. This method blocks.
    :return: opened socket ready to send packets
    """
    return L3RawSocket(iface=iface, promisc=False)


def open_ipv6_socket(iface: NetworkInterface) -> SuperSocket:
    """
    Opens a raw socket, that con be used to send IPv6 packets. This method blocks.
    :return: opened socket ready to send packets
    """
    return L3RawSocket6(iface=iface, promisc=False)


async def save_download_meta(db: Redis, download: models.ProbeDownloadAvailable) -> None:
    """
    Saves the download file metadata in redis.
    :param db: redis connection
    :param download: download metadata
    """
    await db.hset(f"download:file:{download.file_name}", mapping={
        "probe_series_id": download.probe_series_id,
        "server_id": download.server_id,
        "packet_count": download.packet_count,
        "file_name": str(download.file_name),
        "file_size": download.file_size,
        "flushed": str(download.flushed)
    })
    await db.sadd("download:all", str(download.file_name))


async def del_download_meta(db: Redis, file_name: str) -> None:
    """
    Deletes the download file metadata from redis.
    :param db: redis connection
    :param file_name: download file name
    """
    await db.delete(f"download:file:{file_name}")
    await db.srem("download:all", file_name)


async def is_download_available(db: Redis, file_name: str) -> bool:
    """
    Checks if a download file exists in redis.
    :param db: redis connection
    :param file_name: download file name
    :return: true, if the download file metadata exists
    """
    return bool(await db.sismember("download:all", file_name))


async def get_downloads_available(db: Redis, flushed: bool = True) -> AsyncIterator[models.ProbeDownloadAvailable]:
    """
    Get all available download files.
    :param db: redis connection
    :param flushed: only return flushed download files
    :return: iterator over download file metadata
    """
    for file_name in await db.smembers("download:all"):
        download = models.ProbeDownloadAvailable(**await db.hgetall(f"download:file:{file_name}"))
        if not flushed or download.flushed:
            yield download


async def cleanup_download_files() -> None:
    """
    Clean up all empty download files, fix file size metadata if incorrect.
    """
    db = database.get_redis()
    # check files in redis
    async for download in get_downloads_available(db, False):
        path = anyio.Path(settings.SAVE_DIR, download.file_name)
        if not await path.is_file():
            logger.warning("Save file %s does not exist, deleting metadata", download.file_name)
            await del_download_meta(db, str(download.file_name))
            continue
        stat = await path.stat()
        if stat.st_size == 0:
            logger.warning("Save file %s is empty, deleting file and metadata", download.file_name)
            await path.unlink()
            await del_download_meta(db, str(download.file_name))
            continue
        if stat.st_size != download.file_size or download.packet_count == 0 or not download.flushed:
            logger.warning("Save file %s was not flushed to disk correctly, analysing", download.file_name)
            download.packet_count = await analyse_download_file(path)
            download.file_size = stat.st_size
            download.flushed = True
            await save_download_meta(db, download)

    # check files on disk
    name_regex = re.compile(
        "^ps([0-9]+)-([0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})\\.json(\\.br)?$"
    )
    async for file in anyio.Path(settings.SAVE_DIR).glob("*.json*"):
        if await is_download_available(db, file.name):
            continue
        match = name_regex.fullmatch(file.name)
        if not match:
            logger.warning("File %s in save directory does not appear to be a valid download file!", file.name)
            continue
        stat = await file.stat()
        if stat.st_size == 0:
            logger.warning("Found empty file %s in save directory but not in redis, deleting", file.name)
            await file.unlink()
            continue
        logger.warning("Found file %s in save directory but not in redis, analysing", file.name)
        packet_count = await analyse_download_file(file)
        if packet_count == 0:
            logger.warning("File %s in save directory is not empty but does not appear to contain valid data, ignoring",
                           file.name)
            continue
        download = models.ProbeDownloadAvailable(
            probe_series_id=match.group(1),
            server_id=settings.SERVER_ID,
            packet_count=packet_count,
            file_name=file.name,
            file_size=stat.st_size,
            flushed=True
        )
        await save_download_meta(db, download)


async def analyse_download_file(path: anyio.Path) -> int:
    """
    Analyses a download file, counting the packets saved in the file.
    :param path: path to file
    :return: packet count in the file
    """
    packet_count = 0
    compressed = path.name.endswith(".br")
    async with await FileReadStream.from_path(path) as file:  # type: AsyncIterator[bytes]
        if compressed:
            decompressor = brotli.Decompressor()
        try:
            async for chunk in file:
                # simply count RS characters
                if compressed:
                    packet_count += decompressor.process(chunk).count(b"\x1E")
                else:
                    packet_count += chunk.count(b"\x1E")
            if compressed and not decompressor.is_finished():
                logger.error("File compression was not flushed completely: " + path.name)
        except brotli.error as e:
            logger.error("Error while decompressing file: " + path.name)
            logger.exception(e)
    return packet_count


T = TypeVar("T", bound=Probe)
P = TypeVar("P", bound=PublicProbe)
C = TypeVar("C", bound=PublicTCPProbe)


class ProbeSeries(ABC, Generic[T]):
    """
    Base class for all probe series, handles the setup, execution and teardown logic.
    """

    MAX_INCOMING = 3

    def __init__(
            self,
            probe_class: Type[T],
            model: models.ProbeSeries,
            local_targets: list[models.TargetHost],
            servers: list[models.ServerModel],
            interface: NetworkInterface,
            thread_limiter: anyio.CapacityLimiter,
            ipv4_address: Optional[IPv4Address] = None,
            ipv6_address: Optional[IPv6Address] = None
    ):
        self.probe_class: Type[T] = probe_class
        self.id: int = model.id
        self.type: ProbeSeriesType = model.type
        self.status: ProbeSeriesStatus = model.status
        self.config: models.ProbeSeriesConfig = model.config
        self.local_targets: list[models.TargetHost] = local_targets
        self.servers: list[models.ServerModel] = servers

        self.interface: NetworkInterface = interface
        self.ipv4_address: Optional[IPv4Address] = ipv4_address
        self.ipv6_address: Optional[IPv6Address] = ipv6_address
        self.socket_ipv4: Optional[SuperSocket] = None
        self.socket_ipv6: Optional[SuperSocket] = None

        self.targets: TargetDict = TargetDict()
        self.thread_limiter: anyio.CapacityLimiter = thread_limiter

        # incoming packets
        self.incoming: collections.deque[Union[IP, IPv6, TCPFlowError]] = collections.deque()
        # monitor thread
        self.monitor: Optional[GuardedMonitor] = None
        # probes to save to disk
        self.save: asyncio.Queue[dict] = asyncio.Queue()

        # lifecycle flags
        self.can_run: bool = False
        self.running: bool = False
        # save lock to write to the save file
        self.save_lock: anyio.CapacityLimiter = anyio.CapacityLimiter(1)
        self.flush_save_file = False
        # send lock, used to pause probe sending temporarily
        self.send_lock: anyio.CapacityLimiter = anyio.CapacityLimiter(1)
        # stopped event, set as last step in teardown to signal all resources have been cleaned up
        self.stopped: Optional[anyio.Event] = None

        # some statistics
        self.sent = 0
        self.received = 0
        self.t_start = 0
        self.t_stop = 0

    def start(self) -> None:
        """
        Sets the can_run flag to true and resets the stop event, such that this probe series can be started
        by the main server loop.
        """
        # make sure sending is not started if we are paused
        if self.status == ProbeSeriesStatus.PAUSED:
            self.send_lock.acquire_on_behalf_of_nowait(self.id)
            self.save_lock.acquire_on_behalf_of_nowait(self.id)
        elif self.status != ProbeSeriesStatus.ONGOING:
            raise ValueError("can only start ONGOING or PAUSED probe series, this series is " + self.status.name)
        self.can_run = True
        self.stopped = anyio.Event()

    async def stop(self) -> None:
        """
        Stops the send loop and waits for the probe series to release all resources
        """
        self.can_run = False
        # release the send lock if this probe series is currently paused, otherwise the send loop will be stuck
        if self.status == ProbeSeriesStatus.PAUSED:
            self.send_lock.release_on_behalf_of(self.id)
            self.save_lock.release_on_behalf_of(self.id)
        if self.stopped is not None:
            await self.stopped.wait()

    async def pause(self) -> None:
        """
        Pauses the probe sending. Blocks until the current send task has finished and the current save file has been
        flushed to disk.
        """
        logger.info("[PS-%03d] Pausing probe sending...", self.id)
        await self.send_lock.acquire_on_behalf_of(self.id)
        self.status = ProbeSeriesStatus.PAUSED
        # write save file to disk
        self.flush_save_file = True
        await self.save_lock.acquire_on_behalf_of(self.id)

    def resume(self) -> None:
        """
        Resumes the probe sending and opens a new save file.
        """
        logger.info("[PS-%03d] Resuming probe sending...", self.id)
        self.status = ProbeSeriesStatus.ONGOING
        self.send_lock.release_on_behalf_of(self.id)
        self.save_lock.release_on_behalf_of(self.id)

    async def _setup(self) -> None:
        """
        Sets up this probe series.
        """
        if self.running:
            raise ValueError("call the stop method before calling run again!")
        self.running = True
        logger.debug("[PS-%03d] Setting up resources...", self.id)
        # open sockets for sending probes
        if self.ipv4_address is not None:
            self.socket_ipv4 = await anyio.to_thread.run_sync(open_ipv4_socket, self.interface)
        if self.ipv6_address is not None:
            self.socket_ipv6 = await anyio.to_thread.run_sync(open_ipv6_socket, self.interface)
        # add chain to nftables
        await self.nft_add_chain()
        rule = self.get_nft_rule()
        if rule is not None:
            await self.nft_add_rule(rule)
        logger.debug("[PS-%03d] Setup complete", self.id)

    async def _teardown(self) -> None:
        """
        Releases all resources held by this probe series and finally signals the stopped event.
        """
        logger.debug("[PS-%03d] Tearing down resources...", self.id)
        # remove chain from nftables
        await self.nft_del_chain()
        # close sockets
        if self.socket_ipv4 is not None and not self.socket_ipv4.closed:
            await anyio.to_thread.run_sync(self.socket_ipv4.close)
        if self.socket_ipv6 is not None and not self.socket_ipv6.closed:
            await anyio.to_thread.run_sync(self.socket_ipv6.close)
        # trigger stopped event
        await self.stopped.set()
        # reset
        self.running = False
        logger.debug("[PS-%03d] Teardown complete", self.id)

    def get_socket(self, address: Union[IPv4Address, IPv6Address]) -> SuperSocket:
        """
        Gets the correct socket required to send packets to a target.
        :param address: IP address to send packets to
        :return: socket that can be used to send packets to the given target
        """
        return self.socket_ipv4 if address.version == 4 else self.socket_ipv6

    @abstractmethod
    def get_bpf(self) -> Optional[str]:
        """
        Specifies a Berkeley Packet Filter which is used to filter incoming packets before they are passed to the
        matching loop. This filter should be as specific as possible, as the OS is always faster at filtering
        packets than the user process, and this brings a significant performance increase.
        See also: https://scapy.readthedocs.io/en/latest/usage.html#performance-of-scapy
        """

    @abstractmethod
    def get_nft_rule(self) -> Optional[str]:
        """
        Specifies any netfilter rules which should be added to the chain for this series during setup.
        """

    @abstractmethod
    def create_monitor(self, portal: BlockingPortal, task_status: TaskStatus[None]) -> Optional[GuardedMonitor]:
        """
        Creates a new monitor thread, receiving incoming packets for this probe series.
        :param portal: blocking portal to guard the monitor thread
        :param task_status: signal that the monitor thread has started
        :return: a monitor for this probe series, or none if a monitor cannot be created for some reason
        """

    @abstractmethod
    async def send_probe(self, probe: T) -> None:
        """
        Task to send a single probe asynchronously. Depending on the implementation, also wait for a response or
        a timeout to occur. Saves the probe in redis.
        :param probe: the probe to send
        """

    @abstractmethod
    async def receive_packets(self, done: anyio.Event) -> None:
        """
        Task to consume packets from the incoming queue continuously. Increases the number of received probes any time
        a relevant packet was received.
        :param done: event to signal when this task has finished
        """

    def save_probe(self, probe: T) -> None:
        """
        Enqueues the probe to be saved to disk, using the ``encode`` method.
        """
        logger.debug(probe)
        self.save.put_nowait(probe.encode())

    async def nft_add_chain(self) -> None:
        """
        Gets called during setup, installs the base chain for this probe series. The chain uses the input hook with
        priority 'raw' (-300), to process all incoming packets before they get filtered by the normal firewall rules.
        Individual probe series implementations can coll the ``nft_add_rule`` method to add rules to this chain, for
        example to queue specific packets for processing.
        """
        await netfilter.add_chain(f"probeseries-{self.id}", "{ type filter hook input priority raw; }")

    async def nft_del_chain(self) -> None:
        """
        Gets called during teardown, removes the base chain for this probe series.
        """
        await netfilter.delete_chain(f"probeseries-{self.id}")

    async def nft_add_rule(self, rule: str) -> None:
        """
        Adds the specified rule to the chain for this probe series.
        :param rule: the rule to add
        """
        await netfilter.add_rule(f"probeseries-{self.id}", rule)

    async def add_targets(self, services: list[models.TargetServiceModel]) -> None:
        """
        Adds new targets to this probe series, loading their state from redis.
        :param services: targets to add
        """
        # skip any targets that cannot be used
        if self.ipv4_address is None:
            services = [service for service in services if service.address.version != 4]
        if self.ipv6_address is None:
            services = [service for service in services if service.address.version != 6]

        # load target state from the database
        redis = database.get_redis()
        for service in services:
            if service.id in self.targets:
                continue
            self.targets.add(await Target.load_from_redis(self.id, service, redis))

    async def remove_targets(self, services: list[int]) -> None:
        """
        Removes targets from this probe series and saves their state in redis.
        :param services: list of target service IDs to remove
        """
        self.targets.remove_all(services)

    def choose_next_target(self) -> Optional[Target]:
        """
        Chooses the next target to probe.
        :return: the next target to send a packet to
        """
        if len(self.targets) == 0:
            return None
        target = self.targets.get_random()
        target.inc_sequence_number()
        return target

    def choose_next_size(self, target: Target) -> int:
        """
        Chooses the packet size for the next probe
        :param target: the target the next probe is destined for
        :return: The next packet size
        """
        # minimum size depends on L3 headers (20 bytes IPv4, 40 bytes IPv6)
        ip_size = 20 if target.address.version == 4 else 40
        return random.randint(self.probe_class.MIN_SIZE + ip_size, self.probe_class.MAX_SIZE)

    def create_next_probe(self, target: Target) -> T:
        """
        Creates a new probe with random size
        :param target: the target for the probe
        :return: a probe instance
        """
        size = self.choose_next_size(target)
        saddr = self.ipv4_address if target.address.version == 4 else self.ipv6_address
        return self.probe_class(self.id, settings.SERVER_ID, target, saddr, self.config.port, size)

    def get_server_by_address(self, address: Union[IPv4Address, IPv6Address]) -> Optional[models.ServerModel]:
        """
        Finds the server that has sent this probe packet by its source address.
        :param address: address of the probe server
        :return: the server with the given address
        """
        return next((server for server in self.servers if address in server.addresses), None)

    async def run(self, *, task_status: TaskStatus[None] = TASK_STATUS_IGNORED) -> None:
        """
        Executes this probe series. Runs in a shielded cancel scope to tear down all resources correctly on server stop
        """
        with anyio.CancelScope(shield=True):
            # setup myself, specific probe series might overwrite this
            await self._setup()
            task_status.started()

            self.sent = 0
            self.received = 0
            self.t_start = time.time()
            # start with extra task group, one task per probe
            async with anyio.create_task_group() as tg:
                logger.info("[PS-%03d] Starting probe series...", self.id)
                # start network monitor
                await tg.start(self._start_monitor)

                # start consumer
                done = anyio.Event()  # event is set by the receive_packets task
                tg.start_soon(self.receive_packets, done)
                # start task to save data to disk
                tg.start_soon(self.save_data, done)

                # send probes until stop method is called
                while self.can_run:
                    # throttle sending to process incoming packets
                    async with global_throttle:
                        if len(self.incoming) > self.MAX_INCOMING:
                            logger.debug("[PS-%03d] throttling down, %d threads used, %d incoming",
                                         self.id, self.thread_limiter.borrowed_tokens, len(self.incoming))
                            while len(self.incoming) > 0:
                                await anyio.sleep(self.config.interval)
                    # make sure sending is not paused, wait for resume if necessary
                    async with self.send_lock:
                        if not self.can_run:  # double check series was not stopped
                            break
                        target = self.choose_next_target()
                        if target is not None:
                            probe = self.create_next_probe(target)
                            tg.start_soon(self.send_probe, probe)
                        else:
                            logger.debug("[PS-%03d] no targets available", self.id)
                    await anyio.sleep(self.config.interval)
                logger.info("[PS-%03d] Stopped probe sending, waiting for last incoming packets...", self.id)

                # wait for last probes to be matched or time out
                await done.wait()
                logger.info("[PS-%03d] Collected remaining packets, stopping monitor...", self.id)

                # stop monitor
                if self.monitor is not None:
                    await self.monitor.stop()

                # write save file to disk
                self.flush_save_file = True
                async with self.save_lock:
                    pass

            # exit task group context, we are done, print summary
            self.t_stop = time.time()
            logger.info("[PS-%03d] Stopped probe series, sent %d probes and received %d probes in %.2f seconds",
                        self.id, self.sent, self.received, self.t_stop - self.t_start)

            # teardown
            await self._teardown()

    async def _start_monitor(self, *, task_status: TaskStatus[None] = TASK_STATUS_IGNORED):
        """
        Task to start the sniffer thread. Waits for the sniffer to be stopped by another task.
        """
        async with BlockingPortal() as portal:
            self.monitor = self.create_monitor(portal, task_status)
            if self.monitor is not None:
                self.monitor.start()
                await portal.sleep_until_stopped()
            else:
                task_status.started()
                await portal.stop()

    async def _send_probe(self, probe: Probe) -> None:
        """
        Internal method to send the probe using the appropriate socket. Manages the target state and the amount of
        probes sent statistic for this series.
        :param probe: probe to send
        """
        await probe.send_request(self.interface, self.get_socket(probe.target.address), self.thread_limiter)
        self.sent += 1
        await probe.target.save_probe_sent(
            self.id,
            probe.sequence_number + 1,
            probe.t_sent,
            database.get_redis()
        )

    async def save_data(self, done: anyio.Event) -> None:
        """
        Task to save all probe data to disk. Rotates the save file if the flush_save_file flag is set.
        :param done: event to signal that all outstanding probes have been put into the save queue
        """
        # run until no more probes will be put into the save queue, and the queue is empty
        while self.can_run or not done.is_set() or not self.save.empty():
            async with self.save_lock:
                if not self.can_run and done.is_set() and self.save.empty():  # double check series was not stopped
                    break
                probe_count = 0
                file_name = f"ps{self.id}-{uuid.uuid4()}.json.br"

                # set metadata
                download = models.ProbeDownloadAvailable(
                    probe_series_id=self.id,
                    server_id=settings.SERVER_ID,
                    packet_count=probe_count,
                    file_name=file_name,
                    file_size=0
                )
                await save_download_meta(database.get_redis(), download)

                # write packets to disk
                path = anyio.Path(settings.SAVE_DIR, file_name)
                await path.touch(0o640)
                async with await FileWriteStream.from_path(path) as file:  # type: FileWriteStream
                    compressor = brotli.Compressor(brotli.MODE_TEXT)
                    while not self.flush_save_file:  # append data as long as flag is not set
                        with anyio.move_on_after(self.config.timeout) as data_scope:
                            data = await self.save.get()
                        if data_scope.cancel_called:
                            continue
                        try:
                            encoded = orjson.dumps(
                                data,
                                default=default_encode,
                                option=orjson.OPT_APPEND_NEWLINE
                            )
                        except orjson.JSONEncodeError as e:
                            logger.error("[PS-%03d] cannot encode probe as JSON: %s", self.id, e)
                        await file.send(compressor.process(b"\x1E" + encoded))
                        probe_count += 1
                    await file.send(compressor.finish())

                if probe_count == 0:
                    # delete file if no packets were saved
                    await path.unlink()
                    await del_download_meta(database.get_redis(), str(download.file_name))
                    logger.info("[PS-%03d] no probes saved to disk", self.id)
                else:
                    # otherwise update metadata
                    stat = await path.stat()
                    download.packet_count = probe_count
                    download.file_size = stat.st_size
                    download.flushed = True
                    await save_download_meta(database.get_redis(), download)
                    logger.info("[PS-%03d] %d probes saved to disk", self.id, probe_count)

                # reset save flag
                self.flush_save_file = False
        logger.info("[PS-%03d] Exiting save task", self.id)


class OutstandingDict:
    """
    Dictionary of outstanding probes, sorted by the target address.
    """

    def __init__(self):
        self._data: dict[Union[IPv4Address, IPv6Address], list[P]] = {}

    def __contains__(self, item: Union[IPv4Address, IPv6Address]) -> bool:
        return item in self._data

    def __getitem__(self, item: Union[IPv4Address, IPv6Address]) -> list[P]:
        return self._data.get(item, [])

    def add(self, probe: Probe):
        address = probe.target.address
        if address in self._data:
            self._data[address].append(probe)
        else:
            self._data[address] = [probe]

    def remove(self, probe: Probe):
        address = probe.target.address
        outstanding = self._data[address]
        outstanding.remove(probe)
        if len(outstanding) == 0:
            del self._data[address]

    def __len__(self) -> int:
        return len(self._data)


class PublicProbeSeries(ProbeSeries, ABC, Generic[P]):
    """
    Public probe series expect a response packet for each probe sent. This base class handles probe sending as well as
    waiting for and matching of incoming packets. Probes are saved to redis after their corresponding response has
    arrived or a timeout occurs.
    """

    def __init__(
            self,
            probe_class: Type[P],
            model: models.ProbeSeries,
            local_targets: list[models.TargetHost],
            servers: list[models.ServerModel],
            interface: NetworkInterface,
            thread_limiter: anyio.CapacityLimiter,
            ipv4_address: Optional[IPv4Address] = None,
            ipv6_address: Optional[IPv6Address] = None
    ):
        super().__init__(
            probe_class, model, local_targets, servers, interface, thread_limiter, ipv4_address, ipv6_address
        )
        # outstanding probes
        self.outstanding = OutstandingDict()

    async def reset_connection(self, probe: T) -> None:
        """
        Task to reset an individual open connection. Only relevant for TCP probes.
        """

    async def send_probe(self, probe: T) -> None:
        """
        Task to send a single probe asynchronously. Waits for the probe to be matched or a timeout to occur,
        whichever comes first, and saves the probe to redis. Outstanding probes are tracked with a hash set.
        """
        # send probe and add to list for matching responses
        self.outstanding.add(probe)
        # wait for received event, time out after configured period
        with anyio.move_on_after(self.config.timeout) as timeout:
            await self._send_probe(probe)
            await probe.wait()
        # remove from outstanding again
        self.outstanding.remove(probe)
        # save result
        if probe.sent:
            self.save_probe(probe)
        # close connection if probe timed out
        if timeout.cancel_called:
            await self.reset_connection(probe)

    async def receive_packets(self, done: anyio.Event) -> None:
        """
        Task to wait for incoming probes and pass them to the match_incoming method one at a time.
        :param done: event that will be set after the match loop has finished
        """
        # stop loop when series is stopped and no packets are outstanding anymore
        while self.can_run or len(self.outstanding) > 0:
            # wait for next incoming packet
            try:
                packet = self.incoming.popleft()
            except IndexError:
                await anyio.sleep(self.config.interval)
                continue
            await self.match_incoming(packet)
        # notify we are done matching
        await done.set()

    async def match_incoming(self, packet: Union[IP, IPv6]) -> bool:
        """
        Handles an incoming packet that was received by this probe series.
        :param packet: a packet from the incoming queue
        :return: true, if the match was successful
        """
        # match against all outstanding probes
        address = ipaddress.ip_address(packet.src)
        for probe in self.outstanding[address]:
            if not probe.is_pending():
                continue
            if probe.match_response(packet):
                await probe.notify_received()
                self.received += 1
                return True
        return False


class PublicTCPProbeSeries(PublicProbeSeries, ABC, Generic[P]):
    """
    Public probe series with TCP connection management. Connections can be kept open or closed between probes.
    """

    # close the TCP connection after each request
    CLOSE_IMMEDIATELY = False

    def __init__(
            self,
            probe_class: Type[P],
            model: models.ProbeSeries,
            local_targets: list[models.TargetHost],
            servers: list[models.ServerModel],
            interface: NetworkInterface,
            thread_limiter: anyio.CapacityLimiter,
            ipv4_address: Optional[IPv4Address] = None,
            ipv6_address: Optional[IPv6Address] = None
    ):
        super().__init__(
            probe_class, model, local_targets, servers, interface, thread_limiter, ipv4_address, ipv6_address
        )

        self.monitor: Optional[GuardedNFQTCPConsumer] = None

    def get_bpf(self) -> Optional[str]:
        return None

    def get_nft_rule(self) -> Optional[str]:
        if isinstance(self.config.port, int):
            port = self.config.port
        elif isinstance(self.config.port, tuple):
            port = f"{self.config.port[0]}-{self.config.port[1]}"
        else:
            raise RuntimeError(f"PS{self.id}: Invalid port or port range configured")
        return f"tcp dport {port} queue num {self.id}"

    def create_monitor(self, portal: BlockingPortal, task_status: TaskStatus[None]) -> Optional[GuardedMonitor]:
        """
        TCP probes always use netfilter queues to bypass the kernel.
        """
        if isinstance(self.config.port, int):
            min_port = self.config.port
            max_port = self.config.port
        elif isinstance(self.config.port, tuple):
            min_port = self.config.port[0]
            max_port = self.config.port[1]
        else:
            raise RuntimeError(f"PS{self.id}: Invalid port or port range configured")
        self.monitor = GuardedNFQTCPConsumer(
            portal,
            self.id,
            self.incoming,
            self.ipv4_address,
            self.ipv6_address,
            min_port,
            max_port,
            self.interface,
            self.socket_ipv4,
            self.socket_ipv6,
            self.config.timeout,
            task_status
        )
        return self.monitor

    def create_next_probe(self, target: Target) -> T:
        size = self.choose_next_size(target)
        saddr = self.ipv4_address if target.address.version == 4 else self.ipv6_address
        tcp = self.monitor.get_connection(saddr, target.address, target.port, self.CLOSE_IMMEDIATELY)
        return self.probe_class(self.id, settings.SERVER_ID, target, tcp, saddr, tcp.sport, size)

    async def receive_packets(self, done: anyio.Event) -> None:
        """
        Task to wait for incoming probes and pass them to the match_incoming method one at a time.
        :param done: event that will be set after the match loop has finished
        """
        # stop loop when series is stopped and no packets are outstanding anymore
        while self.can_run or len(self.outstanding) > 0:
            # wait for next incoming packet
            try:
                packet = self.incoming.popleft()
            except IndexError:
                await anyio.sleep(self.config.interval)
                continue

            # check if error occurred
            if isinstance(packet, TCPFlowError):
                error = packet
                packet = error.packet
                address = ipaddress.ip_address(packet.src)
                for probe in self.outstanding[address]:
                    probe.anomaly = f"{error.state.name} - {error.anomaly}"
                    probe.response = packet
                    if error.abort_probe:
                        await probe.notify_received()
                continue

            # check if we have sent any probes to this target
            target = self.targets[packet.src]
            if target is None:  # unknown target, ignore any packets
                continue

            segment = packet[TCP]

            # perform actual probe matching
            matched = await self.match_incoming(packet)

            # close immediately if the match was successful
            if self.CLOSE_IMMEDIATELY and matched:
                address = ipaddress.ip_address(packet.src)
                port = segment.dport
                if self.monitor.has_connection(address, port):
                    tcp = self.monitor.get_existing_connection(address, port)
                    await anyio.to_thread.run_sync(tcp.close_connection, limiter=self.thread_limiter)

        # notify we are done matching
        await done.set()

    async def reset_connection(self, probe: PublicTCPProbe) -> None:
        if probe.tcp.is_established():
            await anyio.to_thread.run_sync(probe.tcp.reset_connection, limiter=self.thread_limiter)
        self.monitor.clear_closed_connections(probe.target.address)


class PrivateProbeSeries(ProbeSeries, ABC, Generic[T]):
    """
    Private probe series do not expect any responses to their probe packets, but handle incoming probes from other
    probe servers. Probes are saved to redis immediately after sending them, or receiving ones from other servers.
    """

    # there are no responses, this is a one-way communication
    SEQUENCE_STEP = 1

    def __init__(
            self,
            probe_class: Type[P],
            model: models.ProbeSeries,
            local_targets: list[models.TargetHost],
            servers: list[models.ServerModel],
            interface: NetworkInterface,
            thread_limiter: anyio.CapacityLimiter,
            ipv4_address: Optional[IPv4Address] = None,
            ipv6_address: Optional[IPv6Address] = None
    ):
        super().__init__(
            probe_class, model, local_targets, servers, interface, thread_limiter, ipv4_address, ipv6_address
        )

        # find targets representing this probe server, to use in reconstruction of incoming probes
        self.local_services: dict[Union[IPv4Address, IPv6Address], models.TargetService] = {}
        for host in self.local_targets:
            for service in host.services:
                if service.type == self.type:
                    self.local_services[host.address] = service
        if len(self.local_services) == 0:
            logger.warning(
                "[PS-%03d] This probe server has no services with type %s, the packet monitor will not be started!",
                self.id, self.type
            )

    async def add_targets(self, services: list[models.TargetServiceModel]) -> None:
        """
        Remove this probe server from the list of targets if present
        :param services: services to add
        """
        local_host_ids = [host.id for host in self.local_targets]
        await super().add_targets([service for service in services if service.target_host_id not in local_host_ids])

    def get_bpf(self) -> Optional[str]:
        return None

    def create_monitor(self, portal: BlockingPortal, task_status: TaskStatus[None]) -> Optional[GuardedMonitor]:
        if len(self.local_services) == 0:
            return None
        return GuardedNFQConsumer(portal, self.id, self.incoming, task_status)

    async def send_probe(self, probe: T) -> None:
        """
        Task to send a single probe asynchronously. Saves the probe to redis.
        """
        # send probe
        await self._send_probe(probe)
        # save result
        self.save_probe(probe)

    async def receive_packets(self, done: anyio.Event) -> None:
        """
        Task to save all incoming probes. Increases the number of received probes every time.
        :param done: event that will be set after the match loop has finished
        """
        # stop loop when series is stopped plus one additional timeout period
        last_round = True
        last_time = None
        while self.can_run or last_round:
            # wait for next incoming packet
            try:
                packet = self.incoming.popleft()
                probe = self.reconstruct_probe(packet)
                if probe is not None:
                    self.save_probe(probe)
                    self.received += 1
            except IndexError:
                await anyio.sleep(self.config.interval)
                pass
            # go on for one more timeout period
            if not self.can_run:
                if last_time is None:
                    last_time = time.perf_counter()
                else:
                    last_round = time.perf_counter() - last_time < self.config.timeout

        # notify we are done matching
        await done.set()

    @classmethod
    @abstractmethod
    def extract_identifiers(cls, packet: Union[IP, IPv6]) -> tuple[int, int]:
        """
        Extracts the series id and sequence number from a probe packet that was received by the monitor.
        :param packet: incoming packet
        :return: series id and sequence number
        """

    @classmethod
    @abstractmethod
    def get_dport(cls, packet: Union[IP, IPv6]) -> int:
        """
        Gets the destination port of an IP packet, depending on the transport layer used by this probe series.
        :param packet: the IP packet to extract the destination port from
        :return: the port number
        """

    def reconstruct_probe(self, packet: Union[IP, IPv6]) -> Optional[T]:
        """
        Reconstructs a probe from an incoming packet.
        :param packet: incoming packet
        :return: probe that was sent by another probe server
        """
        series_id, sequence_number = self.extract_identifiers(packet)
        # sanity check, does the packet belong to this probe series?
        if series_id != self.id:
            return None
        server = self.get_server_by_address(ipaddress.ip_address(packet.src))
        if server is None:
            logger.warning("[PS-%03d] Received packet from unknown server %s", self.id, packet.src)
            return None
        # reconstruct probe
        address = ipaddress.ip_address(packet.dst)
        service = self.local_services.get(address, None)
        if service is None:
            logger.warning("[PS-%03d] No local target service defined for IP address %s", self.id, address)
            return None
        port = self.get_dport(packet)
        target = Target(service.id, address, port, sequence_number)
        probe = self.probe_class(series_id, server.id, target, port, len(packet))
        probe.t_recv = packet.time
        probe.request = packet
        return probe
