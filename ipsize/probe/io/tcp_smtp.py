from typing import Union

import anyio
from scapy.layers.inet import IP, TCP
from scapy.layers.inet6 import IPv6
from scapy.packet import Packet, Raw

from ipsize.probe.io.base_probe import PublicTCPProbe
from ipsize.probe.io.base_series import PublicTCPProbeSeries
from ipsize.probe.io.monitor import TCPStateMachine


class TCPSMTPProbe(PublicTCPProbe):
    """
    Relevant RFCs:
    SMTP:   https://datatracker.ietf.org/doc/html/rfc5321#section-4
    """

    # 32 bytes TCP, 6 bytes "NOOP\r\n"
    PREAMBLE = 38
    MIN_SIZE = PREAMBLE
    MAX_SIZE = 1040  # SMTP must not exceed 1000 characters, plus TCP header, plus IP header

    def establish_connection(self) -> bool:
        """
        Establishes the TCP connection to this probes target. Blocks until the connection is established and the SMTP
        opening message from the server has arrived.
        """
        # wait for the SMTP opening message from the server
        return self.tcp.wait_for_connection() and self.tcp.wait_for_data()

    def assemble_payload(self) -> tuple[Packet, int]:
        """
        Assembles an SMTP NOOP command.
        :return: the SMTP NOOP command payload
        """
        # decrease size of preamble if this endpoint does not use TCP timestamps
        if not self.tcp.has_timestamps():
            self.PREAMBLE -= 12

        ip = self.target.get_ip_headers(self.saddr)
        filler = " " * (self.size - len(ip) - self.PREAMBLE)
        command = f"NOOP{filler}\r\n"
        return Raw(command.encode("ascii")), len(command)

    def match_response(self, response: Union[IP, IPv6]) -> bool:
        """
        Checks if the incoming packet acknowledges the reception of the request packet.
        :param response: the response packet
        :return: true, if the packet acknowledgement number is greater than or equal to the request segment number
        """
        if self.request_ack is None:
            return False
        segment = response[TCP]
        if not segment.flags.A or self.tcp.get_segment_ack(segment) < self.request_ack:
            return False

        self.response = response
        self.t_recv = response.time
        return True


def quit_smtp(tcp: TCPStateMachine) -> None:
    quit_command = Raw("QUIT\r\n".encode("ascii"))
    tcp.send_segment(quit_command, len(quit_command))
    tcp.wait_for_close()


class TCPSMTPProbeSeries(PublicTCPProbeSeries[TCPSMTPProbe]):

    async def close_tcp(self, tcp: TCPStateMachine) -> None:
        """
        Close the SMTP session and wait for the server to close the TCP connection.
        """
        async with anyio.move_on_after(self.config.timeout):  # wait for the response from the server
            await anyio.to_thread.run_sync(quit_smtp, tcp, cancellable=True, limiter=self.thread_limiter)
