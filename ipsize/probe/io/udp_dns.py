import math
import random
from typing import Optional, Union

from anyio.abc import TaskStatus
from anyio.from_thread import BlockingPortal

from scapy.layers.dns import DNS, DNSQR, DNSRROPT
from scapy.layers.inet import IP, UDP
from scapy.layers.inet6 import IPv6

from ipsize import models
from ipsize.probe.io.monitor import GuardedMonitor, GuardedNFQConsumer
from ipsize.probe.io.base_probe import PublicProbe
from ipsize.probe.io.base_series import PublicProbeSeries


def get_text_record_length_bytes(length: int) -> int:
    """
    DNS TXT records are split in parts of 255 characters, one additional byte is used to indicate the length of
    each part.
    :param length: length of text record
    :return: number of bytes required to encode a text record of the given length
    """
    if length == 0:
        return 1
    return math.ceil(length / 255)


def get_text_record_length(size: int) -> int:
    """
    Calculates how long a text record has to be to use exactly the specified amount of bytes.
    :param size: number of bytes required
    :return: number of characters required in a text record to be encoded in the given amount of bytes
    """
    if size <= 0:
        raise ValueError("text records are always at least one byte")
    lb = get_text_record_length_bytes(size)
    # account for the case where subtracting the length gets us under the limit for one byte less again
    if get_text_record_length_bytes(size - lb) < lb:
        return size - (lb - 1)
    return size - lb


class UDPDNSProbe(PublicProbe):
    """
    Relevant RFCs:
    DNS:    https://datatracker.ietf.org/doc/html/rfc1035#section-4
    EDNS:   https://datatracker.ietf.org/doc/html/rfc6891#section-4
    """

    # 8 bytes UDP, 12 bytes DNS header, 23 bytes query, 11 bytes additional records,
    # answer: 2 bytes RRname (token), 2 bytes type, 2 bytes class, 4 bytes TTL, 2 bytes data length
    PREAMBLE = 66
    # min size is one byte for the empty text record
    MIN_SIZE = PREAMBLE + 1
    # magic number is 1480, everything above gets truncated
    MAX_SIZE = 1480

    def get_qname(self, size, ip_len) -> str:
        """
        Calculates the length of the text record required to get the correct response packet size and returns the
        corresponding TXT record query name.
        :param size: response packet size
        :param ip_len: size of the IP packet
        :return: domain name for a TXT record that will produce a DNS response of the given size
        """
        length = get_text_record_length(size - ip_len - self.PREAMBLE)
        return f"txt{length:04}.ipsize.de"

    def assemble_request(self) -> Union[IP, IPv6]:
        """
        Assembles a DNS query with the DNS ID set to the sequence number of this probe.
        :return: the DNS query packet
        """
        ip = self.target.get_ip_headers(self.saddr)
        query = DNSQR(qname=self.get_qname(self.size, len(ip)), qtype="TXT")
        dns = DNS(id=self.sequence_number, rd=1, qd=query, ar=DNSRROPT(rclass=self.size))
        return ip / UDP(sport=self.sport, dport=self.target.port) / dns

    def match_response(self, response: Union[IP, IPv6]) -> bool:
        """
        Checks if the incoming packet contains a DNS response and the DNS ID in the header matches the sequence number
        of this probe. Marks an anomaly if the response code is not zero, the message is truncated, or not of the
        expected size.
        :param response: the response packet
        :return: true, if the packet contains a DNS reply to this probes request
        """
        if DNS not in response:
            return False

        return self.match_dns_response(response)

    def match_dns_response(self, response: Union[IP, IPv6]) -> bool:
        """
        Checks if the incoming packet contains a DNS response and the DNS ID in the header matches the sequence number
        of this probe. Marks an anomaly if the response code is not zero, the message is truncated, or not of the
        expected size.
        :param response: the response packet
        :return: true, if the packet contains a DNS reply to this probes request
        """
        # match on DNS message ID
        if response[DNS].id != self.sequence_number:
            return False

        self.response = response
        self.t_recv = response.time
        dns = response[DNS]
        if dns.rcode != 0:
            self.anomaly = "got response code " + str(dns.rcode)
        elif dns.tc == 1:
            self.anomaly = "message is truncated"
        elif len(response) != self.size:
            self.anomaly = f"expected {self.size} bytes"
        return True


class UDPDNSProbeSeries(PublicProbeSeries[UDPDNSProbe]):

    def get_bpf(self) -> Optional[str]:
        return None

    def get_nft_rule(self) -> Optional[str]:
        return f"udp dport {self.config.port} queue num {self.id}"

    def create_monitor(self, portal: BlockingPortal, task_status: TaskStatus[None]) -> Optional[GuardedMonitor]:
        return GuardedNFQConsumer(portal, self.id, self.incoming, task_status)

    def choose_next_size(self, target: models.TargetServiceModel) -> int:
        # minimum size depends on L3 headers (20 bytes IPv4, 40 bytes IPv6)
        ip_size = 20 if target.address.version == 4 else 40
        return self.choose_random_size(ip_size)

    def choose_random_size(self, ip_size):
        size = random.randint(self.probe_class.MIN_SIZE + ip_size, self.probe_class.MAX_SIZE)
        if (size - ip_size - self.probe_class.PREAMBLE) % 256 == 1:  # some sizes are impossible due to the TXT encoding
            return self.choose_random_size(ip_size)
        return size


