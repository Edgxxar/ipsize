import collections
import ipaddress
import logging
import random
import secrets
import select
import threading
import time
from abc import ABC, abstractmethod
from collections import defaultdict
from ipaddress import IPv4Address, IPv6Address
from typing import Literal, Optional, Union

import anyio
import netfilterqueue
from anyio.abc import TaskStatus
from anyio.from_thread import BlockingPortal
from netfilterqueue import NetfilterQueue
from scapy.interfaces import NetworkInterface
from scapy.layers.inet import IP, TCP
from scapy.layers.inet6 import IPv6
from scapy.packet import NoPayload, Packet
from scapy.sendrecv import AsyncSniffer
from scapy.supersocket import SuperSocket

from ipsize.models.enum import TCPState
from ipsize.probe.config import settings
from ipsize.probe.io.exception import TCPFlowError
from ipsize.probe.io.send import send_blocking

logger = logging.getLogger(settings.LOGGER_NAME)


class GuardedMonitor(ABC):
    """
    Asynchronous network monitor using an external thread. Needs to be started within a blocking portal.
    All captured packets are put into an asynchronous queue.
    """

    def __init__(
            self,
            portal: BlockingPortal,
            queue: collections.deque[Union[IP, IPv6, TCPFlowError]],
            task_status: TaskStatus[None]
    ):
        self.task_status = task_status
        self.queue = queue
        self.portal = portal

    @abstractmethod
    def start(self) -> None:
        pass

    @abstractmethod
    async def stop(self) -> None:
        pass


class GuardedTestMonitor(GuardedMonitor):

    def __init__(
            self,
            portal: BlockingPortal,
            queue: collections.deque[Union[IP, IPv6, TCPFlowError]],
            task_status: TaskStatus[None]
    ):
        """
        Creates a dummy monitor which does nothing.
        :param portal: blocking portal to guard this sniffer
        :param queue: queue to put all captured packets into
        :param task_status: signal that the sniffer has started
        """
        super().__init__(portal, queue, task_status)

    def start(self) -> None:
        self.task_status.started()

    async def stop(self) -> None:
        await self.portal.stop()


class GuardedSniffer(GuardedMonitor):

    def __init__(
            self,
            portal: BlockingPortal,
            bpf: str,
            queue: collections.deque[Union[IP, IPv6, TCPFlowError]],
            task_status: TaskStatus[None]
    ):
        """
        Creates a new asynchronous network sniffer. All incoming packets matching the given filter will be captured.
        :param portal: blocking portal to guard this sniffer
        :param bpf: berkley packet filter
        :param queue: queue to put all captured packets into
        :param task_status: signal that the sniffer has started
        """
        super().__init__(portal, queue, task_status)
        self.sniffer = AsyncSniffer(
            store=False,
            filter=bpf,
            prn=self._process_packet,
            started_callback=lambda: portal.call(task_status.started)
        )

    def _process_packet(self, pkt: Packet):
        if IP in pkt:
            self.queue.append(pkt[IP])
        elif IPv6 in pkt:
            self.queue.append(pkt[IPv6])
        else:
            raise RuntimeError("received packet that is neither IPv4 nor IPv6 but " + str(pkt.lastlayer()))

    def start(self) -> None:
        self.sniffer.start()

    async def stop(self) -> None:
        await anyio.to_thread.run_sync(self.sniffer.stop)
        await self.portal.stop()


class GuardedNFQConsumer(GuardedMonitor):

    def __init__(
            self,
            portal: BlockingPortal,
            queue_num: int,
            queue: collections.deque[Union[IP, IPv6, TCPFlowError]],
            task_status: TaskStatus[None]
    ):
        """
        Creates a consumer thread for the given netfilter queue. All incoming IPv4 and IPv6 packets from this queue
        will be read, put into the probe series queue, and then marked as dropped, to indicate to the kernel that no
        further processing of the packet is necessary.
        :param portal: blocking portal to guard this thread
        :param queue_num: netfilter queue number to bind to
        :param queue: queue to put all received packets into
        :param task_status: signal that the thread has started
        """
        super().__init__(portal, queue, task_status)
        self.queue_num: int = queue_num
        self.nfqueue: NetfilterQueue = NetfilterQueue()
        self.thread: threading.Thread = threading.Thread(target=self._run, name="GuardedNFQConsumer", daemon=True)
        self.poll = select.poll()
        self._stop: bool = False

    def _run(self) -> None:
        self.nfqueue.bind(self.queue_num, self._process_packet)
        self.poll.register(self.nfqueue.get_fd(), select.POLLIN)
        self.portal.call(self.task_status.started)
        try:
            # poll for new packets continuously
            while not self._stop:
                for _, event in self.poll.poll(100):
                    # empty event means timeout
                    if not event:
                        continue
                    if event & select.POLLIN:
                        self.nfqueue.run(False)
                    else:
                        raise RuntimeError(f"received unexpected event {bin(event)} from netfilter queue socket")
        finally:
            # unbind
            self.poll.unregister(self.nfqueue.get_fd())
            self.nfqueue.unbind()

    def _process_packet(self, pkt: netfilterqueue.Packet):
        if pkt.hw_protocol == 0x0800:  # IPv4
            p = IP(pkt.get_payload())
        elif pkt.hw_protocol == 0x86DD:  # IPv6
            p = IPv6(pkt.get_payload())
        else:  # should not occur, give packet back to kernel
            pkt.accept()
            return
        self.queue_packet(p)
        pkt.drop()

    def queue_packet(self, packet: Union[IP, IPv6]):
        # queue packet for probe series and drop from netfilter queue
        self.queue.append(packet)

    def start(self) -> None:
        self.thread.start()

    async def stop(self) -> None:
        logger.info("[NFQ-%03d] Stopping thread...", self.queue_num)
        self._stop = True
        # wait for queue to be unbound
        await anyio.to_thread.run_sync(self.thread.join)
        await self.portal.stop()


def _get_time() -> int:
    return int(time.monotonic() * 1000000) % 2 ** 32


def _get_timestamp(segment: TCP) -> Optional[tuple[Literal["Timestamp"], tuple[int, int]]]:
    return next(filter(lambda option: option[0] == "Timestamp", segment.options), None)


class TCPStateMachine:
    """
    Rudimentary TCP state machine. Does not handle retransmission of data or any other of the standard TCP features.

    See also https://datatracker.ietf.org/doc/html/rfc9293
    """

    def __init__(
            self,
            saddr: Union[IPv4Address, IPv6Address, None],
            sport: int,
            daddr: Union[IPv4Address, IPv6Address],
            dport: int,
            interface: NetworkInterface,
            socket: SuperSocket,
            timeout: float
    ):
        self.saddr: Union[IPv4Address, IPv6Address, None] = saddr
        self.sport: int = sport
        self.daddr: Union[IPv4Address, IPv6Address] = daddr
        self.dport: int = dport
        self.interface: NetworkInterface = interface
        self.socket: SuperSocket = socket
        self.timeout: float = timeout

        self._state: TCPState = TCPState.CLOSED  # current state
        self._isn_random: int = secrets.randbelow(2 ** 128)  # secure random number to use as starting point for ISNs
        # sequence variables, see also section 3.3.1 of RFC 9293. These numbers are relative to the data being sent to
        # help calculating offsets and windows without requiring any modulo operations. The actual sequence numbers
        # being sent over the wire are calculated dynamically on packet assembly.
        self._snd_una: int = 0  # oldest unacknowledged sequence number
        self._snd_nxt: int = 0  # next sequence number to be sent
        self._iss: int = 0  # initial send sequence number
        self._rcv_nxt: int = 0  # next sequence number expected on an incoming segment
        self._rcv_wnd: int = 8192  # receive window (offset), fixed size for this implementation
        self._irs: int = 0  # initial receive sequence number
        self._tsopt: bool = False  # use TCP Timestamps option
        self._ts_recent: int = 0

        # synchronization
        self._lock: threading.RLock = threading.RLock()
        self._established: threading.Event = threading.Event()  # set when handshake is complete
        self._data: threading.Event = threading.Event()  # set when first data segment arrives
        self._closed: threading.Event = threading.Event()  # set when connection was closed

    def is_established(self) -> bool:
        return self._established.is_set()

    def is_closed(self) -> bool:
        return self._closed.is_set()

    def has_timestamps(self) -> bool:
        return self._tsopt

    def _assemble(
            self,
            flags: str = "A",
            options: Optional[list] = None
    ) -> Union[IP, IPv6]:
        """
        Assembles an IP/TCP packet for this probes target service with the current sequence numbers.
        :param flags: TCP flags
        :return: the IP/TCP packet without any payload
        """
        # calculate raw sequence numbers to use in packets
        seq = (self._iss + self._snd_nxt) % 2 ** 32
        ack = (self._irs + self._rcv_nxt) % 2 ** 32
        return self._assemble_raw(seq, ack, flags, options)

    def _assemble_raw(
            self,
            seq: int,
            ack: int,
            flags: str = "A",
            options: Optional[list] = None
    ) -> Union[IP, IPv6]:
        """
        Assembles an IP/TCP packet for this probes target service.
        :param seq: raw sequence number to use
        :param ack: raw acknowledgement number to use
        :param flags: TCP flags
        :return: the IP/TCP packet without any payload
        """
        if options is None:
            options = []
        if self._tsopt:  # add timestamp option to all segments
            if len(options) == 0:  # if options is empty, add to NOP padding options first
                options.extend([("NOP", None), ("NOP", None)])
            options.append(("Timestamp", (_get_time(), self._ts_recent)))
        tcp = TCP(sport=self.sport, dport=self.dport, seq=seq, ack=ack,
                  flags=flags, window=self._rcv_wnd, options=options)
        if self.daddr.version == 4:
            ip = IP(src=str(self.saddr) if self.saddr is not None else None, dst=str(self.daddr))
        else:
            ip = IPv6(src=str(self.saddr) if self.saddr is not None else None, dst=str(self.daddr))
        return ip / tcp

    def send_blocking(self, packet: Union[IP, IPv6]) -> None:
        """
        Sends a packet using this state machines interface and socket
        :param packet: packet to send
        """
        send_blocking(packet, self.interface, self.socket)

    def get_segment_seq(self, segment: TCP):
        """
        Calculates the relative sequence number for a received TCP segment and the current connection.
        :param segment: received TCP segment
        :return: relative sequence number of the segment
        """
        return (segment.seq - self._irs) % 2 ** 32

    def get_segment_ack(self, segment: TCP):
        """
        Calculates the relative acknowledgement number for a received TCP segment and the current connection.
        :param segment: received TCP segment
        :return: relative acknowledgement number of the segment
        """
        return (segment.ack - self._iss) % 2 ** 32

    def _get_next_segment_seq(self, segment: TCP):
        """
        Calculates the relative sequence number for the next TCP segment and the current connection.
        :param segment: current TCP segment
        :return: relative sequence number of the next segment
        """
        seq = segment.seq + len(segment.payload)
        if segment.flags.S:  # account for SYN
            seq += 1
        if segment.flags.F:  # account for FIN
            seq += 1
        return (seq - self._iss) % 2 ** 32

    def _generate_isn(self) -> int:
        """
        Generates a random initial sequence number (ISN) for a TCP connection. See also RFC 9293, chapter 3.4.1.
        The described pseudorandom function is simplified as one unique, random secret per target.
        :return: the initial sequence number.
        """
        return (int(time.monotonic() * 250000) + self._isn_random) % 2 ** 32

    def _get_maximum_segment_size(self) -> int:
        """
        :return: the maximum segment size that can be received from this target
        """
        # MTU - IP - TCP
        return settings.PROBE_MTU - (20 if self.daddr.version == 4 else 40) - 20

    def wait_for_connection(self) -> bool:
        """
        Waits for a connection to be established. Establishes a new connection if the current state is closed.
        """
        with self._lock:
            if self._state == TCPState.CLOSED:
                # set state and reset events
                self._state = TCPState.SYN_SENT
                self._data.clear()
                self._closed.clear()
                # send SYN packet
                self._iss = self._generate_isn()
                self._snd_una = 0
                self._snd_nxt = 0
                self._irs = 0
                self._rcv_nxt = 0
                syn = self._assemble("S", [
                    ("MSS", self._get_maximum_segment_size()),  # set Maximum Segment Size
                    ("SAckOK", ""),  # offer Selective Acknowledgements
                    ("Timestamp", (_get_time(), 0)),  # offer Timestamps
                    ("NOP", None),  # padding to align last option
                    ("WScale", 7),  # offer Window Scale
                ])  # SYN
                self._snd_nxt += 1  # increase snd.nxt for SYN flag
                self.send_blocking(syn)

        # wait for handshake to be completed
        return self._established.wait(self.timeout)

    def wait_for_data(self) -> bool:
        """
        Waits for the first data segment to be received
        """
        return self._data.wait(self.timeout)

    def send_segment(
            self, payload: Packet, payload_length: int
    ) -> tuple[Union[IP, IPv6], int]:
        """
        Creates a TCP PSH packet with payload and increases the send sequence number
        :param payload: payload to send
        :param payload_length: advance the send sequence number by this amount
        :return: packet that was sent and sequence number that needs to be acknowledged to confirm the arrival
        """
        with self._lock:
            # if self._state != TCPState.ESTABLISHED:
            #     raise TCPFlowError(self._state, "connection is not established")
            packet = self._assemble("PA") / payload
            self._snd_nxt += payload_length  # increase snd.nxt for segment length
            self.send_blocking(packet)
            return packet, self._get_next_segment_seq(packet[TCP])

    def close_connection(self):
        """
        Creates a TCP FIN packet, sets the TCP state to FIN_WAIT_1 and increases the send sequence number by one.
        """
        with self._lock:
            if self._state == TCPState.ESTABLISHED:
                # set state and clear events
                self._state = TCPState.FIN_WAIT_1
                self._established.clear()
                # send FIN packet
                fin = self._assemble("FA")
                self._snd_nxt += 1  # increase snd.nxt for FIN flag
                self.send_blocking(fin)

    def wait_for_close(self) -> None:
        """
        Waits for the connection to be closed
        """
        self._closed.wait(self.timeout)

    def get_closed(self) -> threading.Event:
        return self._closed

    def reset_connection(self, segment: Optional[TCP] = None):
        """
        Creates a TCP RST packet, sets the TCP state to CLOSED.
        :return: TCP RST packet
        """
        with self._lock:
            if segment is None:
                reset = self._assemble("RA")
            else:
                reset = self._assemble_raw(segment.ack if segment.flags.A else 0, 0, "R")
            self.send_blocking(reset)
            self.set_closed()

    def set_closed(self) -> None:
        """
        Overwrite the TCP state to CLOSED, such that tasks waiting for closed connections can continue.
        """
        with self._lock:
            self._tsopt = False
            self._state = TCPState.CLOSED
            self._closed.set()

    def handle_segment(self, segment: TCP) -> TCPState:
        """
        Handles in incoming TCP segment as described in section 3.10.7 of RFC 9293.
        :param segment: TCP packet that was received
        :return: the state before processing the packet
        :raises TCPFlowError: when the segment contains invalid data.
        """
        with self._lock:
            # connection is closed
            state = self._state
            if state == TCPState.CLOSED:
                if segment.flags.R:  # discard RST segments silently
                    return state
                self.reset_connection(segment)
                raise TCPFlowError(state, "connection closed")

            # check the RST bit, we do not mitigate TCP reset attacks on purpose
            if segment.flags.R:
                self.set_closed()
                raise TCPFlowError(state, "received TCP reset")

            # cannot handle segments without ACK
            if not segment.flags.A:
                raise TCPFlowError(state, "expected TCP ACK", abort_probe=False)

            seg_seq = self.get_segment_seq(segment)

            # waiting for connection establishment
            if state == TCPState.SYN_SENT:
                # first, check the ACK is acceptable
                if not self._check_ack(segment):
                    self.reset_connection(segment)
                    raise TCPFlowError(state, "invalid ACK")
                # last, check the SYN bit
                if not segment.flags.S:  # SYN bit must be set at this point
                    raise TCPFlowError(state, "expected TCP SYN")

                self._irs = segment.seq  # save raw sequence as irs
                self._rcv_nxt += 1  # increase rcv.nxt for SYN flag
                self._state = TCPState.ESTABLISHED
                tsopt = _get_timestamp(segment)
                if tsopt is not None:  # enable timestamps option if segment contains it
                    self._tsopt = True
                    self._ts_recent = tsopt[1][0]

                ack = self._assemble()  # ACK
                self.send_blocking(ack)
                self._established.set()
                return state

            # connection is in synchronized state

            # handle Timestamp Option, see RFC 7323 Section 4.3
            if self._tsopt:
                tsopt = _get_timestamp(segment)
                if tsopt is None:
                    raise TCPFlowError(state, "TSopt missing", abort_probe=False)
                tsval = tsopt[1][0]
                if tsval >= self._ts_recent and seg_seq <= self._rcv_nxt:
                    self._ts_recent = tsval

            # check sequence number, send ACK with expected sequence numbers if necessary
            if seg_seq < self._rcv_nxt:  # discard duplicates
                ack = self._assemble()  # ACK with expected sequence numbers
                self.send_blocking(ack)
                return state
            if seg_seq != self._rcv_nxt:  # raise flow error if segment is out of order
                ack = self._assemble()
                self.send_blocking(ack)
                raise TCPFlowError(state, "out of order segment")

            # after this point packets should not contain the SYN flag
            if segment.flags.S:
                self.reset_connection(segment)
                raise TCPFlowError(state, "unexpected TCP SYN")

            # check for acceptable ack
            if not self._check_ack(segment):
                ack = self._assemble()  # ACK with expected sequence numbers
                self.send_blocking(ack)
                return state

            # process the segment
            if state == TCPState.ESTABLISHED:
                if isinstance(segment.payload, NoPayload) and not segment.flags.F:  # received empty ACK
                    return state

                if not isinstance(segment.payload, NoPayload):  # increase rcv.nxt for segment length
                    self._rcv_nxt += len(segment.payload)

                if segment.flags.F:  # close connection if FIN is set
                    self._state = TCPState.LAST_ACK
                    self._established.clear()
                    self._rcv_nxt += 1  # increase rcv.nxt for FIN
                    ack = self._assemble("FA")  # FIN/ACK
                    self._snd_nxt += 1  # increase snd.nxt for FIN
                else:
                    ack = self._assemble()  # ACK

                self.send_blocking(ack)
                if not isinstance(segment.payload, NoPayload):  # set data event
                    self._data.set()
                return state

            # waiting for FIN/ACK
            if state == TCPState.FIN_WAIT_1:
                if segment.flags.F:  # 3-way close, move directly to CLOSED
                    self.set_closed()
                    self._rcv_nxt += 1  # increase rcv.nxt for FIN flag
                    ack = self._assemble()  # ACK
                    self.send_blocking(ack)
                    return state
                # just an ack, move to 4-way close
                self._state = TCPState.FIN_WAIT_2
                return state

            # waiting for FIN
            if state == TCPState.FIN_WAIT_2:
                if segment.flags.F:  # 4-way close
                    self.set_closed()
                    self._rcv_nxt += 1  # increase rcv.nxt for FIN flag
                    ack = self._assemble()  # ACK
                    self.send_blocking(ack)
                return state

            # only thing remaining is the last ACK
            if state == TCPState.LAST_ACK:
                self.set_closed()
                return state

            return state

    def _check_ack(self, segment: TCP) -> bool:
        """
        Checks for an acceptable TCP ACK value. The segment ACK is acceptable if it is greater than or equal to snd.una
        and less than or equal to snd.nxt. The snd.una value gets set to the segment ACK if it is greater.
        :param segment: TCP ACK segment
        :return: true, if the segment ACK is acceptable
        """
        seg_ack = self.get_segment_ack(segment)
        if not self._snd_una <= seg_ack <= self._snd_nxt:
            return False
        if self._snd_una < seg_ack:
            self._snd_una = seg_ack
        return True


class GuardedNFQTCPConsumer(GuardedNFQConsumer):

    def __init__(
            self,
            portal: BlockingPortal,
            queue_num: int,
            queue: collections.deque[Union[IP, IPv6, TCPFlowError]],
            ipv4_address: Optional[IPv4Address],
            ipv6_address: Optional[IPv6Address],
            port_min: int,
            port_max: int,
            interface: NetworkInterface,
            socket_ipv4: Optional[SuperSocket],
            socket_ipv6: Optional[SuperSocket],
            timeout: float,
            task_status: TaskStatus[None]
    ):
        super().__init__(portal, queue_num, queue, task_status)
        self.ipv4_address = ipv4_address
        self.ipv6_address = ipv6_address
        self.port_min = port_min
        self.port_max = port_max
        self.interface = interface
        self.socket_ipv4 = socket_ipv4
        self.socket_ipv6 = socket_ipv6
        self.timeout = timeout

        self._connections: dict[Union[IPv4Address, IPv6Address], dict[int, TCPStateMachine]] = defaultdict(dict)

    def get_socket(self, address: Union[IPv4Address, IPv6Address]) -> SuperSocket:
        return self.socket_ipv4 if address.version == 4 else self.socket_ipv6

    def create_reset(self, address: Union[IPv4Address, IPv6Address], segment: TCP) -> TCP:
        if address.version == 4:
            ip = IP(src=str(self.ipv4_address), dst=str(address))
        else:
            ip = IPv6(src=str(self.ipv6_address), dst=str(address))
        return ip / TCP(sport=segment.dport, dport=segment.sport, flags="R")

    def queue_packet(self, packet: Union[IP, IPv6]):
        segment = packet[TCP]
        address = ipaddress.ip_address(packet.src)
        port = segment.dport
        if not self.has_connection(address, port):  # no connection, send reset
            send_blocking(self.create_reset(address, segment), self.interface, self.get_socket(address))
            return

        # do the TCP flow
        tcp = self.get_existing_connection(address, port)
        try:
            state = tcp.handle_segment(segment)
            if state == TCPState.ESTABLISHED:  # state was established, queue packet for processing in probe series
                self.queue.append(packet)
            if tcp.is_closed():  # state is closed, clear closed connections
                self.clear_closed_connections(address)
        except TCPFlowError as e:
            e.packet = packet
            self.queue.append(e)
        except BaseException as e:
            logger.error("Error while trying to handle TCP packet:")
            logger.error(packet.summary())
            logger.exception(e)

    async def stop(self) -> None:
        async with anyio.move_on_after(self.timeout):
            await anyio.to_thread.run_sync(self._close_connections, cancellable=True)
        await super().stop()

    def _close_connections(self):
        start = time.perf_counter()
        # close all connections and collect events
        events = []
        logger.info("[NFQ-%03d] Closing connections...", self.queue_num)
        for conns in list(self._connections.values()):
            for tcp in list(conns.values()):
                tcp.close_connection()
                events.append(tcp.get_closed())
        # wait for close of all connections
        logger.info("[NFQ-%03d] Waiting for close...", self.queue_num)
        for close in events:
            if time.perf_counter() - start > self.timeout:
                logger.info("[NFQ-%03d] Close timed out...", self.queue_num)
                return
            close.wait(self.timeout)
        logger.info("[NFQ-%03d] Closed all connections successfully.", self.queue_num)

    def get_connection(
            self,
            saddr: Union[IPv4Address, IPv6Address],
            daddr: Union[IPv4Address, IPv6Address],
            dport: int,
            create_new: bool = True
    ) -> TCPStateMachine:
        # create new state if no connections are managed yet
        if daddr not in self._connections:
            sport = random.randint(self.port_min, self.port_max)
            tcp = TCPStateMachine(saddr, sport, daddr, dport, self.interface, self.get_socket(daddr), self.timeout)
            self._connections[daddr][sport] = tcp
            return tcp
        # always create new state
        if create_new:
            used_ports = self._connections[daddr].keys()
            if len(used_ports) > self.port_max - self.port_min:
                raise RuntimeError("All available ports in use")
            sport = random.choice([p for p in range(self.port_min, self.port_max + 1) if p not in used_ports])
            tcp = TCPStateMachine(saddr, sport, daddr, dport, self.interface, self.get_socket(daddr), self.timeout)
            self._connections[daddr][sport] = tcp
            return tcp
        # return any state if no new connections should be opened
        return next(iter(self._connections[daddr].values()))

    def get_existing_connection(self, address: Union[IPv4Address, IPv6Address], port: int) -> TCPStateMachine:
        return self._connections[address][port]

    def has_connection(self, address: Union[IPv4Address, IPv6Address], port: int) -> bool:
        return address in self._connections and port in self._connections[address]

    def clear_closed_connections(self, address: Union[IPv4Address, IPv6Address]) -> None:
        if address not in self._connections:
            return
        cons = self._connections[address]
        for port, tcp in list(cons.items()):
            if tcp.is_closed():
                del cons[port]
        if len(cons) == 0:
            del self._connections[address]
