import ipaddress
import random
from ipaddress import IPv4Address, IPv6Address
from typing import Optional, Union

from redis.asyncio.client import Redis
from scapy.layers.inet import IP
from scapy.layers.inet6 import IPv6

from ipsize import models


class Target:
    """Local representation of a target service for one probe series at one probe server"""

    def __init__(
            self,
            service_id: int,
            address: Union[IPv4Address, IPv6Address],
            port: Optional[int] = None,
            sequence_number: int = 0,
            last_probe: float = 0
    ):
        self.service_id: int = service_id
        self.address: Union[IPv4Address, IPv6Address] = address
        self.port: Optional[int] = port
        self.next_sequence_number: int = sequence_number
        self.last_probe: float = last_probe

        self._ip = None

    @classmethod
    async def load_from_redis(cls, series_id: int, service: models.TargetServiceModel, redis: Redis) -> "Target":
        data = await redis.hgetall(f"probeseries:{series_id}:target:{service.id}")
        if len(data) == 0:
            return Target(service.id, service.address, service.port)
        return Target(
            service.id,
            service.address,
            service.port,
            int(data.get("sequence_number", 0)),
            float(data.get("last_probe", 0))
        )

    def inc_sequence_number(self):
        """
        Increases the sequence number by one locally.
        """
        self.next_sequence_number += 1

    async def save_probe_sent(self, series_id: int, next_sequence_number: int, t_sent: float, redis: Redis):
        """
        Called when a probe was sent to this target. Increases the sequence number and saves the last probe time.
        :param series_id: series id the probe was sent on
        :param next_sequence_number: set the next sequence number to this
        :param t_sent: time the probe was sent at
        :param redis: redis client to save values with
        """
        redis_name = f"probeseries:{series_id}:target:{self.service_id}"
        await redis.hset(redis_name, "sequence_number", str(next_sequence_number))
        await redis.hset(redis_name, "last_probe", str(t_sent))
        self.last_probe = t_sent

    def get_ip_headers(self, saddr: Union[None, IPv4Address, IPv6Address]) -> Union[IP, IPv6]:
        """
        Creates an IP or IPv6 packet, depending on the destination address, without any payload.
        :return: The L3 packet headers with this targets address as the destination
        """
        if self._ip is None:
            if self.address.version == 4:
                self._ip = IP(src=str(saddr) if saddr is not None else None, dst=str(self.address))
            else:
                self._ip = IPv6(src=str(saddr) if saddr is not None else None, dst=str(self.address))
        return self._ip

    def has_last_probe(self) -> bool:
        """
        :return: True, if there was another probe sent from this probe server to this target before
        """
        return self.last_probe > 0

    def get_delta_millis(self, time: float) -> int:
        """
        Gets the amount of milliseconds since the last probe was sent from this probe server to this target.
        :param time: time to subtract the last probe time from
        :return: delta in milliseconds
        """
        return int((time - self.last_probe) * 1000)


class TargetDict:
    """
    Dictionary of targets that provides access via the service ID or the host address, as well as random access. This
    dictionary is not thread save!
    """

    def __init__(self):
        self._data_by_id: dict[int, Target] = {}
        self._data_by_address: dict[Union[IPv4Address, IPv6Address], Target] = {}
        self._random_access: list[int] = []
        self._cursor = 0

    def add(self, target: Target):
        self._data_by_id[target.service_id] = target
        self._data_by_address[target.address] = target
        self._random_access.insert(random.randint(0, len(self._random_access)), target.service_id)

    def remove_all(self, service_ids: list[int]):
        for service_id in service_ids:
            if service_id not in self._data_by_id:
                continue
            target = self._data_by_id[service_id]
            del self._data_by_id[service_id]
            del self._data_by_address[target.address]
            self._random_access.remove(service_id)

    def get_random(self) -> Target:
        if len(self) == 0:
            raise RuntimeError("dict is empty")
        if self._cursor >= len(self._random_access):
            self._cursor = 0
            random.shuffle(self._random_access)
        rid = self._random_access[self._cursor]
        target = self._data_by_id[rid]
        self._cursor += 1
        return target

    def __contains__(self, item: Union[int, str, IPv4Address, IPv6Address, Target]):
        if isinstance(item, int):
            return item in self._data_by_id
        if isinstance(item, str):
            return ipaddress.ip_address(item) in self._data_by_address
        if isinstance(item, IPv4Address) or isinstance(item, IPv6Address):
            return item in self._data_by_address
        if isinstance(item, Target):
            return item.service_id in self._data_by_id
        raise ValueError("invalid item type")

    def __getitem__(self, key: Union[int, str, IPv4Address, IPv6Address]) -> Target:
        if isinstance(key, int):
            return self._data_by_id.get(key)
        if isinstance(key, str):
            return self._data_by_address.get(ipaddress.ip_address(key))
        if isinstance(key, IPv4Address) or isinstance(key, IPv6Address):
            return self._data_by_address.get(key)
        raise ValueError("invalid key type")

    def __len__(self) -> int:
        return len(self._data_by_id)

    def values(self):
        return self._data_by_id.values()
