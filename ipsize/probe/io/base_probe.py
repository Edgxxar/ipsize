from abc import ABC, abstractmethod
from datetime import datetime
from ipaddress import IPv4Address, IPv6Address
from typing import Any, Optional, Union

import anyio
from scapy.compat import raw
from scapy.interfaces import NetworkInterface
from scapy.layers.inet import IP
from scapy.layers.inet6 import IPv6
from scapy.packet import Packet
from scapy.supersocket import SuperSocket

from ipsize.probe.config import settings
from ipsize.probe.io.monitor import TCPStateMachine
from ipsize.probe.io.send import send_blocking
from ipsize.probe.io.target import Target


class Probe(ABC):
    """
    Base class for all probes.
    """

    # minimum amount of bytes a packet can be big, _excluding_ the L3 headers
    MIN_SIZE = 0
    # maximum amount of bytes a packet can be big, _including_ the L3 headers
    MAX_SIZE = settings.PROBE_MTU

    def __init__(
            self,
            series_id: int,
            server_id: int,
            target: Target,
            saddr: Union[None, IPv4Address, IPv6Address] = None,
            sport: Optional[int] = None,
            size: int = 60
    ):
        """
        Creates a new request/response probe of a specific size
        :param series_id: id of the probe series this probe belongs to
        :param server_id: id of the probe server this probe was sent from
        :param target: the target service this probe is addressed to, with state
        :param saddr: the source address to use in request assembly
        :param sport: source port to use in request assembly
        :param size: total size in bytes the request packet will be
        """
        self.series_id: int = series_id
        self.server_id: int = server_id
        self.target: Target = target
        self.sequence_number: int = target.next_sequence_number  # immutable sequence number for this probe
        self.saddr: Union[None, IPv4Address, IPv6Address] = saddr
        self.sport: Optional[int] = sport
        self.size: int = size

        self.sent: bool = False  # any packet was sent, either connection establishment or probe packet itself
        self.t_sent: Optional[float] = None  # send time of actual probe packet
        self.t_recv: Optional[float] = None  # if not none, time of probe response
        self.delta: Optional[int] = None
        self.request: Union[IP, IPv6, None] = None

    async def send_request(self, iface: NetworkInterface, socket: SuperSocket, limiter: anyio.CapacityLimiter) -> None:
        """
        Assembles the request packet for this probe and sends it asynchronously. Marks the send time and the delta.
        :param iface: the network interface to send the probe with
        :param socket: the socket to send the probe with
        :param limiter: worker thread limiter
        """
        self.request = self.assemble_request()
        self.sent = True
        await anyio.to_thread.run_sync(send_blocking, self.request, iface, socket, cancellable=True, limiter=limiter)
        self.t_sent = self.request.sent_time
        if self.target.has_last_probe():
            self.delta = self.target.get_delta_millis(self.t_sent)

    @abstractmethod
    def assemble_request(self) -> Union[IP, IPv6]:
        """
        Constructs a request packet of the specified size.
        :return: L3 packet of size given in constructor
        """

    @abstractmethod
    def encode(self) -> dict[str, Any]:
        """
        Transform this probe to a packet entry that can be JSON serialized and saved to disk.
        """

    def __hash__(self) -> int:
        """
        Calculates a hash that is based on the sequence number and the target service id of this probe. This implies
        that the hashes for two probes of different probe series are not necessarily different, which is accepted
        because probes should only ever be compared within the same probe series.
        :return: a hash based on the sequence number and target service id of this probe
        """
        return hash((self.sequence_number, self.target.service_id))


class PublicProbe(Probe, ABC):
    """
    Base class for all public probes
    """

    def __init__(
            self,
            series_id: int,
            server_id: int,
            target: Target,
            saddr: Union[None, IPv4Address, IPv6Address],
            sport: Optional[int] = None,
            size: int = 60
    ):
        super().__init__(series_id, server_id, target, saddr, sport, size)

        self.probe_time: datetime = datetime.now().astimezone()
        self.response: Union[IP, IPv6, None] = None
        self.anomaly: Optional[str] = None

        # event is set by the match_response method. The sending task waits on this, to save the results in redis
        self._received: anyio.Event = anyio.Event()

    def is_pending(self) -> bool:
        """
        Checks if this probe has an outstanding response packet and is not timed out yet.
        :return: true, iff the request was sent and no response has been received yet
        """
        return self.sent and not self._received.is_set()

    async def wait(self) -> None:
        """
        Wait until this packet has been received
        """
        await self._received.wait()

    async def notify_received(self) -> None:
        """
        Notifies all received listeners.
        """
        await self._received.set()

    @abstractmethod
    def match_response(self, response: Union[IP, IPv6]) -> bool:
        """
        Matches an incoming response packet to this probes request packet. Also marks the current time as the received
        time, if the packet belongs to this probe.
        :param response: the response packet
        :return: true, if the given packet is the response to this probe, false otherwise
        """

    def encode(self) -> dict[str, Any]:
        if not self.sent:
            raise ValueError("probe was never sent by this server")

        packet = {
            "sequence_number": self.sequence_number,
            "server_id": self.server_id,
            "target_service_id": self.target.service_id,
            "size": self.size,
            "probe_time": self.probe_time
        }
        if self.t_sent is not None:  # actual probe may not have been sent, if connection setup failed
            packet["send_time"] = datetime.fromtimestamp(self.t_sent).astimezone(),
        if self.delta is not None:
            packet["delta"] = self.delta
        if self.anomaly is not None:  # save data only in abnormal cases
            packet["anomaly"] = self.anomaly
            packet["data"] = raw(self.response)
        if self.t_recv is not None:
            packet["recv_time"] = datetime.fromtimestamp(self.t_recv).astimezone()
            if self.t_sent is not None:
                packet["latency"] = int((self.t_recv - self.t_sent) * 1000000)  # latency in microseconds
        return packet

    def __str__(self) -> str:
        request_len = 0 if self.request is None else len(self.request)
        response_len = 0 if self.response is None else len(self.response)
        latency = "-" if self.t_recv is None else f"{int((self.t_recv - self.t_sent) * 1000)}ms"
        return f"{self.__class__.__name__}(target: {self.target.service_id}/{self.target.address}, seq_no: " \
               f"{self.sequence_number}, delta: {self.delta}, size: {self.size}, sent: {self.t_sent is not None}, " \
               f"request: {request_len}, response: {response_len}, latency: {latency}, anomaly: {self.anomaly})"


class PublicTCPProbe(PublicProbe, ABC):
    """
    Public probe using a TCP connection.
    """

    def __init__(
            self,
            series_id: int,
            server_id: int,
            target: Target,
            tcp: TCPStateMachine,
            saddr: Union[None, IPv4Address, IPv6Address] = None,
            sport: Optional[int] = None,
            size: int = 60
    ):
        super().__init__(series_id, server_id, target, saddr, sport, size)
        self.tcp = tcp
        self.request_ack = None
        self.request_received = False

    def establish_connection(self) -> bool:
        """
        Establishes the TCP connection to this probes target. Blocks until the connection is established.
        """
        return self.tcp.wait_for_connection()

    async def send_request(self, iface: NetworkInterface, socket: SuperSocket, limiter: anyio.CapacityLimiter) -> None:
        """
        Assembles the request packet for this probe and sends it asynchronously. Establishes a TCP connection to the
        target service if necessary, before sending the packet. Marks the send time and the delta.
        :param iface: the network interface to send the probe with
        :param socket: the socket to send the probe with
        :param limiter: worker thread limiter
        """
        self.sent = True
        # establish a new connection if necessary
        if not self.tcp.is_established():
            if not await anyio.to_thread.run_sync(self.establish_connection, cancellable=True, limiter=limiter):
                return

        # assemble the TCP packet and save the required acknowledgement number of the request
        payload, length = self.assemble_payload()
        self.request, self.request_ack = await anyio.to_thread.run_sync(
            self.tcp.send_segment, payload, length, cancellable=True, limiter=limiter
        )
        self.t_sent = self.request.sent_time
        if self.target.has_last_probe():
            self.delta = self.target.get_delta_millis(self.t_sent)

    def assemble_request(self) -> Union[IP, IPv6]:
        raise RuntimeError("do not assemble request directly, use assemble_payload method")

    @abstractmethod
    def assemble_payload(self) -> tuple[Packet, int]:
        """
        Assembles the TCP payload to send as a request
        :return: tuple of the payload and its length
        """

    def encode(self) -> dict[str, Any]:
        encoded = super().encode()
        if self.request_received:
            encoded["request_received"] = True
        return encoded


class PrivateProbe(Probe, ABC):
    """
    Base class for all private probes
    """

    def encode(self) -> dict[str, Any]:
        if self.request is None:  # request field is set for sent and received packets
            raise ValueError("probe was never sent nor received by this server")

        packet = {
            "sequence_number": self.sequence_number,
            "server_id": self.server_id,
            "target_service_id": self.target.service_id,
            "size": len(self.request),
        }
        if self.t_sent is not None:  # probe was sent by this server
            packet["send_time"] = datetime.fromtimestamp(self.t_sent).astimezone()
        if self.t_recv is not None:  # probe was received by this server
            packet["recv_time"] = datetime.fromtimestamp(self.t_recv).astimezone()
        return packet

    def __str__(self) -> str:
        return f"{self.__class__.__name__}(target: {self.target.service_id}/{self.target.address}, seq_no: " \
               f"{self.sequence_number}, size: {self.size}, sent: {self.sent}, received: {self.t_recv is not None}, " \
               f"packet: {len(self.request)})"
