from ipaddress import IPv4Address, IPv6Address
from pathlib import Path
from typing import Optional

from pydantic import AnyHttpUrl

from ipsize.config import Settings


class ProbeSettings(Settings):
    LOG_FILENAME: str = "probe.log"

    REDIS_SOCKET: str = "/run/redis/redis-server.sock"
    REDIS_DB: int = 0

    PROBE_INTERFACE: Optional[str] = None
    PROBE_IPV4_ADDRESS: Optional[IPv4Address] = None
    PROBE_IPV6_ADDRESS: Optional[IPv6Address] = None
    PROBE_PRIVATE_UDP_PORT: int = 10042
    PROBE_PRIVATE_TCP_PORT: int = 10043

    WORKER_THREAD_LIMIT: int = 1000

    SAVE_DIR: Path = "data"

    CONTROL_SERVER_URL: AnyHttpUrl = "https://localhost:8000"
    SERVER_ID: int = 1
    SERVER_SECRET: str = "!ChangeMe!"

    PROBE_MTU: int = 1500


settings = ProbeSettings()
