from contextlib import asynccontextmanager
from logging.config import dictConfig

import anyio
from fastapi import FastAPI

from ipsize import rest
from ipsize.config import LogConfig
from ipsize.probe.api import auth, probe, status
from ipsize.probe.config import settings
from ipsize.probe.server import probe_server

# set to false during testing
do_lifecycle = True


@asynccontextmanager
async def lifespan(fastapi: FastAPI):
    # setup logging
    if settings.LOG_TO_FILE:
        await anyio.Path(settings.LOG_DIR).mkdir(mode=0o755, parents=True, exist_ok=True)
    dictConfig(LogConfig(settings.LOG_FILENAME))

    async with anyio.create_task_group() as tg:
        if do_lifecycle:
            tg.start_soon(probe_server.startup)

        yield

        if do_lifecycle:
            await probe_server.shutdown()


# start app
app = FastAPI(
    title="IPS Probe",
    description="IPSize Probe Server API.",
    version="0.0.1",
    lifespan=lifespan
)

app.state.limiter = rest.limiter

app.include_router(status.router, tags=["status"])
app.include_router(auth.router, tags=["security"])
app.include_router(probe.router, tags=["probe"])
