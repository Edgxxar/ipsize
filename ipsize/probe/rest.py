import logging
from contextlib import asynccontextmanager

from httpx import AsyncClient, HTTPError

from ipsize import models
from ipsize.probe.config import settings
from ipsize.rest import control_paths, format_path

logger = logging.getLogger(settings.LOGGER_NAME)

control_token = None  # bearer token to authenticate at control server
hashed_control_secret = None  # hashed secret used by control server to authenticate here


@asynccontextmanager
async def get_control_client() -> AsyncClient:
    """
    Creates an asynchronous HTTP client to send authorized requests to the control server.
    :return: async http client
    """
    try:
        async with AsyncClient(
                base_url=str(settings.CONTROL_SERVER_URL), timeout=settings.REST_CLIENT_TIMEOUT
        ) as client:
            token = await get_control_token(client)
            client.headers["Authorization"] = f"Bearer {token}"
            yield client
    except HTTPError as e:
        logger.error("Unexpected HTTP error while requesting resource from control server: %s %s - %s",
                     e.request.method, e.request.url, e)


async def get_control_token(client: AsyncClient) -> str:
    """
    Gets the bearer token required to authorize requests at the control server. Loads the token by authenticating at
    the control server first if necessary.
    :param client: async http client used to authenticate if necessary
    :return: the control token
    """
    global control_token
    if control_token is None:
        response = await client.post(
            control_paths.AUTH,
            auth=(str(settings.SERVER_ID), settings.SERVER_SECRET),
            data={"grant_type": "client_credentials"}
        )
        response.raise_for_status()
        token = models.AccessToken(**response.json())
        control_token = token.access_token
        logger.info("Successfully authenticated with control server")
    return control_token


async def get_control_secret(refresh: bool = False) -> str:
    """
    Gets the hashed secret used by the control server to authorize requests. Queries the control server for a
    new version if necessary.
    :param refresh: set to true to force a reload from the control server
    :return: the hashed control secret
    """
    global hashed_control_secret
    if refresh or hashed_control_secret is None:
        async with get_control_client() as client:
            response = await client.get(
                format_path(control_paths.SERVER_GET_CONTROL_SECRET, server_id=settings.SERVER_ID)
            )
            response.raise_for_status()
            hashed_control_secret = response.json()
    return hashed_control_secret
