from typing import Annotated, Optional

from fastapi import APIRouter, Depends, HTTPException, Query, status
from starlette.requests import Request

from ipsize import models, security
from ipsize.probe.api import deps
from ipsize.probe.config import settings
from ipsize.probe.rest import get_control_secret
from ipsize.rest import limiter, probe_paths
from ipsize.security import OAuth2ClientCredentials

router = APIRouter()

RefreshParameter = Annotated[Optional[bool], Query(
    description="Force refresh of cached control secret by requesting the secret from the control server."
)]


@router.post(probe_paths.AUTH, response_model=models.AccessToken)
@limiter.limit("3/minute")
async def auth(request: Request, credentials: Annotated[OAuth2ClientCredentials, Depends()],
               refresh: RefreshParameter = False):
    """
    Generate access token by authenticating via control server secret. Only one client in defined, which is the control
    server, thus the client_id field is ignored. The client_secret is compared against the hashed control secret for
    this probe server, which is queried from the control server at startup. This server does not store any secrets.
    """
    # get hash from control server, use cache unless refresh parameter is set
    hashed_control_secret = await get_control_secret(refresh)
    if hashed_control_secret is None:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Error loading control secret")

    if not security.verify_password(credentials.client_secret, hashed_control_secret):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Incorrect client id or secret")

    # content of the access token does not matter, there is only one control server
    token = security.create_access_token("You're a Wizard, Harry!",
                                         settings.ACCESS_TOKEN_EXPIRE_MINUTES,
                                         settings.SECRET_KEY)
    return models.AccessToken(access_token=token)


@router.post(probe_paths.TEST_TOKEN, response_model=dict)
async def test_token(token: dict = Depends(deps.get_authenticated)):
    """
    Test access token
    """
    return token
