from fastapi import Depends, HTTPException, status
from jose import exceptions, jwt
from pydantic import ValidationError
from redis.asyncio.client import Redis

from ipsize import security
from ipsize.probe.config import settings
from ipsize.probe.persistence import database
from ipsize.rest import probe_paths
from ipsize.security import OAuth2TokenBearer

oauth2_scheme = OAuth2TokenBearer(
    client_credentials_token_url=probe_paths.AUTH
)


async def get_redis() -> Redis:
    """
    Gives access to the redis server.
    :return: async redis client
    """
    return database.get_redis()


def get_authenticated(token: str = Depends(oauth2_scheme)) -> dict:
    """
    If used as dependency, a valid access token mast be used to query the path (enforces authentication).
    Returns the user object to the path method.
    """
    try:
        payload = jwt.decode(
            token, settings.SECRET_KEY, algorithms=[security.ALGORITHM]
        )
    except (exceptions.JWTError, ValidationError):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Could not validate credentials")

    return payload
