from typing import Annotated

import anyio
from fastapi import APIRouter, BackgroundTasks, Body, Depends, HTTPException, status
from fastapi.responses import FileResponse
from redis.asyncio.client import Redis

from ipsize import models
from ipsize.models.enum import ProbeSeriesStatus
from ipsize.probe.api import deps
from ipsize.probe.config import settings
from ipsize.probe.io.base_series import ProbeSeries, del_download_meta, is_download_available, get_downloads_available
from ipsize.probe.rest import get_control_client
from ipsize.probe.server import load_target_services, probe_server
from ipsize.rest import probe_paths

router = APIRouter()


async def update_probe_series(update: models.ProbeSeriesTargetUpdate, redis: Redis):
    async with get_control_client() as client:
        for series_id, u in update.updates.items():
            if not probe_server.has_probe_series(series_id):
                continue
            series = probe_server.get_probe_series(series_id)
            await series.remove_targets(u.removed)
            targets = await load_target_services(redis, client, u.added)
            await series.add_targets(targets)


@router.post(
    probe_paths.PROBE_SERIES_POST_TARGETS,
    dependencies=[Depends(deps.get_authenticated)],
    status_code=status.HTTP_204_NO_CONTENT
)
async def post_probe_series_targets(
        update: models.ProbeSeriesTargetUpdate,
        background_tasks: BackgroundTasks,
        redis: Redis = Depends(deps.get_redis)
):
    """
    Notifies the probe server about changes with probe series targets.
    """
    background_tasks.add_task(update_probe_series, update, redis)


@router.get(
    probe_paths.PROBE_SERIES_GET_AVAILABLE,
    dependencies=[Depends(deps.get_authenticated)],
    response_model=list[models.ProbeDownloadAvailable]
)
async def get_probe_series_available(redis: Redis = Depends(deps.get_redis)):
    """
    Get all available download files
    """
    return [d async for d in get_downloads_available(redis)]


@router.post(
    probe_paths.PROBE_SERIES_PAUSE,
    dependencies=[Depends(deps.get_authenticated)]
)
async def post_probe_series_pause():
    """
    Pause all probe series where the current status is ONGOING
    """
    for series in probe_server.series.values():
        if series.status == ProbeSeriesStatus.ONGOING:
            await series.pause()


@router.post(
    probe_paths.PROBE_SERIES_RESUME,
    dependencies=[Depends(deps.get_authenticated)]
)
async def post_probe_series_resume():
    """
    Resume all probe series where the current status is PAUSED
    """
    for series in probe_server.series.values():
        if series.status == ProbeSeriesStatus.PAUSED:
            series.resume()


@router.get(
    probe_paths.PROBE_SERIES_DOWNLOAD,
    dependencies=[Depends(deps.get_authenticated)],
    response_class=FileResponse
)
async def download_probe_series_data(file_name: str, db: Redis = Depends(deps.get_redis)):
    """
    Download generated data for a probe series.
    """
    if not await is_download_available(db, file_name):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="File not found in redis")
    path = anyio.Path(settings.SAVE_DIR, file_name)
    if not await path.is_file():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="File not found on disk")
    return FileResponse(path=path, media_type="application/json-seq")


@router.delete(
    probe_paths.PROBE_SERIES_DOWNLOAD,
    dependencies=[Depends(deps.get_authenticated)],
    status_code=status.HTTP_204_NO_CONTENT
)
async def delete_probe_series_data(file_name: str, db: Redis = Depends(deps.get_redis)):
    """
    Delete a download file
    """
    if not await is_download_available(db, file_name):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="File not found in redis")
    path = anyio.Path(settings.SAVE_DIR, file_name)
    if not await path.is_file():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="File not found on disk")
    await path.unlink()
    await del_download_meta(db, file_name)


def get_probe_series_state_model(series: ProbeSeries) -> models.ProbeSeriesLocalState:
    return models.ProbeSeriesLocalState(
        series_id=series.id,
        status=series.status,
        can_run=series.can_run,
        running=series.running,
        targets=len(series.targets),
        sent=series.sent,
        received=series.received,
        incoming=len(series.incoming),
        sending=series.thread_limiter.borrowed_tokens,
        save_pending=series.save.qsize(),
        started_at=None if series.t_start == 0 else series.t_start,
        stopped_at=None if series.t_stop == 0 else series.t_stop
    )


@router.get(
    probe_paths.PROBE_SERIES_GET_STATE,
    dependencies=[Depends(deps.get_authenticated)],
    response_model=models.ProbeSeriesLocalState
)
async def get_probe_series_state(series_id: int):
    """
    Gets the current local state of this probe series at this probe server.
    """
    if not probe_server.has_probe_series(series_id):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Unknown probe series id")
    series = probe_server.get_probe_series(series_id)
    return get_probe_series_state_model(series)


@router.put(
    probe_paths.PROBE_SERIES_PUT_STATUS,
    dependencies=[Depends(deps.get_authenticated)],
    response_model=models.ProbeSeriesLocalState
)
async def put_probe_series_status(
        series_id: int,
        probe_series_status: Annotated[ProbeSeriesStatus, Body()],
        background_tasks: BackgroundTasks
):
    """
    Sets the status for a probe series. Starts or stops the execution if necessary.
    """
    if not probe_server.has_probe_series(series_id):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Unknown probe series id")

    series = probe_server.get_probe_series(series_id)
    old_status = series.status
    if old_status == probe_series_status:  # no changes
        return get_probe_series_state_model(series)

    series.status = probe_series_status
    if probe_series_status == ProbeSeriesStatus.ONGOING:  # start or resume series
        if old_status == ProbeSeriesStatus.PAUSED:
            series.resume()
        else:
            series.start()
    elif probe_series_status == ProbeSeriesStatus.PAUSED:  # pause series
        await series.pause()
    elif old_status == ProbeSeriesStatus.ONGOING:  # stop series
        background_tasks.add_task(series.stop)
    return get_probe_series_state_model(series)
