from fastapi import APIRouter

from ipsize import models
from ipsize.probe.server import probe_server
from ipsize.rest import probe_paths

router = APIRouter()


@router.get(probe_paths.STATUS, response_model=models.ProbeServerStatus)
async def get_status():
    return probe_server.status
