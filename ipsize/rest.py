"""
API paths used to communicate between control and probe servers
"""

import slowapi
from fastapi import Response

from ipsize.util import get_remote_address


class BinaryResponse(Response):
    media_type = "application/octet-stream"


class ControlServerRESTPaths:
    STATUS = "/status"

    AUTH = "/auth"
    TEST_TOKEN = "/test-token"
    USER_REGISTER = "/user/register"
    USER_LOGIN = "/user/login"
    USER_TEST_TOKEN = "/user/test-token"

    SERVER_CREATE = "/server"
    SERVER_POST_ADDRESS = "/server/{server_id}/address"
    SERVER_DELETE_ADDRESS = "/server/{server_id}/address/{address}"
    SERVER_UPDATE_SERVER_SECRET = "/server/{server_id}/server_secret"
    SERVER_GET_CONTROL_SECRET = "/server/{server_id}/control_secret"
    SERVER_UPDATE_CONTROL_SECRET = "/server/{server_id}/control_secret"
    SERVER_GET = "/server/{server_id}"
    SERVER_GET_ALL = "/servers"
    SERVER_GET_BY_NAME = "/server/by-name/{name}"
    SERVER_GET_BY_ADDRESS = "/server/by-address/{address}"

    TARGET_HOST_CREATE = "/target/host"
    TARGET_HOST_GET = "/target/host/{host_id}"
    TARGET_HOST_GET_BY_ADDRESS = "/target/host/by-address/{address}"
    TARGET_HOST_CHECK = "/target/host/check/{address}"
    TARGET_HOST_ENABLE = "/target/host/enable/{address}"
    TARGET_HOST_DISABLE = "/target/host/disable/{address}"

    TARGET_SERVICE_CREATE = "/target/service"
    TARGET_SERVICE_PUT_STATUS = "/target/service/{service_id}"
    TARGET_SERVICE_GET_ALL = "/target/services"

    PROBE_SERIES_CREATE = "/probe/series"
    PROBE_SERIES_GET_AVAILABLE = "/probe/series/available"
    PROBE_SERIES_GET = "/probe/series/{series_id}"
    PROBE_SERIES_PUT_CONFIG = "/probe/series/{series_id}/config"
    PROBE_SERIES_PUT_STATUS = "/probe/series/{series_id}/status"
    PROBE_SERIES_GET_TARGETS = "/probe/series/{series_id}/targets"


class ProbeServerRESTPaths:
    STATUS = "/status"

    AUTH = "/auth"
    TEST_TOKEN = "/test-token"

    PROBE_SERIES_POST_TARGETS = "/probe/series/targets"
    PROBE_SERIES_GET_AVAILABLE = "/probe/series/available"
    PROBE_SERIES_PAUSE = "/probe/series/pause"
    PROBE_SERIES_RESUME = "/probe/series/resume"
    PROBE_SERIES_DOWNLOAD = "/probe/series/download/{file_name}"
    PROBE_SERIES_GET_STATE = "/probe/series/{series_id}/state"
    PROBE_SERIES_PUT_STATUS = "/probe/series/{series_id}/status"


control_paths = ControlServerRESTPaths()
probe_paths = ProbeServerRESTPaths()


def format_path(path: str, **kwargs):
    """
    Formats a path by replacing all variables by the values given.
    :param path: path containing path variables
    :param kwargs: dict of variables to replace in path
    :return: formatted path
    """
    for key, value in kwargs.items():
        path = path.replace(f"{{{key}}}", str(value))
    return path


# limiter used by some endpoints
limiter = slowapi.Limiter(key_func=get_remote_address)
