import logging
from abc import ABC, abstractmethod
from ipaddress import IPv4Address, IPv6Address
from typing import Union

import anyio
from anyio import TASK_STATUS_IGNORED
from anyio.abc import TaskStatus

from ipsize.config import settings
from ipsize.models.enum import Status
from ipsize.models.status import ServerStatus
from ipsize.util import Counter

logger = logging.getLogger(settings.LOGGER_NAME)


async def ping(
        host: Union[IPv4Address, IPv6Address],
        counter: Counter,
        source: Union[None, IPv4Address, IPv6Address] = None
):
    command = ["ping", f"-{host.version}", "-c", "1", "-W", "3"]
    if source is not None:
        command.extend(["-I", str(source)])
    result = await anyio.run_process([*command, str(host)], check=False)
    logger.debug("ping %s returned code %d", host, result.returncode)
    counter += result.returncode == 0


class Server(ABC):

    @property
    @abstractmethod
    def status(self) -> ServerStatus:
        """
        Status of this server
        """

    @abstractmethod
    async def startup(self, *, task_status: TaskStatus[None] = TASK_STATUS_IGNORED) -> None:
        """
        Starts this server by performing some preliminary checks.
        :param task_status: signal that the server has started successfully
        """

    @abstractmethod
    async def shutdown(self) -> None:
        """
        Stops this server gracefully.
        """

    def update_status(self) -> None:
        # get all status attributes except the global status
        stats = [a for a in dir(self.status) if not a.startswith("__")
                 and isinstance(getattr(self.status, a), Status)
                 and not a == "status"]
        # global status is error if any status is error
        self.status.status = Status.ERROR if any(getattr(self.status, a) == Status.ERROR for a in stats) else Status.OK

    async def check_ipv4_connectivity(self, source: IPv4Address = None) -> None:
        # ping IPv4 hosts, error if more than one fails
        total = len(settings.CONNECTIVITY_CHECK_IPV4)
        pings = Counter()
        async with anyio.create_task_group() as tg:
            for host in settings.CONNECTIVITY_CHECK_IPV4:
                tg.start_soon(ping, host, pings, source)
        self.status.ipv4 = Status.ERROR if pings < total // 2 else Status.OK
        logger.info("IPv4 connectivity check: %d/%d hosts reachable, status %s", pings, total, self.status.ipv4.name)

    async def check_ipv6_connectivity(self, source: IPv6Address = None) -> None:
        # ping IPv6 hosts, error if more than one fails
        total = len(settings.CONNECTIVITY_CHECK_IPV6)
        pings = Counter()
        async with anyio.create_task_group() as tg:
            for host in settings.CONNECTIVITY_CHECK_IPV6:
                tg.start_soon(ping, host, pings, source)
        self.status.ipv6 = Status.ERROR if pings < total // 2 else Status.OK
        logger.info("IPv6 connectivity check: %d/%d hosts reachable, status %s", pings, total, self.status.ipv6.name)
