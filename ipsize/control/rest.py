import logging
from contextlib import asynccontextmanager

from httpx import AsyncClient

from ipsize import models, security
from ipsize.control.config import settings
from ipsize.rest import probe_paths

logger = logging.getLogger(settings.LOGGER_NAME)

server_tokens: dict[int, str] = {}  # bearer tokens to authenticate at probe servers


@asynccontextmanager
async def get_server_client(server: models.Server) -> AsyncClient:
    """
    Creates an asynchronous HTTP client to send authorized requests to the given probe server.
    :param server: probe server to send requests to
    :return: async http client
    """
    async with AsyncClient(base_url=server.url, timeout=settings.REST_CLIENT_TIMEOUT) as client:
        token = await get_server_token(client, server)
        client.headers["Authorization"] = f"Bearer {token}"
        yield client


async def get_server_token(client: AsyncClient, server: models.Server) -> str:
    """
    Gets the bearer token required to authorize requests at the given probe server. Loads the token by authenticating at
    the probe server first if necessary.
    :param client: async http client used to authenticate if necessary
    :param server: probe server to get the token for
    :return: the control token
    """
    if server.id not in server_tokens:
        secret = security.decrypt_string(server.control_secret, settings.SECRET_KEY, server.control_secret_salt)
        response = await client.post(
            probe_paths.AUTH,
            auth=("1", secret),
            data={"grant_type": "client_credentials"}
        )
        response.raise_for_status()
        token = models.AccessToken(**response.json())
        server_tokens[server.id] = token.access_token
        logger.info("Successfully authenticated with probe server %s", server.name)
    return server_tokens[server.id]
