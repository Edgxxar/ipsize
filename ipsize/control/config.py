from pathlib import Path
from typing import Annotated, Any, Optional

from pydantic import Field, FieldValidationInfo, PostgresDsn, field_validator

from ipsize.config import Settings


class ControlSettings(Settings):
    LOG_FILENAME: str = "control.log"

    POSTGRES_HOST: str = "localhost"
    POSTGRES_PORT: int = 5432
    POSTGRES_USER: str = "ipsize"
    POSTGRES_PASSWORD: str = "!ChangeMe!"
    POSTGRES_DB: str = "ipsize"
    SQLALCHEMY_DATABASE_URI: Optional[PostgresDsn] = None
    SQLALCHEMY_POOL_SIZE: int = 20
    SQLALCHEMY_POOL_OVERFLOW: int = 10

    @field_validator("SQLALCHEMY_DATABASE_URI", mode="before")
    @classmethod
    def assemble_db_connection(cls, v, info: FieldValidationInfo) -> Any:
        if isinstance(v, str):
            return v
        return PostgresDsn.build(
            scheme="postgresql+psycopg",
            username=info.data["POSTGRES_USER"],
            password=info.data["POSTGRES_PASSWORD"],
            host=info.data["POSTGRES_HOST"],
            port=info.data["POSTGRES_PORT"],
            path=info.data['POSTGRES_DB']
        )

    MONITOR_INTERVAL_MAX_OK: Annotated[int, Field(ge=1)] = 6 * 60 * 60  # 6 hours
    MONITOR_INTERVAL_MAX_ERROR: Annotated[int, Field(ge=1)] = 48 * 60 * 60  # 48 hours
    MONITOR_INTERVAL_STEP: Annotated[int, Field(ge=1)] = 10  # seconds
    MONITOR_INTERVAL_FLAP: Annotated[int, Field(ge=1)] = 120  # seconds
    MONITOR_INTERVAL_LOAD: Annotated[float, Field(gt=0)] = 1  # seconds
    MONITOR_STATUS_STICKY: Annotated[int, Field(ge=1)] = 3  # repeated status measurements until service status changes
    MONITOR_MAX_CONCURRENCY: Annotated[int, Field(ge=1)] = 200  # maximum number of services to check at once

    FLAPPING_LOOKBACK: Annotated[int, Field(ge=2)] = 21  # how many status entries to consider for flap detection
    FLAPPING_THRESHOLD_LOW: Annotated[float, Field(ge=0, le=1)] = 0.25  # stops flapping if change value drops below
    FLAPPING_THRESHOLD_HIGH: Annotated[float, Field(ge=0, le=1)] = 0.5  # starts flapping if change value exceeds this
    FLAPPING_CHANGE_WEIGHT: Annotated[float, Field(ge=0, le=2)] = 0.5  # more recent changes get valued more

    SERIES_UPDATE_TARGETS_INTERVAL: Annotated[int, Field(ge=1)] = 900  # seconds

    DOWNLOAD_INTERVAL: Annotated[int, Field(ge=1)] = 360  # minutes
    DOWNLOAD_DIR: Path = "download"

    SHODAN_API_KEY: Optional[str] = None


settings = ControlSettings()
