import asyncio
import logging
import math
import subprocess
import time
from datetime import datetime, timedelta
from ipaddress import IPv4Address, IPv6Address
from random import Random
from typing import Iterable, Sequence, Union

import anyio
import dns
from aiosmtplib import SMTP
from anyio import TASK_STATUS_IGNORED
from anyio.abc import TaskGroup, TaskStatus
from dns import asyncquery, message
from httpx import HTTPError
from sqlalchemy.ext.asyncio import async_sessionmaker

from ipsize import models
from ipsize.control.config import settings
from ipsize.control.persistence import crud, entities
from ipsize.control.rest import get_server_client
from ipsize.models.enum import ProbeSeriesStatus, ProbeSeriesType, Status
from ipsize.rest import probe_paths

logger = logging.getLogger(settings.LOGGER_NAME)

# monitor these types
MONITOR_TYPES = {
    ProbeSeriesType.PUBLIC_ICMP_PING,
    ProbeSeriesType.PUBLIC_UDP_DNS,
    ProbeSeriesType.PUBLIC_TCP_DNS,
    ProbeSeriesType.PUBLIC_TCP_SMTP
}

# calculate how many times we need to double the step interval, until we reach the max interval
MONITOR_MAX_REPEAT_OK = int(
    math.log(settings.MONITOR_INTERVAL_MAX_OK / settings.MONITOR_INTERVAL_STEP) // math.log(2)
)
MONITOR_MAX_REPEAT_ERROR = int(
    math.log(settings.MONITOR_INTERVAL_MAX_ERROR / settings.MONITOR_INTERVAL_STEP) // math.log(2)
)
# how many history entries to fetch
MONITOR_HISTORY_FETCH = max(MONITOR_MAX_REPEAT_OK, MONITOR_MAX_REPEAT_ERROR, settings.FLAPPING_LOOKBACK)


async def check_status_icmp(address: Union[IPv4Address, IPv6Address]) -> Status:
    """
    Updates the status for a target service by sending a single ICMP echo-request packet.
    :param address: the service address
    """
    try:
        # execute in a shielded scope, fail after three seconds, in case the ping process does not run correctly
        async with anyio.fail_after(3.5, shield=True):  # wait a little bit longer than the ping timeout itself
            r = await anyio.run_process([
                "ping", f"-{address.version}", "-c", "1", "-W", "3", str(address)
            ], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, check=False)
            return Status.OK if r.returncode == 0 else Status.ERROR
    except Exception:
        pass
    return Status.ERROR


async def check_status_dns_udp(address: Union[IPv4Address, IPv6Address], port: int) -> Status:
    qname = dns.name.from_text("txt0010.ipsize.de")
    query = message.make_query(qname, dns.rdatatype.TXT)
    try:
        r = await asyncquery.udp(query, str(address), timeout=3, port=port)
        if r.rcode() == dns.rcode.NOERROR:
            try:
                rrset = r.find_rrset(r.answer, qname, dns.rdataclass.IN, dns.rdatatype.TXT)
                return Status.OK if len(rrset) == 1 else Status.ERROR
            except KeyError:
                return Status.ERROR
    except Exception:
        pass
    return Status.ERROR


async def check_status_dns_tcp(address: Union[IPv4Address, IPv6Address], port: int) -> Status:
    qname = dns.name.from_text("txt0010.ipsize.de")
    query = message.make_query(qname, dns.rdatatype.TXT)
    try:
        r = await asyncquery.tcp(query, str(address), timeout=3, port=port)
        if r.rcode() == dns.rcode.NOERROR:
            try:
                rrset = r.find_rrset(r.answer, qname, dns.rdataclass.IN, dns.rdatatype.TXT)
                return Status.OK if len(rrset) == 1 else Status.ERROR
            except KeyError:
                return Status.ERROR
    except Exception:
        pass
    return Status.ERROR


async def check_status_smtp(address: Union[IPv4Address, IPv6Address], port: int) -> Status:
    try:
        address = str(address)
        async with SMTP(address, port, validate_certs=False, use_tls=False, start_tls=False) as client:
            await client.noop()
        return Status.OK
    except Exception:
        pass
    return Status.ERROR


random = Random()


def get_next_check_interval(
        new_status: Status,
        history: list[entities.TargetServiceHistory],
        randomize: bool = True
) -> int:
    """
    Calculates the amount of seconds until the service needs to be checked again. Counts how many consecutive entries
    in the service history had the same status as the newly measured one, and doubles the step interval each time,
    until the max interval is reached.
    :param new_status: newly measured status
    :param history: history of status checks, ordered by timestamp (most recent first)
    :param randomize: return a random number between the current and previous step interval for this service
    :return: amount of seconds until the next check
    """
    # count how many consecutive checks had the same status
    consecutive = 0
    for s in history:
        if s.status == new_status:
            consecutive += 1
        else:
            break

    max_interval = settings.MONITOR_INTERVAL_MAX_OK if new_status == Status.OK else settings.MONITOR_INTERVAL_MAX_ERROR
    max_repeat = MONITOR_MAX_REPEAT_OK if new_status == Status.OK else MONITOR_MAX_REPEAT_ERROR
    return _get_next_check_interval(consecutive, max_interval, max_repeat, randomize)


def _get_next_check_interval(
        consecutive: int,
        max_interval: int,
        max_repeat: int,
        randomize: bool
) -> int:
    if randomize:
        if consecutive == 0:
            return settings.MONITOR_INTERVAL_STEP
        if consecutive > max_repeat:
            min_interval = settings.MONITOR_INTERVAL_STEP * (2 ** max_repeat)
        else:
            min_interval = settings.MONITOR_INTERVAL_STEP * (2 ** (consecutive - 1))
            max_interval = settings.MONITOR_INTERVAL_STEP * (2 ** consecutive)
        return random.randint(min_interval, max_interval)

    if consecutive > max_repeat:
        return max_interval
    # double the interval for each consecutive occurrence
    return settings.MONITOR_INTERVAL_STEP * (2 ** consecutive)


def get_change_value(
        new_status: Status,
        history: list[entities.TargetServiceHistory],
        weight: float
) -> float:
    """
    Calculates the weighted change value for a history of service states, with 1.0 meaning the state changed between
    every entry and 0.0 meaning the state has not changed at all. Earlier state changes are weighted more than later
    state changes.
    :param new_status: the newly measured status
    :param history: history of status checks, ordered by timestamp (most recent first)
    :param weight: weight between 0.0 and 2.0
    :return: amount of change in this history
    """
    if len(history) == 0:
        raise ValueError("at least one history entry required")
    if not 0 <= weight <= 2:
        raise ValueError("weight has to be between 0.0 and 2.0 (both inclusive)")
    # weighing makes no sense with a weight of zero or for minimal histories
    unweighted = weight == 0 or len(history) == 1
    max_changes = len(history)  # maximum amount of changes in this history
    base = 1 + weight / 2  # base value for each state change
    divisor = 1 if unweighted else (1 / weight) * (max_changes - 1)  # divisor of x

    x = 0  # current state change
    change = 0  # summed change
    last = new_status  # track state change
    for s in history:
        if s.status != last:
            change += 1 if unweighted else base - x / divisor
        last = s.status
        x += 1

    # round value to account for floating point errors from the calculation
    change = round(change / max_changes, 8)
    return change


def detect_flapping_state(
        flapping: bool,
        new_status: Status,
        history: list[entities.TargetServiceHistory]
) -> bool:
    """
    Detects whether a service is currently flapping or not. A service is considered flapping, if the percentage of
    state changes exceeds the configured high threshold, and stops flapping if it drops below the configured low
    threshold.
    :param flapping: current flapping state
    :param new_status: the newly measured status
    :param history: history of status checks, ordered by timestamp (most recent first)
    :return: true, if the service is flapping
    """
    if len(history) < settings.FLAPPING_LOOKBACK - 1:
        return False
    change = get_change_value(new_status, history, settings.FLAPPING_CHANGE_WEIGHT)
    if not flapping and change >= settings.FLAPPING_THRESHOLD_HIGH:
        return True
    if flapping and change < settings.FLAPPING_THRESHOLD_LOW:
        return False
    return flapping


def get_status_sticky(
        current_status: Status,
        new_status: Status,
        history: list[entities.TargetServiceHistory],
        sticky: int
) -> Status:
    """
    Gets the "sticky" status for a service.

    :param current_status: current status of a service
    :param new_status: newly measured status of a service
    :param history: history of status measurements, ordered by timestamp (descending)
    :param sticky: amount of consecutive status measurements in the history that must match the new status
    :return: the new status, if enough consecutive measurements in the history match it, otherwise the current status
    """
    if len(history) < sticky:
        return current_status
    if sticky == 0:
        return new_status
    consecutive = 0
    for s in history:
        if s.status == new_status:
            consecutive += 1
        else:
            break
    if consecutive >= sticky:
        return new_status
    return current_status


class TargetServiceContainer:
    """
    Container for target service entities that are ready to be checked, currently being checked, or have recently been
    checked but not saved to the database yet.
    """

    def __init__(self):
        self._loaded: set[int] = set()  # ids of services currently loaded
        self._ready: asyncio.Queue[entities.TargetService] = asyncio.Queue()  # services ready to be checked
        self._done: list[tuple[entities.TargetService, Status, datetime]] = []  # service check results to be saved

    def add_ready(self, services: Sequence[entities.TargetService]) -> None:
        """
        Adds a sequence of services to the queue of services ready to be checked.
        :param services: services ready to check
        """
        for s in services:
            if s.id in self._loaded:
                logger.error("[Monitor] service already loaded in this container: " + s.id)
                continue
            self._loaded.add(s.id)
            self._ready.put_nowait(s)

    def get_loaded(self) -> set[int]:
        """
        :return: all service IDs currently loaded
        """
        return self._loaded

    async def get_next_ready(self) -> entities.TargetService:
        """
        Gets the next service that is ready to be checked. Blocks until a new service is available, if necessary.
        :return: a target service ready to be checked
        """
        return await self._ready.get()

    def count_ready(self) -> int:
        """
        :return: the amount of target services in the ready queue
        """
        return self._ready.qsize()

    def count_done(self) -> int:
        """
        :return: the amount of target services that have been checked and can be saved to the database
        """
        return len(self._done)

    def set_done(self, service: entities.TargetService, new_status: Status) -> None:
        """
        Marks a service check as done and saves the newly measured status and the current time.
        :param service: service that was checked
        :param new_status: newly measured status for the given service
        """
        self._done.append((service, new_status, datetime.now()))

    def get_all_done(self) -> list[tuple[entities.TargetService, Status, datetime]]:
        """
        Gets all checked services and their results for saving in the database. Removes them from the list of
        done services, subsequent calls to this method will return new results.
        :return: list of 3-tuples containing the service, its newly measured status, and the time of measurement
        """
        done = self._done.copy()
        self._done.clear()
        return done

    def remove_all(self, service_ids: Iterable[int]) -> None:
        """
        Removes all given IDs from this container. Should be called after service updates have been saved
        to the database.
        :param service_ids: list of target service ids
        """
        self._loaded.difference_update(service_ids)

    def __len__(self) -> int:
        """
        :return: total count of all loaded services
        """
        return len(self._loaded)


async def _notify_probe_server(server: entities.Server, update: models.ProbeSeriesTargetUpdate):
    try:
        async with get_server_client(server) as client:
            response = await client.post(probe_paths.PROBE_SERIES_POST_TARGETS, json=update.model_dump())
            response.raise_for_status()
    except HTTPError as e:
        logger.error("[Monitor] Error while trying to update targets for probe server %s: %s %s - %s",
                     server.name, e.request.method, e.request.url, e)


class TargetMonitor:

    def __init__(self, db_session: async_sessionmaker):
        self.db_session = db_session

        # lock indicating new services are being loaded into the ready list
        self._loading: anyio.Lock = anyio.Lock()
        self._last_load: float = 0
        # capacity limiter for simultaneous service checks
        self._limiter: anyio.Semaphore = anyio.Semaphore(settings.MONITOR_MAX_CONCURRENCY)
        # services currently being checked
        self.services: TargetServiceContainer = TargetServiceContainer()

    def start_tasks(self, tg: TaskGroup):
        """
        starts all tasks necessary for target monitoring
        :param tg: task group to start tasks in
        """
        tg.start_soon(self.monitor_services)
        tg.start_soon(self.save_updated_services)
        tg.start_soon(self.update_probe_series_targets)
        tg.start_soon(self.log_statistics)

    async def log_statistics(self):
        """
        Task to log monitoring statistics
        """
        while True:
            async with self.db_session() as db:
                target_count = await crud.target_service.get_count_where_type_in(db, MONITOR_TYPES)
                scheduled = await crud.target_service.get_count_updatable_where_type_in(db, MONITOR_TYPES)
            logger.info("[Monitor] Monitoring %d targets, %d scheduled, %d loaded: %d ready, %d ongoing, %d done.",
                        target_count, scheduled, len(self.services), self.services.count_ready(),
                        self.count_ongoing(), self.services.count_done())
            await anyio.sleep(60)

    def count_ongoing(self) -> int:
        """
        :return: amount of currently ongoing service checks
        """
        return settings.MONITOR_MAX_CONCURRENCY - self._limiter.value

    async def monitor_services(self) -> None:
        """
        Task to monitor the target services continuously.
        """
        async with anyio.create_task_group() as tg:
            while not tg.cancel_scope.cancel_called:
                # load new services if list gets empty (and we are not already loading more)
                if self.services.count_ready() < settings.MONITOR_MAX_CONCURRENCY / 2 and not self._loading.locked():
                    await tg.start(self.load_next_updatable)

                # get next service, wait at most one load interval to force reload if necessary
                async with anyio.move_on_after(settings.MONITOR_INTERVAL_LOAD) as service_scope:
                    service = await self.services.get_next_ready()
                if not service_scope.cancel_called:
                    # check next service, wait for task to start before starting the next
                    await tg.start(self.check_target_service, service)

    async def load_next_updatable(self, *, task_status: TaskStatus[None] = TASK_STATUS_IGNORED):
        """
        Task to refresh the list of targets to check. Acquires the loading lock to make sure only one task is loading
        new services at a time.
        :param task_status: called as soon as the lock has been acquired
        """
        async with self._loading:
            task_status.started()
            # load at most once per load interval
            last_load_delta = time.perf_counter() - self._last_load
            if last_load_delta < settings.MONITOR_INTERVAL_LOAD:
                await anyio.sleep(settings.MONITOR_INTERVAL_LOAD - last_load_delta)
            async with self.db_session() as db:
                # get next services to update that are not already loaded
                services = await crud.target_service.get_updatable_where_type_in(
                    db,
                    MONITOR_TYPES,
                    self.services.get_loaded(),
                    settings.MONITOR_MAX_CONCURRENCY
                )
                db.expunge_all()  # remove from database session, the save task uses its own session
                self.services.add_ready(services)
                logger.debug("[Monitor] Loaded %d services for checking", len(services))
            self._last_load = time.perf_counter()

    async def check_target_service(self, service: entities.TargetService,
                                   *, task_status: TaskStatus[None] = TASK_STATUS_IGNORED) -> None:
        """
        Checks a services status by performing a connection test. Uses the capacity limiter to limit the amount of
        concurrent service checks.
        :param service: the service to check
        :param task_status: called as soon as the task starts (when the capacity limiter has a token available)
        """
        async with self._limiter:
            task_status.started()
            if service.type == ProbeSeriesType.PUBLIC_ICMP_PING:
                new_status = await check_status_icmp(service.host.address)
            elif service.type == ProbeSeriesType.PUBLIC_UDP_DNS:
                new_status = await check_status_dns_udp(service.host.address, service.port)
            elif service.type == ProbeSeriesType.PUBLIC_TCP_DNS:
                new_status = await check_status_dns_tcp(service.host.address, service.port)
            elif service.type == ProbeSeriesType.PUBLIC_TCP_SMTP:
                new_status = await check_status_smtp(service.host.address, service.port)
            else:
                logger.warning("[Monitor] Don't know how to monitor target of type %s.", service.type)
                new_status = service.status

            self.services.set_done(service, new_status)

    async def save_updated_services(self):
        """
        Task to save updated services at the same rate new services get loaded. Calculates the new service status based
        on the recent measurement and the service status history. Saves the status in the service entries and creates
        an additional history entry for each service that was checked.
        """
        while True:
            done = self.services.get_all_done()
            # skip if there is nothing to do
            if len(done) == 0:
                await anyio.sleep(settings.MONITOR_INTERVAL_LOAD)
                continue

            t = time.perf_counter()
            updates = []
            inserts = []
            async with self.db_session() as db:
                for service, new_status, timestamp in done:
                    service = await db.merge(service, load=False)  # attach the service to this database session
                    history = await crud.target_service_history.get_last(db, service.id, MONITOR_HISTORY_FETCH)

                    # calculate new status
                    flapping = detect_flapping_state(service.flapping, new_status, history)
                    if flapping:
                        service.status = Status.OK  # keep service as OK as long as flapping is detected
                        interval = settings.MONITOR_INTERVAL_FLAP
                    else:
                        service.status = get_status_sticky(service.status, new_status, history,
                                                           settings.MONITOR_STATUS_STICKY)
                        interval = get_next_check_interval(new_status, history)
                    service.flapping = flapping
                    service.next_update = timestamp + timedelta(seconds=interval)
                    service.last_update = timestamp

                    updates.append(service)
                    inserts.append({
                        "target_service_id": service.id,
                        "timestamp": timestamp,
                        "status": new_status,
                        "flapping": flapping
                    })
                if len(inserts) > 0:
                    await crud.target_service_history.create_bulk(db, inserts)
                    await crud.target_service.update_bulk(db, updates)
            # remove IDs from container after database operations
            self.services.remove_all([s.id for s, _, _ in done])
            diff = time.perf_counter() - t
            logger.debug("[Monitor] Saved %d service updates in %.3fs (%d loaded: %d ready, %d ongoing, %d done).",
                         len(inserts), diff, len(self.services), self.services.count_ready(),
                         self.count_ongoing(), self.services.count_done())
            # sleep at most one load interval
            if diff < settings.MONITOR_INTERVAL_LOAD:
                await anyio.sleep(settings.MONITOR_INTERVAL_LOAD - diff)

    async def update_probe_series_targets(self) -> None:
        """
        Task to update the probe series targets periodically.
        """
        while True:
            update = models.ProbeSeriesTargetUpdate()
            async with self.db_session() as db:
                ongoing_series = await crud.probe_series.get_where_status(db, ProbeSeriesStatus.ONGOING)
                for series in ongoing_series:
                    config = models.ProbeSeriesConfig.model_validate(series.config)

                    removed = []
                    # remove disabled hosts
                    removed.extend(await crud.target_service.delete_where_host_disabled(db, series.id))
                    # remove errored services
                    if config.exclude_errored:
                        removed.extend(await crud.target_service.delete_where_series_and_status(
                            db, series.id, Status.ERROR
                        ))
                    # remove flapping service
                    if config.exclude_flapping:
                        removed.extend(await crud.target_service.delete_where_series_and_flapping(
                            db, series.id, True
                        ))

                    # get target count after removal
                    temp_target_count = await crud.probe_series.get_target_count_where_series(db, series.id)

                    added = []
                    added_v4 = []
                    added_v6 = []
                    added_rem = []
                    # add available targets
                    if temp_target_count < config.targets:
                        # add 50% IPv4 targets
                        used_v4 = await crud.target_service.get_count_used(db, series.id, 4)
                        available_v4 = await crud.target_service.get_count_available(
                            db, series.id, series.type, config.exclude_errored, config.exclude_flapping, 4
                        )
                        want_v4 = int((config.targets / 2) - used_v4)
                        if available_v4 > 0 and want_v4 > 0:
                            added_v4 = await crud.target_service.insert_available(
                                db, series.id, series.type, config.exclude_errored, config.exclude_flapping, 4, want_v4
                            )
                        # add 50% IPv6 targets
                        used_v6 = await crud.target_service.get_count_used(db, series.id, 6)
                        available_v6 = await crud.target_service.get_count_available(
                            db, series.id, series.type, config.exclude_errored, config.exclude_flapping, 6
                        )
                        want_v6 = int((config.targets / 2) - used_v6)
                        if available_v6 > 0 and want_v6 > 0:
                            added_v6 = await crud.target_service.insert_available(
                                db, series.id, series.type, config.exclude_errored, config.exclude_flapping, 6, want_v6
                            )
                        # fill up with any remaining
                        if temp_target_count + len(added_v4) + len(added_v6) < config.targets:
                            added_rem = await crud.target_service.insert_available(
                                db, series.id, series.type, config.exclude_errored, config.exclude_flapping, None,
                                config.targets - temp_target_count - len(added_v4) - len(added_v6)
                            )
                        added = added_v4 + added_v6 + added_rem

                    # get final target count
                    target_count = await crud.probe_series.get_target_count_where_series(db, series.id)
                    if config.targets > target_count:
                        logger.warning(
                            "[PS-%03d] Need %d targets, only %d are available (removed %d errored or flapping targets, "
                            "added %d new targets [%d IPv4, %d IPv6, %d rem])",
                            series.id, config.targets, target_count, len(removed),
                            len(added), len(added_v4), len(added_v6), len(added_rem)
                        )
                    else:
                        logger.info(
                            "[PS-%03d] Need %d targets, currently using %d (removed %d errored or flapping targets, "
                            "added %d new targets [%d IPv4, %d IPv6, %d rem])",
                            series.id, config.targets, target_count, len(removed),
                            len(added), len(added_v4), len(added_v6), len(added_rem)
                        )
                    if len(added) > 0 or len(removed) > 0:
                        update.updates[series.id] = models.ProbeSeriesTargetChanges(
                            added=added, removed=removed, total=target_count
                        )

            # notify probe servers
            if len(update.updates) > 0:
                async with self.db_session() as db:
                    servers = await crud.server.get_all(db)
                async with anyio.create_task_group() as tg:
                    for server in servers:
                        tg.start_soon(_notify_probe_server, server, update)

            # sleep for next round
            await anyio.sleep(settings.SERIES_UPDATE_TARGETS_INTERVAL)
