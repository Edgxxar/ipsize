from datetime import datetime
from typing import Generic, Optional, Sequence, Type, TypeVar

from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from sqlalchemy import func, insert, select
from sqlalchemy.ext.asyncio import AsyncSession

from ipsize.control.persistence.database import Base

EntityType = TypeVar("EntityType", bound=Base)
ModelType = TypeVar("ModelType", bound=BaseModel)
CreateModelType = TypeVar("CreateModelType", bound=BaseModel)


class CRUDBase(Generic[EntityType, CreateModelType]):
    def __init__(self, entity: Type[EntityType]):
        self.entity = entity

    async def get(self, db: AsyncSession, entity_id: int) -> Optional[EntityType]:
        query = select(self.entity).filter(self.entity.id == entity_id)
        result = await db.scalars(query)
        return result.first()

    async def get_all(self, db: AsyncSession, skip: int = 0, limit: int = None) -> Sequence[EntityType]:
        query = select(self.entity)
        if skip > 0:
            query.offset(skip)
        if limit is not None:
            query.limit(limit)
        result = await db.scalars(query)
        return result.all()

    async def get_count_all(self, db: AsyncSession) -> int:
        query = select(func.count(self.entity.id))
        result = await db.scalar(query)
        return result

    async def create(self, db: AsyncSession, create_model: CreateModelType) -> EntityType:
        obj_in_data = jsonable_encoder(create_model, custom_encoder={
            datetime: lambda v: v  # dont encode datetime values
        })
        db_obj = self.entity(**obj_in_data)
        db.add(db_obj)
        await db.commit()
        await db.refresh(db_obj)
        return db_obj

    async def create_bulk(self, db: AsyncSession, entities: list[dict]):
        await db.execute(insert(self.entity), entities)
        await db.commit()

    async def update(self, db: AsyncSession, entity: EntityType) -> EntityType:
        db.add(entity)
        await db.commit()
        await db.refresh(entity)
        return entity

    async def update_bulk(self, db: AsyncSession, entities: list[EntityType]) -> None:
        db.add_all(entities)
        await db.commit()
