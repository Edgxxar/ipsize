from typing import Optional, Sequence

from sqlalchemy import func, select, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload

from ipsize import models
from ipsize.control.persistence import entities
from ipsize.control.persistence.crud.base import CRUDBase
from ipsize.control.persistence.entities.relationships import probe_series_targets
from ipsize.models.enum import ProbeSeriesStatus


class CRUDProbeSeries(CRUDBase[entities.ProbeSeries, models.ProbeSeriesCreate]):

    async def get_where_status_not_in(
            self,
            db: AsyncSession,
            status: list[ProbeSeriesStatus]
    ) -> Sequence[entities.ProbeSeries]:
        query = select(entities.ProbeSeries).filter(entities.ProbeSeries.status.not_in(status))
        result = await db.scalars(query)
        return result.all()

    async def get_where_status(
            self,
            db: AsyncSession,
            status: ProbeSeriesStatus
    ) -> Sequence[entities.ProbeSeries]:
        query = select(entities.ProbeSeries).filter(entities.ProbeSeries.status == status)
        result = await db.scalars(query)
        return result.all()

    async def update_config(self, db: AsyncSession, series: entities.ProbeSeries, config: models.ProbeSeriesConfig):
        query = update(entities.ProbeSeries).values(
            {entities.ProbeSeries.config: config.model_dump()}
        ).filter(
            entities.ProbeSeries.id == series.id
        )
        await db.execute(query)
        await db.commit()
        await db.refresh(series)
        return series

    async def update_status(self, db: AsyncSession, series: entities.ProbeSeries, status: ProbeSeriesStatus):
        query = update(entities.ProbeSeries).values(
            {entities.ProbeSeries.status: status}
        ).filter(
            entities.ProbeSeries.id == series.id
        )
        await db.execute(query)
        await db.commit()
        await db.refresh(series)
        return series

    async def get_target_count_where_series(self, db: AsyncSession, series_id: int) -> int:
        query = select(func.count(probe_series_targets.c.target_service_id)).filter(
            probe_series_targets.c.probe_series_id == series_id
        )
        result = await db.scalar(query)
        return result


class CRUDProbeDownload(CRUDBase[entities.ProbeDownload, models.ProbeDownloadCreate]):

    async def get_next_where_not_imported(self, db: AsyncSession) -> Optional[entities.ProbeDownload]:
        query = select(
            entities.ProbeDownload
        ).filter(
            entities.ProbeDownload.imported_at.is_(None)
        ).filter(
            entities.ProbeDownload.error.is_(None)
        ).options(
            joinedload(entities.ProbeDownload.probe_series)
        ).order_by(entities.ProbeDownload.downloaded_at.asc()).limit(1)
        result = await db.scalars(query)
        return result.first()

    async def get_where_series_and_file(
            self,
            db: AsyncSession,
            probe_series_id: int,
            file_name: str
    ) -> Optional[entities.ProbeDownload]:
        query = select(
            entities.ProbeDownload
        ).filter(
            entities.ProbeDownload.probe_series_id == probe_series_id
        ).filter(
            entities.ProbeDownload.file_name == file_name
        ).options(
            joinedload(entities.ProbeDownload.probe_series)
        )
        result = await db.scalars(query)
        return result.first()

    async def get_all_where_errored_and_series(
            self,
            db: AsyncSession,
            probe_series_id: int,
    ) -> Sequence[entities.ProbeDownload]:
        query = select(
            entities.ProbeDownload
        ).filter(
            entities.ProbeDownload.probe_series_id == probe_series_id
        ).filter(
            entities.ProbeDownload.error.is_(True)
        ).options(
            joinedload(entities.ProbeDownload.probe_series)
        )
        result = await db.scalars(query)
        return result.all()

    async def get_all_where_name_like(
            self,
            db: AsyncSession,
            name: str,
    ) -> Sequence[entities.ProbeDownload]:
        query = select(
            entities.ProbeDownload
        ).filter(
            entities.ProbeDownload.file_name.like(name)
        )
        result = await db.scalars(query)
        return result.all()


probe_series = CRUDProbeSeries(entities.ProbeSeries)
probe_download = CRUDProbeDownload(entities.ProbeDownload)
