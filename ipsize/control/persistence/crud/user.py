from typing import Optional

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ipsize import models, security
from ipsize.control.persistence import entities
from ipsize.control.persistence.crud.base import CRUDBase


class CRUDUser(CRUDBase[entities.User, models.UserCreate]):

    async def get_by_email(self, db: AsyncSession, email: str) -> Optional[entities.User]:
        query = select(entities.User).filter(entities.User.email == email)
        result = await db.scalars(query)
        return result.first()

    async def create(self, db: AsyncSession, user: models.UserCreate) -> entities.User:
        db_user = entities.User(email=user.email, hashed_password=security.hash_password(user.password))
        db.add(db_user)
        await db.commit()
        await db.refresh(db_user)
        return db_user


user = CRUDUser(entities.User)
