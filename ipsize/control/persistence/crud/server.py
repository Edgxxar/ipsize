from datetime import datetime
from ipaddress import IPv4Address, IPv6Address
from typing import Optional, Sequence, Union

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload

from ipsize import models, security
from ipsize.control.config import settings
from ipsize.control.persistence import entities
from ipsize.control.persistence.crud.base import CRUDBase


class CRUDServer(CRUDBase[entities.Server, models.ServerCreate]):

    async def get_join_addresses(self, db: AsyncSession, id: int) -> Optional[entities.Server]:
        query = select(
            entities.Server
        ).filter(
            entities.Server.id == id
        ).options(
            joinedload(entities.Server.addresses)
        )
        result = await db.scalars(query)
        return result.first()

    async def get_all_join_addresses(self, db: AsyncSession) -> Sequence[entities.Server]:
        query = select(
            entities.Server
        ).options(
            joinedload(entities.Server.addresses)
        )
        result = await db.execute(query)
        return result.unique().scalars().all()

    async def get_by_name(self, db: AsyncSession, name: str) -> Optional[entities.Server]:
        query = select(
            entities.Server
        ).filter(
            entities.Server.name == name
        ).options(
            joinedload(entities.Server.addresses)
        )
        result = await db.scalars(query)
        return result.first()

    async def get_by_address(self, db: AsyncSession, address: str) -> Optional[entities.Server]:
        query = select(
            entities.Server
        ).join(
            entities.Server.addresses
        ).filter(
            entities.ServerAddress.address == address
        ).options(
            joinedload(entities.Server.addresses)
        )
        result = await db.scalars(query)
        return result.first()

    async def get_latest_download_time(self, db: AsyncSession) -> Optional[datetime]:
        query = select(
            entities.Server.last_download
        ).order_by(
            entities.Server.last_download.desc().nulls_last()
        ).limit(1)
        result = await db.scalar(query)
        return result

    async def get_one_order_by_last_download(self, db: AsyncSession) -> Optional[entities.Server]:
        query = select(
            entities.Server
        ).order_by(
            entities.Server.last_download.asc().nulls_first()
        ).options(
            joinedload(entities.Server.addresses)
        ).limit(1)
        result = await db.scalars(query)
        return result.first()

    async def create(self, db: AsyncSession, server: models.ServerCreate) -> entities.Server:
        server_secret = security.hash_password(server.server_secret)
        control_secret, salt = security.encrypt_string(server.control_secret, settings.SECRET_KEY)
        db_server = entities.Server(
            **server.model_dump(exclude={"server_secret", "control_secret"}),
            last_update=datetime.now(),
            server_secret=server_secret,
            control_secret=control_secret,
            control_secret_salt=salt
        )
        db.add(db_server)
        await db.commit()
        await db.refresh(db_server)
        return db_server

    async def update_server_secret(self, db: AsyncSession, server: entities.Server, secret: str) -> entities.Server:
        server.server_secret = security.hash_password(secret)
        server.last_update = datetime.now()
        db.add(server)
        await db.commit()
        await db.refresh(server)
        return server

    async def update_control_secret(self, db: AsyncSession, server: entities.Server, secret: str) -> entities.Server:
        control_secret, salt = security.encrypt_string(secret, settings.SECRET_KEY)
        server.control_secret = control_secret
        server.control_secret_salt = salt
        server.last_update = datetime.now()
        db.add(server)
        await db.commit()
        await db.refresh(server)
        return server

    async def get_address(
            self, db: AsyncSession, address: Union[IPv4Address, IPv6Address]
    ) -> Optional[entities.ServerAddress]:
        query = select(
            entities.ServerAddress
        ).filter(
            entities.ServerAddress.address == address
        ).options(
            joinedload(entities.ServerAddress.server)
        )
        result = await db.scalars(query)
        return result.first()

    async def add_address(self, db: AsyncSession, server_id: int, address: Union[IPv4Address, IPv6Address]):
        db_address = entities.ServerAddress(server_id=server_id, address=address)
        db.add(db_address)
        await db.commit()

    async def del_address(self, db: AsyncSession, address: entities.ServerAddress):
        await db.delete(address)
        await db.commit()


server = CRUDServer(entities.Server)
