from typing import Iterable, Literal, Optional, Sequence

from pydantic import IPvAnyAddress, IPvAnyNetwork
from sqlalchemy import and_, delete, func, insert, literal, or_, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload, selectinload

from ipsize import models
from ipsize.control.persistence import entities
from ipsize.control.persistence.crud.base import CRUDBase
from ipsize.control.persistence.entities.relationships import probe_series_targets
from ipsize.models.enum import ProbeSeriesType, Status


class CRUDTargetHost(CRUDBase[entities.TargetHost, models.TargetHostCreate]):

    async def get_by_address(self, db: AsyncSession, address: IPvAnyAddress) -> Optional[entities.TargetHost]:
        query = select(
            entities.TargetHost
        ).filter(
            entities.TargetHost.address == address
        )
        result = await db.scalars(query)
        return result.first()

    async def get_by_address_join_services(
            self, db: AsyncSession, address: IPvAnyAddress
    ) -> Optional[entities.TargetHost]:
        query = select(
            entities.TargetHost
        ).filter(
            entities.TargetHost.address == address
        ).options(
            joinedload(entities.TargetHost.services)
        )
        result = await db.scalars(query)
        return result.first()

    async def get_where_in_network_load_services(
            self, db: AsyncSession, network: IPvAnyNetwork
    ) -> Sequence[entities.TargetHost]:
        query = select(
            entities.TargetHost
        ).filter(
            entities.TargetHost.address.op("<<", is_comparison=True)(network)
        ).options(
            selectinload(entities.TargetHost.services)
        )
        result = await db.scalars(query)
        return result.all()

    async def get_where_address_version(
            self, db: AsyncSession, version: Literal[4, 6]
    ) -> Sequence[entities.TargetHost]:
        query = select(
            entities.TargetHost
        ).filter(
            func.inet.family(entities.TargetHost.address) == version
        ).options(
            selectinload(entities.TargetHost.services)
        )
        result = await db.scalars(query)
        return result.all()

    async def get_join_services(self, db: AsyncSession, host_id: int) -> Optional[entities.TargetHost]:
        query = select(
            entities.TargetHost
        ).filter(
            entities.TargetHost.id == host_id
        ).options(
            joinedload(entities.TargetHost.services)
        )
        result = await db.scalars(query)
        return result.first()

    async def get_where_service_not_exists(
            self,
            db: AsyncSession,
            type: ProbeSeriesType
    ) -> Sequence[entities.TargetHost]:
        query = select(
            entities.TargetHost
        ).filter(
            ~entities.TargetHost.id.in_(
                select(
                    entities.TargetService.target_host_id
                ).filter(
                    entities.TargetService.type == type
                )
            )
        )
        result = await db.scalars(query)
        return result.all()

    async def get_where_location_is_null(self, db: AsyncSession) -> Sequence[entities.TargetHost]:
        query = select(
            entities.TargetHost
        ).join(
            entities.TargetHost.services
        ).filter(
            entities.TargetHost.location.is_(None)
        ).filter(
            entities.TargetHost.enabled.is_(True)
        ).group_by(
            entities.TargetHost.id
        ).having(
            func.count(entities.TargetService.id).filter(entities.TargetService.status == Status.OK) > 0
        ).order_by(
            entities.TargetHost.last_update.asc()
        )
        result = await db.scalars(query)
        return result.all()


class CRUDTargetService(CRUDBase[entities.TargetService, models.TargetServiceCreate]):

    async def get_join_host(self, db: AsyncSession, service_id: int) -> Optional[entities.TargetService]:
        query = select(entities.TargetService).filter(
            entities.TargetService.id == service_id
        ).options(
            joinedload(entities.TargetService.host)
        )
        result = await db.scalars(query)
        return result.first()

    async def get_count_where_type_in(self, db: AsyncSession, types: set[ProbeSeriesType]) -> int:
        query = select(
            func.count(entities.TargetService.id)
        ).join(
            entities.TargetService.host
        ).filter(  # match types
            entities.TargetService.type.in_(types)
        ).filter(  # make sure host is enabled
            entities.TargetHost.enabled.is_(True)
        )
        result = await db.scalar(query)
        return result

    async def get_count_updatable_where_type_in(
            self,
            db: AsyncSession,
            types: set[ProbeSeriesType],
    ) -> int:
        query = select(
            func.count(entities.TargetService.id)
        ).join(
            entities.TargetService.host
        ).filter(  # match types
            entities.TargetService.type.in_(types)
        ).filter(  # find only services where next_update is null or not in the future
            or_(entities.TargetService.next_update <= func.now(), entities.TargetService.next_update.is_(None))
        ).filter(  # make sure host is enabled
            entities.TargetHost.enabled.is_(True)
        )
        result = await db.scalar(query)
        return result

    async def get_updatable_where_type_in(
            self,
            db: AsyncSession,
            types: set[ProbeSeriesType],
            exclude: Iterable[int],
            limit: int
    ) -> Sequence[entities.TargetService]:
        query = select(
            entities.TargetService
        ).join(
            entities.TargetService.host
        ).filter(  # match types
            entities.TargetService.type.in_(types)
        ).filter(  # find only services where next_update is null or not in the future
            or_(entities.TargetService.next_update <= func.now(), entities.TargetService.next_update.is_(None))
        ).filter(  # exclude services that are already prefetched
            entities.TargetService.id.notin_(exclude)
        ).filter(  # make sure host is enabled
            entities.TargetHost.enabled.is_(True)
        ).order_by(  # order by next update first
            entities.TargetService.next_update.asc().nulls_last()
        ).order_by(  # then find all other services
            entities.TargetService.last_update.asc()
        ).options(  # join with host
            joinedload(entities.TargetService.host)
        ).limit(limit)
        result = await db.scalars(query)
        return result.all()

    async def get_count_where_series(self, db: AsyncSession, series_id: int) -> int:
        query = select(func.count(entities.TargetService.id)) \
            .join(entities.TargetService.probe_series) \
            .filter(entities.ProbeSeries.id == series_id)
        result = await db.scalar(query)
        return result

    def map_to_model(self, service: entities.TargetService) -> models.TargetServiceModel:
        return models.TargetServiceModel(
            id=service.id,
            type=service.type,
            port=service.port,
            target_host_id=service.host.id,
            address=service.host.address,
        )

    async def get_models_where_ids_in(self, db: AsyncSession, ids: list[int]) -> list[models.TargetServiceModel]:
        query = select(entities.TargetService).filter(
            entities.TargetService.id.in_(ids)
        ).options(
            joinedload(entities.TargetService.host)
        )
        result = await db.scalars(query)
        return [self.map_to_model(service) for service in result.all()]

    async def get_models_where_id_range(
            self,
            db: AsyncSession,
            min_id: int,
            max_id: int
    ) -> Sequence[models.TargetServiceModel]:
        query = select(entities.TargetService).filter(
            entities.TargetService.id >= min_id
        ).filter(
            entities.TargetService.id <= max_id
        ).options(
            joinedload(entities.TargetService.host)
        )
        result = await db.scalars(query)
        return [self.map_to_model(service) for service in result.all()]

    async def get_models_where_series(
            self,
            db: AsyncSession,
            series_id: int,
            limit: int,
            offset: int
    ) -> list[models.TargetServiceModel]:
        query = select(
            entities.TargetService
        ).join(
            entities.TargetService.probe_series
        ).join(
            entities.TargetService.host
        ).filter(
            entities.ProbeSeries.id == series_id
        ).order_by(
            entities.TargetService.id
        ).options(
            joinedload(entities.TargetService.host)
        ).limit(limit).offset(offset)

        result = await db.scalars(query)
        return [self.map_to_model(service) for service in result.all()]

    async def get_ids_where_series(
            self,
            db: AsyncSession,
            series_id: int,
    ) -> Sequence[int]:
        query = select(
            entities.TargetService.id
        ).join(
            probe_series_targets,
            and_(
                probe_series_targets.c.target_service_id == entities.TargetService.id,
                probe_series_targets.c.probe_series_id == series_id
            )
        ).order_by(entities.TargetService.id)

        result = await db.scalars(query)
        return result.all()

    async def delete_where_host_disabled(
            self,
            db: AsyncSession,
            series_id: int
    ) -> Sequence[int]:
        subquery = select(
            entities.TargetService.id
        ).join(
            probe_series_targets,
            and_(
                probe_series_targets.c.target_service_id == entities.TargetService.id,
                probe_series_targets.c.probe_series_id == series_id,
            )
        ).join(
            entities.TargetHost,
            and_(
                entities.TargetHost.id == entities.TargetService.target_host_id,
                entities.TargetHost.enabled.is_(False)
            )
        )
        query = delete(probe_series_targets).filter(
            probe_series_targets.c.target_service_id.in_(subquery)
        ).returning(probe_series_targets.c.target_service_id)
        result = await db.scalars(query)
        await db.commit()
        return result.all()

    async def delete_where_series_and_status(
            self,
            db: AsyncSession,
            series_id: int,
            status: Status) -> Sequence[int]:
        subquery = select(
            entities.TargetService.id
        ).join(
            probe_series_targets,
            and_(
                probe_series_targets.c.target_service_id == entities.TargetService.id,
                probe_series_targets.c.probe_series_id == series_id,
                entities.TargetService.status == status
            )
        )
        query = delete(probe_series_targets).filter(
            probe_series_targets.c.target_service_id.in_(subquery)
        ).returning(probe_series_targets.c.target_service_id)
        result = await db.scalars(query)
        await db.commit()
        return result.all()

    async def delete_where_series_and_flapping(
            self,
            db: AsyncSession,
            series_id: int,
            flapping: bool):
        subquery = select(
            entities.TargetService.id
        ).join(
            probe_series_targets,
            and_(
                probe_series_targets.c.target_service_id == entities.TargetService.id,
                probe_series_targets.c.probe_series_id == series_id,
                entities.TargetService.flapping == flapping
            )
        )
        query = delete(probe_series_targets).filter(
            probe_series_targets.c.target_service_id.in_(subquery)
        ).returning(probe_series_targets.c.target_service_id)
        result = await db.scalars(query)
        await db.commit()
        return result.all()

    async def get_count_used(
            self,
            db: AsyncSession,
            series_id: int,
            address_family: Literal[4, 6]
    ) -> int:
        query = select(
            func.count(entities.TargetService.id)
        ).join(
            probe_series_targets,
            and_(
                probe_series_targets.c.target_service_id == entities.TargetService.id,
                probe_series_targets.c.probe_series_id == series_id
            )
        ).join(
            entities.TargetHost,
            and_(
                entities.TargetHost.id == entities.TargetService.target_host_id,
                func.inet.family(entities.TargetHost.address) == address_family
            )
        )
        result = await db.scalar(query)
        return result

    async def get_count_available(
            self,
            db: AsyncSession,
            series_id: int,
            type: ProbeSeriesType,
            exclude_errored: bool,
            exclude_flapping: bool,
            address_family: Literal[4, 6]
    ) -> int:
        sel = select(
            entities.TargetService.id,
        ).join(
            probe_series_targets,
            and_(
                probe_series_targets.c.target_service_id == entities.TargetService.id,
                probe_series_targets.c.probe_series_id == series_id
            ),
            isouter=True
        ).join(
            entities.TargetHost,
            and_(
                entities.TargetHost.id == entities.TargetService.target_host_id,
                entities.TargetHost.enabled.is_(True),
                func.inet.family(entities.TargetHost.address) == address_family
            )
        ).filter(
            probe_series_targets.c.probe_series_id.is_(None)
        ).filter(
            entities.TargetService.type == type
        )
        if exclude_errored:
            sel = sel.filter(entities.TargetService.status == Status.OK)
        else:
            sel = sel.filter(entities.TargetService.status != Status.UNKNOWN)
        if exclude_flapping:
            sel = sel.filter(entities.TargetService.flapping.is_(False))
        query = select(func.count(entities.TargetService.id)).filter(
            entities.TargetService.id.in_(sel)
        )
        result = await db.scalar(query)
        return result

    async def insert_available(
            self,
            db: AsyncSession,
            series_id: int,
            type: ProbeSeriesType,
            exclude_errored: bool,
            exclude_flapping: bool,
            address_family: Optional[Literal[4, 6]],
            count: int
    ) -> Sequence[int]:
        if address_family is None:
            join_host = and_(
                entities.TargetHost.id == entities.TargetService.target_host_id,
                entities.TargetHost.enabled.is_(True)
            )
        else:
            join_host = and_(
                entities.TargetHost.id == entities.TargetService.target_host_id,
                entities.TargetHost.enabled.is_(True),
                func.inet.family(entities.TargetHost.address) == address_family
            )
        sel = select(
            entities.TargetService.id,
            literal(series_id)
        ).join(
            probe_series_targets,
            and_(
                probe_series_targets.c.target_service_id == entities.TargetService.id,
                probe_series_targets.c.probe_series_id == series_id
            ),
            isouter=True
        ).join(
            entities.TargetHost, join_host
        ).filter(
            probe_series_targets.c.probe_series_id.is_(None)
        ).filter(
            entities.TargetService.type == type
        )
        if exclude_errored:
            sel = sel.filter(entities.TargetService.status == Status.OK)
        else:
            sel = sel.filter(entities.TargetService.status != Status.UNKNOWN)
        if exclude_flapping:
            sel = sel.filter(entities.TargetService.flapping.is_(False))
        sel = sel.order_by(entities.TargetService.last_update.desc()).limit(count)
        query = insert(probe_series_targets).from_select(
            [probe_series_targets.c.target_service_id, probe_series_targets.c.probe_series_id], sel
        ).returning(probe_series_targets.c.target_service_id)
        result = await db.scalars(query)
        await db.commit()
        return result.all()

    async def insert_target(
            self,
            db: AsyncSession,
            series_id: int,
            service_id: int
    ):
        query = insert(probe_series_targets).values({
            probe_series_targets.c.probe_series_id: series_id,
            probe_series_targets.c.target_service_id: service_id
        })
        await db.execute(query)
        await db.commit()


class CRUDTargetServiceHistory(CRUDBase[entities.TargetServiceHistory, models.TargetServiceHistory]):

    async def get_last(self, db: AsyncSession, service_id: int, limit: int) -> Sequence[entities.TargetServiceHistory]:
        query = select(entities.TargetServiceHistory).filter(
            entities.TargetServiceHistory.target_service_id == service_id
        ).order_by(entities.TargetServiceHistory.timestamp.desc()).limit(limit)
        result = await db.scalars(query)
        return result.all()


target_host = CRUDTargetHost(entities.TargetHost)
target_service = CRUDTargetService(entities.TargetService)
target_service_history = CRUDTargetServiceHistory(entities.TargetServiceHistory)
