"""
Create, Read, Update, Delete operations. Takes in and returns pydantic models as arguments,
queries the database using entities. Mapping between the classes is done automatically by SQLAlchemy .
"""
from .probe import probe_series, probe_download
from .server import server
from .target import target_host, target_service, target_service_history
from .user import user
