from sqlalchemy import Column, ForeignKey, Table

from ipsize.control.persistence.database import Base

# association table between ProbeSeries and TargetServices
probe_series_targets = Table(
    "probe_series_targets",
    Base.metadata,
    Column("probe_series_id", ForeignKey("probe_series.id"), primary_key=True, index=True),
    Column("target_service_id", ForeignKey("target_services.id"), primary_key=True, index=True)
)
