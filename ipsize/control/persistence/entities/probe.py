from datetime import datetime
from typing import Optional

from pydantic import Json
from sqlalchemy import BigInteger, Boolean, Column, DateTime, ForeignKey, Integer, SmallInteger, String, Table, sql
from sqlalchemy.dialects.postgresql import BYTEA
from sqlalchemy.orm import Mapped, mapped_column, relationship

from ipsize.control.persistence.database import Base
from ipsize.models.enum import ProbeSeriesStatus, ProbeSeriesType


class ProbeSeries(Base):
    __tablename__ = "probe_series"

    id: Mapped[int] = mapped_column(SmallInteger, primary_key=True, index=True)
    type: Mapped[ProbeSeriesType]
    config: Mapped[Json]
    status: Mapped[ProbeSeriesStatus]
    start_time: Mapped[Optional[datetime]]
    finish_time: Mapped[Optional[datetime]]


class ProbeDownload(Base):
    __tablename__ = "probe_download"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    probe_series_id: Mapped[int] = mapped_column(SmallInteger, ForeignKey("probe_series.id"))
    server_id: Mapped[int] = mapped_column(SmallInteger, ForeignKey("probe_servers.id"))
    file_name: Mapped[str]
    file_size: Mapped[int] = mapped_column(BigInteger)
    downloaded_at: Mapped[datetime]
    imported_at: Mapped[Optional[datetime]]
    import_time: Mapped[Optional[float]]
    packet_count: Mapped[Optional[int]]
    error: Mapped[Optional[bool]]

    probe_series: Mapped[ProbeSeries] = relationship()


# table descriptions for bulk storage without primary key, cannot use ORM mapped declaration

probe_packets_sent = Table(  # private probe series - sent packets
    "probe_packets_sent",
    Base.metadata,
    Column("probe_series_id", SmallInteger, nullable=False),
    Column("sequence_number", Integer, nullable=False),
    Column("server_id", SmallInteger, nullable=False),
    Column("target_service_id", Integer, nullable=False),
    Column("send_time", DateTime(timezone=True), nullable=False),
    Column("size", SmallInteger, nullable=False),
)

probe_packets_received = Table(  # private probe series - received packets
    "probe_packets_received",
    Base.metadata,
    Column("probe_series_id", SmallInteger, nullable=False),
    Column("sequence_number", Integer, nullable=False),
    Column("server_id", SmallInteger, nullable=False),
    Column("target_service_id", Integer, nullable=False),
    Column("recv_time", DateTime(timezone=True), nullable=False),
    Column("size", SmallInteger, nullable=False),
)

probe_packets_public = Table(  # public probe series - one entry per probe
    "probe_packets_public",
    Base.metadata,
    Column("probe_series_id", SmallInteger, nullable=False),
    Column("sequence_number", Integer, nullable=False),
    Column("server_id", SmallInteger, nullable=False),
    Column("target_service_id", Integer, nullable=False),
    Column("probe_time", DateTime(timezone=True), nullable=False),
    Column("send_time", DateTime(timezone=True), nullable=True),
    Column("recv_time", DateTime(timezone=True), nullable=True),
    Column("request_received", Boolean, nullable=False, server_default=sql.false()),
    Column("latency", Integer, nullable=True),
    Column("delta", Integer, nullable=True),
    Column("size", SmallInteger, nullable=False),
    Column("data", BYTEA, nullable=True),
    Column("anomaly", String, nullable=True),
)
