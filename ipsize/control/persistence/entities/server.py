from datetime import datetime
from typing import List, Optional

from pydantic import IPvAnyAddress, Json
from sqlalchemy import ForeignKey, SmallInteger
from sqlalchemy.orm import Mapped, mapped_column, relationship

from ipsize.control.persistence.database import Base
from ipsize.models.enum import ConnectionType


class Server(Base):
    __tablename__ = "probe_servers"

    id: Mapped[int] = mapped_column(SmallInteger, primary_key=True, index=True)
    name: Mapped[str] = mapped_column(unique=True, index=True)
    url: Mapped[str]
    connection_type: Mapped[ConnectionType]
    status: Mapped[Optional[Json]]
    last_update: Mapped[datetime]
    last_download: Mapped[Optional[datetime]]
    server_secret: Mapped[str]
    control_secret: Mapped[str]
    control_secret_salt: Mapped[str]

    addresses: Mapped[List["ServerAddress"]] = relationship(back_populates="server", cascade="all, delete")


class ServerAddress(Base):
    __tablename__ = "probe_server_addresses"

    address: Mapped[IPvAnyAddress] = mapped_column(primary_key=True, index=True)
    server_id: Mapped[int] = mapped_column(SmallInteger, ForeignKey("probe_servers.id", ondelete="CASCADE"))

    server: Mapped[Server] = relationship(back_populates="addresses")
