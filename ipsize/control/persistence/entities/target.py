from datetime import datetime
from typing import List, Optional

from pydantic import IPvAnyAddress, Json
from sqlalchemy import ForeignKey, UniqueConstraint, func, sql
from sqlalchemy.orm import Mapped, mapped_column, relationship

from ipsize.control.persistence.database import Base
from ipsize.control.persistence.entities import ProbeSeries
from ipsize.control.persistence.entities.relationships import probe_series_targets
from ipsize.models.enum import ProbeSeriesType, Status


class TargetHost(Base):
    __tablename__ = "target_hosts"

    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    address: Mapped[IPvAnyAddress] = mapped_column(unique=True, index=True)
    location: Mapped[Optional[Json]]
    autonomous_system: Mapped[Optional[Json]]
    enabled: Mapped[bool] = mapped_column(server_default=sql.true())
    last_update: Mapped[datetime] = mapped_column(server_default=func.now())

    services: Mapped[List["TargetService"]] = relationship(back_populates="host")


class TargetService(Base):
    __tablename__ = "target_services"
    __table_args__ = (
        UniqueConstraint("target_host_id", "type"),
    )

    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    target_host_id: Mapped[int] = mapped_column(ForeignKey("target_hosts.id"), index=True)
    type: Mapped[ProbeSeriesType]
    port: Mapped[Optional[int]]
    status: Mapped[Status] = mapped_column(server_default=Status.UNKNOWN)
    flapping: Mapped[bool] = mapped_column(server_default=sql.false())
    last_update: Mapped[datetime] = mapped_column(server_default=func.now())
    next_update: Mapped[Optional[datetime]]

    host: Mapped[TargetHost] = relationship(back_populates="services")
    probe_series: Mapped[List[ProbeSeries]] = relationship(secondary=probe_series_targets)


class TargetServiceHistory(Base):
    __tablename__ = "target_services_history"

    target_service_id: Mapped[int] = mapped_column(ForeignKey("target_services.id"), primary_key=True, index=True)
    timestamp: Mapped[datetime] = mapped_column(primary_key=True, index=True)
    status: Mapped[Status]
    flapping: Mapped[bool] = mapped_column(server_default=sql.false())

    service: Mapped[TargetService] = relationship()
