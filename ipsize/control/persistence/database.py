from contextlib import asynccontextmanager
from datetime import datetime
from typing import Any, AsyncIterator

from psycopg import AsyncConnection, AsyncCopy, sql
from pydantic import Json
from pydantic.networks import IPvAnyAddress
from sqlalchemy import DateTime, Integer, Table
from sqlalchemy.dialects.postgresql import BYTEA, INET, JSONB
from sqlalchemy.ext.asyncio import AsyncAttrs
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.sql.functions import GenericFunction

from ipsize.control.config import settings


# logging.basicConfig()
# logging.getLogger("sqlalchemy.engine").setLevel(logging.INFO)


class Base(AsyncAttrs, DeclarativeBase):
    """
    Base class for all ORM tables. Includes type annotations for special types used throughout.
    """
    type_annotation_map = {
        # datetime always has a timezone
        datetime: DateTime(timezone=True),
        # special postgres types
        bytes: BYTEA,
        IPvAnyAddress: INET,
        Json: JSONB
    }


class InetFamily(GenericFunction):
    """
    IP address function returning the address family,
    see https://www.postgresql.org/docs/current/functions-net.html#CIDR-INET-FUNCTIONS-TABLE
    """
    type = Integer
    package = "inet"
    name = "family"
    identifier = "family"
    inherit_cache = True


@asynccontextmanager
async def psycopg_connect() -> AsyncIterator[AsyncConnection]:
    async with await AsyncConnection.connect(
            host=settings.POSTGRES_HOST,
            port=settings.POSTGRES_PORT,
            user=settings.POSTGRES_USER,
            password=settings.POSTGRES_PASSWORD,
            dbname=settings.POSTGRES_DB
    ) as conn:
        yield conn


class CopyManager:
    """
    Utility class to copy bulk data using the PostgreSQL COPY protocol, see also:
    https://www.postgresql.org/docs/current/sql-copy.html and
    https://www.psycopg.org/psycopg3/docs/basic/copy.html
    """

    def __init__(self, table: Table):
        """
        Creates a new copy manager for the given table.
        :param table: database table
        """
        self.table: str = table.name
        self.columns: list[str] = table.columns.keys()

    async def copy(self, data: AsyncIterator[list[Any]]) -> int:
        """
        Copies all rows from the given iterator into the configured table. The iterator has to yield rows that match
        the configured columns exactly.
        :param data: data to copy
        :return: count of rows inserted
        """
        query = sql.SQL("COPY {} ({}) FROM STDIN").format(
            sql.Identifier(self.table),
            sql.SQL(", ").join(map(sql.Identifier, self.columns))
        )
        inserted = 0
        async with psycopg_connect() as conn:  # type: AsyncConnection
            async with conn.cursor() as cur:
                async with cur.copy(query) as copy:  # type: AsyncCopy
                    async for row in data:
                        await copy.write_row(row)
                        inserted += 1
        return inserted


class SplitCopyManager:

    def __init__(self, table1: Table, table2: Table):
        """
        Creates a new copy manager that can copy data into two tables simultaneously.
        :param table1: first database table
        :param table2: second database table
        """
        self.table1: str = table1.name
        self.columns1: list[str] = table1.columns.keys()
        self.table2: str = table2.name
        self.columns2: list[str] = table2.columns.keys()

    async def copy(self, data: AsyncIterator[tuple[str, list[Any]]]) -> int:
        """
        Copies all rows from the given iterator into the configured table. The iterator has to yield rows that match
        the configured columns exactly.
        :param data: data to copy
        :return: count of rows inserted
        """
        query1 = sql.SQL("COPY {} ({}) FROM STDIN").format(
            sql.Identifier(self.table1),
            sql.SQL(", ").join(map(sql.Identifier, self.columns1))
        )
        query2 = sql.SQL("COPY {} ({}) FROM STDIN").format(
            sql.Identifier(self.table2),
            sql.SQL(", ").join(map(sql.Identifier, self.columns2))
        )
        inserted = 0
        async with psycopg_connect() as conn1:  # type: AsyncConnection
            async with psycopg_connect() as conn2:  # type: AsyncConnection
                async with conn1.cursor() as cur1:
                    async with conn2.cursor() as cur2:
                        async with cur1.copy(query1) as copy1:  # type: AsyncCopy
                            async with cur2.copy(query2) as copy2:  # type: AsyncCopy
                                async for table, row in data:
                                    if table == self.table1:
                                        await copy1.write_row(row)
                                    elif table == self.table2:
                                        await copy2.write_row(row)
                                    else:
                                        raise ValueError("unknown table name")
                                    inserted += 1
        return inserted
