from typing import Generator, Optional, Union

from fastapi import Depends, HTTPException, status
from jose import exceptions, jwt
from pydantic import ValidationError
from sqlalchemy.ext.asyncio import AsyncSession

from ipsize import models, security
from ipsize.control.config import settings
from ipsize.control.persistence import crud, entities
from ipsize.control.server import control_server
from ipsize.rest import control_paths
from ipsize.security import OAuth2TokenBearer

oauth2_scheme = OAuth2TokenBearer(
    password_token_url=control_paths.USER_LOGIN,
    client_credentials_token_url=control_paths.AUTH,
    auto_error=False
)


async def get_db() -> Generator:
    """
    Gives access to the postgres database
    """
    async with control_server.db_session() as db:
        yield db


def decode_token(token: str) -> Optional[models.TokenPayload]:
    try:
        payload = jwt.decode(
            token, settings.SECRET_KEY, algorithms=[security.ALGORITHM]
        )
        return models.TokenPayload(**payload)
    except (exceptions.JWTError, ValidationError):
        return None


async def check_token_bearer(token: Optional[str] = Depends(oauth2_scheme)) -> Optional[models.TokenPayload]:
    if token is None:
        return None
    return decode_token(token)


async def get_authenticated(
        db: AsyncSession = Depends(get_db),
        token: Optional[str] = Depends(oauth2_scheme)
) -> Union[entities.User, entities.Server]:
    """
    If used as dependency, a valid access token must be used to query the path (enforces authentication).
    Returns the user object or the server object encoded in the token to the path method.
    """
    if token is None:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated",
                            headers={"WWW-Authenticate": "Bearer"})
    token_data = decode_token(token)
    if token_data is None:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Could not validate credentials")

    if token_data.is_user_subject():
        user = await crud.user.get(db, token_data.get_id())
        if not user:
            raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="User not found")
        if not user.is_active:
            raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="User is not active")
        return user

    if token_data.is_server_subject():
        server = await crud.server.get_join_addresses(db, token_data.get_id())
        if not server:
            raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Server not found")
        return server

    # should never be reached, already checked by pydantic validation of token payload
    raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                        detail="Mustn't ask us, not it's business. Gollum, gollum.")


async def get_current_user(
        user: Union[entities.User, entities.Server] = Depends(get_authenticated)
) -> entities.User:
    """
    Checks the authentication token contains a valid user subject. Returns the user object to the path method.
    """
    if isinstance(user, entities.User):
        return user
    raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="User token required")


async def get_current_server(
        server: Union[entities.User, entities.Server] = Depends(get_authenticated)
) -> entities.Server:
    """
    Checks the authentication token contains a valid server subject. Returns the server object to the path method.
    """
    if isinstance(server, entities.Server):
        return server
    raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Server token required")
