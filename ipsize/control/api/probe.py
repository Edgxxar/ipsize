from typing import Annotated, Union

from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.ext.asyncio import AsyncSession

from ipsize import models
from ipsize.control.api import deps
from ipsize.control.persistence import crud, entities
from ipsize.models.enum import ProbeSeriesStatus
from ipsize.rest import BinaryResponse, control_paths
from ipsize.util import encode_integers

router = APIRouter()


@router.post(
    control_paths.PROBE_SERIES_CREATE,
    dependencies=[Depends(deps.get_current_user)],
    response_model=models.ProbeSeries
)
async def create_probe_series(series: models.ProbeSeriesCreate, db: AsyncSession = Depends(deps.get_db)):
    """
    Creates a new probe series. Restricted to users only.
    """
    return await crud.probe_series.create(db, series)


@router.get(
    control_paths.PROBE_SERIES_GET_AVAILABLE,
    response_model=list[models.ProbeSeries]
)
async def get_probe_series_available(
        auth: Union[entities.User, entities.Server] = Depends(deps.get_authenticated),
        db: AsyncSession = Depends(deps.get_db)
):
    """
    Returns the currently available probe series (status is not CLOSED or CANCELLED). If the token bearer is a user,
    returns all available series; if the token bearer is a server, only returns series available to that server.
    """
    series = await crud.probe_series.get_where_status_not_in(
        db, [ProbeSeriesStatus.CLOSED, ProbeSeriesStatus.CANCELLED]
    )

    if isinstance(auth, entities.Server):
        series = [s for s in series if
                  'servers' not in s.config or s.config['servers'] is None or auth.id in s.config['servers']]

    return series


@router.get(
    control_paths.PROBE_SERIES_GET,
    dependencies=[Depends(deps.get_authenticated)],
    response_model=models.ProbeSeries
)
async def get_probe_series(series_id: int, db: AsyncSession = Depends(deps.get_db)):
    """
    Returns a probe series by
    """
    series = await crud.probe_series.get(db, series_id)
    if not series:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Unknown probe series id: {series_id}")
    return series


@router.put(
    control_paths.PROBE_SERIES_PUT_CONFIG,
    dependencies=[Depends(deps.get_current_user)],
    response_model=models.ProbeSeries
)
async def put_probe_series_config(
        series_id: int,
        config: models.ProbeSeriesConfig,
        db: AsyncSession = Depends(deps.get_db)
):
    """
    Updates the configuration of a probe series.
    """
    db_series = await crud.probe_series.get(db, series_id)
    if not db_series:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Unknown probe series id: {series_id}")
    # force validation by pydantic
    try:
        models.ProbeSeriesCreate(type=db_series.type, config=config)
    except ValueError as e:
        raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=str(e))

    return await crud.probe_series.update_config(db, db_series, config)


@router.put(
    control_paths.PROBE_SERIES_PUT_STATUS,
    dependencies=[Depends(deps.get_current_user)],
    response_model=models.ProbeSeries
)
async def put_probe_series_status(
        series_id: int,
        series_status: Annotated[ProbeSeriesStatus, Body()],
        db: AsyncSession = Depends(deps.get_db)
):
    """
    Updates the status of a probe series.
    """
    db_series = await crud.probe_series.get(db, series_id)
    if not db_series:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Unknown probe series id: {series_id}")

    return await crud.probe_series.update_status(db, db_series, series_status)


@router.get(
    control_paths.PROBE_SERIES_GET_TARGETS,
    dependencies=[Depends(deps.get_authenticated)],
    response_class=BinaryResponse
)
async def get_probe_series_targets(series_id: int, db: AsyncSession = Depends(deps.get_db)):
    """
    Returns a list of target ids for a specific probe series.
    """
    series = await crud.probe_series.get(db, series_id)
    if not series:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Unknown probe series id: {series_id}")

    targets = await crud.target_service.get_ids_where_series(db, series.id)
    return BinaryResponse(content=encode_integers(targets))
