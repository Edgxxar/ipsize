from typing import Annotated, Union

from cryptography.fernet import InvalidToken
from fastapi import APIRouter, Body, Depends, HTTPException, status
from pydantic import IPvAnyAddress
from sqlalchemy.ext.asyncio import AsyncSession

from ipsize import models, security
from ipsize.control.api import deps
from ipsize.control.config import settings
from ipsize.control.persistence import crud, entities
from ipsize.rest import control_paths

router = APIRouter()


@router.post(
    control_paths.SERVER_CREATE,
    dependencies=[Depends(deps.get_current_user)],
    response_model=models.ServerModel
)
async def create_server(server: models.ServerCreate, db: AsyncSession = Depends(deps.get_db)):
    """
    Create a new probe server. Only allowed for authenticated users.
    """
    db_server = await crud.server.get_by_name(db, server.name)
    if db_server:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Server with this name already exists")
    db_server = await crud.server.create(db, server)
    return await crud.server.get_join_addresses(db, db_server.id)


@router.post(
    control_paths.SERVER_POST_ADDRESS,
    status_code=status.HTTP_201_CREATED
)
async def post_server_address(
        server_id: int,
        address: Annotated[IPvAnyAddress, Body()],
        auth: Union[entities.User, entities.Server] = Depends(deps.get_authenticated),
        db: AsyncSession = Depends(deps.get_db)
):
    """
    Add an IP address to a probe server. Servers can only add addresses to their own server entry.
    """
    if isinstance(auth, entities.Server) and auth.id != server_id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Cannot add address for another server.")
    db_address = await crud.server.get_address(db, address)
    if db_address is not None:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Server address already exists")
    await crud.server.add_address(db, server_id, address)


@router.delete(
    control_paths.SERVER_DELETE_ADDRESS,
    status_code=status.HTTP_204_NO_CONTENT
)
async def delete_server_address(
        server_id: int,
        address: IPvAnyAddress,
        auth: Union[entities.User, entities.Server] = Depends(deps.get_authenticated),
        db: AsyncSession = Depends(deps.get_db)
):
    """
    Removes an IP address from a probe server. Servers can only remove addresses from their own server entry.
    """
    if isinstance(auth, entities.Server) and auth.id != server_id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Cannot remove address for another server.")
    db_address = await crud.server.get_address(db, address)
    if db_address is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Unknown server address")
    await crud.server.del_address(db, db_address)


@router.post(
    control_paths.SERVER_UPDATE_SERVER_SECRET,
    dependencies=[Depends(deps.get_current_user)],
    response_model=models.ServerModel
)
async def update_server_secret(server_id: int, secret: Annotated[str, Body()], db: AsyncSession = Depends(deps.get_db)):
    """
    Updates the secret used by the probe server to authenticate at the control server.
    Only allowed for authenticated users.
    """
    server = await crud.server.get_join_addresses(db, server_id)
    if not server:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Unknown probe server id: {server_id}")

    server = await crud.server.update_server_secret(db, server, secret)
    return server


@router.post(
    control_paths.SERVER_UPDATE_CONTROL_SECRET,
    dependencies=[Depends(deps.get_current_user)],
    response_model=models.ServerModel
)
async def update_control_secret(server_id: int, secret: Annotated[str, Body()],
                                db: AsyncSession = Depends(deps.get_db)):
    """
    Updates the secret used by the control server to authenticate at the probe server.
    Only allowed for authenticated users.
    """
    server = await crud.server.get_join_addresses(db, server_id)
    if not server:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Unknown probe server id: {server_id}")

    server = await crud.server.update_control_secret(db, server, secret)
    return server


@router.get(
    control_paths.SERVER_GET,
    dependencies=[Depends(deps.get_authenticated)],
    response_model=models.ServerModel
)
async def get_server(server_id: int, db: AsyncSession = Depends(deps.get_db)):
    """
    Get a probe server by its ID
    """
    server = await crud.server.get_join_addresses(db, server_id)
    if not server:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Unknown probe server id: {server_id}")
    return server


@router.get(
    control_paths.SERVER_GET_ALL,
    dependencies=[Depends(deps.get_authenticated)],
    response_model=list[models.ServerModel]
)
async def get_servers(db: AsyncSession = Depends(deps.get_db)):
    """
    Get all probe servers
    """
    return await crud.server.get_all_join_addresses(db)


@router.get(
    control_paths.SERVER_GET_BY_NAME,
    dependencies=[Depends(deps.get_authenticated)],
    response_model=models.ServerModel
)
async def get_server_by_name(name: str, db: AsyncSession = Depends(deps.get_db)):
    """
    Get a probe server by its name
    """
    server = await crud.server.get_by_name(db, name)
    if not server:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Unknown probe server name: {name}")
    return server


@router.get(
    control_paths.SERVER_GET_BY_ADDRESS,
    dependencies=[Depends(deps.get_authenticated)],
    response_model=models.ServerModel
)
async def get_server_by_address(address: IPvAnyAddress, db: AsyncSession = Depends(deps.get_db)):
    """
    Get a probe server by its address
    """
    server = await crud.server.get_by_address(db, address)
    if not server:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Unknown probe server address: {address}")
    return server


@router.get(
    control_paths.SERVER_GET_CONTROL_SECRET,
    response_model=str
)
async def get_server_control_secret(server_id: int, server: entities.Server = Depends(deps.get_current_server)):
    """
    Gets the hashed secret used by the control server to authenticate at the probe server. Only allowed for the probe
    server whose control secret is requested.
    """
    if server.id != server_id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail="Cannot get control secret for other servers.")
    # decrypt the control secret and return the hashed value
    try:
        control_secret = security.decrypt_string(server.control_secret, settings.SECRET_KEY, server.control_secret_salt)
    except InvalidToken:
        raise HTTPException(
            status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
            detail=f"Unable to decrypt control server secret for probe server {server_id}, "
                   f"did the server secret change?"
        )
    return security.hash_password(control_secret)
