from fastapi import APIRouter

from ipsize import models
from ipsize.control.server import control_server
from ipsize.rest import control_paths

router = APIRouter()


@router.get(control_paths.STATUS, response_model=models.ControlServerStatus)
async def get_status():
    """
    Get the current server status
    """
    return control_server.status
