import ipaddress
from datetime import datetime
from typing import Annotated

from fastapi import APIRouter, Body, Depends, HTTPException, status
from pydantic import IPvAnyAddress
from sqlalchemy.ext.asyncio import AsyncSession
from starlette.requests import Request

from ipsize import models
from ipsize.control.api import deps
from ipsize.control.persistence import crud, entities
from ipsize.models.enum import Status
from ipsize.rest import control_paths, limiter
from ipsize.util import decode_integers, get_remote_address

router = APIRouter()


@router.post(
    control_paths.TARGET_HOST_CREATE,
    dependencies=[Depends(deps.get_authenticated)],
    response_model=models.TargetHost
)
async def create_target_host(host: models.TargetHostCreate, db: AsyncSession = Depends(deps.get_db)):
    """
    Create a target host
    """
    db_host = await crud.target_host.get_by_address(db, host.address)
    if db_host:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Host with this address already registered")

    db_host = await crud.target_host.create(db, host)
    return await crud.target_host.get_join_services(db, db_host.id)


@router.get(
    control_paths.TARGET_HOST_GET,
    dependencies=[Depends(deps.get_authenticated)],
    response_model=models.TargetHost
)
async def get_target_host(host_id: int, db: AsyncSession = Depends(deps.get_db)):
    """
    Get a target host by its ID
    """
    host = await crud.target_host.get_join_services(db, host_id)
    if not host:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Unknown target host id: {host_id}")
    return host


@router.get(
    control_paths.TARGET_HOST_GET_BY_ADDRESS,
    dependencies=[Depends(deps.get_authenticated)],
    response_model=models.TargetHost
)
async def get_target_host_by_address(address: IPvAnyAddress, db: AsyncSession = Depends(deps.get_db)):
    """
    Get a target host by its address
    """
    host = await crud.target_host.get_by_address_join_services(db, address)
    if not host:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Unknown target host address: {address}")
    return host


async def _verify_target_host_check(
        request: Request,
        address: IPvAnyAddress,
        db: AsyncSession = Depends(deps.get_db),
        token: models.TokenPayload = Depends(deps.check_token_bearer)
) -> entities.TargetHost:
    # check remote address if not authenticated
    if token is None:
        client_address = get_remote_address(request)
        if ipaddress.ip_address(client_address) != address:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                                detail=f"Please authorize, or query this endpoint from the address {address}")
    host = await crud.target_host.get_by_address(db, address)
    if not host:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Unknown target host address: {address}")
    return host


@router.get(
    control_paths.TARGET_HOST_CHECK,
    response_model=models.TargetHostEnabled
)
@limiter.limit("10/minute")
async def check_target_host(
        request: Request,
        address: IPvAnyAddress,
        db: AsyncSession = Depends(deps.get_db),
        token: models.TokenPayload = Depends(deps.check_token_bearer)
):
    """
    Check if a host is part of this experiment.
    """
    return await _verify_target_host_check(request, address, db, token)


@router.post(
    control_paths.TARGET_HOST_ENABLE,
    response_model=models.TargetHostEnabled
)
@limiter.limit("10/minute")
async def enable_target_host(
        request: Request,
        address: IPvAnyAddress,
        db: AsyncSession = Depends(deps.get_db),
        token: models.TokenPayload = Depends(deps.check_token_bearer)
):
    """
    Enable a host for participation in this experiment.
    """
    host = await _verify_target_host_check(request, address, db, token)
    host.enabled = True
    host.last_update = datetime.now()
    await crud.target_host.update(db, host)
    return host


@router.post(
    control_paths.TARGET_HOST_DISABLE,
    response_model=models.TargetHostEnabled
)
@limiter.limit("10/minute")
async def disable_target_host(
        request: Request,
        address: IPvAnyAddress,
        db: AsyncSession = Depends(deps.get_db),
        token: models.TokenPayload = Depends(deps.check_token_bearer)
):
    """
    Disable a host for participation in this experiment.
    """
    host = await _verify_target_host_check(request, address, db, token)
    host.enabled = False
    host.last_update = datetime.now()
    await crud.target_host.update(db, host)
    return host


@router.post(
    control_paths.TARGET_SERVICE_CREATE,
    dependencies=[Depends(deps.get_authenticated)],
    response_model=models.TargetService
)
async def create_target_service(service: models.TargetServiceCreate, db: AsyncSession = Depends(deps.get_db)):
    """
    Create a service for a target host.
    """
    host = await crud.target_host.get(db, service.target_host_id)
    if not host:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail=f"Unknown target host id: {service.target_host_id}")
    return await crud.target_service.create(db, service)


@router.put(
    control_paths.TARGET_SERVICE_PUT_STATUS,
    dependencies=[Depends(deps.get_authenticated)],
)
async def put_target_service_status(
        service_id: int,
        service_status: Annotated[Status, Body(title="status")],
        db: AsyncSession = Depends(deps.get_db)
):
    """
    Sets the status of a target service
    """
    service = await crud.target_service.get(db, service_id)
    if service is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Unknown target service id: {service_id}")

    service.status = service_status
    service.last_update = datetime.now()
    await crud.target_service.update(db, service)


@router.post(
    control_paths.TARGET_SERVICE_GET_ALL,
    dependencies=[Depends(deps.get_authenticated)],
    response_model=list[models.TargetServiceModel]
)
async def get_target_services(
        ids: Annotated[bytes, Body(media_type="application/octet-stream", description="Encoded service IDs")],
        db: AsyncSession = Depends(deps.get_db)
):
    """
    Get a list of service definitions.
    """
    if len(ids) == 0:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Specify targets in body")
    service_ids = decode_integers(ids)
    if len(service_ids) == 0:
        return []
    return await crud.target_service.get_models_where_ids_in(db, service_ids)
