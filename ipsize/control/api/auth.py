from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.ext.asyncio import AsyncSession
from starlette.requests import Request

from ipsize import models, security
from ipsize.control.api import deps
from ipsize.control.config import settings
from ipsize.control.persistence import crud, entities
from ipsize.rest import control_paths, limiter
from ipsize.security import OAuth2ClientCredentials

router = APIRouter()


@router.post(control_paths.AUTH, response_model=models.AccessToken)
@limiter.limit("3/minute")
async def auth(request: Request, credentials: Annotated[OAuth2ClientCredentials, Depends()],
               db: AsyncSession = Depends(deps.get_db)):
    """
    Generate access token by authenticating via probe server secret
    """
    if not credentials.client_id.isdigit():
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid client id")

    server = await crud.server.get(db, int(credentials.client_id))
    if not server:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Incorrect client id or secret")

    if not security.verify_password(credentials.client_secret, server.server_secret):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Incorrect client id or secret")

    token = security.create_access_token(models.TokenPayload.from_server_id(server.id),
                                         settings.ACCESS_TOKEN_EXPIRE_MINUTES,
                                         settings.SECRET_KEY)
    return models.AccessToken(access_token=token)


@router.post(control_paths.TEST_TOKEN, response_model=models.ServerModel)
async def test_token(server: entities.Server = Depends(deps.get_current_server)):
    """
    Test user access token
    """
    return server


@router.post(control_paths.USER_LOGIN, response_model=models.AccessToken)
@limiter.limit("3/minute")
async def user_login(request: Request, form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
                     db: AsyncSession = Depends(deps.get_db)):
    """
    Generate access token by authenticating via username and password
    """
    user = await crud.user.get_by_email(db, form_data.username)
    if not user:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Incorrect username or password")

    if not security.verify_password(form_data.password, user.hashed_password):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Incorrect username or password")

    if not user.is_active:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="User is not active")

    token = security.create_access_token(models.TokenPayload.from_user_id(user.id),
                                         settings.ACCESS_TOKEN_EXPIRE_MINUTES,
                                         settings.SECRET_KEY)
    return models.AccessToken(access_token=token)


@router.post(control_paths.USER_TEST_TOKEN, response_model=models.UserModel)
async def user_test_token(user: entities.User = Depends(deps.get_current_user)):
    """
    Test user access token
    """
    return user


@router.post(
    control_paths.USER_REGISTER,
    dependencies=[Depends(deps.get_current_user)],
    response_model=models.UserModel
)
async def user_register(user: models.UserCreate, db: AsyncSession = Depends(deps.get_db)):
    """
    Create a new user with password. Only allowed for already authenticated users.
    """
    db_user = await crud.user.get_by_email(db, user.email)
    if db_user:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Email already registered")

    return await crud.user.create(db, user)
