import logging
from datetime import datetime, timedelta
from typing import Optional

import anyio
import sqlalchemy
from anyio import TASK_STATUS_IGNORED
from anyio.abc import TaskStatus
from sqlalchemy.ext.asyncio import AsyncEngine, async_sessionmaker, create_async_engine

from ipsize.control.config import settings
from ipsize.control.download import ProbeDownloadImporter, ProbeSeriesDownloader
from ipsize.control.monitor import TargetMonitor
from ipsize.control.persistence import crud
from ipsize.models.enum import Status
from ipsize.models.status import ControlServerStatus
from ipsize.server import Server

logger = logging.getLogger(settings.LOGGER_NAME)


class ControlServer(Server):
    """
    Represents the current state of the control server. A new instance of this class is created at startup and added to
    the fastapi state dictionary, to be available in endpoint functions using the request method.
    """

    def __init__(self) -> None:
        super().__init__()
        self._cancel_scope: Optional[anyio.CancelScope] = None
        self._status = ControlServerStatus()

        self.db_engine: Optional[AsyncEngine] = None
        self.db_session: Optional[async_sessionmaker] = None

    @property
    def status(self) -> ControlServerStatus:
        return self._status

    async def startup(self, *, task_status: TaskStatus[None] = TASK_STATUS_IGNORED) -> None:
        """
        Starts this server by performing some preliminary checks.
        """
        self.init_database()
        await anyio.Path(settings.DOWNLOAD_DIR).mkdir(mode=0o750, parents=True, exist_ok=True)
        task_status.started()

        async with anyio.create_task_group() as tg:
            self._cancel_scope = tg.cancel_scope
            # check connectivity
            await self.check_connectivity()

            # start continuous tasks
            monitor = TargetMonitor(self.db_session)
            monitor.start_tasks(tg)
            tg.start_soon(self.download_probe_data)
            tg.start_soon(self.import_probe_data)

    async def shutdown(self) -> None:
        """
        Stops this server gracefully.
        """
        if self._cancel_scope is not None:
            await self._cancel_scope.cancel()
        await self.db_engine.dispose()

    async def check_connectivity(self) -> None:
        """
        Checks connectivity with all required components.
        """
        timeout = 1
        tries = 0
        while True:
            logger.info("Checking connectivity...")
            await self.check_ipv4_connectivity()
            await self.check_ipv6_connectivity()

            await self.check_database_connectivity()

            self.update_status()

            if self.status.status == Status.OK:
                break
            # else wait a while and try again, with exponential backoff
            tries += 1
            logger.info("Trying again in %d second(s)", timeout)
            await anyio.sleep(timeout)
            timeout = 1 if tries < 2 else 60

    def init_database(self) -> None:
        """
        Initializes the async session factory for database connections.
        """
        self.db_engine = create_async_engine(
            str(settings.SQLALCHEMY_DATABASE_URI),
            pool_size=settings.SQLALCHEMY_POOL_SIZE,
            max_overflow=settings.SQLALCHEMY_POOL_OVERFLOW
        )
        self.db_session = async_sessionmaker(self.db_engine, autoflush=False, autocommit=False, expire_on_commit=False)

    async def check_database_connectivity(self) -> None:
        async with self.db_session() as db:
            try:
                await db.execute(sqlalchemy.text("SELECT 1"))
                logger.info("Successfully connected to database.")
                self.status.database = Status.OK
            except Exception as e:
                logger.error("Error trying to connect to database:")
                logger.error(e)
                self.status.database = Status.ERROR

    async def download_probe_data(self) -> None:
        """
        Task to download probe data from all probe servers
        """
        async with anyio.create_task_group() as tg:
            while not tg.cancel_scope.cancel_called:
                # find latest download time and server count, calculate when the next download is due and wait
                async with self.db_session() as db:
                    last_download = await crud.server.get_latest_download_time(db)
                    server_count = await crud.server.get_count_all(db)
                if last_download is None:
                    next_download = datetime.now().astimezone()
                else:
                    delta = timedelta(minutes=settings.DOWNLOAD_INTERVAL / server_count)
                    next_download = last_download.astimezone() + delta
                logger.info("[Download] Next probe download scheduled for %s",
                            next_download.strftime("%d.%m.%Y %H:%M:%S"))
                sleep = (next_download - datetime.now().astimezone()).total_seconds()
                if sleep > 0:
                    await anyio.sleep(sleep)

                # get oldest server and start download
                async with self.db_session() as db:
                    server = await crud.server.get_one_order_by_last_download(db)
                    server.last_download = datetime.now()
                    await crud.server.update(db, server)
                downloader = ProbeSeriesDownloader(self.db_session)
                tg.start_soon(downloader.download_probe_data, server)

    async def import_probe_data(self) -> None:
        """
        Task to import downloaded probe data
        """
        while True:
            async with self.db_session() as db:
                download = await crud.probe_download.get_next_where_not_imported(db)
                db.expunge_all()
            if download is None:
                await anyio.sleep(60)
                continue
            importer = ProbeDownloadImporter(self.db_session, download)
            await importer.import_downloaded_packets()


# global server instance
control_server = ControlServer()
