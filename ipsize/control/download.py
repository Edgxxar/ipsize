import base64
import logging
import time
import zlib
from contextlib import asynccontextmanager
from datetime import datetime
from pathlib import PurePath
from typing import Any, AsyncIterator

import anyio
import brotli
import humanize
import orjson
import pbjson
from anyio.streams.file import FileReadStream, FileWriteStream
from httpx import AsyncClient, HTTPError, Response, Timeout
from sqlalchemy.ext.asyncio import async_sessionmaker

from ipsize import models
from ipsize.control.config import settings
from ipsize.control.persistence import crud, entities
from ipsize.control.persistence.database import CopyManager, SplitCopyManager
from ipsize.control.rest import get_server_client
from ipsize.rest import format_path, probe_paths

logger = logging.getLogger(settings.LOGGER_NAME)


async def decompress_brotli(stream: AsyncIterator[bytes]) -> AsyncIterator[bytes]:
    """
    Decompresses a brotli-compressed stream while reading it.
    :param stream: the brotli-compressed data
    :return: stream of decompressed data
    """
    decompressor = brotli.Decompressor()
    try:
        async for data in stream:
            decompressed = decompressor.process(data)
            if decompressed:
                yield decompressed
        if not decompressor.is_finished():
            logger.error("[Import] Data file is incomplete.")
    except brotli.error as e:
        logger.error("[Import] Error while decompressing file.")
        raise e


async def decompress_gzip(stream: AsyncIterator[bytes]) -> AsyncIterator[bytes]:
    """
    Decompresses a gzip-compressed stream while reading it.
    :param stream: the gzip-compressed data
    :return: stream of decompressed data
    """
    decompressor = zlib.decompressobj(16 + zlib.MAX_WBITS)
    try:
        async for data in stream:
            decompressed = decompressor.decompress(data)
            if decompressed:
                yield decompressed
        tail = decompressor.flush()
        if tail:
            yield tail
    except zlib.error as e:
        logger.error("[Import] Error while decompressing file.")
        raise e


async def read_probe_packets_binary(stream: AsyncIterator[bytes]) -> AsyncIterator[dict[str, Any]]:
    """
    Reads encoded probe data from a stream of length-encoded packed binary JSON.
    :param stream: bytes to read
    :return: individual probe packets
    """
    remaining = bytes()  # buffer for bytes left over from the previous chunk
    async for chunk in stream:
        data = remaining + chunk  # prepend bytes left over from previous chunk
        i = 0  # current index in the data array
        while i < len(data) - 1:  # need at least 2 bytes to read next length
            length = int.from_bytes(data[i:i + 2], "big", signed=False)
            if i + 2 + length > len(data):  # only read packet if possible
                break

            i += 2
            yield pbjson.loads(data[i:i + length])
            i += length
        # save left over data for next chunk
        remaining = data[i:]
    if len(remaining) > 0:
        logger.warning("[Import] Extra data remaining in probe data.")


async def read_probe_packets_sequence(stream: AsyncIterator[bytes]) -> AsyncIterator[dict[str, Any]]:
    """
    Reads encoded probe data from a stream of JSON text sequences as proposed in RFC 7464.
    :param stream: bytes to read
    :return: individual probe packets
    """
    remaining = bytes()  # buffer for bytes left over from the previous chunk
    async for chunk in stream:
        data = remaining + chunk  # prepend bytes left over from previous chunk
        if len(data) > 0 and data[0] != 0x1E:
            raise ValueError("JSON values must be preceded by one RS character")
        i = 0  # current index in the data array
        while i < len(data):
            sep = data.find(0x1E, i + 1)  # find next RS
            if sep == -1:
                break
            yield orjson.loads(data[i + 1:sep])
            i = sep
        # save left over data for next chunk
        remaining = data[i:]
    # yield last entry
    if len(remaining) > 1:
        try:
            last = orjson.loads(remaining[1:])
            if not isinstance(last, dict):
                raise ValueError
            yield last
        except ValueError:
            logger.warning("[Import] Extra data remaining in probe data.")


async def filter_duplicates(series_id: int, packets: AsyncIterator[dict[str, Any]]) -> AsyncIterator[dict[str, Any]]:
    """
    Filters out duplicate packets in a stream of packet data while iterating.
    :param series_id: probe series id
    :param packets: stream of packets
    :return: stream containing only unique packets
    """
    seen = set()
    duplicates = 0
    async for packet in packets:
        packet_id = hash((packet["sequence_number"], packet["server_id"], packet["target_service_id"]))
        if packet_id in seen:
            duplicates += 1
            continue
        seen.add(packet_id)
        yield packet
    if duplicates > 0:
        logger.warning("[Import] Encountered %d duplicate packets during import for probe series %d",
                       duplicates, series_id)


async def prepare_copy_public(
        series_id: int,
        packets: AsyncIterator[dict[str, Any]],
) -> AsyncIterator[list[Any]]:
    """
    Prepares the probe packets from the given iterator for insertion using a CopyManager.
    Removes duplicate packets.
    :param series_id: probe series id
    :param packets: probe packets to prepare
    :return: probe packets as lists in column order
    """
    async for packet in filter_duplicates(series_id, packets):
        if "latency" in packet and packet["latency"] < 0:
            del packet["latency"]
        if "delta" in packet and packet["delta"] >= 2**31:
            del packet["delta"]
        yield [
            series_id,
            packet["sequence_number"],
            packet["server_id"],
            packet["target_service_id"],
            packet["probe_time"],
            packet.get("send_time", None),
            packet.get("recv_time", None),
            packet.get("request_received", False),
            packet.get("latency", None),
            packet.get("delta", None),
            packet["size"],
            base64.b64decode(packet["data"]) if "data" in packet else None,
            packet.get("anomaly", None),
        ]


async def prepare_copy_private(
        series_id: int,
        packets: AsyncIterator[dict[str, Any]],
) -> AsyncIterator[tuple[str, list[Any]]]:
    """
    Prepares the probe packets from the given iterator for insertion using a SplitCopyManager.
    Removes duplicate packets (one server should never have sent and received the same packet).
    :param series_id: probe series id
    :param packets: probe packets to prepare
    :return: probe packets as lists in column order
    """
    async for packet in filter_duplicates(series_id, packets):
        if "send_time" in packet:
            yield (entities.probe_packets_sent.name, [
                series_id,
                packet["sequence_number"],
                packet["server_id"],
                packet["target_service_id"],
                packet["send_time"],
                packet["size"],
            ])
        elif "recv_time" in packet:
            yield (entities.probe_packets_received.name, [
                series_id,
                packet["sequence_number"],
                packet["server_id"],
                packet["target_service_id"],
                packet["recv_time"],
                packet["size"],
            ])
        else:
            logger.warning("[Import] Got packet with no send or receive time: (%d, %d, %d, %d)",
                           series_id, packet["sequence_number"], packet["server_id"], packet["target_service_id"])


class ProbeSeriesDownloader:

    def __init__(self, db_session: async_sessionmaker) -> None:
        self.db_session = db_session

    async def download_probe_data(self, server: entities.Server) -> None:
        """
        Download all packets from all available probe series from the given probe server. Stops the probe series
        execution for all probe series on that server before the download, and restarts it again after the download.
        :param server: server to download packets from
        """

        with anyio.CancelScope(shield=True):  # start download tasks in a shielded scope
            try:
                async with get_server_client(server) as client:
                    # pause all probe series
                    logger.info("[Download] Pausing execution of probe series on %s...", server.name)
                    response = await client.post(
                        probe_paths.PROBE_SERIES_PAUSE,
                        timeout=Timeout(settings.REST_CLIENT_TIMEOUT, read=300)  # give server time to flush save files
                    )
                    response.raise_for_status()

                    # download all files sequentially
                    try:
                        response = await client.get(probe_paths.PROBE_SERIES_GET_AVAILABLE)
                        response.raise_for_status()
                        available_downloads = [models.ProbeDownloadAvailable(**d) for d in response.json()]

                        async with self.db_session() as db:
                            for download in available_downloads:
                                db_download = await crud.probe_download.get_where_series_and_file(
                                    db, download.probe_series_id, str(download.file_name)
                                )
                                if db_download is not None:
                                    logger.warning("[Download] File %s was already downloaded, skipping",
                                                   download.file_name)
                                    await self._delete_file(server, download, client)
                                    continue
                                await self._download_file(server, download, client)
                    except HTTPError as e:
                        logger.error("[Download] Error while trying to get available downloads on %s: %s %s - %s",
                                     server.name, e.request.method, e.request.url, e)

                    # resume all series
                    response = await client.post(probe_paths.PROBE_SERIES_RESUME)
                    response.raise_for_status()
                    logger.info("[Download] Resumed execution of probe series on %s.", server.name)
            except HTTPError as e:
                logger.error("[Download] Error while trying to download data from probe server %s: %s %s - %s",
                             server.name, e.request.method, e.request.url, e)

    async def _download_file(
            self,
            server: entities.Server,
            download: models.ProbeDownloadAvailable,
            client: AsyncClient
    ) -> None:
        """
        Task to download packets from the probe server and save them to disk.
        """
        path = anyio.Path(settings.DOWNLOAD_DIR, download.file_name)
        t_start = time.perf_counter()
        try:
            logger.info("[Download] Downloading data of probe series %d on %s...",
                        download.probe_series_id, server.name)
            await path.touch(0o640)
            async with await FileWriteStream.from_path(path) as file:  # type: FileWriteStream
                async with client.stream(
                        "GET",
                        format_path(probe_paths.PROBE_SERIES_DOWNLOAD, file_name=download.file_name)
                ) as stream:  # type: Response
                    stream.raise_for_status()
                    async for data in stream.aiter_bytes():
                        await file.send(data)
        except HTTPError as e:
            logger.error("[Download] Error while trying to download file %s from server %s: %s %s - %s",
                         download.file_name, server.name, e.request.method, e.request.url, e)
            return
        t_stop = time.perf_counter()
        # check file is not empty
        stat = await path.stat()
        if stat.st_size == 0:
            logger.warning("[Download] File %s was empty, skipping", download.file_name)
            await path.unlink()
            return
        if stat.st_size != download.file_size:
            logger.warning("[Download] File %s is %d bytes, expected %d bytes",
                           download.file_name, stat.st_size, download.file_size)
        # save download in database
        async with self.db_session() as db:
            download = await crud.probe_download.create(db, models.ProbeDownloadCreate(
                probe_series_id=download.probe_series_id,
                server_id=server.id,
                packet_count=download.packet_count,
                file_name=download.file_name,
                file_size=stat.st_size,
                downloaded_at=datetime.now()
            ))
        logger.info("[Download] Finished download %d: saved %s (%s) in %.2f seconds.",
                    download.id, download.file_name, humanize.naturalsize(stat.st_size), t_stop - t_start)
        # delete file on server
        await self._delete_file(server, download, client)

    async def _delete_file(
            self,
            server: entities.Server,
            download: models.ProbeDownloadAvailable,
            client: AsyncClient) -> None:
        try:
            await client.delete(format_path(probe_paths.PROBE_SERIES_DOWNLOAD, file_name=download.file_name))
        except HTTPError as e:
            logger.error("[Download] Error while trying to delete file %s from server %s: %s %s - %s",
                         download.file_name, server.name, e.request.method, e.request.url, e)


class ProbeDownloadImporter:

    def __init__(self, db_session: async_sessionmaker, download: entities.ProbeDownload) -> None:
        self.db_session: async_sessionmaker = db_session
        self.download: entities.ProbeDownload = download

        # statistics
        self._error: bool = False
        self._packet_count: int = 0
        self._time: float = 0

    async def import_downloaded_packets(self) -> None:
        """
        Imports all probes from the given download into the database.
        """
        path = anyio.Path(settings.DOWNLOAD_DIR, self.download.file_name)
        if not await path.is_file():
            logger.error("[Import] Cannot find download file %d at %s, marking as errored.", self.download.id, path)
            async with self.db_session() as db:
                download = await db.merge(self.download)
                download.error = True
                await crud.probe_download.update(db, download)
            return

        logger.info("[Import] Starting import of probe data download %d for series %d from server %d from %s",
                    self.download.id, self.download.probe_series_id, self.download.server_id,
                    self.download.downloaded_at.astimezone().strftime("%d.%m.%Y %H:%M:%S"))
        # start in a shielded scope, prevent from shutdown during import
        async with anyio.CancelScope(shield=True):
            if self.download.probe_series.type.is_private():
                await self.save_downloaded_packets_private()
            else:
                await self.save_downloaded_packets_public()

            async with self.db_session() as db:
                download = await db.merge(self.download)
                download.imported_at = datetime.now()
                download.import_time = self._time
                download.packet_count = self._packet_count
                download.error = self._error
                await crud.probe_download.update(db, download)

            logger.info(
                "[Import] Saved %d packets of download %d for series %d from server %d in %.2f seconds.",
                download.packet_count, download.id, download.probe_series_id, download.server_id, download.import_time
            )

    @asynccontextmanager
    async def open_file(self) -> AsyncIterator[bytes]:
        """
        Opens a binary file and streams the content asynchronously. If the file was brotli or gzip compressed, it is
        decompressed while iterating over the data.
        :return: stream of decompressed binary data
        """
        path = PurePath(settings.DOWNLOAD_DIR, self.download.file_name)
        async with await FileReadStream.from_path(path) as file:
            if self.download.file_name.endswith(".br"):
                yield decompress_brotli(file)
            elif self.download.file_name.endswith(".gz"):
                yield decompress_gzip(file)
            else:
                yield file

    async def save_downloaded_packets_public(self) -> None:
        """
        Task to save packets from a download file in the database in bulk. Uses the PostgreSQL COPY protocol.
        """
        t_start = time.perf_counter()
        mgr = CopyManager(entities.probe_packets_public)
        async with self.open_file() as file:  # type: FileReadStream
            try:
                self._packet_count = await mgr.copy(
                    prepare_copy_public(self.download.probe_series_id, read_probe_packets_sequence(file))
                )
            except Exception as e:
                logger.error("[Import] Error trying to copy public probe data into database:")
                logger.exception(e)
                self._error = True
        self._time = time.perf_counter() - t_start

    async def save_downloaded_packets_private(self) -> None:
        """
        Task to save packets from a download file in the database in bulk. Uses the PostgreSQL COPY protocol.
        """
        t_start = time.perf_counter()
        mgr = SplitCopyManager(entities.probe_packets_sent, entities.probe_packets_received)
        async with self.open_file() as file:  # type: FileReadStream
            try:
                self._packet_count = await mgr.copy(
                    prepare_copy_private(self.download.probe_series_id, read_probe_packets_sequence(file))
                )
            except Exception as e:
                logger.error("[Import] Error trying to copy private probe data into database:")
                logger.exception(e)
                self._error = True
        self._time = time.perf_counter() - t_start
