from typing import Annotated

import typer
from pydantic import EmailStr
from rich import print
from sqlalchemy.ext.asyncio import async_sessionmaker

from ipsize import models
from ipsize.control.cli.deps import AsyncTyper, DBOption
from ipsize.control.persistence import crud

cli = AsyncTyper()


@cli.async_command()
async def list(session: async_sessionmaker = DBOption):
    async with session() as db:
        users = await crud.user.get_all(db)
    print("Found %d users in database:" % len(users))
    for user in users:
        print("%3d: %s" % (user.id, user.email))


@cli.async_command()
async def show(
        email: Annotated[EmailStr, typer.Argument(parser=EmailStr)],
        session: async_sessionmaker = DBOption
):
    async with session() as db:
        user = await crud.user.get_by_email(db, email)
    if not user:
        print("User with that email does not exist")
        raise typer.Exit()

    print(models.UserModel.model_validate(user))


@cli.async_command()
async def create(
        email: Annotated[EmailStr, typer.Argument(parser=EmailStr)],
        password: Annotated[str, typer.Option(prompt=True, confirmation_prompt=True, hide_input=True)],
        session: async_sessionmaker = DBOption
):
    async with session() as db:
        user = await crud.user.get_by_email(db, email)
        if user:
            print("Email already registered")
            raise typer.Abort()

        user = await crud.user.create(db, models.UserCreate(
            email=email,
            password=password
        ))
    print("User created successfully:")
    print(models.UserModel.model_validate(user))
