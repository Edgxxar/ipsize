import os
from contextlib import contextmanager
from datetime import datetime
from pathlib import Path
from typing import Annotated, Iterator, Optional

import anyio
import brotli
import typer
from anyio import TASK_STATUS_IGNORED, to_process
from anyio.abc import TaskStatus
from anyio.streams.file import FileReadStream
from psycopg import Connection, sql
from rich import print
from rich.progress import BarColumn, FileSizeColumn, MofNCompleteColumn, Progress, SpinnerColumn, TaskID, \
    TaskProgressColumn, TextColumn, TimeElapsedColumn, TimeRemainingColumn
from scapy.layers.inet import IP
from scapy.layers.inet6 import IPv6
from scapy.utils import wireshark
from sqlalchemy.ext.asyncio import async_sessionmaker

from ipsize import models
from ipsize.control.cli.deps import AsyncTyper, DBOption
from ipsize.control.config import settings
from ipsize.control.download import ProbeDownloadImporter, ProbeSeriesDownloader, read_probe_packets_binary
from ipsize.control.persistence import crud, entities

cli = AsyncTyper()


@cli.async_command()
async def download(
        server_id: Annotated[Optional[int], typer.Argument()] = None,
        all: Annotated[bool, typer.Option()] = False,
        session: async_sessionmaker = DBOption
):
    if server_id is None and not all:
        print("Specify server ID or the --all flag")
        raise typer.Abort()
    async with session() as db:
        if server_id is not None:
            server = await crud.server.get(db, server_id)
            downloader = ProbeSeriesDownloader(session)
            await downloader.download_probe_data(server)
        elif all:
            servers = await crud.server.get_all(db)
            async with anyio.create_task_group() as tg:
                for server in servers:
                    downloader = ProbeSeriesDownloader(session)
                    tg.start_soon(downloader.download_probe_data, server)


@cli.async_command()
async def validate(
        path: Annotated[Path, typer.Argument()]
):
    if not path.is_file():
        print("no such file")
        raise typer.Abort()

    packets = 0
    error = None
    stat = os.stat(path)
    with Progress(
            TextColumn("[progress.description]{task.description}"),
            FileSizeColumn(),
            TaskProgressColumn(),
            TimeRemainingColumn(),
    ) as progress:
        task = progress.add_task("Validating file...", total=stat.st_size)
        async with await FileReadStream.from_path(path) as file:  # type: FileReadStream
            try:
                async for _ in read_probe_packets_binary(file):
                    packets += 1
                    progress.update(task, completed=await file.tell())
            except ValueError as e:
                print(e)
                error = await file.tell()

    if error is None:
        print(f"Successfully read {packets} packets")
    else:
        print(f"Read {packets} packets, got error at position {error} in file")


@cli.async_command(name="import")
async def import_file(
        series_id: int,
        server_id: int,
        path: Annotated[Path, typer.Argument()],
        force: bool = False,
        session: async_sessionmaker = DBOption
):
    if not path.is_file():
        print("no such file")
        raise typer.Abort()

    stat = await anyio.Path(path).stat()
    async with session() as db:
        series = await crud.probe_series.get(db, series_id)
        if series is None:
            print("no series with id %d" % series_id)
            raise typer.Abort()
        server = await crud.server.get(db, server_id)
        if server is None:
            print("no server with id %d" % server_id)
            raise typer.Abort()
        download = await crud.probe_download.get_where_series_and_file(db, series_id, path.name)
        if download is not None and download.imported_at is not None and not force:
            print("This download file was already imported, use --force to reimport")
            raise typer.Abort()
        if download is None:
            download = await crud.probe_download.create(db, models.ProbeDownloadCreate(
                probe_series_id=series_id,
                server_id=server_id,
                packet_count=0,
                file_name=path.name,
                file_size=stat.st_size,
                downloaded_at=datetime.now()
            ))
            download.probe_series = series
        db.expunge_all()

    importer = ProbeDownloadImporter(session, download)
    await importer.import_downloaded_packets()
    print("Done")


@cli.async_command()
async def import_errored(
        series_id: int,
        session: async_sessionmaker = DBOption
):
    async with session() as db:
        series = await crud.probe_series.get(db, series_id)
        if series is None:
            print("no series with id %d" % series_id)
            raise typer.Abort()
        downloads = await crud.probe_download.get_all_where_errored_and_series(db, series_id)
        db.expunge_all()

    with Progress(
            SpinnerColumn(),
            TextColumn("[progress.description]{task.description}"),
            MofNCompleteColumn(),
            TimeElapsedColumn()
    ) as progress:
        task = progress.add_task("Reimporting errored downloads", total=len(downloads))
        for download in downloads:
            importer = ProbeDownloadImporter(session, download)
            await importer.import_downloaded_packets()
            progress.update(task, advance=1)


@cli.async_command()
async def compress(
        concurrency: Annotated[int, typer.Option("--concurrency", "-c")] = 4,
        session: async_sessionmaker = DBOption
):
    async with session() as db:
        downloads = await crud.probe_download.get_all_where_name_like(db, "%.json")
        db.expunge_all()
    if len(downloads) == 0:
        print("No uncompressed download files found.")
        raise typer.Exit()
    with Progress(
            TextColumn("[progress.description]{task.description}"),
            BarColumn(),
            MofNCompleteColumn(),
            TimeRemainingColumn()
    ) as progress:
        task = progress.add_task("Compressing files", total=len(downloads))
        limiter = anyio.Semaphore(concurrency)
        async with anyio.create_task_group() as tg:
            for download in downloads:
                await tg.start(compress_download_file, session, limiter, progress, task, download)


async def compress_download_file(
        session: async_sessionmaker,
        limiter: anyio.Semaphore,
        progress: Progress,
        task: TaskID,
        download: entities.ProbeDownload,
        *,
        task_status: TaskStatus[None] = TASK_STATUS_IGNORED
):
    async with limiter:
        task_status.started()
        path = anyio.Path(settings.DOWNLOAD_DIR, download.file_name)
        if not await path.exists():
            gz_path = anyio.Path(settings.DOWNLOAD_DIR, download.file_name + ".gz")
            if await gz_path.exists():
                print("Download file %s is already gzip compressed" % download.file_name)
                stat = await gz_path.stat()
                async with session() as db:
                    download.file_name = download.file_name + ".gz"
                    download.file_size = stat.st_size
                    await crud.probe_download.update(db, download)
                progress.update(task, advance=1)
                return
            br_path = anyio.Path(settings.DOWNLOAD_DIR, download.file_name + ".br")
            if await br_path.exists():
                print("Download file %s is already brotli compressed" % download.file_name)
                stat = await br_path.stat()
                async with session() as db:
                    download.file_name = download.file_name + ".br"
                    download.file_size = stat.st_size
                    await crud.probe_download.update(db, download)
                progress.update(task, advance=1)
                return
            print("Download file %s does not exist." % download.file_name)
            progress.update(task, advance=1)
            return

        compressed_path = await to_process.run_sync(_compress_file, str(await path.absolute()))
        stat = await anyio.Path(compressed_path).stat()
        async with session() as db:
            download.file_name = download.file_name + ".br"
            download.file_size = stat.st_size
            await crud.probe_download.update(db, download)
        progress.update(task, advance=1)


def _compress_file(path: str) -> str:
    compressed_path = path + ".br"
    with open(path, "rb") as src:
        with open(compressed_path, "wb") as dest:
            compressor = brotli.Compressor(brotli.MODE_TEXT)
            while data := src.read(2 ** 16):
                dest.write(compressor.process(data))
            dest.write(compressor.finish())
    os.unlink(path)
    return compressed_path


@contextmanager
def pg_connect() -> Iterator[Connection]:
    with Connection.connect(
            host=settings.POSTGRES_HOST,
            port=settings.POSTGRES_PORT,
            user=settings.POSTGRES_USER,
            password=settings.POSTGRES_PASSWORD,
            dbname=settings.POSTGRES_DB
    ) as conn:
        yield conn


@cli.command()
def inspect(
        table: str,
        probe_series_id: int,
        server_id: int,
        target_service_id: int,
        sequence_number: int,
        ws: Annotated[bool, typer.Option("--ws")] = False
):
    with pg_connect() as con:  # type: Connection
        result = con.execute(sql.SQL("""
        SELECT data FROM {table}
        WHERE probe_series_id = %s AND server_id = %s AND target_service_id = %s AND sequence_number = %s
        """).format(table=sql.Identifier(table)), (probe_series_id, server_id, target_service_id, sequence_number))
        data = result.fetchone()
    if not data:
        print("No packet in table %s with this ID" % table)
        raise typer.Abort()
    data = data[0]
    if not data:
        print("No data saved for packet with this ID")
        raise typer.Abort()
    if data[0] == 0x45:
        packet = IP(data)
        print(packet)
        if ws:
            wireshark(packet)
    elif data[0] == 0x60:
        packet = IPv6(data)
        print(packet)
        if ws:
            wireshark(packet)
    else:
        print(data)


@cli.command()
def import_database(database: str, src_table: str, dst_table: str, query: Annotated[str, typer.Option("--query")] = None):
    with Connection.connect(database) as src:
        with pg_connect() as dest:  # type: Connection
            with Progress(
                    SpinnerColumn(),
                    TextColumn("[progress.description]{task.description}"),
                    TimeElapsedColumn()
            ) as progress:
                task = progress.add_task("Importing data from external database...", total=1)
                if query is None:
                    sql_src = f"COPY {src_table} TO STDOUT (FORMAT BINARY)"
                else:
                    sql_src = f"COPY ({query}) TO STDOUT (FORMAT BINARY)"
                with src.cursor().copy(sql_src) as copy_src:
                    with dest.cursor().copy(f"COPY {dst_table} FROM STDOUT (FORMAT BINARY)") as copy_dest:
                        for data in copy_src:
                            copy_dest.write(data)
                progress.update(task, completed=1)


@cli.command()
def copy_bulk():
    with pg_connect() as con_src:  # type: Connection
        with pg_connect() as con_dst:  # type: Connection
            with Progress(
                    SpinnerColumn(),
                    TextColumn("[progress.description]{task.description}"),
                    TimeElapsedColumn()
            ) as progress:
                task = progress.add_task("Copying data from bulk to public table...", total=1)
                query_src = """
                COPY probe_packets_bulk (
                    probe_series_id, sequence_number, server_id, target_service_id, 
                    send_time, recv_time, latency, delta, size, dropped, data, anomaly
                ) TO STDOUT (FORMAT BINARY)
                """
                query_dst = """
                COPY probe_packets_public (
                    probe_series_id, sequence_number, server_id, target_service_id, 
                    send_time, recv_time, latency, delta, size, dropped, data, anomaly
                ) FROM STDIN (FORMAT BINARY)
                """
                with con_src.cursor().copy(query_src) as copy_src:
                    with con_dst.cursor().copy(query_dst) as copy_dest:
                        for data in copy_src:
                            copy_dest.write(data)
                progress.update(task, completed=1)
