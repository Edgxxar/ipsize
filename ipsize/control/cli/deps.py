import asyncio
from functools import wraps
from logging.config import dictConfig

import typer
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine

from ipsize.config import LogConfig
from ipsize.control.config import settings

dictConfig(LogConfig(settings.LOG_FILENAME))


class AsyncTyper(typer.Typer):
    def async_command(self, *args, **kwargs):
        def decorator(async_func):
            @wraps(async_func)
            def sync_func(*_args, **_kwargs):
                return asyncio.run(async_func(*_args, **_kwargs))

            self.command(*args, **kwargs)(sync_func)
            return async_func

        return decorator


def parse_db(uri) -> async_sessionmaker:
    """
    Creates a database session, required to trick typer into accepting CLI options of type Session.
    """
    engine = create_async_engine(uri)
    return async_sessionmaker(engine, autoflush=False, autocommit=False, expire_on_commit=False)


DBOption = typer.Option(parser=parse_db, hidden=True, default=str(settings.SQLALCHEMY_DATABASE_URI))
