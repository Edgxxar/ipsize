import typer
from cryptography.fernet import InvalidToken
from sqlalchemy.ext.asyncio import async_sessionmaker

from ipsize import security
from ipsize.control.cli.deps import AsyncTyper, DBOption
from ipsize.control.config import settings
from ipsize.control.persistence import crud

cli = AsyncTyper()


@cli.async_command()
async def decrypt_secret(server_id: int, session: async_sessionmaker = DBOption):
    async with session() as db:
        server = await crud.server.get(db, server_id)
    if server is None:
        print("Invalid server ID")
        raise typer.Abort()

    try:
        control_secret = security.decrypt_string(server.control_secret, settings.SECRET_KEY, server.control_secret_salt)
    except InvalidToken:
        print(f"Unable to decrypt control server secret for probe server {server_id}, did the server secret change?")
        raise typer.Abort()
    print(control_secret)
