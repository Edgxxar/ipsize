import asyncio
import gzip
import ipaddress
import random
from datetime import datetime
from ipaddress import IPv4Address, IPv4Network, IPv6Address
from pathlib import Path
from typing import Annotated

import anyio
import dns.asyncresolver
import dns.reversename
import shodan
import typer
from anyio import TASK_STATUS_IGNORED
from anyio.abc import TaskStatus
from censys.search import CensysHosts
from dns.exception import DNSException
from pydantic import IPvAnyAddress, IPvAnyNetwork
from rich import print
from rich.progress import BarColumn, MofNCompleteColumn, Progress, SpinnerColumn, TaskID, TextColumn, \
    TimeElapsedColumn, TimeRemainingColumn
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker

from ipsize import models
from ipsize.control.cli.deps import AsyncTyper, DBOption
from ipsize.control.config import settings
from ipsize.control.monitor import check_status_dns_udp, check_status_icmp, check_status_smtp
from ipsize.control.persistence import crud, entities
from ipsize.models.enum import ProbeSeriesType, Status
from ipsize.util import Counter

cli = AsyncTyper()


# ===== general target commands ===== #


@cli.async_command()
async def show(
        address: Annotated[IPvAnyAddress, typer.Argument(parser=IPvAnyAddress)],
        session: async_sessionmaker = DBOption
):
    async with session() as db:
        host = await crud.target_host.get_by_address_join_services(db, address)
    if host is None:
        print("Host does not exist")
    else:
        print(models.TargetHost.model_validate(host))


@cli.async_command()
async def find(
        network: Annotated[IPvAnyNetwork, typer.Argument(parser=IPvAnyNetwork)],
        session: async_sessionmaker = DBOption
):
    async with session() as db:
        hosts = await crud.target_host.get_where_in_network_load_services(db, network)
    print("Found %d hosts in %s" % (len(hosts), network))
    print([models.TargetHost.model_validate(host) for host in hosts])


@cli.async_command()
async def find_ipv6(
        session: async_sessionmaker = DBOption
):
    async with session() as db:
        hosts = await crud.target_host.get_where_address_version(db, 4)
    with Progress() as progress:
        task = progress.add_task("Searching for IPv6 addresses...", total=len(hosts))
        async with anyio.create_task_group() as tg:
            for host in hosts:
                if not host.address.is_global:
                    continue
                tg.start_soon(find_target_ipv6_address, session, host, progress, task)


async def find_target_ipv6_address(
        session: async_sessionmaker, target: entities.TargetHost, progress: Progress, task: TaskID
):
    n = dns.reversename.from_address(str(target.address))
    try:
        answer = await dns.asyncresolver.resolve(n, "PTR", raise_on_no_answer=False)
        if answer.rrset:
            hostname = answer.rrset[0].target
            ipv6 = await dns.asyncresolver.resolve(hostname, "AAAA", raise_on_no_answer=False)
            if ipv6.rrset:
                await create_ipv6_target(session, target, ipv6.rrset[0].address)
    except DNSException:
        pass
    progress.update(task, advance=1)


async def create_ipv6_target(session: async_sessionmaker, target: entities.TargetHost, address: str):
    async with session() as db:
        ipv6_target = await crud.target_host.get_by_address_join_services(db, ipaddress.ip_address(address))
        if ipv6_target is None:
            print("Creating host with address %s" % address)
            ipv6_target = await crud.target_host.create(db, models.TargetHostCreate(address=address))
            ipv6_target = await crud.target_host.get_join_services(db, ipv6_target.id)
        else:
            print("Host with address %s already exists" % address)
        model = models.TargetHost.model_validate(ipv6_target)
        for service in target.services:  # type: entities.TargetService
            if not model.has_service_type(service.type):
                print("  creating %s service " % service.type.name)
                await crud.target_service.create(db, models.TargetServiceCreate(
                    target_host_id=ipv6_target.id,
                    type=service.type,
                    port=service.port
                ))


@cli.async_command()
async def fix_icmp(session: async_sessionmaker = DBOption):
    async with session() as db:
        hosts = await crud.target_host.get_where_service_not_exists(db, ProbeSeriesType.PUBLIC_ICMP_PING)
        if len(hosts) == 0:
            print("All hosts have the ICMP service.")
            raise typer.Exit
        last_update = datetime.now()
        services = [{
            "type": ProbeSeriesType.PUBLIC_ICMP_PING,
            "target_host_id": h.id,
            "last_update": last_update
        } for h in hosts]
        await crud.target_service.create_bulk(db, services)
    print("Created ICMP service for %d hosts." % len(hosts))


async def update_target_host(
        host: entities.TargetHost,
        info: dict,
        db: AsyncSession,
        *,
        column_last_update: str = "last_updated_at",
) -> models.TargetHost:
    if "location" in info:
        host.location = info["location"]
    if "autonomous_system" in info:
        host.autonomous_system = info["autonomous_system"]
    elif "asn" in info:
        host.autonomous_system = info["asn"]
    host.last_update = info[column_last_update] if column_last_update in info else datetime.now()
    host = await crud.target_host.update(db, host)
    await host.awaitable_attrs.services
    return models.TargetHost.model_validate(host)


# ===== Shodan Commands ===== #


@cli.async_command()
async def sh_download(
        query: str,
        limit: Annotated[int, typer.Option("--limit", "-l")] = 100,
        offset: Annotated[int, typer.Option("--offset", "-o")] = 0,
        debug: Annotated[bool, typer.Option("--debug", "-d")] = False,
        session: async_sessionmaker = DBOption
):
    """
    Searches for DNS servers in the shodan dataset.
    """
    with Progress() as progress:
        task = progress.add_task("Finding servers using Shodan API...", total=limit)
        api = shodan.Shodan(settings.SHODAN_API_KEY)
        try:
            results = api.search(query, limit=limit, offset=offset)
            print(f"%d hosts found in total, using results %d-%d" % (results["total"], offset, offset + limit))
            progress.update(task, total=min(limit, results["total"]))
            async with session() as db:
                for result in results["matches"]:
                    if debug:
                        print(result)
                    # update host
                    address = ipaddress.ip_address(result["ip_str"])
                    host = await crud.target_host.get_by_address_join_services(db, address)
                    if host is None:
                        host = await crud.target_host.create(db, models.TargetHostCreate(address=address))
                    host = await update_target_host(host, result, db, column_last_update="timestamp")

                    # ICMP
                    if not host.has_service_type(ProbeSeriesType.PUBLIC_ICMP_PING):
                        await crud.target_service.create(db, models.TargetServiceCreate(
                            target_host_id=host.id,
                            type=ProbeSeriesType.PUBLIC_ICMP_PING,
                        ))

                    # DNS
                    if result["port"] == 53:
                        if not host.has_service_type(ProbeSeriesType.PUBLIC_UDP_DNS):
                            await crud.target_service.create(db, models.TargetServiceCreate(
                                target_host_id=host.id,
                                type=ProbeSeriesType.PUBLIC_UDP_DNS,
                                port=result["port"]
                            ))
                        if not host.has_service_type(ProbeSeriesType.PUBLIC_TCP_DNS):
                            await crud.target_service.create(db, models.TargetServiceCreate(
                                target_host_id=host.id,
                                type=ProbeSeriesType.PUBLIC_TCP_DNS,
                                port=result["port"]
                            ))

                    # SMTP
                    elif result["port"] == 25:
                        if not host.has_service_type(ProbeSeriesType.PUBLIC_TCP_SMTP):
                            await crud.target_service.create(db, models.TargetServiceCreate(
                                target_host_id=host.id,
                                type=ProbeSeriesType.PUBLIC_TCP_SMTP,
                                port=result["port"]
                            ))
                    progress.update(task, advance=1)
        except shodan.APIError as e:
            print(e)
            raise typer.Abort()


# ===== Censys Commands ===== #

@cli.async_command()
async def cs_download(
        address: Annotated[IPvAnyAddress, typer.Argument(parser=IPvAnyAddress)],
        create: bool = True,
        session: async_sessionmaker = DBOption
):
    async with session() as db:
        host = await crud.target_host.get_by_address_join_services(db, address)
        if host is None and not create:
            print("Host does not exist, skipping download.")
            raise typer.Abort()

        if host is None:
            host = await crud.target_host.create(db, models.TargetHostCreate(address=address))

        with Progress(SpinnerColumn(), TextColumn("[progress.description]{task.description}"),
                      transient=True) as progress:
            progress.add_task(description="Querying censys...")
            search = CensysHosts()
            info = search.view(str(host.address))

        host = await update_target_host(host, info, db)
    print(host)


@cli.async_command()
async def cs_download_dns(
        count: int,
        prefixlen: Annotated[int, typer.Option("--prefixlen", "-p", help="Netmask size of the random subnets")] = 11,
        session: async_sessionmaker = DBOption
):
    """
    Searches for DNS servers in the censys dataset, by choosing a random subnet of the available IPv4 address space to
    query for. This process is repeated until the specified amount of servers has been found.
    """
    with Progress() as progress:
        task = progress.add_task("Finding new dns servers using Censys API...", total=count)

        while not progress.finished:
            # choose random subnet of the IPv4 address space
            while True:
                address = IPv4Address(random.getrandbits(prefixlen) << (32 - prefixlen))
                network = IPv4Network(f"{address}/{prefixlen}")
                # discard private, multicast and reserved networks
                if network.is_global and not network.is_multicast and not network.is_reserved:
                    break

            progress.console.print(f"Querying censys for DNS servers in {network}")
            search = CensysHosts()
            # search for forwarding DNS servers with EDNS support
            results = search.search(
                f"ip: {network} and services.service_name: DNS and services.dns.resolves_correctly: true and "
                f"services.dns.server_type: FORWARDING and services.dns.edns.do: true",
                per_page=100,
                pages=1,
                virtual_hosts="EXCLUDE"
            )

            progress.console.print("Updating database...")
            async with session() as db:
                for page in results:
                    for hit in page:
                        # update host
                        address = ipaddress.ip_address(hit["ip"])
                        host = await crud.target_host.get_by_address_join_services(db, address)
                        if host is None:
                            host = await crud.target_host.create(db, models.TargetHostCreate(address=address))
                        host = await update_target_host(host, hit, db)

                        # update services
                        for service in hit["services"]:
                            if service["service_name"] == "DNS":
                                if not host.has_service_type(ProbeSeriesType.PUBLIC_UDP_DNS):
                                    await crud.target_service.create(db, models.TargetServiceCreate(
                                        target_host_id=host.id,
                                        type=ProbeSeriesType.PUBLIC_UDP_DNS,
                                        port=service["port"]
                                    ))
                                if not host.has_service_type(ProbeSeriesType.PUBLIC_TCP_DNS):
                                    await crud.target_service.create(db, models.TargetServiceCreate(
                                        target_host_id=host.id,
                                        type=ProbeSeriesType.PUBLIC_TCP_DNS,
                                        port=service["port"]
                                    ))
                        progress.update(task, advance=1)
            await asyncio.sleep(1)


@cli.async_command()
async def cs_download_smtp(
        count: int,
        prefixlen: Annotated[int, typer.Option("--prefixlen", "-p", help="Netmask size of the random subnets")] = 11,
        session: async_sessionmaker = DBOption
):
    """
    Searches for SMTP servers in the censys dataset, by choosing a random subnet of the available IPv4 address space to
    query for. This process is repeated until the specified amount of servers has been found.
    """
    with Progress() as progress:
        task = progress.add_task("Finding new smtp servers using Censys API...", total=count)

        while not progress.finished:
            # choose random subnet of the IPv4 address space
            while True:
                address = IPv4Address(random.getrandbits(prefixlen) << (32 - prefixlen))
                network = IPv4Network(f"{address}/{prefixlen}")
                # discard private, multicast and reserved networks
                if network.is_global and not network.is_multicast and not network.is_reserved:
                    break

            progress.console.print(f"Querying censys for SMTP servers in {network}")
            search = CensysHosts()
            # search for SMTP servers on port 25
            results = search.search(
                f"ip: {network} and services.service_name = SMTP and services.port = 25",
                per_page=100,
                pages=1,
                virtual_hosts="EXCLUDE"
            )

            progress.console.print("Updating database...")
            async with session() as db:
                for page in results:
                    for hit in page:
                        # update host
                        address = ipaddress.ip_address(hit["ip"])
                        host = await crud.target_host.get_by_address_join_services(db, address)
                        if host is None:
                            host = await crud.target_host.create(db, models.TargetHostCreate(address=address))
                        host = await update_target_host(host, hit, db)

                        # update services
                        for service in hit["services"]:
                            if service["service_name"] == "SMTP" and service["port"] == 25:
                                if not host.has_service_type(ProbeSeriesType.PUBLIC_TCP_SMTP):
                                    await crud.target_service.create(db, models.TargetServiceCreate(
                                        target_host_id=host.id,
                                        type=ProbeSeriesType.PUBLIC_TCP_SMTP,
                                        port=service["port"]
                                    ))
                        progress.update(task, advance=1)
            await asyncio.sleep(1)


@cli.async_command()
async def cs_update_info(session: async_sessionmaker = DBOption):
    async with session() as db:
        hosts = await crud.target_host.get_where_location_is_null(db)
        search = CensysHosts()
        with Progress(
                TextColumn("[progress.description]{task.description}"),
                BarColumn(),
                MofNCompleteColumn(),
                TimeRemainingColumn()
        ) as progress:
            task = progress.add_task("Updating host info using Censys API", total=len(hosts))
            for host in hosts:
                info = search.view(str(host.address))
                await update_target_host(host, info, db)
                progress.update(task, advance=1)


# ===== CAIDA Data Server Commands ===== #

@cli.async_command()
async def import_ipv6(path: Annotated[Path, typer.Argument()], session: async_sessionmaker = DBOption):
    if not path.is_file():
        print("no such file")
        raise typer.Abort()

    limiter = anyio.Semaphore(1000)
    count_hosts = Counter()
    count_dns = Counter()
    count_smtp = Counter()
    with Progress(
            SpinnerColumn(),
            TextColumn("[progress.description]{task.description}"),
            TimeElapsedColumn()
    ) as progress:
        progress.add_task(description="Loading IPv6 targets...", total=None)
        async with anyio.create_task_group() as tg:
            with gzip.open(path, mode="rt", encoding="UTF-8") as file:
                while line := file.readline():
                    try:
                        _, ip, _ = line.split()
                    except ValueError:
                        print("Invalid line: " + line)
                        continue
                    await tg.start(check_ipv6_host, session, ipaddress.ip_address(ip), limiter,
                                   count_hosts, count_dns, count_smtp)
    print(f"Imported {count_hosts} hosts, {count_dns} DNS services, and {count_smtp} SMTP services")


async def check_ipv6_host(session: async_sessionmaker, ip: IPv6Address, limiter: anyio.Semaphore,
                          count_hosts, count_dns, count_smtp, *, task_status: TaskStatus[None] = TASK_STATUS_IGNORED):
    async with limiter:
        task_status.started()

        status_ping = await check_status_icmp(ip)
        if status_ping == Status.ERROR:  # abort if host cannot be pinged
            return
        status_dns_udp = await check_status_dns_udp(ip, 53)
        status_smtp = await check_status_smtp(ip, 25)
        if status_dns_udp == Status.ERROR and status_smtp == Status.ERROR:  # abort if no interesting service
            return
        async with session() as db:
            host = await crud.target_host.get_by_address_join_services(db, ip)
            if host is not None:
                return
            host = await crud.target_host.create(db, models.TargetHostCreate(address=ip))
            count_hosts += 1
            await crud.target_service.create(db, models.TargetServiceCreate(
                target_host_id=host.id,
                type=ProbeSeriesType.PUBLIC_ICMP_PING
            ))
            if status_dns_udp == Status.OK:
                await crud.target_service.create(db, models.TargetServiceCreate(
                    target_host_id=host.id,
                    type=ProbeSeriesType.PUBLIC_UDP_DNS,
                    port=53
                ))
                await crud.target_service.create(db, models.TargetServiceCreate(
                    target_host_id=host.id,
                    type=ProbeSeriesType.PUBLIC_TCP_DNS,
                    port=53
                ))
                count_dns += 1
            if status_smtp == Status.OK:
                await crud.target_service.create(db, models.TargetServiceCreate(
                    target_host_id=host.id,
                    type=ProbeSeriesType.PUBLIC_TCP_SMTP,
                    port=25
                ))
                count_smtp += 1
