import base64
import math
from typing import Any

from bitarray import bitarray
from bitarray.util import ba2int, int2ba
from starlette.requests import Request


def get_remote_address(request: Request):
    """
    Gets the remote address that corresponds to an HTTP request. Uses the value of the "X-Real-IP" header if present.
    :param request: the HTTP request
    :return: the remote IP address
    """
    if "X-Real-IP" in request.headers:
        return request.headers.get("X-Real-IP")
    return request.client.host if request.client is not None else "127.0.0.1"


def default_encode(obj: Any) -> Any:
    """
    Transform an object for JSON encoding.
    :param obj: object to transform
    :return: representation of the object that con either be directly used in JSON, or passed to this method again
    """
    if isinstance(obj, bytes):
        return base64.b64encode(obj).decode()
    raise TypeError


def encode_integers(data: list[int]) -> bytes:
    """
    Encodes a sorted list of positive integers as a bytestring. The first byte is an integer representing how many bits
    are used to represent the elements of the list. After that all elements of the list follow.
    :param data: sorted list of positive integers (ascending)
    :return: encoded bytestring
    """
    if len(data) == 0:
        return bytes(1)  # one byte of zeros to indicate empty list
    # list is sorted, last element is the largest
    try:
        bits_per = math.floor(math.log2(data[len(data) - 1])) + 1
    except ValueError:
        raise ValueError("data needs to be strictly positive")
    encoded = bitarray()
    # first byte is how many bits per element
    encoded += int2ba(bits_per, 8)
    # all elements get appended
    try:
        for v in data:
            if v <= 0:
                raise ValueError("data needs to be strictly positive")
            encoded += int2ba(v, bits_per, signed=False)
    except OverflowError:
        raise ValueError("data needs to be sorted")
    return encoded.tobytes()


def decode_integers(encoded: bytes) -> list[int]:
    """
    Decodes a bytestring produced by ``encode_integers``.
    :param encoded: encoded bytestring
    :return: list of numbers
    """
    if len(encoded) == 0:
        raise ValueError("empty data is not allowed")
    # first byte is how many bits per element
    bits_per = encoded[0]
    if bits_per == 0:
        return []
    data = bitarray()
    data.frombytes(encoded)
    decoded = []
    # all elements are appended
    for i in range(8, len(data), bits_per):
        v = ba2int(data[i:i + bits_per], signed=False)
        if v == 0:  # the only zeros would be padding at the end, we are done
            break
        decoded.append(v)
    return decoded


class Counter:
    """
    Mutable integer, not thread save. Used to sum up results in async tasks.
    """

    def __init__(self, initial: int = 0) -> None:
        self._i = initial

    def __iadd__(self, other) -> None:
        self._i += other

    def __eq__(self, other) -> bool:
        return self._i == other

    def __ne__(self, other) -> bool:
        return self._i != other

    def __gt__(self, other) -> bool:
        return self._i > other

    def __ge__(self, other) -> bool:
        return self._i >= other

    def __lt__(self, other) -> bool:
        return self._i < other

    def __le__(self, other) -> bool:
        return self._i <= other

    def __int__(self) -> int:
        return self._i

    def __repr__(self) -> str:
        return repr(self._i)
