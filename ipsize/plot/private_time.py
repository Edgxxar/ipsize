import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from matplotlib.patches import Patch

from ipsize.plot.base import DataPlotter, ScaleFormatter, TimeFormatter, get_server_labels, set_size

# exported via:
"""
SELECT 
    ps.name AS "server",
    x.send_time,
    x.probe_count, 
    x.recv_count, 
    1 - x.recv_count::float / x.probe_count::float AS drop_rate, 
    x.latency
FROM (
    SELECT
        server_id,
        EXTRACT(EPOCH FROM send_time)::int / 3600 AS send_time,  -- timestamps in hours
        count(*) AS probe_count,
        count(recv_time) AS recv_count,
        EXTRACT(EPOCH FROM percentile_cont(0.5) WITHIN GROUP (ORDER BY latency)) AS latency
    FROM probe_packets_private
    GROUP BY 1, 2
) x
JOIN probe_servers ps ON ps.id = x.server_id
ORDER BY 1, 2
"""

"""
SELECT 
    ps.name AS "server",
    x.ip,
    x.send_time,
    x.probe_count, 
    x.recv_count, 
    1 - x.recv_count::float / x.probe_count::float AS drop_rate, 
    x.latency
FROM (
    SELECT
        server_id,
        FAMILY(th.address) AS ip,
        EXTRACT(EPOCH FROM send_time)::int / 3600 AS send_time,  -- timestamps in hours
        count(*) AS probe_count,
        count(recv_time) AS recv_count,
        EXTRACT(EPOCH FROM percentile_cont(0.5) WITHIN GROUP (ORDER BY latency)) AS latency
    FROM probe_packets_private p
    JOIN target_services ts ON ts.id = p.target_service_id
    JOIN target_hosts th ON th.id = ts.target_host_id
    GROUP BY 1, 2, 3
) x
JOIN probe_servers ps ON ps.id = x.server_id
ORDER BY 1, 2, 3
"""

data = pd.read_csv("results/private_time.csv")
data_ip = pd.read_csv("results/private_time_ip.csv")
servers = data_ip["server"].unique()


class LossByTime(DataPlotter):

    def plot(self):
        fig, axs = plt.subplots(1, 2, figsize=set_size((1, 2)), sharex=True)
        self.wspace = 0.18

        for ax in axs:
            ax.grid()
            ax.set_xlabel("Time of Day (UTC)")
            ax.set_xticks(range(0, 24, 3))
            ax.set_xlim([0, 23])

        axs[0].set_ylabel("Loss Rate (\\%)")
        axs[0].yaxis.set_major_formatter(ScaleFormatter(100, 2))

        axs[1].set_ylabel("Latency (ms)")
        axs[1].yaxis.set_major_formatter(ScaleFormatter(1000, 0))

        df = pd.DataFrame(data).reset_index()
        df["hour"] = df["send_time"].mod(24)

        sns.lineplot(df, x="hour", y="drop_rate", estimator="median", errorbar="ci", ax=axs[0])
        sns.lineplot(df, x="hour", y="latency", estimator="mean", errorbar="se", ax=axs[1])


class ServerLossByTime(DataPlotter):

    def plot(self):
        fig, axs = plt.subplots(10, 2, figsize=set_size((8, 2)))
        self.hspace = 0.77
        self.wspace = 0.15

        grouped = data_ip.groupby(["ip", "server", "send_time"]).mean()
        df = pd.DataFrame(grouped).reset_index()

        server_it = iter(servers)
        palette = sns.color_palette("colorblind", 2)
        for row in axs:
            for ax in row:
                server = next(server_it)
                dfs = df.loc[df["server"] == server]

                sns.lineplot(ax=ax, data=dfs, x="send_time", y="drop_rate", hue="ip",
                             linewidth=.7, linestyle="-", palette=palette, legend=False)

                ax.grid(axis="y")
                ax.set_title(server)
                ax.yaxis.set_major_formatter(ScaleFormatter(100, 1))
                ax.set_xlabel(None)
                x_start = dfs["send_time"].min()
                x_start = x_start - (x_start % 24) + 24
                ax.set_xticks(range(x_start, dfs["send_time"].max(), 24))
                ax.xaxis.set_major_formatter(TimeFormatter(fmt="%d.%m."))
                ax.set_xticklabels(ax.get_xticklabels(), rotation=33, ha='right', size=8)
            row[0].set_ylabel("Loss Rate (\\%)")
            row[1].set_ylabel(None)


class ServerLossByIp(DataPlotter):

    def plot(self):
        fig, axs = plt.subplots(1, 2, sharex=True, figsize=set_size((1, 2)))
        self.wspace = 0.22

        df = pd.DataFrame(data_ip.groupby(["ip", "server", "send_time"])["drop_rate"].median()).reset_index()
        df = df.loc[df["drop_rate"] > 0]
        sns.barplot(data=df, x="server", y="drop_rate", hue="ip", estimator="median", errorbar="ci",
                    palette="colorblind", err_kws={"linewidth": 1, "zorder": 11}, legend=None, zorder=10, ax=axs[0])
        axs[0].set_ylabel("Loss Rate (\\%)")
        axs[0].yaxis.set_major_formatter(ScaleFormatter(100, 1))

        df = pd.DataFrame(data_ip.groupby(["ip", "server", "send_time"])["latency"].mean()).reset_index()
        sns.barplot(data=df, x="server", y="latency", hue="ip", estimator="mean", errorbar="se",
                    palette="colorblind", err_kws={"linewidth": 1, "zorder": 11}, legend=None, zorder=10, ax=axs[1])
        axs[1].set_ylabel("Latency (ms)")
        axs[1].yaxis.set_major_formatter(ScaleFormatter(1000, 0))

        for ax in axs:
            ax.set_xlabel("Sending Probe Server")
            ax.set_xticks(range(len(servers)), get_server_labels(servers), rotation=90)
            ax.grid(zorder=0, axis="y")

        colors = sns.color_palette("colorblind")
        legend = [
            Patch(facecolor=colors[0], edgecolor=None, linewidth=.6, label='IPv4'),
            Patch(facecolor=colors[1], edgecolor=None, linewidth=.6, label='IPv6')
        ]
        axs[0].legend(loc="upper left", handles=legend)
        axs[1].legend(bbox_to_anchor=(0.26, 1), handles=legend)


ServerLossByTime("private_server_loss_by_time")
LossByTime("private_time")
ServerLossByIp("private_server_loss_by_ip")
