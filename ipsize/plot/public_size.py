import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from matplotlib.axes import Axes
from matplotlib.lines import Line2D
from matplotlib.ticker import FixedLocator

from ipsize.plot.base import DataPlotter, ScaleFormatter, plot_regression, set_size

"""
SELECT 
    p.probe_series_id,
    FAMILY(th.address) AS ip,
    p."size",
    count(*) AS probe_count,
    count(p.send_time) AS sent_count,
    count(p.recv_time) AS recv_count,
    percentile_cont(0.5) WITHIN GROUP (ORDER BY p.latency) AS latency
FROM probe_packets_public p
JOIN target_performance tp ON 
    tp.probe_series_id = p.probe_series_id AND 
    tp.target_service_id = p.target_service_id AND 
    tp.probe_drop_rate < 0.1
JOIN target_services ts ON ts.id = p.target_service_id
JOIN target_hosts th ON th.id = ts.target_host_id
WHERE 
    (p.probe_series_id = 31 AND tp.anomaly_rate < 0.03) OR  -- ICMP probes
    (p.probe_series_id = 32 AND tp.anomaly_rate < 0.20) OR  -- UDP/DNS probes
    (p.probe_series_id = 44 AND tp.anomaly_rate < 0.20) OR  -- TCP/DNS probes
    (p.probe_series_id = 45 AND tp.anomaly_rate < 0.03)     -- SMTP probes
GROUP BY 1, 2, 3
ORDER BY 1, 2, 3
"""

data_cl = pd.read_csv("results/public_size_cl.csv")
data_cl["drop_rate"] = 1 - data_cl["recv_count"] / data_cl["probe_count"]

data_co = pd.read_csv("results/public_size_co.csv")
data_co["drop_rate"] = 1 - data_co["recv_count"] / data_co["sent_count"]

data_tdns = pd.read_csv("results/public_size_tdns.csv")


def plot_scatter_ip(data: pd.DataFrame, ax: Axes, title, y: str = "drop_rate", legend_loc="upper left"):
    ax.set_title(title)
    colors = sns.color_palette("colorblind", 2)
    sns.scatterplot(data, x="size", y=y, hue="ip", style="ip", palette=colors, markers=["1", "2"], s=2, linewidths=.2,
                    legend=False, zorder=10, ax=ax)
    ax.set_xticks(range(100, 1501, 200))
    ax.set_xlabel("Packet Size (Bytes)")
    ax.grid(zorder=0)
    ax.legend(loc=legend_loc, handles=[
        Line2D([], [], marker="1", linestyle='None', color=colors[0], label="IPv4"),
        Line2D([], [], marker="2", linestyle='None', color=colors[1], label="IPv6")
    ])


class PublicSizeCl(DataPlotter):
    def plot(self):
        fig, axs = plt.subplots(1, 2, sharex=True, figsize=set_size((1, 2)))
        self.wspace = .18

        icmp = data_cl.loc[data_cl["probe_series_id"] == 31]
        udns = data_cl.loc[data_cl["probe_series_id"] == 32]

        axs[0].set_yscale("symlog", linthresh=.04)
        axs[0].set_yticks([.01, .02, .03, .04, .1, .2, .4])
        axs[0].yaxis.set_minor_locator(FixedLocator([.05, .06, .07, .08, .09, .3]))
        plot_scatter_ip(icmp, axs[0], title="ICMP")
        plot_scatter_ip(udns, axs[1], title="UDP/DNS")

        for ax in axs:
            ax.set_ylabel("Loss Rate (\\%)")
            ax.yaxis.set_major_formatter(ScaleFormatter(100, 0))


class PublicSizeClTrend(DataPlotter):
    def plot(self):
        fig, axs = plt.subplots(1, 2, sharex=True, sharey=True, figsize=set_size((1, 2)))
        self.wspace = .07

        axs[0].set_title("ICMP")
        icmp_v4 = data_cl.loc[(data_cl["probe_series_id"] == 31) & (data_cl["ip"] == 4) &
                              (data_cl["size"] < 1420)]
        plot_regression(axs[0], icmp_v4["size"], icmp_v4["drop_rate"], name=f"IPv4", sti=0)
        icmp_v6 = data_cl.loc[(data_cl["probe_series_id"] == 31) & (data_cl["ip"] == 6) &
                              (data_cl["size"] < 1420)]
        plot_regression(axs[0], icmp_v6["size"], icmp_v6["drop_rate"], name=f"IPv6", sti=1)
        axs[0].legend(loc="center right")

        axs[1].set_title("UDP/DNS")
        udns_v4 = data_cl.loc[(data_cl["probe_series_id"] == 32) & (data_cl["ip"] == 4) &
                              (data_cl["size"] < 400)]
        plot_regression(axs[1], udns_v4["size"], udns_v4["drop_rate"], sti=0)
        udns_v4 = data_cl.loc[(data_cl["probe_series_id"] == 32) & (data_cl["ip"] == 4) &
                              (data_cl["size"] >= 400) & (data_cl["size"] <= 1239)]
        plot_regression(axs[1], udns_v4["size"], udns_v4["drop_rate"], name=f"IPv4", sti=0)
        udns_v6 = data_cl.loc[(data_cl["probe_series_id"] == 32) & (data_cl["ip"] == 6) &
                              (data_cl["size"] <= 1271)]
        plot_regression(axs[1], udns_v6["size"], udns_v6["drop_rate"], name=f"IPv6", sti=1)
        axs[1].legend(loc="lower right", ncols=2)

        axs[0].set_ylabel("Loss Rate (\\%)")
        for ax in axs:
            ax.yaxis.set_major_formatter(ScaleFormatter(100, 1))
            ax.set_xticks(range(100, 1301, 200))
            ax.set_xlabel("Packet Size (Bytes)")
            ax.grid(zorder=0)


class PublicSizeLatencyCl(DataPlotter):
    def plot(self):
        fig, axs = plt.subplots(1, 2, sharex=True, figsize=set_size((1, 2)))
        self.wspace = .18

        icmp = data_cl.loc[data_cl["probe_series_id"] == 31]
        udns = data_cl.loc[data_cl["probe_series_id"] == 32]

        plot_scatter_ip(icmp, axs[0], title="ICMP", y="latency", legend_loc="center left")
        plot_scatter_ip(udns, axs[1], title="UDP/DNS", y="latency")

        axs[0].set_ylabel("Latency (ms)")
        axs[0].yaxis.set_major_formatter(ScaleFormatter(.001, 0))

        axs[1].set_ylabel("Latency (s)")
        axs[1].yaxis.set_major_formatter(ScaleFormatter(.000001, 0))


class PublicSizeCo(DataPlotter):
    def plot(self):
        fig, axs = plt.subplots(1, 2, sharex=True, figsize=set_size((1, 2)))
        self.wspace = .2

        tdns = data_co.loc[(data_co["probe_series_id"] == 44) & (data_co["size"] <= 1480)]
        smtp = data_co.loc[data_co["probe_series_id"] == 45]

        plot_scatter_ip(tdns, axs[0], title="TCP/DNS", legend_loc="upper center")
        plot_scatter_ip(smtp, axs[1], title="SMTP", legend_loc="lower right")

        for ax in axs:
            ax.set_ylabel("Loss Rate (\\%)")
        axs[0].yaxis.set_major_formatter(ScaleFormatter(100, 0))
        axs[1].yaxis.set_major_formatter(ScaleFormatter(100, 1))


class PublicSizeCoTrend(DataPlotter):
    def plot(self):
        fig, axs = plt.subplots(1, 2, sharex=True, sharey=True, figsize=set_size((1, 2)))
        self.wspace = .07

        axs[0].set_title("TCP/DNS")
        tdns_v4 = data_co.loc[(data_co["probe_series_id"] == 44) & (data_co["ip"] == 4) &
                              (data_co["size"] >= 102) & (data_co["size"] <= 1252)]
        plot_regression(axs[0], tdns_v4["size"], tdns_v4["drop_rate"], name=f"IPv4", sti=0)
        tdns_v6 = data_co.loc[(data_co["probe_series_id"] == 44) & (data_co["ip"] == 6) &
                              (data_co["size"] >= 122) & (data_co["size"] <= 1272)]
        plot_regression(axs[0], tdns_v6["size"], tdns_v6["drop_rate"], name=f"IPv6", sti=1)

        axs[1].set_title("SMTP")
        smtp_v4 = data_co.loc[(data_co["probe_series_id"] == 45) & (data_co["ip"] == 4)]
        plot_regression(axs[1], smtp_v4["size"], smtp_v4["drop_rate"], name=f"IPv4", sti=0)
        smtp_v6 = data_co.loc[(data_co["probe_series_id"] == 45) & (data_co["ip"] == 6)]
        plot_regression(axs[1], smtp_v6["size"], smtp_v6["drop_rate"], name=f"IPv6", sti=1)

        for ax in axs:
            ax.yaxis.set_major_formatter(ScaleFormatter(100, 1))
            ax.set_xticks(range(100, 1301, 200))
            ax.set_xlabel("Packet Size (Bytes)")
            ax.grid(zorder=0)
        axs[0].set_ylabel("Loss Rate (\\%)")
        axs[0].legend(loc="lower right")
        axs[1].legend(loc="center right", bbox_to_anchor=(1, .55))


class TargetTDNSLoss(DataPlotter):
    def plot(self):
        fig, axs = plt.subplots(1, 2, figsize=set_size((1, 2)))
        self.wspace = .2

        df = data_tdns
        df["res_drop_rate"] = df["req_recv_count"] / df["sent_count"]
        df["res_drop_ratio"] = df["req_recv_count"] / (df["sent_count"] - df["recv_count"])

        plot_scatter_ip(df.loc[data_tdns["size"] <= 1480], axs[0], None, y="res_drop_rate")

        tdns_v4 = data_tdns.loc[(data_tdns["ip"] == 4) & (data_tdns["size"] >= 102) & (data_tdns["size"] <= 1252)]
        plot_regression(axs[1], tdns_v4["size"], tdns_v4["res_drop_rate"], name=f"IPv4", sti=0)
        tdns_v6 = data_tdns.loc[(data_tdns["ip"] == 6) & (data_tdns["size"] >= 122) & (data_tdns["size"] <= 1272)]
        plot_regression(axs[1], tdns_v6["size"], tdns_v6["res_drop_rate"], name=f"IPv6", sti=1)

        for ax in axs:
            ax.set_title("TCP/DNS Responses")
            ax.set_ylabel("Loss Rate (\\%)")
            ax.set_xlabel("Packet Size (Bytes)")
            ax.grid(zorder=0)
        axs[0].set_xticks(range(100, 1501, 200))
        axs[0].yaxis.set_major_formatter(ScaleFormatter(100, 0))
        axs[1].set_xticks(range(100, 1301, 200))
        axs[1].yaxis.set_major_formatter(ScaleFormatter(100, 1))
        axs[1].legend(loc="upper left")


PublicSizeCl("public_size_loss_cl")
PublicSizeClTrend("public_size_loss_cl_trend")
PublicSizeLatencyCl("public_size_latency_cl")
PublicSizeCo("public_size_loss_co")
PublicSizeCoTrend("public_size_loss_co_trend")

TargetTDNSLoss("public_size_loss_tdns")
