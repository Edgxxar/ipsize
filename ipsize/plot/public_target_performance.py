import itertools

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from ipsize.plot.base import DataPlotter, ScaleFormatter, set_size

# exported via
"""
SELECT FAMILY(th.address) AS ip, tp.*
FROM target_performance tp
JOIN target_services ts ON ts.id = tp.target_service_id
JOIN target_hosts th ON th.id = ts.target_host_id
ORDER BY tp.probe_series_id, tp.target_service_id, ip
"""

# TCP series were redone, import two datasets
data_cl = pd.read_csv("results/public_target_performance_cl.csv")
data_co = pd.read_csv("results/public_target_performance_co.csv")
data_tdns = pd.read_csv("results/public_target_performance_tdns.csv")


class TargetLossCl(DataPlotter):
    def plot(self):
        fig, axs = plt.subplots(1, 2, sharey=True, sharex=True, figsize=set_size((1, 2)))
        self.wspace = 0.05

        icmp = data_cl.loc[data_cl["probe_series_id"] == 31]
        udns = data_cl.loc[data_cl["probe_series_id"] == 32]

        colors = itertools.cycle(sns.color_palette())
        c_icmp = next(colors)
        c_udns = next(colors)

        axs[0].ecdf(icmp["drop_rate"], c=c_icmp, label="ICMP")
        axs[0].ecdf(udns["drop_rate"], c=c_udns, label="UDP/DNS")

        axs[1].ecdf(icmp["anomaly_rate"].dropna(), c=c_icmp, label="ICMP")
        axs[1].ecdf(udns["anomaly_rate"].dropna(), c=c_udns, label="UDP/DNS")

        for ax in axs:
            ax.set_xticks(np.arange(0, 1.1, 0.1))
            ax.yaxis.set_major_formatter(ScaleFormatter(100, 0))
            ax.xaxis.set_major_formatter(ScaleFormatter(100, 0))
            ax.grid(zorder=0)
            ax.legend(loc="lower right")

        axs[0].set_ylabel("Target Services (\\%)")
        axs[0].set_xlabel("Loss Rate (\\%)")
        axs[1].set_xlabel("Anomaly Rate (\\%)")

        plt.yticks(np.arange(0, 1.1, 0.1))


class TargetLossCo(DataPlotter):
    def plot(self):
        fig, axs = plt.subplots(1, 2, sharey=True, figsize=set_size((1, 2)))
        self.wspace = 0.05

        tdns = data_co.loc[data_co["probe_series_id"] == 44].dropna()
        smtp = data_co.loc[data_co["probe_series_id"] == 45].dropna()

        colors = itertools.cycle(sns.color_palette())
        c_tdns = next(colors)
        c_smtp = next(colors)

        axs[0].ecdf(tdns["probe_drop_rate"], c=c_tdns, linestyle="-", label="DNS Probes")
        axs[0].ecdf(tdns["con_drop_rate"], c=c_tdns, linestyle="dotted", label="DNS Connections")
        axs[0].ecdf(smtp["probe_drop_rate"], c=c_smtp, linestyle="-", label="SMTP Probes")
        axs[0].ecdf(smtp["con_drop_rate"], c=c_smtp, linestyle="dotted", label="SMTP Connections")

        axs[1].ecdf(tdns["anomaly_rate"].dropna(), c=c_tdns, label="DNS")
        axs[1].ecdf(smtp["anomaly_rate"].dropna(), c=c_smtp, label="SMTP")

        for ax in axs:
            ax.set_xticks(np.arange(0, 1.1, 0.1))
            ax.spines[['right', 'top']].set_visible(False)
            ax.yaxis.set_major_formatter(ScaleFormatter(100, 0))
            ax.xaxis.set_major_formatter(ScaleFormatter(100, 0))
            ax.grid(zorder=0)
            ax.legend(loc="lower right")

        axs[0].set_ylabel("Target Services (\\%)")
        axs[0].set_xlabel("Loss Rate (\\%)")
        axs[1].set_xlabel("Anomaly Rate (\\%)")

        plt.yticks(np.arange(0, 1.1, 0.1))


class TargetTDNSLoss(DataPlotter):
    def plot(self):
        fig, ax = plt.subplots(1, 1, figsize=set_size())

        df = data_tdns.loc[(data_tdns["probe_drop_rate"] < .1) & (data_tdns["anomaly_rate"] < .2)]

        sns.ecdfplot(df, x="res_drop_ratio", stat="percent", ax=ax)

        ax.set_ylabel("TCP/DNS Targets (\\%)")
        ax.set_xlabel("$\\textrm{Response Loss Rate} / \\textrm{Total Loss Rate}$")
        ax.axis([0, 1, 0, 100])
        ax.set_xticks(np.arange(0, 1.1, 0.1))
        ax.set_yticks(np.arange(0, 110, 10))
        ax.grid()


TargetLossCl("public_target_performance_cl")
TargetLossCo("public_target_performance_co")
TargetTDNSLoss("public_target_performance_tdns")
