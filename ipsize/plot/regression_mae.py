import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt

from ipsize.plot.base import DataPlotter, ScaleFormatter, set_size


class RegressionMAE(DataPlotter):
    def plot(self):
        fig, ax = plt.subplots(1, 1, figsize=set_size((1, 2)))

        # draw OLS and MAE lines
        x = np.arange(20, 1500, 1)
        b0 = 0.00105
        b1 = 0.00000013
        reg = b0 + b1 * x
        mae = 0.00005

        colors = sns.color_palette("colorblind", 2)
        ax.plot(x, reg, color=colors[0], linewidth=1.5, label="OLS", zorder=10)
        ax.plot(x, reg + mae, color=colors[0], linestyle="dotted", linewidth=1, label="MAE", zorder=10)
        ax.plot(x, reg - mae, color=colors[0], linestyle="dotted", linewidth=1, zorder=10)

        # draw two predicted values
        p1x = 380
        p1y = b0 + b1 * p1x
        p2x = p1x + (2 * mae) / b1
        p2y = b0 + b1 * p2x
        dy = p1y + mae

        ax.plot(p1x, p1y, marker="*", color=colors[1], zorder=11)
        ax.annotate("$(x_1, \\hat{y}_1)$", xy=(p1x, p1y - 0.00001), xytext=(p1x, p1y - 1.6 * mae), size=10,
                    arrowprops=dict(arrowstyle="->", facecolor="black"))
        ax.plot(p2x, p2y, marker="*", color=colors[1], zorder=11)
        ax.annotate("$(x_2, \\hat{y}_2)$", xy=(p2x, p2y + 0.00001), xytext=(p2x, p2y + 1.6 * mae), size=10,
                    arrowprops=dict(arrowstyle="->", facecolor="black"))

        # draw line between them
        ax.plot(p1x, dy, marker="|", color=colors[1], zorder=11)
        ax.plot(p2x, dy, marker="|", color=colors[1], zorder=11)
        ax.plot([p1x, p2x], [dy, dy], color=colors[1], linestyle="solid", linewidth=1, zorder=11)
        ax.annotate("$2 \\times \\textrm{{MAE}} / b_1$", xy=(p1x + 150, dy), xytext=(p1x - 40, dy + 0.00006), size=10,
                    arrowprops=dict(arrowstyle="->", facecolor="black"))

        ax.set_ylabel("Loss Rate (\\%)")
        ax.yaxis.set_major_formatter(ScaleFormatter(100, 2))
        ax.set_xlabel("Packet Size (Bytes)")
        ax.set_xticks(range(200, 1500, 200))
        ax.set_xlim(np.min(x), np.max(x))
        # ax.grid()
        ax.legend(loc="upper left")


RegressionMAE("regression_mae")
