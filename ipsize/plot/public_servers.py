import matplotlib
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt

from ipsize.plot.base import DataPlotter, ScaleFormatter, get_server_labels, set_size

# exported via
"""
SELECT
    x.probe_series_id,
    ps.name AS "server",
    x.target_service_id,
    family(th.address) AS ip,
    ts."type",
    x.probe_count,
    x.sent_count,
    x.recv_count,
    x.anomaly_count,
    1 - x.sent_count::float / x.probe_count::float AS con_drop_rate,
    CASE WHEN x.sent_count = 0 THEN NULL ELSE 1 - x.recv_count::float / x.sent_count::float END AS probe_drop_rate,
    CASE WHEN x.recv_count = 0 THEN NULL ELSE x.anomaly_count::float / x.recv_count::float END AS anomaly_rate
FROM (
    SELECT
        probe_series_id,
        server_id,
        target_service_id,
        count(*) AS probe_count,
        count(send_time) AS sent_count,
        count(recv_time) AS recv_count,
        count(anomaly) FILTER (WHERE recv_time IS NOT NULL) AS anomaly_count
    FROM probe_packets_public_co
    GROUP BY probe_series_id, server_id, target_service_id
) x
JOIN probe_servers ps ON ps.id = x.server_id
JOIN target_services ts ON ts.id = x.target_service_id
JOIN target_hosts th ON th.id = ts.target_host_id
ORDER BY 1, 2
"""

matplotlib.use("QT5Agg")

data = pd.read_csv("results/public_target_performance_by_server.csv")
servers = data["server"].unique()


class ServerLossByTarget(DataPlotter):

    def plot(self):
        plt.rcParams.update({
            # Use 12pt font in plots
            "axes.labelsize": 6,
            "font.size": 6,
            # Make the legend/label fonts a little smaller
            "legend.fontsize": 6,
            "xtick.labelsize": 5,
            "ytick.labelsize": 5
        })

        fig, axs = plt.subplots(2, 3, sharex=True, figsize=set_size((2, 3)))
        self.hspace = 0.15
        self.wspace = 0.25

        palette = sns.color_palette("husl", len(servers))
        series = iter([[39, 42], [40, 43]])
        for row in axs:
            sids = next(series)
            df = data.loc[data["probe_series_id"].isin(sids)]

            sns.barplot(data=df, ax=row[0], x="server", y="probe_count", hue="server", estimator="sum", errorbar=None, palette=palette)
            row[0].yaxis.set_major_formatter(ScaleFormatter(.000001, 0))
            row[0].set_ylabel("Probes (M)")
            row[0].set_xticks(ticks=range(len(servers)), labels=get_server_labels(servers), rotation=90)

            sns.boxplot(data=df, ax=row[1], x="server", y="con_drop_rate", hue="server", fliersize=.5, linewidth=.5, palette=palette)
            row[1].yaxis.set_major_formatter(ScaleFormatter(100, 0))
            row[1].set_ylabel("Conn. Loss Rate (\\%)")
            row[1].set_xticks(ticks=range(len(servers)), labels=get_server_labels(servers), rotation=90)

            sns.boxplot(data=df, ax=row[2], x="server", y="probe_drop_rate", hue="server", fliersize=.5, linewidth=.5, palette=palette)
            row[2].yaxis.set_major_formatter(ScaleFormatter(100, 0))
            row[2].set_ylabel("Probe Loss Rate (\\%)")
            row[2].set_xticks(ticks=range(len(servers)), labels=get_server_labels(servers), rotation=90)

        axs[1][0].set_xlabel("Probe Server")
        axs[1][1].set_xlabel("Probe Server")
        axs[1][2].set_xlabel("Probe Server")


ServerLossByTarget("public_target_performance_by_server")
