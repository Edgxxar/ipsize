import math
from abc import ABC, abstractmethod
from datetime import datetime

import matplotlib
import numpy as np
import pandas as pd
from matplotlib.legend_handler import HandlerPatch
from matplotlib.patches import Ellipse
from matplotlib.ticker import FuncFormatter
from scipy import stats
import pingouin as pg

matplotlib.use("module://mplcairo.qt")  # use more accurate renderer: https://github.com/matplotlib/mplcairo

import matplotlib.pyplot as plt
import seaborn as sns

# some default values
DOT_SIZE = 72  # matplotlib dots per inch
WIDTH = 600 / DOT_SIZE  # page width in inches
RATIO = 1 / 2  # width to height ratio

matplotlib.rcParams.update({
    # Use LaTeX to write all text
    "text.usetex": True,
    "font.family": "serif",
    # use Charter font family, it's the only one available from the TU Darmstadt CI
    "text.latex.preamble": r"""
        \usepackage[T1]{fontenc}
        \usepackage[bitstream-charter]{mathdesign}
    """,
    # Use 12pt font in plots
    "axes.titlesize": 12,
    "axes.labelsize": 12,
    "font.size": 12,
    # Make the legend/label fonts a little smaller
    "legend.fontsize": 10,
    "xtick.labelsize": 10,
    "ytick.labelsize": 10
})

# Set the color palette to a colorblind-friendly palette
sns.set_palette('colorblind')

# style components to cycle through
colors = sns.color_palette()
lines = [("solid", "dotted"), ("dashed", "dashdot")]
markers = ["1", "2"]


def get_server_number(server: str) -> int:
    return int(server.removeprefix("probe"))


def get_server_labels(names: list[str]) -> list[str]:
    return ["{}".format(get_server_number(s)) for s in names]


def set_legend_opacity(legend, alpha: int = 1):
    for lh in legend.legend_handles:
        lh.set_alpha(alpha)


def plot_regression(ax, x: pd.Series, y: pd.Series, name: str = None, sti: int = 0, scatter=False):
    """
    Plot least-squares solution to the data, see also https://ethanweed.github.io/pythonbook/05.04-regression.html
    """
    model = pg.linear_regression(x, y)  # fit a linear regression Y = b1 * x + b0
    b0 = model["coef"][0]
    b1 = model["coef"][1]
    reg = b1 * x + b0
    res = model.residuals_  # the residuals of the regression
    mae = np.mean(np.abs(res))  # the mean absolute error

    ss_res = np.sum(np.square(res))
    ss_tot = np.sum(np.square(y - np.mean(y)))
    ss_mod = ss_tot - ss_res
    df_mod = model.df_model_
    df_res = model.df_resid_
    ms_mod = ss_mod / df_mod
    ms_res = ss_res / df_res
    f = ms_mod / ms_res
    p = stats.f.sf(f, df_mod, df_res)

    print("{} Regression: Range, N, b1, p_t, p_F, R2, MAE".format(name))
    print(
        " & {} & ${} - {}$ & $\\num{{{}}}$ & $\\num{{{:.4e}}}$ & ${}$ & ${}$ & $\\qty{{{:.2f}}}{{\\percent}}$ & $\\num{{{:.2e}}}$ \\\\".format(
            name, x.min(), x.max(), len(x), b1, _class_p(model["pval"][1]), _class_p(p), model["r2"][1] * 100, mae
        )
    )
    print("$\\num{{{}}}$".format(math.ceil(2 * mae / b1)))

    color = colors[sti]
    line = lines[sti]
    if scatter:
        ax.scatter(x, y, marker=markers[sti], s=2, linewidths=.2, zorder=10)
    ax.plot(x, reg, color=color, linestyle=line[0], linewidth=1.5, label=name, zorder=11)
    ax.plot(x, reg + mae, color=color, linestyle=line[1], linewidth=1, zorder=11)
    ax.plot(x, reg - mae, color=color, linestyle=line[1], linewidth=1, zorder=11)


def _class_p(p) -> str:
    if p < .001:
        return "<0.001"
    if p < .01:
        return "<0.01"
    if p < .05:
        return "<0.05"
    return "\\quad {:.2f}".format(p)


def set_size(subplots=(1, 1), scale=1) -> tuple[float, float]:
    # Figure width in inches
    fig_width_in = WIDTH * scale
    # Figure height in inches
    fig_height_in = fig_width_in * RATIO * (subplots[0] / subplots[1])
    return fig_width_in, fig_height_in


class ScaleFormatter(FuncFormatter):
    def __init__(self, scale: float = 100, decimals: int = 1):
        super().__init__(lambda x, p: "{:.{decimals}f}".format(x * scale, decimals=decimals))


class TimeFormatter(FuncFormatter):
    def __init__(self, scale: int = 3600, fmt: str = "%H:%M"):
        super().__init__(lambda x, p: datetime.utcfromtimestamp(x * scale).strftime(fmt))


class HandlerEllipse(HandlerPatch):
    def create_artists(self, legend, orig_handle, xdescent, ydescent, width, height, fontsize, trans):
        center = 0.5 * width - 0.5 * xdescent, 0.5 * height - 0.5 * ydescent
        p = Ellipse(xy=center, width=orig_handle.radius, height=orig_handle.radius)
        self.update_prop(p, orig_handle, legend)
        p.set_transform(trans)
        return [p]


class DataPlotter(ABC):

    def __init__(self, name: str) -> None:
        self.name = name
        self.debug = False
        self.wspace = 0.15
        self.hspace = 0.15
        self.pad_inches = 0
        self.plot()
        plt.subplots_adjust(wspace=self.wspace, hspace=self.hspace)
        if self.debug:
            plt.show()
        else:
            plt.savefig(f"results/{self.name}.pdf", format="pdf", bbox_inches="tight", pad_inches=self.pad_inches)
        plt.close("all")

    @abstractmethod
    def plot(self):
        """plot the data"""
