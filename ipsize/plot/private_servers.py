import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from ipsize.models.enum import ProbeSeriesType
from ipsize.plot.base import DataPlotter, get_server_labels, set_size

# exported via:
""" 
SELECT
    pss."name" AS sender,
    pst."name" AS receiver,
    family(th.address) AS ip,
    ts."type",
    p.probe_count,
    p.dropped_count,
    p.dropped_count::float / p.probe_count::float AS drop_rate,
    p.latency,
    p.latency_dev
FROM (
    SELECT
        server_id,
        target_service_id,
        count(*) AS probe_count,
        count(*) FILTER (WHERE recv_time IS NULL) AS dropped_count,
        avg(EXTRACT(EPOCH FROM latency)) AS latency,
        stddev_samp(EXTRACT(EPOCH FROM latency)) AS latency_dev
    FROM probe_packets_private
    GROUP BY server_id, target_service_id
) p
JOIN probe_servers pss ON pss.id = p.server_id
JOIN target_services ts ON ts.id = p.target_service_id
JOIN target_hosts th ON th.id = ts.target_host_id
JOIN probe_server_addresses psa ON psa.address = th.address
JOIN probe_servers pst ON pst.id = psa.server_id
ORDER BY 1, 2, 3, 4;
"""

data = pd.read_csv("results/private_servers.csv")
servers = data["sender"].unique()

# probe002 and probe012 don't have IPv6 data, add empty rows so data lines up
replace = ["probe002", "probe012"]
empty = []
for missing in replace.copy():
    for existing in [s for s in servers if s not in replace]:
        for series_type in (ProbeSeriesType.PRIVATE_UDP, ProbeSeriesType.PRIVATE_TCP):
            empty.append([missing, existing, 6, series_type.name, 0, 0, np.NaN, np.NaN, np.NaN])  # send
            empty.append([existing, missing, 6, series_type.name, 0, 0, np.NaN, np.NaN, np.NaN])  # recv
    replace.remove(missing)  # next round this server does not need to be replaced anymore
empty = pd.DataFrame(empty, columns=["sender", "receiver", "ip", "type", "probe_count", "dropped_count", "drop_rate",
                                     "latency", "latency_dev"])
data = pd.concat([data, empty], ignore_index=True)
data = data.sort_values(["sender", "receiver", "ip", "type"]).reset_index(drop=True)


class ServerLatencyByServer(DataPlotter):

    def plot(self):
        fig, axs = plt.subplots(1, 3, figsize=set_size(scale=.92), gridspec_kw={'width_ratios': [1, 1, 0.05]})

        df = pd.DataFrame(data.groupby(["sender", "receiver"])[["latency", "latency_dev"]].mean()).reset_index()

        latencies = df.pivot(index="sender", columns="receiver", values="latency").multiply(1000)
        deviations = df.pivot(index="sender", columns="receiver", values="latency_dev").multiply(1000)
        vmin = df[["latency", "latency_dev"]].min().min() * 1000
        vmax = df[["latency", "latency_dev"]].max().max() * 1000

        labels = get_server_labels(servers)
        sns.heatmap(data=latencies, ax=axs[0], square=True, vmin=vmin, vmax=vmax,
                    xticklabels=labels, yticklabels=labels,
                    cmap="cividis", cbar=False)
        sns.heatmap(data=deviations, ax=axs[1], square=True, vmin=vmin, vmax=vmax,
                    xticklabels=labels, yticklabels=labels,
                    cmap="cividis", cbar_ax=axs[2], cbar_kws={"shrink": 0.8, "label": "Milliseconds"})

        for ax in (axs[0], axs[1]):
            ax.set_xlabel("Receiving Probe Server")
            ax.set_yticklabels(labels=labels, rotation=0, fontsize=8)
            ax.set_xticklabels(labels=labels, rotation=0, fontsize=8)
        axs[0].set_title("Latency")
        axs[0].set_ylabel("Sending Probe Server")
        axs[1].set_title("Standard Deviation")
        axs[1].set_ylabel(None)
        for ax in axs:
            for spine in ax.spines.values():
                spine.set_visible(True)


class ServerLossByServer(DataPlotter):

    def plot(self):
        fig, ax = plt.subplots(1, 1, figsize=set_size())

        df = pd.DataFrame(data.groupby(["sender", "receiver"])["drop_rate"].median()).reset_index()

        loss = df.pivot(index="sender", columns="receiver", values="drop_rate").multiply(100)

        labels = get_server_labels(servers)
        sns.heatmap(data=loss, square=True, xticklabels=labels, yticklabels=labels,
                    cmap="cividis", cbar_kws={"label": "Loss Rate (\\%)"})

        ax.set_xlabel("Receiving Probe Server")
        ax.set_ylabel("Sending Probe Server")
        ax.set_yticklabels(labels=labels, rotation=0, fontsize=8)
        ax.set_xticklabels(labels=labels, rotation=0, fontsize=8)
        for spine in ax.spines.values():
            spine.set_visible(True)


ServerLatencyByServer("private_server_latency_by_server")
ServerLossByServer("private_server_loss_by_server")
