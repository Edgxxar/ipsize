import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pingouin as pg

from ipsize.models.enum import ProbeSeriesType
from ipsize.plot.base import DataPlotter, ScaleFormatter, plot_regression, set_size

# exported via
"""
SELECT
    family(th.address) AS ip,
    ts."type",
    p."size",
    sum(p.probe_count) AS probe_count,
    sum(p.recv_count) AS recv_count
FROM (
    SELECT
        target_service_id,
        "size",
        count(*) AS probe_count,
        count(recv_time) AS recv_count
    FROM probe_packets_private
    GROUP BY target_service_id, "size"
) p
JOIN target_services ts ON ts.id = p.target_service_id
JOIN target_hosts th ON th.id = ts.target_host_id
GROUP BY 1, 2, 3
ORDER BY 1, 2, 3
"""

data = pd.read_csv("results/private_size.csv")


def set_axis(fig, axs, df):
    for ax in axs:
        ax.set_xlim([np.min(df["size"]), np.max(df["size"])])
        ax.set_ylim([np.min(df["drop_rate"]), np.max(df["drop_rate"])])
        ax.yaxis.set_major_formatter(ScaleFormatter(100, 2))
        ax.set_xlabel("Packet Size (Bytes)")
        ax.set_xticks(range(200, 1500, 200))
        ax.grid()


class SizeLoss(DataPlotter):
    def plot(self):
        fig, ax = plt.subplots(1, 1, figsize=set_size((1, 2)))

        df = data.groupby("size")[["probe_count", "recv_count"]].sum().reset_index()
        df["drop_rate"] = 1 - df["recv_count"] / df["probe_count"]
        plot_regression(ax, df["size"], df["drop_rate"], scatter=True)

        set_axis(fig, [ax], df)
        ax.set_ylabel("Loss Rate (\\%)")


class SizeLossResiduals(DataPlotter):
    def plot(self):
        fig, ax = plt.subplots(1, 1, figsize=set_size((1, 2)))

        df = data.groupby("size")[["probe_count", "recv_count"]].sum().reset_index()
        df["drop_rate"] = 1 - df["recv_count"] / df["probe_count"]

        model = pg.linear_regression(df["size"], df["drop_rate"])
        ax.scatter(df["size"], model.residuals_, marker="1", s=4, linewidths=.4, zorder=10)

        xlim = [np.min(df["size"]) - 5, np.max(df["size"]) + 5]
        ylim = np.max(np.abs(model.residuals_)) * 1.05
        ax.plot(xlim, [0, 0], color="black", linestyle="solid", linewidth=.8, zorder=9)

        ax.set_xlabel("Packet Size (Bytes)")
        ax.set_xticks(range(200, 1500, 200))
        ax.set_xlim(xlim)
        ax.set_ylabel("$\\epsilon$ (\\%)")
        ax.yaxis.set_major_formatter(ScaleFormatter(100, 2))
        ax.set_ylim([-ylim, ylim])
        ax.grid()


class SizeLossSplit(DataPlotter):
    def plot(self):
        fig, axs = plt.subplots(1, 2, sharey=True, sharex=True, figsize=set_size((1, 2)))
        self.wspace = 0.05

        df = data.groupby(["ip", "size"])[["probe_count", "recv_count"]].sum().reset_index()
        df["drop_rate"] = 1 - df["recv_count"] / df["probe_count"]
        ipv4_data = df.loc[df["ip"] == 4]
        ipv6_data = df.loc[df["ip"] == 6]

        plot_regression(axs[0], ipv4_data["size"], ipv4_data["drop_rate"], "IPv4", sti=0)
        plot_regression(axs[0], ipv6_data["size"], ipv6_data["drop_rate"], "IPv6", sti=1)

        df = data.groupby(["type", "size"])[["probe_count", "recv_count"]].sum().reset_index()
        df["drop_rate"] = 1 - df["recv_count"] / df["probe_count"]
        udp_data = df.loc[df["type"] == ProbeSeriesType.PRIVATE_UDP.name]
        tcp_data = df.loc[df["type"] == ProbeSeriesType.PRIVATE_TCP.name]

        plot_regression(axs[1], udp_data["size"], udp_data["drop_rate"], name="UDP", sti=0)
        plot_regression(axs[1], tcp_data["size"], tcp_data["drop_rate"], name="TCP", sti=1)

        set_axis(fig, axs, df)
        axs[0].set_ylabel("Loss Rate (\\%)")
        for ax in axs:
            ax.legend(loc="upper left", ncols=2)


SizeLoss("private_size_loss")
SizeLossResiduals("private_size_loss_residuals")
SizeLossSplit("private_size_loss_split")
