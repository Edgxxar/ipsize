import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from matplotlib.axes import Axes
from matplotlib.lines import Line2D

from ipsize.plot.base import DataPlotter, ScaleFormatter, TimeFormatter, set_size

# exported via:
"""
SELECT 
    p.probe_series_id,
    ps.name AS "server",
    p.probe_time,
    p.probe_count,
    p.sent_count,
    p.recv_count,
    1 - p.sent_count::float / p.probe_count::float AS con_drop_rate,
    CASE WHEN p.sent_count = 0 THEN NULL ELSE 1 - p.recv_count::float / p.sent_count::float END AS probe_drop_rate,
    p.latency
FROM (
    SELECT
        probe_series_id,
        server_id,
        EXTRACT(EPOCH FROM probe_time)::int / 3600 AS probe_time,  -- timestamps in hours
        count(*) AS probe_count,
        count(send_time) AS sent_count,
        count(recv_time) AS recv_count,
        percentile_cont(0.5) WITHIN GROUP (ORDER BY latency) AS latency
    FROM probe_packets_public
    GROUP BY 1, 2, 3
) p
JOIN probe_servers ps ON ps.id = p.server_id
ORDER BY 1, 2, 3
"""

data_cl = pd.read_csv("results/public_time_cl.csv")
data_co = pd.read_csv("results/public_time_co.csv")
servers_cl = data_cl["server"].unique()
servers_co = data_co["server"].unique()

series = {
    31: "ICMP",
    32: "UDP/DNS",
    44: "TCP/DNS",
    45: "TCP/SMTP",
}


def set_date_ticks(ax: Axes, df: pd.DataFrame, time_column: str, fmt="%d.%m."):
    x_start = df[time_column].min()
    x_start = x_start - (x_start % 24) + 24
    ax.set_xticks(range(x_start, df[time_column].max(), 24))
    ax.xaxis.set_major_formatter(TimeFormatter(fmt=fmt))
    ax.tick_params(axis="x", labelrotation=33, pad=-2, labelsize=8)


class ServerLossByTimeCl(DataPlotter):

    def plot(self):
        fig, axs = plt.subplots(10, 2, figsize=set_size((8, 2)))
        self.hspace = 0.77
        self.wspace = 0.10

        grouped = data_cl.groupby(["probe_series_id", "server", "send_time"]).mean()
        df = pd.DataFrame(grouped).reset_index()
        df["probe_series_type"] = [series[sid] for sid in df["probe_series_id"]]

        server_it = iter(servers_cl)
        palette = sns.color_palette("colorblind", 2)
        for row in axs:
            for ax in row:
                server = next(server_it)
                dfs = df.loc[df["server"] == server]

                sns.lineplot(ax=ax, data=dfs, x="send_time", y="probe_drop_rate", hue="probe_series_type",
                             linewidth=.7, linestyle="-", palette=palette, legend=False)

                ax.grid(axis="y")
                ax.set_title(server)
                ax.yaxis.set_major_formatter(ScaleFormatter(100, 0))
                ax.set_xlabel(None)
                set_date_ticks(ax, dfs, "send_time")
            row[0].set_ylabel("Loss Rate (\\%)")
            row[1].set_ylabel(None)


class ServerLossByTimeCo(DataPlotter):

    def plot(self):
        fig, axs = plt.subplots(9, 2, figsize=set_size((8, 2)))
        self.hspace = 0.70
        self.wspace = 0.12

        grouped = data_co.groupby(["probe_series_id", "server", "probe_time"]).mean()
        df = pd.DataFrame(grouped).reset_index()
        df["probe_series_type"] = [series[sid] for sid in df["probe_series_id"]]

        server_it = iter(servers_co)
        palette = sns.color_palette("colorblind", 2)
        for row in axs:
            for ax in row:
                server = next(server_it)
                dfs = df.loc[df["server"] == server]

                sns.lineplot(ax=ax, data=dfs, x="probe_time", y="probe_drop_rate", hue="probe_series_type",
                             linewidth=.7, linestyle="-", palette=palette, legend=False)
                sns.lineplot(ax=ax, data=dfs, x="probe_time", y="con_drop_rate", hue="probe_series_type",
                             linewidth=1, linestyle="dotted", palette=palette, legend=False)

                ax.grid(axis="y")
                ax.set_title(server)
                ax.yaxis.set_major_formatter(ScaleFormatter(100, 0))
                ax.set_xlabel(None)
                set_date_ticks(ax, dfs, "probe_time")
            row[0].set_ylabel("Loss Rate (\\%)")
            row[1].set_ylabel(None)


class LossByTimeCl(DataPlotter):
    def plot(self):
        fig, axs = plt.subplots(1, 2, figsize=set_size((1, 2)), sharex=True)
        self.wspace = 0.2

        axs[0].set_ylabel("Loss Rate Change (\\%)")
        axs[0].yaxis.set_major_formatter(ScaleFormatter())

        axs[1].set_ylabel("Latency Change (ms)")
        axs[1].yaxis.set_major_formatter(ScaleFormatter(.001, 0))

        df = pd.DataFrame(data_cl).reset_index()
        df["hour"] = df["send_time"].mod(24)
        for sid, name in {31: "ICMP", 32: "UDP/DNS"}.items():
            sdf = df.loc[df["probe_series_id"] == sid].copy()

            loss0 = sdf.groupby("hour")["probe_drop_rate"].median().min()
            sdf["drop_rate_resized"] = sdf["probe_drop_rate"] - loss0
            sns.lineplot(sdf, x="hour", y="drop_rate_resized", estimator="median", errorbar="ci",
                         label=name, ax=axs[0])

            latency0 = sdf.groupby("hour")["latency"].mean().min()
            sdf["latency_resized"] = sdf["latency"] - latency0
            sns.lineplot(sdf, x="hour", y="latency_resized", estimator="mean", errorbar="se",
                         label=name, ax=axs[1])

        for ax in axs:
            ax.grid()
            ax.set_xlabel("Time of Day (UTC)")
            ax.set_xticks(range(0, 24, 3))
            ax.set_xlim([0, 23])
            ax.legend(loc="upper left")


class LossByTimeCo(DataPlotter):
    def plot(self):
        fig, axs = plt.subplots(1, 2, figsize=set_size((1, 2)), sharex=True)
        self.wspace = 0.23

        axs[0].set_ylabel("Loss Rate Change (\\%)")
        axs[0].yaxis.set_major_formatter(ScaleFormatter())

        axs[1].set_ylabel("Latency Change (ms)")
        axs[1].yaxis.set_major_formatter(ScaleFormatter(.001, 0))

        df = pd.DataFrame(data_co).reset_index()
        df["hour"] = df["probe_time"].mod(24)
        for sid, name in {44: "TCP/DNS", 45: "SMTP"}.items():
            sdf = df.loc[df["probe_series_id"] == sid].copy()

            loss0 = sdf.groupby("hour")["probe_drop_rate"].median().min()
            sdf["drop_rate_resized"] = sdf["probe_drop_rate"] - loss0
            sns.lineplot(sdf, x="hour", y="drop_rate_resized", estimator="median", errorbar="ci",
                         label=name, ax=axs[0])

            if sid != 45:  # don't evaluate SMTP latencies because of the bug
                latency0 = sdf.groupby("hour")["latency"].mean().min()
                sdf["latency_resized"] = sdf["latency"] - latency0
                sns.lineplot(sdf, x="hour", y="latency_resized", estimator="mean", errorbar="se",
                             label=name, ax=axs[1])

        for ax in axs:
            ax.grid()
            ax.set_xlabel("Time of Day (UTC)")
            ax.set_xticks(range(0, 24, 3))
            ax.set_xlim([0, 23])
            ax.legend(loc="upper left")


def plot_loss_with_restarts(df: pd.DataFrame, ax_loss, ax_lat, ymin_loss, ymax_loss, ymin_lat, ymax_lat, legend_loc):
    restarts = [471609, 471637, 471659, 471709, 471732, 471752, 471776]
    colors = sns.color_palette("colorblind", 2)

    sns.lineplot(df, x="probe_time", y="probe_drop_rate", estimator="median", errorbar="ci",
                 color=colors[0], zorder=10, ax=ax_loss)
    sns.lineplot(df, x="probe_time", y="con_drop_rate", estimator="median", errorbar="ci",
                 linestyle="dotted", color=colors[1], zorder=10, ax=ax_loss)
    sns.lineplot(df, x="probe_time", y="latency", estimator="mean", errorbar="se",
                 color=colors[0], zorder=10, ax=ax_lat)
    ax_loss.vlines(restarts, ymin_loss, ymax_loss, colors="gray", linestyles="dashed", linewidth=.7, zorder=0)
    ax_lat.vlines(restarts, ymin_lat, ymax_lat, colors="gray", linestyles="dashed", linewidth=.7, zorder=0)

    xlim = [df["probe_time"].min(), df["probe_time"].max()]

    ax_loss.set_ylabel("Loss Rate (\\%)")
    ax_loss.yaxis.set_major_formatter(ScaleFormatter())
    ax_loss.set_xlabel("Probe Series Runtime")
    set_date_ticks(ax_loss, df, "probe_time", "%d.%m.%y")
    ax_loss.set_xlim(xlim)
    ax_loss.legend(loc=legend_loc, handles=[
        Line2D([], [], linestyle="dotted", color=colors[1], label="Connections"),
        Line2D([], [], linestyle="solid", color=colors[0], label="Probes")
    ])

    ax_lat.set_ylabel("Latency (ms)")
    ax_lat.yaxis.set_major_formatter(ScaleFormatter(.001, 0))
    ax_lat.set_xlabel("Probe Series Runtime")
    set_date_ticks(ax_lat, df, "probe_time", "%d.%m.%y")
    ax_lat.set_xlim(xlim)


class LossByTimeCoGlobal(DataPlotter):
    def plot(self):
        fig, axs = plt.subplots(2, 2, figsize=set_size((2, 2), scale=.95), sharex=True)
        self.wspace = 0.22
        self.hspace = 0.25

        plt.figtext(0.5, 0.93, "TCP/DNS", ha="center", va="top", fontsize=12)
        df = data_co.loc[(data_co["probe_series_id"] == 44) & (data_co["probe_time"] >= 471571)]
        plot_loss_with_restarts(df, axs[0][0], axs[0][1], .016, .074, 240000, 283000, "upper right")
        plt.figtext(0.5, 0.50, "SMTP", ha="center", va="top", fontsize=12)
        df = data_co.loc[data_co["probe_series_id"] == 45]
        plot_loss_with_restarts(df, axs[1][0], axs[1][1], .03, .42, 130000, 570000, "upper left")


ServerLossByTimeCl("public_server_loss_by_time_cl")
ServerLossByTimeCo("public_server_loss_by_time_co")
LossByTimeCl("public_time_loss_cl")
LossByTimeCo("public_time_loss_co")
LossByTimeCoGlobal("public_time_loss_co_global")
