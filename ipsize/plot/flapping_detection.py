import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

from plot.base import DataPlotter, set_size


class FlappingExample(DataPlotter):

    def plot(self):
        fig, axs = plt.subplots(1, 2, sharex=True, figsize=set_size((1, 3)))
        self.wspace = 0.2

        states = np.array([1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0])
        change = np.array([1, 3, 5, 6, 9])
        palette = sns.color_palette("colorblind")
        colors = [palette[2] if s else palette[3] for s in states]

        times = [f"$t_{{{i}}}$" for i in range(1, len(states))]

        axs[0].set_xticks(np.arange(1, len(states)) - 0.5, labels=times)
        axs[0].set_xlabel("Time $\\Rightarrow$")
        axs[0].set_yticks([0.5, 1.5], ["ERROR", "OK"], rotation=90)
        axs[0].set_ylabel("State")

        axs[0].bar(range(len(states)), states + 0.5, width=0.8, color=colors)
        axs[0].scatter(change - 0.5, np.ones(len(change)) * 1.6, marker="v")

        axs[1].set_ylabel("Weight")
        axs[1].set_yticks([0.5, 1, 1.5])
        axs[1].set_xlabel("Time $\\Rightarrow$")
        axs[1].vlines(change - 0.5, 0, change / (len(states) - 2) + 0.38, linestyles="dotted")
        axs[1].plot([0.5, len(states) - 1.5], [0.5, 1.5], color="k")
        axs[1].axis([-0.5, len(states) - 0.5, 0, 2])
        # axs[1].grid(axis="y")

        for ax in axs:
            ax.spines[['right', 'top']].set_visible(False)


FlappingExample("service_flapping")
