import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns

from plot.base import DataPlotter, ScaleFormatter, set_size

# exported via:
"""
SELECT 
    p.probe_series_id,
    ps.name AS "server",
    p.send_time,
    p.probe_count, 
    p.sent_count,
    p.recv_count,
    1 - p.sent_count::float / p.probe_count::float AS con_drop_rate,
    CASE WHEN p.sent_count = 0 THEN NULL ELSE 1 - p.recv_count::float / p.sent_count::float END AS probe_drop_rate
FROM (
    SELECT
        probe_series_id,
        server_id,
        EXTRACT(EPOCH FROM probe_time)::int / 60 AS send_time,
        count(*) AS probe_count,
        count(send_time) AS sent_count,
        count(recv_time) AS recv_count
    FROM probe_packets_public
    WHERE probe_time IS NOT NULL
    GROUP BY 1, 2, 3
) p
JOIN probe_servers ps ON ps.id = p.server_id
ORDER BY 1, 2, 3
"""

data_co = pd.read_csv("results/public_time_co.csv")
data_pt = pd.read_csv("results/public_prototype_tcp.csv")


class PrototypeLossBySeries(DataPlotter):

    def plot(self):
        fig, axs = plt.subplots(2, 1, figsize=set_size((1, 1)))
        self.hspace = 0.45

        colors = iter(sns.color_palette("colorblind", 2))

        series = iter([(6, "TCP/DNS"), (7, "SMTP")])
        for ax in axs:
            sid, name = next(series)
            color = next(colors)
            df = data_pt.loc[data_pt["probe_series_id"] == sid]
            sns.lineplot(ax=ax, data=df, x="send_time", y="probe_drop_rate",
                         linewidth=.7, linestyle="-", c=color, legend=False)
            sns.lineplot(ax=ax, data=df, x="send_time", y="con_drop_rate",
                         linewidth=1, linestyle="dotted", c=color, legend=False)

            ax.set_title(name)
            ax.yaxis.set_major_formatter(ScaleFormatter(100, 0))
            ax.grid(axis="y")
            ax.set_xlabel(None)
            ax.set_ylabel("Loss Rate (\\%)")


PrototypeLossBySeries("public_prototype_tcp")
