import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from matplotlib.axes import Axes
from matplotlib.patches import Patch
from matplotlib.ticker import FixedLocator

from ipsize.plot.base import DataPlotter, ScaleFormatter, set_size

# exported via:
"""
SELECT 
    regexp_replace(p.anomaly, 'expected [0-9]+ bytes', 'wrong size') AS anomaly, 
    round(tp.anomaly_rate * 100) AS anomaly_rate, 
    count(*)
FROM probe_packets_public p
JOIN target_performance tp ON tp.target_service_id = p.target_service_id AND tp.probe_series_id = p.probe_series_id
WHERE
    p.probe_series_id = 32 AND
    p.recv_time IS NOT NULL AND 
    anomaly IS NOT NULL AND
    tp.probe_drop_rate < 0.1
GROUP BY 1, 2
"""

"""
SELECT 
    regexp_replace(p.anomaly, 'expected [0-9]+ bytes', 'wrong size') AS anomaly, 
    p."size", 
    count(*)
FROM probe_packets_public p
JOIN target_performance tp ON tp.target_service_id = p.target_service_id AND tp.probe_series_id = p.probe_series_id
WHERE
    p.probe_series_id = 32 AND
    p.recv_time IS NOT NULL AND 
    anomaly IS NOT NULL AND
    tp.probe_drop_rate < 0.1
GROUP BY 1, 2
"""

data_rates_udns = pd.read_csv("results/public_anomaly_rates_udns.csv")
data_sizes_udns = pd.read_csv("results/public_anomaly_sizes_udns.csv")
data_rates_tdns = pd.read_csv("results/public_anomaly_rates_tdns.csv")
data_sizes_tdns = pd.read_csv("results/public_anomaly_sizes_tdns.csv")


def plot_anomaly_hist(data: pd.DataFrame, anomaly_names: dict[str, str], ax: Axes):
    df = data.loc[data["anomaly"].isin(anomaly_names.keys())]
    anomalies = pd.Series(df["anomaly"].unique()).sort_values()  # sort anomalies by name as they will be used in order
    colors = sns.color_palette("colorblind", len(anomalies))

    sns.histplot(data=df, x="anomaly_rate", hue="anomaly", weights="count", stat="percent", element="step",
                 bins=100, discrete=True, multiple="stack", cumulative=False, palette=colors, linewidth=.1, alpha=1,
                 legend=False, zorder=10, ax=ax)

    ax.legend(loc="upper left", handles=[
        Patch(facecolor=colors[idx], edgecolor='black', linewidth=.2, label=anomaly_names[anomaly])
        for idx, anomaly in enumerate(anomalies)
    ])
    ax.set_xticks(range(0, 101, 10))
    ax.set_xlabel("Anomaly Rate (\\%)")
    ax.set_yscale("symlog", linthresh=5)
    ax.set_yticks([1, 2, 3, 4, 5, 10, 20, 40, 80])
    ax.yaxis.set_major_formatter(ScaleFormatter(1, 0))
    ax.yaxis.set_minor_locator(FixedLocator([6, 7, 8, 9, 30, 50, 60, 70]))
    ax.set_ylabel("Target Services (\\%)")
    ax.grid(axis="y", zorder=0)


def plot_anomaly_sizes(data: pd.DataFrame, anomaly_names: dict[str, str], ax: Axes):
    df = data.loc[data["anomaly"].isin(anomaly_names.keys())].sort_values("anomaly").reset_index()
    anomalies = pd.Series(df["anomaly"].unique()).sort_values()  # sort anomalies by name as they will be used in order
    colors = sns.color_palette("colorblind", len(anomalies))

    sizes = df["size"].unique()
    sns.histplot(data=df, x="size", hue="anomaly", weights="count", stat="percent", element="step",
                 bins=len(sizes), discrete=True, multiple="stack", palette=colors, linewidth=.1, alpha=1,
                 legend=False, ax=ax)

    ax.legend(loc="upper left", handles=[
        Patch(facecolor=colors[idx], edgecolor='black', linewidth=.2, label=anomaly_names[anomaly])
        for idx, anomaly in enumerate(anomalies)
    ])
    ax.set_xticks(range(200, 1500, 200))
    ax.set_xlabel("Packet Size (Bytes)")
    ax.set_ylabel("Anomaly Probes (\\%)")


class AnomalyDistUDNS(DataPlotter):

    def plot(self):
        fig, axs = plt.subplots(1, 2, figsize=set_size((1, 2)))
        self.wspace = .2

        anomaly_names = {
            "got response code 2": "RCODE 2",
            "message is truncated": "Message Truncated",
            "wrong size": "Wrong Size"
        }

        plot_anomaly_hist(data_rates_udns, anomaly_names, axs[0])

        ymin = 0
        ymax = 0.256
        axs[1].axis([70, 1500, ymin, ymax])
        axs[1].vlines(
            [512, 1252, 1420],
            ymin=ymin, ymax=ymax, colors="gray", linestyles="dashed", linewidth=.7, zorder=0
        )
        plot_anomaly_sizes(data_sizes_udns, anomaly_names, axs[1])


class AnomalyDistTDNS(DataPlotter):

    def plot(self):
        fig, axs = plt.subplots(1, 2, figsize=set_size((1, 2)))
        self.wspace = .25

        anomaly_names = {
            "got response code 2": "RCODE 2",
            "wrong size": "Wrong Size",
            "response was fragmented": "Response Fragmented"
        }

        plot_anomaly_hist(data_rates_tdns, anomaly_names, axs[0])

        ymin = 0
        ymax = 0.2
        axs[1].axis([70, 1500, ymin, ymax])
        axs[1].vlines(
            [1267, 1435],
            ymin=ymin, ymax=ymax, colors="gray", linestyles="dashed", linewidth=.7, zorder=0
        )
        plot_anomaly_sizes(data_sizes_tdns, anomaly_names, axs[1])


AnomalyDistUDNS("public_anomalies_udns")
AnomalyDistTDNS("public_anomalies_tdns")
