import pandas as pd
from matplotlib import pyplot as plt

from ipsize.plot.base import DataPlotter, ScaleFormatter, plot_regression, set_size

data_pr = pd.read_csv("results/private_size.csv")
data_cl = pd.read_csv("results/public_size_cl.csv")
data_co = pd.read_csv("results/public_size_co.csv")


def get_mean_loss(df: pd.DataFrame, probe_count="probe_count", recv_count="recv_count") -> pd.DataFrame:
    mean = 1 - df[recv_count].sum() / df[probe_count].sum()
    df1 = pd.DataFrame(columns=["size", "mean_loss"])
    df1["size"] = df["size"]
    df1["mean_loss"] = 1 - df[recv_count] / df[probe_count] - mean
    return df1


class CombinedSize(DataPlotter):
    def plot(self):
        fig, ax = plt.subplots(1, 1, figsize=set_size())

        # get data sections
        private = data_pr.groupby("size")[["probe_count", "recv_count"]].sum().reset_index()

        icmp_v4 = data_cl.loc[(data_cl["probe_series_id"] == 31) & (data_cl["ip"] == 4) &
                              (data_cl["size"] < 1420)]
        icmp_v6 = data_cl.loc[(data_cl["probe_series_id"] == 31) & (data_cl["ip"] == 6) &
                              (data_cl["size"] < 1420)]
        udns_v4 = data_cl.loc[(data_cl["probe_series_id"] == 32) & (data_cl["ip"] == 4) &
                              (data_cl["size"] >= 400) & (data_cl["size"] <= 1239)]
        udns_v6 = data_cl.loc[(data_cl["probe_series_id"] == 32) & (data_cl["ip"] == 6) &
                              (data_cl["size"] <= 1271)]

        tdns_v4 = data_co.loc[(data_co["probe_series_id"] == 44) & (data_co["ip"] == 4) &
                              (data_co["size"] >= 102) & (data_co["size"] < 1254)]
        tdns_v6 = data_co.loc[(data_co["probe_series_id"] == 44) & (data_co["ip"] == 6) &
                              (data_co["size"] >= 122) & (data_co["size"] < 1274)]
        smtp_v4 = data_co.loc[(data_co["probe_series_id"] == 45) & (data_co["ip"] == 4)]
        smtp_v6 = data_co.loc[(data_co["probe_series_id"] == 45) & (data_co["ip"] == 6)]

        combined = pd.concat([
            get_mean_loss(private),
            get_mean_loss(icmp_v4),
            get_mean_loss(icmp_v6),
            get_mean_loss(udns_v4),
            get_mean_loss(udns_v6),
            get_mean_loss(tdns_v4, probe_count="sent_count"),
            get_mean_loss(tdns_v6, probe_count="sent_count"),
            get_mean_loss(smtp_v4, probe_count="sent_count"),
            get_mean_loss(smtp_v6, probe_count="sent_count"),
        ])
        combined = combined.loc[(combined["size"] >= 100) & (combined["size"] <= 1249)]

        plot_regression(ax, combined["size"], combined["mean_loss"], scatter=False)

        ax.set_ylabel("Loss Rate Change (\\%)")
        ax.yaxis.set_major_formatter(ScaleFormatter(100, 2))
        ax.set_xlabel("Packet Size (Bytes)")
        ax.set_xticks(range(200, 1300, 200))
        ax.grid()


CombinedSize("combined_size_loss")
