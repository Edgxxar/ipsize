import cartopy.crs as ccrs
import cartopy.feature as cf
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib.colors import LogNorm
from matplotlib.lines import Line2D
from matplotlib.transforms import Bbox

from ipsize.models.enum import ProbeSeriesType
from ipsize.plot.base import DataPlotter, set_size

# exported via
"""
SELECT 
    th.id,
    ts."type",
    FAMILY(th.address) AS ip,
    COALESCE(th."location"->>'latitude', th."location"->'coordinates'->>'latitude') AS latitude,
    COALESCE(th."location"->>'longitude', th."location"->'coordinates'->>'longitude') AS longitude,
    th."location"->>'country_code' AS country_code,
    th."location"->>'city' AS city,
    COALESCE(th.autonomous_system->>'asn', substring(th.autonomous_system#>>'{}' FROM 3)) AS asn
FROM target_hosts th
JOIN target_services ts ON ts.target_host_id = th.id
JOIN target_performance tp ON tp.target_service_id = ts.id
WHERE tp.probe_series_id IN (31, 32, 44, 45)
ORDER BY 2, 3, 6, 7
"""

targets = pd.read_csv("results/target_service_info.csv")

summary = targets.groupby(["type", "ip"])["id"].count().reset_index()
print(summary)


class TargetWorldMap(DataPlotter):
    def plot(self):
        data_crs = ccrs.PlateCarree()
        fig = plt.figure(figsize=set_size())
        ax = fig.add_axes((.85, .85, .85, .85), projection=data_crs)

        ax.set_global()
        ax.add_feature(cf.COASTLINE, edgecolor="tab:gray", linewidth=0.2)
        ax.add_feature(cf.BORDERS, edgecolor="tab:gray", linewidth=0.1)
        ax.set_extent([-168, 180, 75, -58], crs=data_crs)

        df = targets.groupby("id").first().reset_index()
        df["grid_lon"] = df["longitude"].mul(2).round().div(2)
        df["grid_lat"] = df["latitude"].mul(2).round().div(2)
        df["count"] = np.zeros(len(df))
        df = df.groupby(["grid_lon", "grid_lat"])["count"].count().reset_index()

        norm = LogNorm(vmin=df["count"].min(), vmax=df["count"].max())

        def plot(cmin, cmax, zorder, legend):
            sns.scatterplot(data=df.loc[df["count"].between(cmin, cmax, inclusive="both")],
                            x="grid_lon", y="grid_lat", hue="count", size="count",
                            sizes=(.7, 2), size_norm=norm, hue_norm=norm, palette="dark:red", edgecolor=None,
                            alpha=.8, zorder=zorder, legend=legend, ax=ax)

        plot(0, 250, 10, "brief")
        plot(251, df["count"].max(), 12, "brief")

        legend = ax.legend(loc="lower left")
        for lh in legend.legend_handles:
            lh.set_alpha(1)


class TargetCountryCdf(DataPlotter):
    def plot(self):
        fig, axs = plt.subplots(1, 2, sharex=True, sharey=True, figsize=set_size((1, 2)))
        self.wspace = 0.05

        df = targets.sort_values("country_code")

        # distribution by IP
        countries = pd.Series(df["country_code"].unique())
        colors = sns.color_palette("colorblind", 2)
        axs[0].plot([0, len(countries) - 1], [0, 1], color="tab:grey", linewidth=.8, linestyle="dotted")
        sns.ecdfplot(df, x="country_code", hue="ip", palette=colors, stat="proportion", legend=None, ax=axs[0])
        axs[0].axis([0, len(countries) - 1, 0, 1])

        axs[0].set_xticks(range(0, len(countries), 20))
        axs[0].set_xlabel("Country")

        ci = pd.Index(countries)

        def annot(country, y, xoff=-20, yoff=.05):
            x = ci.get_loc(country)
            axs[0].annotate(country, xy=(x, y), xytext=(x + xoff, y + yoff), size=8,
                        arrowprops=dict(arrowstyle="->", facecolor="black"))

        annot("BR", .08, yoff=.01)
        annot("CA", .11)
        annot("CN", .18)
        annot("DE", .24, yoff=.06)
        annot("FR", .31, yoff=.01)
        annot("GB", .34)
        annot("HK", .36, xoff=-21)
        annot("IN", .40, xoff=-18)
        annot("IT", .43, xoff=-18, yoff=.10)
        annot("JP", .47, xoff=-15, yoff=.12)
        annot("KR", .52, xoff=-13, yoff=.14)
        annot("NL", .56, xoff=-21, yoff=.06)
        annot("RU", .63, yoff=.03)
        annot("SG", .66, yoff=.08)
        annot("US", .845)

        axs[0].legend(loc="upper left", handles=[
            Line2D([], [], color=colors[0], label="IPv4"),
            Line2D([], [], color=colors[1], label="IPv6")
        ])

        # distribution by type
        types = df["type"].unique()
        colors = sns.color_palette("colorblind", len(types))
        axs[1].plot([0, len(countries) - 1], [0, 1], color="tab:grey", linewidth=.8, linestyle="dotted")
        sns.ecdfplot(df, x="country_code", hue="type", palette=colors, stat="proportion", legend=None, ax=axs[1])
        axs[1].axis([0, len(countries) - 1, 0, 1])
        axs[1].set_xlabel("Country")

        type_names = {
            ProbeSeriesType.PUBLIC_ICMP_PING.name: "ICMP",
            ProbeSeriesType.PUBLIC_UDP_DNS.name: "UDP/DNS",
            ProbeSeriesType.PUBLIC_TCP_DNS.name: "TCP/DNS",
            ProbeSeriesType.PUBLIC_TCP_SMTP.name: "SMTP",
        }
        handles = []
        for idx, t in enumerate(types):
            handles.append(Line2D([], [], color=colors[idx], label=type_names[t]))
        axs[1].add_artist(plt.legend(loc="upper left", handles=handles[:2]))
        axs[1].add_artist(plt.legend(loc="lower right", handles=handles[2:]))


TargetWorldMap("target_world_map")
TargetCountryCdf("target_country_cdf")
